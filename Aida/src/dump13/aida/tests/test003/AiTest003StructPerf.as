package dump13.aida.tests.test003 {
	import dump13.aida.level0.core.AiList;
	import dump13.aida.level0.core.AiListNode;
	import dump13.msl.types.Set;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	/**
	 * testA: 110...175 (~140) per 1e5.
	 * testB: 15...16 per 1e5.
	 * testC: 109...187 per 1e5.
	 * testD: 15...31 per 1e5.
	 * @author dump13
	 */
	public class AiTest003StructPerf {
		
		private var _host:Sprite;
		private var _stage:Stage;
		
		private var _numIterations:int = 1e5;
		private var _set:Set;
		private var _list:AiList;
		
		
		public function AiTest003StructPerf (host:Sprite)
		{
			_host = host;
			_stage = _host.stage;
			
			//prepareObjectsA ();
			//prepareObjectsB ();
			//prepareObjectsC ();
			prepareObjectsD ();
			
			_stage.addEventListener (MouseEvent.CLICK, onStageClick);
		}
		
		private function prepareObjectsA ():void
		{
			_list = new AiList ();
		}
		
		private function prepareObjectsB ():void
		{
			_list = new AiList ();
			
			var num:int = _numIterations;
			for (var n:int = 0; n < num; n++)
			{
				var obj:Ai003Listed = new Ai003Listed ();
				_list.add (obj._node);
			}
		}
		
		private function prepareObjectsC ():void
		{
			_set = new Set ();
		}
		
		private function prepareObjectsD ():void
		{
			_set = new Set ();
			
			var num:int = _numIterations;
			for (var n:int = 0; n < num; n++)
			{
				var obj:Ai003Listed = new Ai003Listed ();
				_set.add (obj);
			}
		}
		
		/// Filling the list with objects being created on the fly.
		private function testA ():void
		{
			var time:int = getTimer ();
			
			var list:AiList = _list;
			var num:int = _numIterations;
			for (var n:int = 0; n < num; n++)
			{
				var obj:Ai003Listed = new Ai003Listed ();
				//_list.add (obj._node);
				list.add (obj._node);
			}
			
			trace (getTimer () - time);
		}
		
		/// Getting objects inside the list.
		private function testB ():void
		{
			var time:int = getTimer ();
			
			var curNode:AiListNode = _list.head;
			while (curNode)
			{
				var listed:Ai003Listed = Ai003Listed (curNode.object);
				curNode = curNode.next;
			}
			
			trace (getTimer () - time);
		}
		
		private function testC ():void
		{
			var time:int = getTimer ();
			
			var s:Set = _set;
			var num:int = _numIterations;
			for (var n:int = 0; n < num; n++)
			{
				s.add (new Ai003Listed ());
			}
			
			trace (getTimer () - time);
		}
		
		private function testD ():void
		{
			var time:int = getTimer ();
			
			var s:Set = _set;
			for (var i:Object in s)
			{
				var listed:Ai003Listed = Ai003Listed (i);
			}
			
			trace (getTimer () - time);
		}
		
		private function onStageClick (e:MouseEvent):void
		{
			//testA ();
			//testB ();
			//testC ();
			testD ();
		}
		
	}

}