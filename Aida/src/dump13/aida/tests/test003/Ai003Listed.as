package dump13.aida.tests.test003 {
	import dump13.aida.level0.core.AiListNode;
	/**
	 * Объект, который можно поместить в список.
	 * @author dump13
	 */
	public class Ai003Listed {
		
		internal var _node:AiListNode;
		
		
		public function Ai003Listed ()
		{
			_node = new AiListNode (this);
		}
		
		public function destroy ():void
		{
			_node.destroy ();
			_node = null;
		}
		
	}

}