package dump13.aida.tests.test002 {
	import dump13.aida.level0.core.AiBlob;
	import dump13.aida.level0.core.AiListNode;
	import dump13.aida.level0.core.AiLocation;
	import dump13.pusher.level0.utils.PuKeyboard;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiTest002Visuals {
		
		private var _host:Sprite;
		private var _stage:Stage;
		
		private var _location:AiLocation;
		
		private var _screen:Bitmap;
		private var _buffer:BitmapData;
		
		private var _keys:PuKeyboard;
		private var _mouseIsDown:Boolean;
		
		private var _prevTime:Number = 0;
		
		private var _cameraSpeed:Number = 100;
		
		
		public function AiTest002Visuals (host:Sprite)
		{
			_host = host;
			_stage = _host.stage;
			
			_buffer = new BitmapData (_stage.stageWidth, _stage.stageHeight, true, 0xff000000);
			_screen = new Bitmap (_buffer, PixelSnapping.ALWAYS, false);
			_host.addChild (_screen);
			
			_location = new AiLocation (_host);
			
			_keys = new PuKeyboard (_stage);
			_keys.createTasks (Keyboard.SPACE, Keyboard.A, Keyboard.D, Keyboard.W, Keyboard.S, Keyboard.SHIFT);
			
			_prevTime = getTimer () * 1e-3;
			
			_stage.addEventListener (Event.ENTER_FRAME, onEnterFrame);
			_stage.addEventListener (MouseEvent.MOUSE_DOWN, onStageMouseDown);
			_stage.addEventListener (MouseEvent.MOUSE_UP, onStageMouseUp);
		}
		
		private function onEnterFrame (e:Event):void
		{
			var curTime:Number = getTimer () * 1e-3;
			var deltaTime:Number = curTime - _prevTime;
			
			var curNode:AiListNode = _location.objects.head;
			while (curNode)
			{
				var blob:AiBlob = curNode.object as AiBlob;
				if (blob)
				{
					blob.radius += .002;
				}
				
				curNode = curNode.next;
			}
			
			var usedSpeed:Number = _cameraSpeed;
			if (_keys.keyIsDown (Keyboard.SHIFT))
			{
				usedSpeed *= 3;
			}
			
			if (_keys.keyIsDown (Keyboard.A) && !_keys.keyIsDown (Keyboard.D))
			{
				_location.camera.x -= usedSpeed * deltaTime;
			}
			if (_keys.keyIsDown (Keyboard.D) && !_keys.keyIsDown (Keyboard.A))
			{
				_location.camera.x += usedSpeed * deltaTime;
			}
			if (_keys.keyIsDown (Keyboard.W) && !_keys.keyIsDown (Keyboard.S))
			{
				_location.camera.y -= usedSpeed * deltaTime;
			}
			if (_keys.keyIsDown (Keyboard.S) && !_keys.keyIsDown (Keyboard.W))
			{
				_location.camera.y += usedSpeed * deltaTime;
			}
			
			_location.update (deltaTime);
			
			_prevTime = curTime;
		}
		
		private function onStageMouseDown (e:MouseEvent):void
		{
			if (_mouseIsDown)
			{
				return;
			}
			_mouseIsDown = true;
			
			var globalX:Number = e.stageX + _location.camera.x - _location.camera.screenWidth * .5;
			var globalY:Number = e.stageY + _location.camera.y - _location.camera.screenHeight * .5;
			
			//var blob:AiBlob = new AiBlob ();
			//blob.radius = 20;
			//blob.setXY (globalX, globalY);
			//
			//_location.addObject (blob);
			createBlobs (globalX, globalY);
		}
		
		private function createBlobs (gx:Number, gy:Number):void
		{
			var radius:Number = 10;
			var dist:Number = 50;
			for (var n:int = 0; n < 10; n++)
			{
				var blob:AiBlob = new AiBlob ();
				blob.radius = radius;
				blob.setXY (gx + (Math.random () - .5) * dist, gy + (Math.random () - .5) * dist);
				_location.addObject (blob);
			}
		}
		
		private function onStageMouseUp (e:MouseEvent):void
		{
			_mouseIsDown = false;
		}
		
	}

}