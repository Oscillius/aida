package dump13.aida.tests.test007 {
	import dump13.aida.level0.core.AiList;
	import dump13.msl.types.Set;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	/**
	 * Тест для сравнения Set'а и Vector'а.
	 * 
	 * Number of objects: 1e+6
	 * Vector lookup test: 16
	 * Set lookup test: 0
	 * Vector traversal test: 93
	 * Set traversal test: 94 (78 at minimum)
	 * Vector write test: 140
	 * Set write test: 374
	 * @author dump13
	 */
	public class AiTest007VectorAndSetLookup
	{
		private var _host:Sprite;
		private var _stage:Stage;
		
		private var _set:Set = new Set ();
		private var _vector:Vector.<Object> = new Vector.<Object> ();
		
		private var _middleObject:Object;
		private var _lastObject:Object;
		private var _objectsToWrite:Vector.<Object> = new Vector.<Object> ();
		
		private var _numObjects:int = 1e6;
		private var _numObjectsToWrite:int = 1e6;
		
		
		public function AiTest007VectorAndSetLookup (host:Sprite)
		{
			_host = host;
			_stage = host.stage;
			
			prepare ();
			
			_stage.addEventListener (MouseEvent.CLICK, onStageClick);
		}
		
		// Vector, Set.
		private function prepare ():void
		{
			var middleIndex:int = _numObjects / 2;
			var lastIndex:int = _numObjects - 1;
			for (var n:int = 0; n < _numObjects; n++)
			{
				var obj:Object = { data:"DATA" };
				_vector.push (obj);
				_set.add (obj);
				
				if (n == middleIndex)
				{
					_middleObject = obj;
				}
				else if (n == lastIndex)
				{
					_lastObject = obj;
				}
			}
			
			for (n = 0; n < _numObjectsToWrite; n++)
			{
				_objectsToWrite[n] = { data:"DATA" };
			}
		}
		
		// ================== LOOKUP ==================
		
		private function vectorLookupLastTest ():void
		{
			var startTime:int = getTimer ();
			
			//var index:int = _vector.indexOf (_lastObject);
			var index:int = _vector.indexOf (_middleObject);
			
			var deltaTime:int = getTimer () - startTime;
			trace ("Vector lookup test: " + deltaTime);
			
			setTimeout (setLookupLastTest, 1000);
		}
		
		private function setLookupLastTest ():void
		{
			var startTime:int = getTimer ();
			
			//var has:Boolean = _set.has (_lastObject);
			var has:Boolean = _set.has (_middleObject);
			
			var deltaTime:int = getTimer () - startTime;
			trace ("Set lookup test: " + deltaTime);
			
			setTimeout (vectorTraversalTest, 1000);
		}
		
		// ================ TRAVERSE =================
		
		private function vectorTraversalTest ():void
		{
			var startTime:int = getTimer ();
			
			var num:int = _vector.length;
			for (var n:int = 0; n < num; n++)
			{
				var obj:Object = _vector[n];
			}
			
			var deltaTime:int = getTimer () - startTime;
			trace ("Vector traversal test: " + deltaTime);
			
			setTimeout (setTraversalTest, 1000);
		}
		
		private function setTraversalTest ():void
		{
			var startTime:int = getTimer ();
			
			for (var i:Object in _set)
			{
				var obj:Object = i;
			}
			
			var deltaTime:int = getTimer () - startTime;
			trace ("Set traversal test: " + deltaTime);
			
			setTimeout (vectorWriteTest, 1000);
		}
		
		// ================== WRITE ==================
		
		private function vectorWriteTest ():void
		{
			var startTime:int = getTimer ();
			
			var num:int = _numObjectsToWrite;
			var objectsToWrite:Vector.<Object> = _objectsToWrite;
			for (var n:int = 0; n < num; n++)
			{
				_vector.push (objectsToWrite[n]);
			}
			
			var deltaTime:int = getTimer () - startTime;
			trace ("Vector write test: " + deltaTime);
			
			setTimeout (setWriteTest, 1000);
		}
		
		private function setWriteTest ():void
		{
			var startTime:int = getTimer ();
			
			var num:int = _numObjectsToWrite;
			var objectsToWrite:Vector.<Object> = _objectsToWrite;
			for (var n:int = 0; n < num; n++)
			{
				_set.add (objectsToWrite[n]);
			}
			
			var deltaTime:int = getTimer () - startTime;
			trace ("Set write test: " + deltaTime);
		}
		
		private function onStageClick (e:MouseEvent):void
		{
			trace ("Number of objects: " + _numObjects.toExponential ());
			vectorLookupLastTest ();
		}
		
	}

}