package dump13.aida.tests.test000 {
	import dump13.aida.level0.core.AiList;
	import dump13.aida.level0.core.AiListNode;
	import flash.display.Sprite;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiTest000List {
		
		public function AiTest000List (host:Sprite)
		{
			//testA ();
			testB ();
		}
		
		private function testA ():void
		{
			var list:AiList = new AiList ();
			list.add (new AiListNode ({ name:"Node 0" }));
			list.add (new AiListNode ({ name:"Node 1" }));
			trace (list.head.object.name);
			
			list.remove (list.head.next);
			trace (list.head.object.name);
			
			list.remove (list.head);
			trace (list.head);
		}
		
		private function testB ():void
		{
			var unsortedList:AiList = new AiList ();
			var sortedList:AiList = new AiList ();
			
			var numNodes:int = 10;
			for (var n:int = 0; n < numNodes; n++)
			{
				var obj:Object = { id:Math.floor (Math.random () * 100) };
				sortedList.addSorted (new AiListNode (obj), testBCompare);
				unsortedList.add (new AiListNode (obj));
			}
			
			trace ("========== UNSORTED ==========");
			testBTraceList (unsortedList);
			trace ("=========== SORTED ===========");
			testBTraceList (sortedList);
			
			sortedList.clear ();
			trace (sortedList.head);
		}
		
		private function testBCompare (node1:AiListNode, node2:AiListNode):Number
		{
			if (node1.object.id < node2.object.id)
			{
				return -1;
			}
			return 1;
		}
		
		private function testBTraceList (list:AiList):void
		{
			var curNode:AiListNode = list.head;
			while (curNode)
			{
				trace ("id = " + curNode.object.id);
				curNode = curNode.next;
			}
		}
		
	}

}