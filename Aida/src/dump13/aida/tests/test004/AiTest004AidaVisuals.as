package dump13.aida.tests.test004 {
	import dump13.aida.level0.core.AiBlob;
	import dump13.aida.level0.core.AiBlobVisual;
	import dump13.aida.level0.core.AiCamera;
	import dump13.aida.level0.core.AiCollisionCategory;
	import dump13.aida.level0.core.AiEnemy;
	import dump13.aida.level0.core.AiLightning;
	import dump13.aida.level0.core.Aida;
	import dump13.aida.level0.core.AiGun;
	import dump13.aida.level0.core.AiListNode;
	import dump13.aida.level0.core.AiLocation;
	import dump13.aida.level0.core.AiPlayer;
	import dump13.pusher.level0.utils.PuKeyboard;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	
	/**
	 * ...
	 * @author dump13
	 */
	public class AiTest004AidaVisuals {
		
		private var _location:AiLocation;
		
		private var _keys:PuKeyboard;
		private var _mouseIsDown:Boolean;
		
		private var _prevTime:Number = 0;
		private var _cameraSpeed:Number = 100;
		
		private var _textField:TextField;
		
		private var _dynCounter:int = 0;
		private var _intCounter:int = 0;
		
		
		public function AiTest004AidaVisuals (host:Sprite)
		{
			new Aida (host);
			Aida.instance.prepare (onAidaComplete);
		}
		
		private function onAidaComplete ():void
		{
			var stage:Stage = Aida.instance.stage;
			
			_location = new AiLocation ();
			
			_keys = new PuKeyboard (stage);
			_keys.createTasks (Keyboard.SPACE, Keyboard.A, Keyboard.D, Keyboard.W, Keyboard.S, Keyboard.SHIFT);
			
			_prevTime = getTimer () * 1e-3;
			
			var format:TextFormat = new TextFormat ("Calibri", 14, 0xffffff);
			_textField = new TextField ();
			_textField.defaultTextFormat = format;
			_textField.wordWrap = true;
			_textField.multiline = true;
			_textField.selectable = false;
			_textField.width = 200;
			_textField.height = 70;
			_textField.x = 10;
			_textField.y = 10;
			_textField.text = "TEXT";
			Aida.instance.host.parent.addChild (_textField);
			
			createScene ();
			
			trace ("COMPLETE");
			
			stage.addEventListener (Event.ENTER_FRAME, onEnterFrame);
			stage.addEventListener (MouseEvent.MOUSE_DOWN, onStageMouseDown);
			stage.addEventListener (MouseEvent.MOUSE_UP, onStageMouseUp);
		}
		
		private function createScene ():void
		{
			var dist:Number = 250;
			var radius:Number = 20;
			var angle:Number = 0;
			var maxAngle:Number = Math.PI * 2;
			
			while (angle < maxAngle)
			{
				var blob:AiBlob = new AiBlob ();
				var curDist:Number = dist * (1 + Math.random () * .2);
				blob.x = Math.cos (angle) * curDist;
				blob.y = Math.sin (angle) * curDist;
				blob.z = -.5 + 1 * Math.random ();
				blob.radius = radius * (.8 + .2 * Math.random ());
				blob.isDynamic = false;
				
				_location.addObject (blob);
				
				//angle += Math.PI / 45;
				angle += Math.PI / 90;
			}
			
			// Player.
			var player:AiPlayer = new AiPlayer ();
			player.name = "player";
			player.setXY (0, -100);
			_location.addObject (player);
			
			var gun:AiGun = new AiGun ();
			player.attachGun (gun);
			//_location.addObject (gun);
			
			//gun = new AiGun ();
			//gun.minRadius = 2;
			//gun.maxRadius = 4;
			//gun.slotAngle = Math.PI / 10;
			//player.attachGun (gun);
			//_location.addObject (gun);
			//
			//gun = new AiGun ();
			//gun.minRadius = 2;
			//gun.maxRadius = 4;
			//gun.slotAngle = -Math.PI / 10;
			//player.attachGun (gun);
			//_location.addObject (gun);
			
			// Lightning.
			var lightning:AiLightning = new AiLightning ();
			player.attachLightning (lightning);
			
		}
		
		private function onEnterFrame (e:Event):void
		{
			var curTime:Number = getTimer () * 1e-3;
			var deltaTime:Number = curTime - _prevTime;
			
			//var curNode:AiListNode = _location.objects.head;
			//while (curNode)
			//{
				//var blob:AiBlob = curNode.object as AiBlob;
				//if (blob)
				//{
					//blob.radius += .002;
				//}
				//
				//curNode = curNode.next;
			//}
			
			var usedSpeed:Number = _cameraSpeed;
			if (_keys.keyIsDown (Keyboard.SHIFT))
			{
				usedSpeed *= 3;
			}
			
			var camera:AiCamera = Aida.instance.camera;
			//if (_keys.keyIsDown (Keyboard.A) && !_keys.keyIsDown (Keyboard.D))
			//{
				//camera.x -= usedSpeed * deltaTime;
			//}
			//if (_keys.keyIsDown (Keyboard.D) && !_keys.keyIsDown (Keyboard.A))
			//{
				//camera.x += usedSpeed * deltaTime;
			//}
			//if (_keys.keyIsDown (Keyboard.W) && !_keys.keyIsDown (Keyboard.S))
			//{
				//camera.y -= usedSpeed * deltaTime;
			//}
			//if (_keys.keyIsDown (Keyboard.S) && !_keys.keyIsDown (Keyboard.W))
			//{
				//camera.y += usedSpeed * deltaTime;
			//}
			
			var player:AiPlayer = _location.getObjectByName ("player") as AiPlayer;
			//var plAcc:Number = 120;
			//if (_keys.keyIsDown (Keyboard.A) && !_keys.keyIsDown (Keyboard.D))
			//{
				//player.velocityX -= plAcc * deltaTime;
			//}
			//if (_keys.keyIsDown (Keyboard.D) && !_keys.keyIsDown (Keyboard.A))
			//{
				//player.velocityX += plAcc * deltaTime;
			//}
			//if (_keys.keyIsDown (Keyboard.W) && !_keys.keyIsDown (Keyboard.S))
			//{
				//player.velocityY -= plAcc * deltaTime;
			//}
			//if (_keys.keyIsDown (Keyboard.S) && !_keys.keyIsDown (Keyboard.W))
			//{
				//player.velocityY += plAcc * deltaTime;
			//}
			
			var moveFore:Boolean = _keys.keyIsDown (Keyboard.W) && !_keys.keyIsDown (Keyboard.S);
			var moveBack:Boolean = _keys.keyIsDown (Keyboard.S) && !_keys.keyIsDown (Keyboard.W);
			var moveLeft:Boolean = _keys.keyIsDown (Keyboard.A) && !_keys.keyIsDown (Keyboard.D);
			var moveRight:Boolean = _keys.keyIsDown (Keyboard.D) && !_keys.keyIsDown (Keyboard.A);
			
			var moveDir:Number = 0;
			var dir45:Number = Math.PI * .25;
			var dir90:Number = Math.PI * .5;
			
			// Вперед или назад, в том числе по диагонали.
			if (moveFore || moveBack)
			{
				// Foreward.
				if (moveFore)
				{
					moveDir = 0;
					if (moveLeft)
					{
						moveDir = -dir45;
					}
					else if (moveRight)
					{
						moveDir = dir45;
					}
				}
				// Backward.
				else
				{
					moveDir = Math.PI;
					if (moveLeft)
					{
						moveDir += dir45;
					}
					else if (moveRight)
					{
						moveDir -= dir45;
					}
				}
				player.move (moveDir);
			}
			// Влево или вправо, но не по диагонали.
			else if (moveLeft || moveRight)
			{
				if (moveRight)
				{
					moveDir = dir90;
				}
				else
				{
					moveDir = -dir90;
				}
				player.move (moveDir);
			}
			// Стоим на месте.
			else
			{
				player.stop ();
			}
			
			// Radius = 20.
			// 4 comps - 30 ms per 1000 objects.
			// 2 comps (fill + stroke) - 22 ms per 1000 objects.
			// Fill only - 15 ms per 1000 objects.
			
			// Radius = 10.
			// 4 comps - 18 ms per 1000 objects.
			// 2 comps (fill + stroke) - 13...14 ms per 1000 objects.
			// Fill only - 10 ms per 1000 objects.
			
			//if (_dynCounter++ < 350 - 181)
			_intCounter++;
			if (_intCounter > 4)
			{
				_intCounter = 0;
				
				//if (_dynCounter++ < 200 - 181)
				//{
					//_intCounter = 0;
					//createDynamicBlob (0, 0);
				//}
				
				if (_dynCounter++ < 200 - 181)
				{
					_intCounter = 0;
					createEnemy (0, 200);
				}
				
				// Тест корректного удаления приаттаченных сущностей.
				/*else
				{
					// removing the gun
					//if (player.numGuns) player.detachGun (player.guns[0]);
					
					//if (player)
					//{
						//_location.removeObjectDeffered (player);
					//}
				}*/
			}
			
			var updStart:int = getTimer ();
			
			Aida.instance.timer.preUpdate ();
			_location.update ();
			Aida.instance.timer.postUpdate ();
			
			var str:String = "Update time: " + (getTimer () - updStart);
			str += "\nNum objects: " + _location.numObjects;
			str += "\nNum appearing: " + AiBlobVisual.numAppearing;
			_textField.text = str;
			
			AiBlobVisual.numAppearing = 0;
			
			_prevTime = curTime;
		}
		
		private function onStageMouseDown (e:MouseEvent):void
		{
			if (_mouseIsDown)
			{
				return;
			}
			_mouseIsDown = true;
			
			var camera:AiCamera = Aida.instance.camera;
			var globalX:Number = e.stageX + camera.x - camera.screenWidth * .5;
			var globalY:Number = e.stageY + camera.y - camera.screenHeight * .5;
			
			//createBlobs (globalX, globalY);
			//createDynamicBlob (globalX, globalY);
		}
		
		private function createBlobs (gx:Number, gy:Number):void
		{
			var radius:Number = 10;
			var dist:Number = 50;
			for (var n:int = 0; n < 10; n++)
			{
				var blob:AiBlob = new AiBlob ();
				//blob.radius = radius * (Math.random () * .5 + .5);
				blob.radius = radius;
				blob.setXY (gx + (Math.random () - .5) * dist, gy + (Math.random () - .5) * dist);
				//blob.setXY (gx, gy);
				_location.addObject (blob);
			}
		}
		
		private function createDynamicBlob (gx:Number, gy:Number):void
		{
			var blob:AiBlob = new AiBlob ();
			blob.isDynamic = true;
			blob.collCategory = AiCollisionCategory.ENEMY;
			blob.collMask = AiCollisionCategory.SCENERY | AiCollisionCategory.PLAYER;
			//blob.collMask = AiCollisionCategory.SCENERY | AiCollisionCategory.ENEMY | AiCollisionCategory.PLAYER;
			blob.radius = 10;
			blob.setXY (gx, gy);
			blob.makeAppearing (.5);
			
			var vel:Number = 150;
			var angle:Number = Math.random () * Math.PI * 2;
			blob.velocityX = Math.cos (angle) * vel;
			blob.velocityY = Math.sin (angle) * vel;
			blob.useVelocity = true;
			
			_location.addObject (blob);
		}
		
		private function createEnemy (gx:Number, gy:Number):void
		{
			var enemy:AiEnemy = new AiEnemy ();
			enemy.setXY (gx, gy);
			enemy.makeAppearing (.3);
			
			_location.addObject (enemy);
		}
		
		private function onStageMouseUp (e:MouseEvent):void
		{
			_mouseIsDown = false;
		}
		
	}

}