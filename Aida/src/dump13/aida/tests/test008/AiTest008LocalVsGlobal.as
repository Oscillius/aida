package dump13.aida.tests.test008 {
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	/**
	 * Доступ к локальной переменной иногда больше, чем к глобальной, иногда меньше. Опять же, заморачиваться не стоит.
	 * @author dump13
	 */
	public class AiTest008LocalVsGlobal
	{
		private var _host:Sprite;
		private var _stage:Stage;
		
		private var _numIterations:int = 1e7;
		
		private var _globalInt:int = 8;
		
		
		public function AiTest008LocalVsGlobal (host:Sprite)
		{
			_host = host;
			_stage = host.stage;
			
			//setTimeout (runTestForGlobal, 1000);
			_stage.addEventListener (MouseEvent.CLICK, onStageClick);
		}
		
		private function runTestForGlobal ():void
		{
			var startTime:int = getTimer ();
			
			var num:int = _numIterations;
			var integer:int;
			for (var n:int = 0; n < num; n++)
			{
				_globalInt;
			}
			
			var deltaTime:int = getTimer () - startTime;
			trace ("GLOBAL: " + deltaTime);
			
			setTimeout (runTestForLocal, 1000);
		}
		
		private function runTestForLocal ():void
		{
			var startTime:int = getTimer ();
			
			var num:int = _numIterations;
			var localInt:int = _globalInt;
			for (var n:int = 0; n < num; n++)
			{
				localInt;
			}
			
			var deltaTime:int = getTimer () - startTime;
			trace ("LOCAL:  " + deltaTime);
		}
		
		private function onStageClick (e:MouseEvent):void
		{
			runTestForGlobal ();
		}
		
	}

}