package dump13.aida.tests.test005 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	/**
	 * Тест для инлайна. Для включения во FlashDevelop: Project Properties -> SDK -> (Select AIR 22.0.0).
	 * Without inlining: 289 ms (average).
	 * With inlining:    216 ms (average).
	 * @author dump13
	 */
	public class AiTest005Inlining 
	{
		private var _host:Sprite;
		private var _stage:Stage;
		private var _testsPassed:int;
		private var _sumMs:Number = 0;
		
		
		public function AiTest005Inlining (host:Sprite)
		{
			_host = host;
			_stage = host.stage;
			_stage.addEventListener (MouseEvent.CLICK, onClick);
		}
		
		private function runTest ():void
		{
			var startTime:int = getTimer ();
			
			var numIterations:int = 1e6;
			for (var n:int = 0; n < numIterations; n++)
			{
				var len:Number = doStuff (1, 2, 3, 4);
			}
			
			var deltaTime:int = getTimer () - startTime;
			_testsPassed++;
			_sumMs += deltaTime;
			var averageMs:Number = Math.round (_sumMs / _testsPassed);
			
			trace ("Test results: " + deltaTime + " ms, average: " + averageMs + " ms.");
		}
		
		[Inline]
		private final function doStuff (x0:Number, y0:Number, x1:Number, y1:Number):Number
		{
			var dx:Number = x1 - x0;
			var dy:Number = y1 - y0;
			return dx * dx + dy * dy;
		}
		
		private function onClick (e:MouseEvent):void
		{
			runTest ();
		}
		
	}

}