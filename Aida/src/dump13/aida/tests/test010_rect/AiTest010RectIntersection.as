package dump13.aida.tests.test010_rect 
{
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.geom.Rectangle;
	import flash.utils.getTimer;
	import flash.utils.setInterval;
	import flash.utils.setTimeout;
	import nexus.math.Random;
	import nexus.math.TinyMersenneTwisterGenerator;
	/**
	 * Intersection test.
	 * 53...55 ms for native rect.
	 * 49...50 ms for custom.
	 * 
	 * Containment test.
	 * 30 ms for native rect.
	 * @author dump13
	 */
	public class AiTest010RectIntersection 
	{
		private var _host:Sprite;
		private var _stage:Stage;
		
		private var _rect:Rectangle = new Rectangle (-10, -10, 20, 20);
		private var _compareSet:Vector.<Rectangle> = new Vector.<Rectangle>();
		
		private var _left:Number = -10;
		private var _top:Number = -10;
		private var _width:Number = 20;
		private var _height:Number = 20;
		
		private var _random:Random = new Random (new TinyMersenneTwisterGenerator (1));
		
		private var _numIterations:int = 1e5;
		
		private var _sumTime:Number = 0;
		private var _numMeasures:int = 0;
		
		
		public function AiTest010RectIntersection (host:Sprite)
		{
			_host = host;
			_stage = host.stage;
			
			prepare ();
			
			setInterval (runTestNative, 100);
			//setInterval (runTestCustom, 100);
		}
		
		private function prepare ():void
		{
			for (var n:int = 0; n < _numIterations; n++)
			{
				var left:Number = getRandomCoord ();
				var right:Number = getRandomCoord ();
				var top:Number = getRandomCoord ();
				var bottom:Number = getRandomCoord ();
				var r:Rectangle = new Rectangle (left, top, right - left, bottom - top);
				_compareSet[n] = r;
			}
		}
		
		[Inline]
		private final function getRandomCoord ():Number
		{
			return _random.float ( -100, 100);
		}
		
		private function runTestNative ():void
		{
			var startTime:int = getTimer ();
			
			for (var n:int = 0; n < _numIterations; n++)
			{
				_rect.containsRect (_compareSet[n]);
				//_rect.intersects (_compareSet[n]);
			}
			
			var deltaTime:int = getTimer () - startTime;
			_sumTime += deltaTime;
			_numMeasures++;
			var averageTime:Number = _sumTime / _numMeasures;
			trace ("Test for native rect: " + deltaTime+", average: " + averageTime);
		}
		
		private function runTestCustom ():void
		{
			var startTime:int = getTimer ();
			
			for (var n:int = 0; n < _numIterations; n++)
			{
				var rect:Rectangle = _compareSet[n];
				var r:Number = _left + _width;
				var b:Number = _top + _height;
				var intersects:Boolean = rect.right >= _left && rect.left < r && rect.bottom >= _top && rect.top < b;
			}
			
			var deltaTime:int = getTimer () - startTime;
			_sumTime += deltaTime;
			_numMeasures++;
			var averageTime:Number = _sumTime / _numMeasures;
			trace ("Test for custom rect: " + deltaTime+", average: " + averageTime);
		}
		
	}

}