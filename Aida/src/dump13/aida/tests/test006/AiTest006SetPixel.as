package dump13.aida.tests.test006 {
	import dump13.pusher.level0.utils.PuKeyboard;
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.PixelSnapping;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	/**
	 * Тест для оценки скорости BitmapData.setPixel() по сравнению с BitmapData.draw() (совместно с Graphics.lineTo()).
	 * Результаты:
	 * - LINE TO:   31...47 ms
	 * - SET PIXEL: 140...170 ms
	 * Вывод: лучше не заморачиваться.
	 * @author dump13
	 */
	public class AiTest006SetPixel
	{
		private var _host:Sprite;
		private var _stage:Stage;
		private var _screen:Bitmap;
		private var _buffer:BitmapData;
		
		private var _keys:PuKeyboard;
		
		// Test settings.
		private var _numLines:int = 3e3;
		private var _lineLength:int = 400;
		private var _stepX:int = 1;
		
		// Not used.
		private var _averageTime:Number = 0;
		
		
		public function AiTest006SetPixel (host:Sprite)
		{
			_host = host;
			_stage = host.stage;
			
			_buffer = new BitmapData (_stage.stageWidth, _stage.stageHeight, true, 0xff000000);
			_screen = new Bitmap (_buffer, PixelSnapping.ALWAYS, false);
			_host.addChild (_screen);
			
			_keys = new PuKeyboard (_stage);
			_keys.createTask (Keyboard.SHIFT);
			
			_stage.addEventListener (MouseEvent.CLICK, onStageMouseDown);
		}
		
		private function runTestLineTo ():void
		{
			var startTime:int = getTimer ();
			
			_buffer.lock ();
			_buffer.fillRect (_buffer.rect, 0xff000000);
			
			var sprite:Sprite = new Sprite ();
			var g:Graphics = sprite.graphics;
			g.lineStyle (1, 0xffffff);
			
			var numLines:int = _numLines;
			var lineLength:int = _lineLength;
			var curX:int = 0;
			var stepX:int = _stepX;
			var xMax:int = _buffer.width;
			for (var n:int = 0; n < numLines; n++)
			{
				g.moveTo (curX, 0);
				g.lineTo (curX, lineLength);
				
				if ((curX += stepX) >= xMax)
				{
					curX = 0;
				}
			}
			
			_buffer.draw (sprite);
			_buffer.unlock ();
			
			trace ("Test \"LINE TO\": deltaTime = " + String (getTimer () - startTime) + " ms.");
		}
		
		private function runTestSetPixel ():void
		{
			var startTime:int = getTimer ();
			
			var buffer:BitmapData = _buffer;
			buffer.lock ();
			buffer.fillRect (_buffer.rect, 0xff000000);
			
			var numLines:int = _numLines;
			var lineLength:int = _lineLength;
			var curX:Number = 0;
			var stepX:int = _stepX;
			var xMax:int = _buffer.width;
			for (var n:int = 0; n < numLines; n++)
			{
				for (var curY:int = 0; curY < lineLength; curY++)
				{
					//buffer.setPixel32 (curX, curY, 0xffffffff);
					buffer.setPixel (curX, curY, 0xccffcc);
				}
				
				if ((curX += stepX) >= xMax)
				{
					curX = 0;
				}
			}
			
			buffer.unlock ();
			
			trace ("Test \"SET PIXEL\": deltaTime = " + String (getTimer () - startTime) + " ms.");
		}
		
		private function onStageMouseDown (e:MouseEvent):void
		{
			if (!_keys.keyIsDown (Keyboard.SHIFT))
			{
				runTestLineTo ();
			}
			else
			{
				runTestSetPixel ();
			}
		}
		
	}

}