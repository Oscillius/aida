package dump13.aida.tests.test009 {
	import dump13.aida.level0.core.AiQuadTree;
	import dump13.aida.level0.core.IAiBounded;
	import dump13.msl.types.Set;
	import dump13.msl.utils.OGrid;
	import dump13.msl.utils.OGridSmart;
	import dump13.pusher.level0.utils.PuKeyboard;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.utils.getTimer;
	import nexus.math.Random;
	import nexus.math.TinyMersenneTwisterGenerator;
	/**
	 * Задача теста - сравнить производительность AiQuadTree с производительностью OGrid.
	 * - Bruetforce: 600 ms per 1000 circles.
	 * - Quadtree: 60...108 ms per 1000 circles.
	 * - Grid: 25 ms per 1000 circles (cellSize = 32 px).
	 * 
	 * Quadtree: убрал проверку на выход объекта за пределы ноды, теперь получается 57...58 мс на тысячу объектов.
	 * @author dump13
	 */
	public class AiTest009QuadTree
	{
		private var _host:Sprite;
		private var _stage:Stage;
		
		private var _numCircles:int = 1e3;
		private var _maxVelocity:Number = .5;
		
		private var _circles:Vector.<Ai009Circle> = new Vector.<Ai009Circle> ();
		private var _quadTree:AiQuadTree;
		private var _grid:OGrid;
		private var _smartGrid:OGridSmart;
		
		private var _random:Random = new Random (new TinyMersenneTwisterGenerator (1));
		
		private var _textField:TextField;
		private var _showTree:Boolean = false;
		private var _keys:PuKeyboard;
		
		private var _sumConstrTime:Number = 0;
		private var _sumCollTime:Number = 0;
		private var _numMeasures:int = 0;
		
		
		public function AiTest009QuadTree (host:Sprite)
		{
			_host = host;
			_stage = host.stage;
			
			_keys = new PuKeyboard (_stage);
			_keys.addListener (Keyboard.ENTER, PuKeyboard.PRESS, onEnterPress);
			
			createScene ();
			
			createQuadTree ();
			createGrid ();
			createSmartGrid ();
			
			var format:TextFormat = new TextFormat ("Calibri", 14, 0xffffaa);
			_textField = new TextField ();
			_textField.defaultTextFormat = format;
			_textField.wordWrap = true;
			_textField.multiline = true;
			_textField.selectable = false;
			_textField.width = 600;
			_textField.height = 70;
			_textField.x = 10;
			_textField.y = 10;
			_textField.text = "TEXT";
			_host.addChild (_textField);
			
			_stage.addEventListener (Event.ENTER_FRAME, onEnterFrame);
			//_stage.addEventListener (MouseEvent.MOUSE_DOWN, onStageMouseDown);
		}
		
		private function createScene ():void
		{
			for (var n:int = 0; n < _numCircles; n++)
			{
				var circle:Ai009Circle = new Ai009Circle (Math.pow (_random.float (), 3) * 5 + 1);
				//var circle:Ai009Circle = new Ai009Circle (_random.float () * 5 + 1);
				
				circle.x = _random.float () * _stage.stageWidth;
				circle.y = _random.float () * _stage.stageHeight;
				
				//circle.x = _random.float (.8, 1) * _stage.stageWidth;
				//circle.y = _random.float (.8, 1) * _stage.stageHeight;
				
				circle.velocityX = (_random.float () - .5) * _maxVelocity;
				circle.velocityY = (_random.float () - .5) * _maxVelocity;
				_circles.push (circle);
				
				_host.addChild (circle);
			}
		}
		
		private function createQuadTree ():void
		{
			if (_quadTree)
			{
				_host.removeChild (_quadTree.sprite);
				_quadTree.destroy ();
			}
			
			_quadTree = new AiQuadTree (-10, -10, _stage.stageWidth + 20, -3, 14);
			//_quadTree = new AiQuadTree (0, 0, _stage.stageWidth, -3, 10);
			_host.addChild (_quadTree.sprite);
		}
		
		private function createGrid ():void
		{
			_grid = new OGrid (16);
		}
		
		private function createSmartGrid ():void
		{
			_smartGrid = new OGridSmart (16, 8, 6, 10);
		}
		
		private function onEnterFrame (e:Event):void
		{
			//oefQuadTree ();
			//oefOGrid ();
			//oefBruteforce ();
			oefSmartGrid ();
		}
		
		private function oefQuadTree ():void
		{
			_numMeasures++;
			
			//var n:int = 0;
			//
			//createQuadTree ();
			//for (n = 0; n < _numCircles; n++)
			//{
				//_quadTree.addItem (_circles[n]);
			//}
			
			//_quadTree.clear ();
			//for (n = 0; n < _numCircles; n++)
			//{
				//_quadTree.addItem (_circles[n]);
			//}
			
			var n:int;
			
			var creationStartTime:int = getTimer ();
			
			_quadTree.clear ();
			for (n = 0; n < _numCircles; n++)
			{
				_quadTree.addItem (_circles[n]);
			}
			
			var constrDeltaTime:int = getTimer () - creationStartTime;
			_sumConstrTime+= constrDeltaTime;
			var averageConstrTime:Number = Math.floor (_sumConstrTime / _numMeasures);
			_textField.text = "Tree construct time: " + constrDeltaTime+", average: " + averageConstrTime;
			
			//var query:Vector.<IAiBounded> = _quadTree.getItemsInRect (new Rectangle (0, 0, 30, 30));
			//var numInside:int = query.length;
			//for (n = 0; n < numInside; n++)
			//{
				//circle = query[n] as Ai009Circle;
				//circle.redraw (0xbb2222);
			//}
			
			
			if (_showTree)
			{
				_quadTree.debugDraw ();
			}
			else
			{
				_quadTree.sprite.graphics.clear ();
			}
			
			
			// Defaulting the color.
			for (n = 0; n < _numCircles; n++)
			{
				_circles[n].redraw (0x555555);
			}
			
			
			var collStartTime:int = getTimer ();
			
			for (n = 0; n < _numCircles; n++)
			{
				var circle:Ai009Circle = _circles[n];
				circle.x += circle.velocityX;
				circle.y += circle.velocityY;
				
				if (circle.x - circle.radius < 0)
				{
					circle.x = circle.radius;
					circle.velocityX = -circle.velocityX;
				}
				if (circle.x + circle.radius > _stage.stageWidth)
				{
					circle.x = _stage.stageWidth - circle.radius;
					circle.velocityX = -circle.velocityX;
				}
				if (circle.y - circle.radius < 0)
				{
					circle.y = circle.radius;
					circle.velocityY = -circle.velocityY;
				}
				if (circle.y + circle.radius > _stage.stageHeight)
				{
					circle.y = _stage.stageHeight - circle.radius;
					circle.velocityY = -circle.velocityY;
				}
				
				// Collisions.
				var circleRect:Rectangle = circle.rect;
				var nbs:Vector.<IAiBounded> = _quadTree.getItemsInRect (circle.rect);
				var numNeighbors:int = nbs.length;
				for (var m:int = 0; m < numNeighbors; m++)
				{
					var nb:Ai009Circle = nbs[m] as Ai009Circle;
					if (nb === circle)
					{
						continue;
					}
					if (!nb.rect.intersects (circleRect))
					{
						continue;
					}
					var dx:Number = nb.x - circle.x;
					var dy:Number = nb.y - circle.y;
					var dist:Number = Math.sqrt (dx * dx + dy * dy);
					var penetr:Number = circle.radius + nb.radius - dist;
					if (penetr < 0)
					{
						continue;
					}
					
					circle.redraw (0xcc2222);
					nb.redraw (0xcc2222);
					
					//var dirX:Number = dx / dist;
					//var dirY:Number = dy / dist;
					//
					//var circleCollVel:Number = circle.velocityX * dirX + circle.velocityY * dirY;
					//circle.velocityX -= dirX * circleCollVel;
					//circle.velocityY -= dirY * circleCollVel;
					//
					//var nbCollVel:Number = nb.velocityX * dirX + nb.velocityY * dirY;
					//nb.velocityX += dirX * nbCollVel;
					//nb.velocityY += dirY * nbCollVel;
				}
			}
			
			var collDeltaTime:int = getTimer () - collStartTime;
			_sumCollTime += collDeltaTime;
			//_numMeasures++;
			var averageCollTime:Number = _sumCollTime / _numMeasures;
			_textField.appendText ("\nColl time: " + collDeltaTime +", average: " + Math.round (averageCollTime));
			
		}
		
		private function oefOGrid ():void
		{
			var n:int;
			
			_numMeasures++;
			
			var creationStartTime:int = getTimer ();
			
			_grid.clear ();
			for (n = 0; n < _numCircles; n++)
			{
				var c:Ai009Circle = _circles[n];
				_grid.addItemByRealRect (c, c.rect);
			}
			
			var constrDeltaTime:int = getTimer () - creationStartTime;
			_sumConstrTime += constrDeltaTime;
			var averageConstrTime:Number = Math.floor (_sumConstrTime / _numMeasures);
			_textField.text = "Grid construct time: " + constrDeltaTime + ", average: " + averageConstrTime;
			
			// Defaulting the color.
			for (n = 0; n < _numCircles; n++)
			{
				_circles[n].redraw (0x555555);
			}
			
			
			var collStartTime:int = getTimer ();
			
			for (n = 0; n < _numCircles; n++)
			{
				var circle:Ai009Circle = _circles[n];
				circle.x += circle.velocityX;
				circle.y += circle.velocityY;
				
				if (circle.x - circle.radius < 0)
				{
					circle.x = circle.radius;
					circle.velocityX = -circle.velocityX;
				}
				if (circle.x + circle.radius > _stage.stageWidth)
				{
					circle.x = _stage.stageWidth - circle.radius;
					circle.velocityX = -circle.velocityX;
				}
				if (circle.y - circle.radius < 0)
				{
					circle.y = circle.radius;
					circle.velocityY = -circle.velocityY;
				}
				if (circle.y + circle.radius > _stage.stageHeight)
				{
					circle.y = _stage.stageHeight - circle.radius;
					circle.velocityY = -circle.velocityY;
				}
				
				// Collisions.
				var circleRect:Rectangle = circle.rect;
				var nbs:Set = _grid.getItemsInRealRect (circle.rect)
				for (var i:Object in nbs)
				{
					var nb:Ai009Circle = i as Ai009Circle;
					if (nb === circle)
					{
						continue;
					}
					if (!nb.rect.intersects (circleRect))
					{
						continue;
					}
					
					var dx:Number = nb.x - circle.x;
					var dy:Number = nb.y - circle.y;
					var dist:Number = Math.sqrt (dx * dx + dy * dy);
					var penetr:Number = circle.radius + nb.radius - dist;
					if (penetr < 0)
					{
						continue;
					}
					
					circle.redraw (0xcc2222);
					nb.redraw (0xcc2222);
					
					//var dirX:Number = dx / dist;
					//var dirY:Number = dy / dist;
					//
					//var circleCollVel:Number = circle.velocityX * dirX + circle.velocityY * dirY;
					//circle.velocityX -= dirX * circleCollVel;
					//circle.velocityY -= dirY * circleCollVel;
					//
					//var nbCollVel:Number = nb.velocityX * dirX + nb.velocityY * dirY;
					//nb.velocityX += dirX * nbCollVel;
					//nb.velocityY += dirY * nbCollVel;
				}
			}
			
			var collDeltaTime:int = getTimer () - collStartTime;
			_sumCollTime += collDeltaTime;
			//_numMeasures++;
			var averageCollTime:Number = _sumCollTime / _numMeasures;
			_textField.appendText ("\nColl time: " + collDeltaTime +", average: " + Math.round (averageCollTime));
		}
		
		private function oefSmartGrid ():void
		{
			var n:int;
			
			_numMeasures++;
			
			var creationStartTime:int = getTimer ();
			
			_smartGrid.clear ();
			for (n = 0; n < _numCircles; n++)
			{
				var c:Ai009Circle = _circles[n];
				_smartGrid.addItemByRealRect (c, c.rect);
			}
			
			var constrDeltaTime:int = getTimer () - creationStartTime;
			_sumConstrTime += constrDeltaTime;
			var averageConstrTime:Number = Math.floor (_sumConstrTime / _numMeasures);
			_textField.text = "Smart Grid construct time: " + constrDeltaTime + ", average: " + averageConstrTime;
			
			// Defaulting the color.
			for (n = 0; n < _numCircles; n++)
			{
				_circles[n].redraw (0x555555);
			}
			
			
			var collStartTime:int = getTimer ();
			
			for (n = 0; n < _numCircles; n++)
			{
				var circle:Ai009Circle = _circles[n];
				circle.x += circle.velocityX;
				circle.y += circle.velocityY;
				
				if (circle.x - circle.radius < 0)
				{
					circle.x = circle.radius;
					circle.velocityX = -circle.velocityX;
				}
				if (circle.x + circle.radius > _stage.stageWidth)
				{
					circle.x = _stage.stageWidth - circle.radius;
					circle.velocityX = -circle.velocityX;
				}
				if (circle.y - circle.radius < 0)
				{
					circle.y = circle.radius;
					circle.velocityY = -circle.velocityY;
				}
				if (circle.y + circle.radius > _stage.stageHeight)
				{
					circle.y = _stage.stageHeight - circle.radius;
					circle.velocityY = -circle.velocityY;
				}
				
				// Collisions.
				var circleRect:Rectangle = circle.rect;
				var nbs:Set = _smartGrid.getItemsInRealRect (circle.rect)
				for (var i:Object in nbs)
				{
					var nb:Ai009Circle = i as Ai009Circle;
					if (nb === circle)
					{
						continue;
					}
					if (!nb.rect.intersects (circleRect))
					{
						continue;
					}
					
					var dx:Number = nb.x - circle.x;
					var dy:Number = nb.y - circle.y;
					var dist:Number = Math.sqrt (dx * dx + dy * dy);
					var penetr:Number = circle.radius + nb.radius - dist;
					if (penetr < 0)
					{
						continue;
					}
					
					circle.redraw (0xcc2222);
					nb.redraw (0xcc2222);
					
					//var dirX:Number = dx / dist;
					//var dirY:Number = dy / dist;
					//
					//var circleCollVel:Number = circle.velocityX * dirX + circle.velocityY * dirY;
					//circle.velocityX -= dirX * circleCollVel;
					//circle.velocityY -= dirY * circleCollVel;
					//
					//var nbCollVel:Number = nb.velocityX * dirX + nb.velocityY * dirY;
					//nb.velocityX += dirX * nbCollVel;
					//nb.velocityY += dirY * nbCollVel;
				}
			}
			
			var collDeltaTime:int = getTimer () - collStartTime;
			_sumCollTime += collDeltaTime;
			//_numMeasures++;
			var averageCollTime:Number = _sumCollTime / _numMeasures;
			_textField.appendText ("\nColl time: " + collDeltaTime +", average: " + Math.round (averageCollTime));
		}
		
		private function oefBruteforce ():void
		{
			var n:int;
			
			// Defaulting the color.
			for (n = 0; n < _numCircles; n++)
			{
				_circles[n].redraw (0x555555);
			}
			
			
			var collStartTime:int = getTimer ();
			
			for (n = 0; n < _numCircles; n++)
			{
				var circle:Ai009Circle = _circles[n];
				circle.x += circle.velocityX;
				circle.y += circle.velocityY;
				
				if (circle.x - circle.radius < 0)
				{
					circle.x = circle.radius;
					circle.velocityX = -circle.velocityX;
				}
				if (circle.x + circle.radius > _stage.stageWidth)
				{
					circle.x = _stage.stageWidth - circle.radius;
					circle.velocityX = -circle.velocityX;
				}
				if (circle.y - circle.radius < 0)
				{
					circle.y = circle.radius;
					circle.velocityY = -circle.velocityY;
				}
				if (circle.y + circle.radius > _stage.stageHeight)
				{
					circle.y = _stage.stageHeight - circle.radius;
					circle.velocityY = -circle.velocityY;
				}
				
				// Collisions.
				var circleRect:Rectangle = circle.rect;
				for (var m:int = n + 1; m < _numCircles; m++)
				{
					var nb:Ai009Circle = _circles[m];
					if (nb === circle)
					{
						continue;
					}
					if (!nb.rect.intersects (circleRect))
					{
						continue;
					}
					
					var dx:Number = nb.x - circle.x;
					var dy:Number = nb.y - circle.y;
					var dist:Number = Math.sqrt (dx * dx + dy * dy);
					var penetr:Number = circle.radius + nb.radius - dist;
					if (penetr < 0)
					{
						continue;
					}
					
					circle.redraw (0xcc2222);
					nb.redraw (0xcc2222);
				}
			}
			
			var collDeltaTime:int = getTimer () - collStartTime;
			_sumCollTime += collDeltaTime;
			_numMeasures++;
			var averageCollTime:Number = _sumCollTime / _numMeasures;
			_textField.text = "\nColl time: " + collDeltaTime +", average: " + Math.round (averageCollTime);
		}
		
		private function onStageMouseDown (e:MouseEvent):void
		{
			var n:int = 0;
			
			for (n = 0; n < _numCircles; n++)
			{
				var circle:Ai009Circle = _circles[n];
				circle.x = _random.float () * _stage.stageWidth;
				circle.y = _random.float () * _stage.stageHeight;
			}
			
			//createQuadTree ();
			_quadTree.clear ();
			for (n = 0; n < _numCircles; n++)
			{
				_quadTree.addItem (_circles[n]);
			}
		}
		
		private function onEnterPress ():void
		{
			_showTree = !_showTree;
		}
		
	}

}