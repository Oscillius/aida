package dump13.aida.tests.test009 {
	import dump13.aida.level0.core.IAiBounded;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author dump13
	 */
	public class Ai009Circle extends Sprite implements IAiBounded
	{
		private var _radius:Number = 0;
		
		public var velocityX:Number = 0;
		public var velocityY:Number = 0;
		
		
		public function Ai009Circle (radius:Number = 10)
		{
			_radius = radius;
			redraw (0x555555);
		}
		
		public function redraw (color:uint):void
		{
			graphics.clear ();
			graphics.lineStyle (1, 0xffffff);
			graphics.beginFill (color);
			graphics.drawCircle (0, 0, _radius);
			graphics.endFill ();
		}
		
		
		public function get rect ():Rectangle
		{
			var diam:Number = _radius + _radius;
			return new Rectangle (x - _radius, y - _radius, diam, diam);
		}
		
		public function get radius ():Number { return _radius; }
		
	}

}