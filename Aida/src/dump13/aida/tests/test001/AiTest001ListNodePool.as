package dump13.aida.tests.test001 {
	import dump13.aida.level0.core.AiList;
	import dump13.aida.level0.core.AiListNode;
	import dump13.aida.level0.core.AiListNodePool;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.utils.getTimer;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiTest001ListNodePool {
		
		private static const USUAL:int = 0;
		private static const POOL:int = 1;
		
		private var _host:Sprite;
		private var _stage:Stage;
		
		private var _list:AiList = new AiList ();
		private var _pool:AiListNodePool;
		private var _objects:Vector.<Object> = new Vector.<Object>();
		private var _numNodes:int = 1e5;
		private var _state:int = USUAL;
		
		
		public function AiTest001ListNodePool (host:Sprite) {
			_host = host;
			_stage = _host.stage;
			
			_pool = new AiListNodePool ();
			createObjects ();
			
			_stage.addEventListener (MouseEvent.CLICK, onStageMouseClick);
			_stage.addEventListener (Event.ENTER_FRAME, onEnterFrame);
		}
		
		private function createObjects ():void
		{
			var num:int = _numNodes;
			_objects.length = num;
			for (var n:int = 0; n < num; n++)
			{
				_objects[n] = { id:n };
			}
		}
		
		private function onStageMouseClick (e:MouseEvent):void
		{
			switch (_state)
			{
				case USUAL:
					_state = POOL;
					break;
				case POOL:
					_state = USUAL;
					break;
				default:
					throw new Error ("Unknown state.");
			}
		}
		
		private function onEnterFrame (e:Event):void
		{
			var n:int = 0;
			var num:int = _numNodes;
			
			var time:int = getTimer ();
			
			switch (_state)
			{
				case USUAL:
					
					_list.clear ();
					for (n = 0; n < num; n++)
					{
						_list.add (new AiListNode (_objects[n]));
					}
					break;
					
				case POOL:
					
					var curNode:AiListNode = _list.head;
					
					while (curNode)
					{
						var nextNode:AiListNode = curNode.next;
						
						//curNode.object = null;
						
						_pool.disposeNode (curNode);
						
						curNode = nextNode;
					}
					_list.clear ();
					
					//while (curNode)
					//{
						//var nextNode:AiListNode = curNode.next;
						//_list.remove (curNode);
						//
						//curNode.next = null;
						//curNode.prev = null;
						//curNode.object = null;
						//
						//_pool.disposeNode (curNode);
						//
						//curNode = nextNode;
					//}
					
					for (n = 0; n < num; n++)
					{
						curNode = _pool.getNode ();
						curNode.object = _objects[n];
						_list.add (curNode);
					}
					
					break;
			}
			
			trace ("State: " + (_state == USUAL? "USUAL," : "POOL, ") + " time: " + (getTimer () - time) + ", pool length: " + _pool.vectorLength);
			
		}
		
	}

}