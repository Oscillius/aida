package dump13.aida.level0.defs {
	/**
	 * ...
	 * @author dump13
	 */
	public class AiDef {
		
		public static const D_NONE:int = 0;
		public static const D_LOCATION:int = 1;
		
		public var type:int;
		public var children:Vector.<AiDef> = new Vector.<AiDef> ();
		
		
		public function AiDef (type:int) {
			this.type = type;
		}
		
	}

}