package dump13.aida.level0.core {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.PixelSnapping;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiCamera {
		
		private var _x:Number = 0;
		private var _y:Number = 0;
		private var _xRound:Number = 0;
		private var _yRound:Number = 0;
		private var _screenWidth:Number = 0;
		private var _screenHeight:Number = 0;
		private var _screenHalfWidth:Number = 0;
		private var _screenHalfHeight:Number = 0;
		private var _areaHalfWidth:Number = 0;
		private var _areaHalfHeight:Number = 0;
		
		private var _buffer:BitmapData;
		
		
		public function AiCamera (screenWidth:Number, screenHeight:Number, areaWidth:Number = NaN, areaHeight:Number = NaN)
		{
			_screenWidth = screenWidth;
			_screenHeight = screenHeight;
			_screenHalfWidth = _screenWidth * .5;
			_screenHalfHeight = _screenHeight * .5;
			if (isNaN (areaWidth))
			{
				areaWidth = _screenWidth;
			}
			if (isNaN (areaHeight))
			{
				areaHeight = _screenHeight;
			}
			_areaHalfWidth = areaWidth * .5;
			_areaHalfHeight = areaHeight * .5;
			
			_buffer = new BitmapData (screenWidth, screenHeight, true, 0xff000000);
		}
		
		public function destroy ():void
		{
			_buffer.dispose ();
			_buffer = null;
		}
		
		/// [Optional]. Создает новый объект Bitmap, у которого свойство bitmapData установлено в буфер этой камеры.
		public function createScreen ():Bitmap
		{
			return new Bitmap (_buffer, PixelSnapping.ALWAYS, false);
		}
		
		public function lock ():void
		{
			_buffer.lock ();
		}
		
		public function unlock ():void
		{
			_buffer.unlock ();
		}
		
		public function fill (color:uint = 0xff000000):void
		{
			_buffer.fillRect (_buffer.rect, color);
		}
		
		public function setPosition (x:Number, y:Number):void
		{
			_x = x;
			_y = y;
			_xRound = Math.round (_x);
			_yRound = Math.round (_y);
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/// Координата x камеры. Положение камеры - это положение центра экрана.
		public function get x ():Number { return _x; }
		public function set x (value:Number):void
		{
			_x = value;
			_xRound = Math.round (value);
		}
		
		/// Координата y камеры. Положение камеры - это положение центра экрана.
		public function get y ():Number { return _y; }
		public function set y (value:Number):void
		{
			_y = value;
			_yRound = Math.round (value);
		}
		
		public function get xRound ():Number { return _xRound; }
		
		public function get yRound ():Number { return _yRound; }
		
		public function get screenWidth ():Number { return _screenWidth; }
		
		public function get screenHeight ():Number { return _screenHeight; }
		
		public function get screenHalfWidth ():Number { return _screenHalfWidth; }
		
		public function get screenHalfHeight ():Number { return _screenHalfHeight; }
		
		/// Область, заданная в координатах локации, визуалы внутри которой должны быть обновлены и перерисованы.
		public function get area ():Rectangle
		{
			return new Rectangle (_x - _areaHalfWidth, _y - _areaHalfHeight, _areaHalfWidth + _areaHalfWidth, _areaHalfHeight + _areaHalfHeight);
		}
		
		/// Изображение, в которое происходит рендеринг визуалов.
		public function get buffer ():BitmapData { return _buffer; }
		
	}

}