package dump13.aida.level0.core {
	/**
	 * ...
	 * @author dump13
	 */
	public class AiSettings {
		
		/// Размер ячейки сетки для регистрации Сущностей.
		public static const STATICS_GRID_CELL_SIZE:Number = 128;
		/// Размер ячейки сетки для регистрации диманических сущностей.
		public static const DYNAMICS_GRID_CELL_SIZE:Number = 64;
		/// Половина длины стороны квадрата, определяющего область, внутри которой будут рассчитываться столкновения.
		public static const PHYSICS_AREA_HALF_SIZE:Number = 1600;
		/// Использовать ли "умную" сетку для регистрации динамики.
		public static const PHYSICS_USES_SMART_GRID:Boolean = false;
		/// Размер ячейки сетки для регистрации визуалов.
		public static const VISUALS_GRID_CELL_SIZE:Number = 32;
		/// Максимальный размер круговой маски. Используется классом AiCircleBank для кэширования масок.
		public static const CIRCLE_MAX_RADIUS:Number = 100;
		/// Шаг, с которым изменяется радиус круговой маски.
		public static const CIRCLE_RADIUS_STEP:Number = .25;
		/// Размер изображения, которое будет использоваться для одноцветной заливки.
		public static const FLAT_SQUARE_SIZE:int = 200;
		/// Размер клипов, которые будут использоваться в качестве масок для "проявления" объектов.
		public static const ALPHA_SQUARE_SIZE:int = 200;
		/// Шаг, с которым изменяется альфа объектов.
		public static const ALPHA_STEP:Number = .1;
		
		/// Максимальный уровень "аккуратного" раскалывания блобов. Это показатель степени, в которую нужно возвести число 2, чтобы получить количество получаемых при раскалывании частей. Раскалывание на больее чем 2^MAX_SPLIT_LEVEL частей буду считать невозможным.
		public static const MAX_SPLIT_LEVEL:int = 3;
		/// Максимальное количество частей, на которое можно "аккуратно" расколоть блоб. Это единица, сдвинутая влево на MAX_SPLIT_LEVEL бит.
		public static const MAX_SPLIT_PARTS:int = 1 << AiSettings.MAX_SPLIT_LEVEL;
		/// Минимально возможный радиус части блоба, получаемый при "аккуратном" раскалывании. Если раскалывание дает меньшие по размеру части, то оно заменяется "распылением" блоба - его простым уничтожением.
		public static const MIN_SPLIT_RADIUS:Number = 1;
		
		/// Количество кэшированных направлений в экземпляре AiDirections.
		public static const NUM_DIRECTIONS:int = 720;
		
		/// Оптимизировать ли регистрацию молний в физической сетке. Если true, то молния не будет регистрироваться в сетке для динамических объектов локации.
		public static const OPTIMIZE_LIGHTNINGS_PHYS_RECT:Boolean = true;
		
		
		public function AiSettings () {
			
		}
		
	}

}