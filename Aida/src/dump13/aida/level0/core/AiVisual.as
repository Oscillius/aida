package dump13.aida.level0.core {
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiVisual {
		
		/// Сущность-владелец этого визуала.
		protected var _entity:AiEntity;
		/// Нужно ли перерисовать визуал (например, _sprite.graphics).
		internal var _needsRedraw:Boolean;
		
		/// Нода для хранения визуала в списке визуалов, попадающих в область видимости. Создается в любом случае.
		internal var _locVisualsOnScreenNode:AiListNode;
		
		
		public function AiVisual ()
		{
			_needsRedraw = true;
			
			_locVisualsOnScreenNode = new AiListNode (this);
		}
		
		public function destroy ():void
		{
			if (_entity)
			{
				throw new Error ("Remove from entity first.");
			}
			
			_locVisualsOnScreenNode.destroy ();
			_locVisualsOnScreenNode = null;
		}
		
		public function clone ():AiVisual
		{
			var visual:AiVisual = new AiVisual ();
			visual.clonePropertiesFrom (this);
			return visual;
		}
		
		protected function clonePropertiesFrom (visual:AiVisual):void
		{
			// Nothing to copy (_needsRedraw will be equal to true after creation).
		}
		
		internal function connectToEntity (entity:AiEntity):void
		{
			_entity = entity;
		}
		
		internal function shutdownFromEntity ():void
		{
			_entity = null;
		}
		
		/**
		 * Метод, который вызывается из локации, если _needsRedraw визуала равен true. Это какое-то одноразовое действие, не связанное с финальным рендерингом (рисование в graphics спрайта или обновление каких-то внутренних свойств, необходимых для дальнейшего рендеринга). Наличие лэйзи-перерисовки позволит более оптимально рисовать статику, не меняющуюся со временем.
		 */
		public function redraw ():void
		{
			throw new Error ("Abstract method.");
		}
		
		public function render (camera:AiCamera):void
		{
			throw new Error ("Abstract method.");
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/// Сущность-владелец этого визуала.
		public function get entity ():AiEntity { return _entity; }
		
		/// Ограничивающий прямоугольник визуала в системе координат локации.
		public function get rect ():Rectangle { throw new Error ("Abstract method."); return null; }
		
	}

}