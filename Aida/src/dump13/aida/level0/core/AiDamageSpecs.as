package dump13.aida.level0.core {
	/**
	 * Характеристики повреждения.
	 * @author dump13
	 */
	public class AiDamageSpecs {
		
		private static var _cloneCreation:Boolean = false;
		
		private var _directCoef:Number = 0;
		private var _splitCoef:Number = 0;
		private var _sumCoef:Number = 0;
		private var _directRatio:Number = 0;
		private var _splitRatio:Number = 0;
		
		
		public function AiDamageSpecs (directCoef:Number = 0, splitCoef:Number = 1)
		{
			_directCoef = directCoef;
			_splitCoef = splitCoef;
			
			if (!_cloneCreation)
			{
				onCoefChange ();
			}
		}
		
		/// Создает копию текущих характеристик повреждения.
		public function clone ():AiDamageSpecs
		{
			_cloneCreation = true;
			
			var specs:AiDamageSpecs = new AiDamageSpecs (_directCoef, _splitCoef);
			specs._sumCoef = _sumCoef;
			specs._directRatio = _directRatio;
			specs._splitRatio = _splitRatio;
			
			_cloneCreation = false;
			
			return specs;
		}
		
		/// Установка коэффициентов повреждения.
		public function setUp (directCoef:Number = 0, splitCoef:Number = 1):void
		{
			_directCoef = directCoef;
			_splitCoef = splitCoef;
			onCoefChange ();
		}
		
		private function onCoefChange ():void
		{
			_sumCoef = _directCoef + _splitCoef;
			_directRatio = _directCoef / _sumCoef;
			//_splitRatio = _splitCoef / _sumCoef;
			_splitRatio = 1 - _directRatio;
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/**
		 * Коэффициент прямого урона. Чтобы найти прямой урон снаряда, необходимо умножить его массу на коэффициент прямого урона.
		 */
		public function get directCoef ():Number { return _directCoef; }
		public function set directCoef (value:Number):void
		{
			_directCoef = value;
			onCoefChange ();
		}
		
		/**
		 * Коэффициент раскалывающего урона. Чтобы найти раскалывающий урон снаряда, необходимо умножить его массу на коэффициент раскалывающего урона.
		 */
		public function get splitCoef ():Number { return _splitCoef; }
		public function set splitCoef (value:Number):void
		{
			_splitCoef = value;
			onCoefChange ();
		}
		
		/**
		 * Сумма коэффициентов directCoef и splitCoef.
		 */
		public function get sumCoef ():Number { return _sumCoef; }
		
		/**
		 * Отношение коэффициента прямого урона к сумме коэффициентов урона.
		 */
		public function get directRatio ():Number { return _directRatio; }
		
		/**
		 * Отношение коэффициента раскалывающего урона к сумме коэффициентов урона.
		 */
		public function get splitRatio ():Number { return _splitRatio; }
		
	}

}