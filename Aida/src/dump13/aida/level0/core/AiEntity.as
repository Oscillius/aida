package dump13.aida.level0.core {
	import dump13.msl.types.Set;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiEntity extends AiObject {
		
		// ======= CLONEABLE =======
		
		protected var _x:Number = 0;
		protected var _y:Number = 0;
		protected var _z:Number = 0;
		protected var _isDynamic:Boolean;
		
		// (Non-cloneable)
		protected var _visual:AiVisual;
		
		/// Набор битов, определяющих категорию сущности по признаку collision.
		public var collCategory:uint = 0x00000001;
		/// Набор битов, определяющих маску сущности по признаку collision, т.е. категории сущностей, с которыми будет сталкиваться эта сущность. Установка в нулевое значение оптимизирует проверку столкновений для этого объекта (проверка производится вне цикла). Свойство заменяет флаг isGhost.
		public var collMask:uint = 0x00000000;
		/// Маска: категории сущностей, по отношению к которым эта сущность является камикадзе. Например, снаряд и враг взаимодействуют по принципу камикадзе-урона, и хотя бы один из них при столкновении будет уничтожен. Во взаимодействии должен быть только один камикадзе.
		public var kamikazeMask:uint = 0x00000000;
		
		public var velocityX:Number = 0;
		public var velocityY:Number = 0;
		/// Применять ли скорость к координатам при обновлении. Не работает для статики.
		public var useVelocity:Boolean = true;
		protected var _maxVelocity:Number = 200;
		protected var _maxVelocitySquared:Number = _maxVelocity * _maxVelocity;
		public var useMaxVelocity:Boolean = true;
		
		/// Набор прикрепленных сущностей. Они будут добавлены в локацию вместе с этой сущностью. Подклассы AiEntity должны сами следить за своевременным заполнением этого массива и его очищением при уничтожении; сам AiEntity не содержит функционала по уничтожению присоединенных объектов и их клонированию.
		internal var _attachedEntities:Vector.<AiEntity>;
		
		/// Вызывать ли postUpdate() сущности в конце обновления локации.
		private var _enablePostUpdate:Boolean;
		
		// ======= NON-CLONEABLE =======
		
		
		public function AiEntity ()
		{
			
		}
		
		override public function destroy ():void
		{
			if (_visual)
			{
				var vis:AiVisual = _visual;
				//visual = null;
				clearVisual ();
				vis.destroy ();
			}
			
			super.destroy ();
		}
		
		override public function clone ():AiObject
		{
			var entity:AiEntity = new AiEntity ();
			entity.clonePropertiesFrom (this);
			return entity;
		}
		
		override protected function clonePropertiesFrom (object:AiObject):void
		{
			super.clonePropertiesFrom (object);
			
			var entity:AiEntity = AiEntity (object);
			setXY (entity.x, entity.y);
			z = entity.z;
			isDynamic = entity.isDynamic;
			collCategory = entity.collCategory;
			collMask = entity.collMask;
			kamikazeMask = entity.kamikazeMask;
			velocityX = entity.velocityX;
			velocityY = entity.velocityY;
			useVelocity = entity.useVelocity;
			_maxVelocity = entity._maxVelocity;
			_maxVelocitySquared = entity._maxVelocitySquared;
			useMaxVelocity = entity.useMaxVelocity;
			
			// С одной стороны, visual может назначаться при создании подкласса сущности, с другой стороны - любой пользователь может назначить сущности другой визуал. Как быть - клонировать визуал или нет?
			// Визуал не будет клонироваться - предполагается, что setVisual() будет вызываться только при создании сущности.
			// (Old) Clone the visual.
			//visual = entity.visual.clone ();
		}
		
		/**
		 * Устанавливает визуал. Метод должен вызываться только при создании подкласса сущности, а visual должен являться существующим экземпляром визуала.
		 * @param	visual Экземпляр визуала (не должен быть равен null).
		 */
		[Inline]
		protected final function setVisual (visual:AiVisual):void
		{
			if (!visual)
			{
				throw new ArgumentError ("Visual must be a valid AiVisual instance, not null.");
			}
			if (_visual)
			{
				throw new Error ("Visual is already set.");
			}
			_visual = visual;
			_visual.connectToEntity (this);
			if (_location)
			{
				_location.addEntityVisual (_visual);
			}
			// end.
		}
		
		/**
		 * Удаляет визуал из этой сущности. Метод должен использоваться только при уничтожении сущности.
		 */
		[Inline]
		protected final function clearVisual ():void
		{
			if (!_visual)
			{
				return;
			}
			if (_location)
			{
				_location.removeEntityVisual (_visual);
			}
			_visual.shutdownFromEntity ();
		}
		
		/**
		 * Обновляет сущность при обновлении локации. Вызывается только для ДИНАМИЧЕСКИХ сущностей, попадающих в область обновления физики. Метод не принимает временного шага в качестве аргумента, но может использовать свойства объекта Aida.timer.
		 */
		public function update ():void
		{
			var deltaTime:Number = Aida.instance.timer.deltaTime;
			
			if (useMaxVelocity && (velocityX || velocityY))
			{
				var velLenSq:Number = velocityX * velocityX + velocityY * velocityY;
				if (velLenSq > _maxVelocitySquared)
				{
					var velLen:Number = Math.sqrt (velLenSq);
					var newVelRatio:Number = _maxVelocity / velLen;
					velocityX *= newVelRatio;
					velocityY *= newVelRatio;
				}
			}
			
			if (useVelocity)
			{
				setXY (_x + velocityX * deltaTime, _y + velocityY * deltaTime);
			}
		}
		
		/// Обновляет сущность в конце обновления локации, после обновления остальных объектов. Метод вызывается локацией, если enablePostUpdate объекта установлен в true.
		public function postUpdate ():void
		{
			throw new Error ("This method is not implemented (at the same time 'enablePostUpdate' is set to 'true'.");
		}
		
		public function setXY (x:Number, y:Number):void
		{
			_x = x;
			_y = y;
			onXYChange ();
		}
		
		/// Вызывается после установки новых координат x и y.
		protected function onXYChange ():void
		{
			if (_location)
			{
				_location.reregisterEntityInPhysicsGrid (this);
				// ...
			}
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		public function get x ():Number { return _x; }
		public function set x (value:Number):void
		{
			_x = value;
			onXYChange ();
		}
		
		public function get y ():Number { return _y; }
		public function set y (value:Number):void
		{
			_y = value;
			onXYChange ();
		}
		
		public function get z ():Number { return _z; }
		public function set z (value:Number):void
		{
			if (_z == value)
			{
				return;
			}
			
			_z = value;
			if (_location)
			{
				_location.onEntityZChange (this);
			}
		}
		
		/// [Abstract]. Ограничивающий прямоугольник Сущности в системе координат локации.
		public function get rect ():Rectangle { throw new Error ("Abstract method."); return null; }
		
		/// Визуал сущности. Сущность может не содержать визуала, тогда она не будет отображаться на экране.
		public function get visual ():AiVisual { return _visual; }
		/*public function set visual (value:AiVisual):void
		{
			if (_visual === value)
			{
				return;
			}
			
			if (_visual)
			{
				if (_location)
				{
					_location.removeEntityVisual (_visual);
				}
				_visual.shutdownFromEntity ();
			}
			_visual = value;
			
			if (_visual)
			{
				_visual.connectToEntity (this);
				if (_location)
				{
					_location.addEntityVisual (_visual);
				}
			}
		}*/
		
		/// Является ли объект динамическим.
		public function get isDynamic ():Boolean { return _isDynamic; }
		public function set isDynamic (value:Boolean):void
		{
			if (_isDynamic == value)
			{
				return;
			}
			if (_location)
			{
				_location.unregisterEntityInPhysicsGrid (this);
			}
			_isDynamic = value;
			if (_location)
			{
				_location.registerEntityInPhysicsGrid (this);
			}
		}
		
		public function get maxVelocity ():Number { return _maxVelocity; }
		public function set maxVelocity (value:Number):void
		{
			_maxVelocity = value;
			_maxVelocitySquared = value * value;
		}
		
		/// Вызывать ли postUpdate() сущности в конце обновления локации. Установить в true, если необходимо обновить объект гарантированно после большинства остальных.
		public final function get enablePostUpdate ():Boolean { return _enablePostUpdate; }
		public final function set enablePostUpdate (value:Boolean):void
		{
			if (_enablePostUpdate == value)
			{
				return;
			}
			_enablePostUpdate = value;
			if (!value && _location)
			{
				_location.onPostUpdatableIsFalse (this);
			}
		}
		
	}

}