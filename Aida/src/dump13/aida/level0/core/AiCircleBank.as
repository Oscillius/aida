package dump13.aida.level0.core {
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	/**
	 * Банк круговых масок.
	 * @author dump13
	 */
	public class AiCircleBank extends EventDispatcher {
		
		private var _clips:Vector.<AiClip> = new Vector.<AiClip> ();
		
		private var _isCreating:Boolean;
		private var _curRadius:Number = 0;
		private var _asyncStartTime:int;
		private var _asyncTimeLimit:int = 25;
		private var _sprite:Sprite;
		private var _margin:Number = 1;
		
		
		public function AiCircleBank () {
			
		}
		
		public function destroy ():void
		{
			if (_isCreating)
			{
				throw new Error ("The creation process is currently in progress.");
			}
			var numClips:int = _clips.length;
			for (var n:int = 0; n < numClips; n++)
			{
				_clips[n].destroy ();
			}
			_clips.length = 0;
		}
		
		/// Запускает процесс создания клипов для банка. По окончании процесса рассылается событие Event.COMPLETE.
		public function createAsync ():void
		{
			if (_isCreating)
			{
				throw new Error ("Already being created.");
			}
			_isCreating = true;
			_curRadius = AiSettings.CIRCLE_RADIUS_STEP;
			_sprite = new Sprite ();
			
			_asyncStartTime = getTimer ();
			createNext ();
		}
		
		private function createNext ():void
		{
			if (_curRadius <= AiSettings.CIRCLE_MAX_RADIUS)
			{
				var diam:Number = _curRadius + _curRadius;
				var size:Number = diam + _margin + _margin;
				var bmd:BitmapData = new BitmapData (size, size, true, 0x00000000);
				
				var g:Graphics = _sprite.graphics;
				g.clear ();
				g.beginFill (0xff0000, 1);
				g.drawEllipse (_margin, _margin, diam, diam);
				g.endFill ();
				
				bmd.draw (_sprite);
				// Центр картинки посчитается автоматом.
				_clips.push (new AiClip (bmd));
				
				_curRadius += AiSettings.CIRCLE_RADIUS_STEP;
				
				var curTime:int = getTimer ();
				if (curTime - _asyncStartTime < _asyncTimeLimit)
				{
					createNext ();
				}
				else
				{
					_asyncStartTime = curTime;
					setTimeout (createNext, 1);
				}
			}
			else
			{
				_isCreating = false;
				_curRadius = 0;
				dispatchEvent (new Event (Event.COMPLETE));
			}
		}
		
		/**
		 * Возвращает клип AiClip, соответствующий кругу радиуса <code>radius</code>.
		 * @param	radius Радиус круга.
		 * @return Клип с изображением круга радиуса radius.
		 */
		public function getClipByRadius (radius:Number):AiClip
		{
			var index:int = Math.round (radius / AiSettings.CIRCLE_RADIUS_STEP) - 1;
			if (index < 0)
			{
				return _clips[0];
			}
			if (index >= _clips.length)
			{
				throw new Error ("Too big radius.");
				return _clips[_clips.length - 1];
			}
			return _clips[index];
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/// Идет ли в данный момент процесс создания клипов.
		public function get isCreating ():Boolean { return _isCreating; }
		
		/// Число от нуля до единицы, соответствующее прогрессу подготовки клипов.
		public function get progress ():Number
		{
			var value:Number = _curRadius / AiSettings.CIRCLE_MAX_RADIUS;
			return value > 1? 1 : value;
		}
		
	}

}