package dump13.aida.level0.core 
{
	import dump13.msl.utils.ColorUtils;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiEnemy extends AiBlob
	{
		private static const TINT:Number = .1;
		private static const STROKE_COLOR:uint = ColorUtils.hslToRGB (TINT, .5, .15);
		private static const SHADE_COLOR:uint = ColorUtils.hslToRGB (TINT, .5, .6);
		private static const COLOR:uint = ColorUtils.hslToRGB (TINT, .5, .7);
		private static const GLOSS_COLOR:uint = ColorUtils.hslToRGB (TINT, .5, .9);
		
		private static const COLL_MASK:uint = AiCollisionCategory.SCENERY |
											  AiCollisionCategory.PLAYER |
											  AiCollisionCategory.BULLET |
											  AiCollisionCategory.SPLIT;
		
		//protected var _angle:Number = 0;
		protected var _acceleration:Number = 25;
		
		
		public function AiEnemy () 
		{
			collCategory = AiCollisionCategory.ENEMY;
			collMask = COLL_MASK;
			
			isDynamic = true;
			
			_strokeColor = STROKE_COLOR;
			_shadeColor = SHADE_COLOR;
			_color = COLOR;
			_glossColor = GLOSS_COLOR;
		}
		
		override public function clone ():AiObject
		{
			var enemy:AiEnemy = new AiEnemy ();
			enemy.clonePropertiesFrom (this);
			return enemy;
		}
		
		override protected function clonePropertiesFrom (object:AiObject):void
		{
			super.clonePropertiesFrom (object);
			
			var enemy:AiEnemy = object as AiEnemy;
			//_angle = enemy._angle;
			_acceleration = enemy._acceleration;
		}
		
		override public function update ():void
		{
			//var player:AiPlayer = _location? _location.player : null;
			//if (!player)
			//{
				//super.update ();
				//return;
			//}
			//
			//var dx:Number = player.x - _x;
			//var dy:Number = player.y - _y;
			//var dist:Number = Math.sqrt (dx * dx + dy * dy);
			//var accRatio:Number = Aida.instance.timer.deltaTime * _acceleration / dist;
			//velocityX += dx * accRatio;
			//velocityY += dy * accRatio;
			
			super.update ();
		}
		
	}

}