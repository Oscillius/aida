package dump13.aida.level0.core 
{
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	/**
	 * Для того чтобы дерево смогло безопасно расширить себя при незначительном выходе объектов за пределы ноды с нулевым уровнем, оно должно иметь minDepth, равную как минимум 3.
	 * @author dump13
	 */
	public class AiQuadTree 
	{
		internal var _rootNode:AiQuadTreeNode;
		internal var _minDepth:int;
		internal var _maxDepth:int;
		
		/// Спрайт для отладочной отрисовки.
		internal var _sprite:Sprite = new Sprite ();
		
		
		public function AiQuadTree (x:Number, y:Number, size:Number, minDepth:int = -6, maxDepth:int = 6) 
		{
			if (minDepth > 0)
			{
				throw new Error ("Mininal depth should be lower than or equal to zero.");
			}
			if (maxDepth < 0)
			{
				throw new Error ("Maximal depth should be greater than or equal to zero.");
			}
			_minDepth = minDepth;
			_maxDepth = maxDepth;
			
			_rootNode = new AiQuadTreeNode (x, y, size, 0);
		}
		
		public function destroy ():void
		{
			_rootNode.destroy ();
			_rootNode = null;
		}
		
		public function clear ():void
		{
			var newRoot:AiQuadTreeNode = AiQuadTreeNode.createFromRect (_rootNode.rect, 0);
			_rootNode.destroy ();
			_rootNode = newRoot;
		}
		
		public function addItem (item:IAiBounded):void
		{
			_rootNode.addItem (item, this);
		}
		
		public function getItemsInRect (rect:Rectangle):Vector.<IAiBounded>
		{
			var result:Vector.<IAiBounded> = new Vector.<IAiBounded> ();
			_rootNode.getItemsInRect (rect, result);
			return result;
		}
		
		public function debugDraw ():void
		{
			_sprite.graphics.clear ();
			_sprite.graphics.lineStyle (1, 0xffffff);
			_rootNode.debugDraw (_sprite.graphics);
		}
		
		
		public function get rootNode ():AiQuadTreeNode { return _rootNode; }
		
		/// Спрайт для отладочной отрисовки.
		public function get sprite ():Sprite { return _sprite; }
		
	}

}