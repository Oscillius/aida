package dump13.aida.level0.core {
	
	/**
	 * Интерфейс для круглых тел. Сущности, реализующие его, представлены круглой формой. Используется при расчете столкновений.
	 * @author dump13
	 */
	public interface IAiCircle {
		
		/// Возвращает массу для расчета столкновений. У статичных объектов она равна Number.POSITIVE_INFINITY.
		function getMassForPhysics ():Number;
		/// Радиус круглого тела.
		function get radius ():Number;
		/// Масса круглого тела.
		function get mass ():Number;
		function set mass (value:Number):void;
		/// Коэффициент упругости круглого тела.
		function get elasticity ():Number;
		
	}
	
}