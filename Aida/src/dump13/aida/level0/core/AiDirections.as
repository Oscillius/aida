package dump13.aida.level0.core 
{
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.geom.Point;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	/**
	 * Набор направлений - радиус-векторов единичной длины, концы которых равномерно распределены по окружности.
	 * @author dump13
	 */
	public class AiDirections extends EventDispatcher
	{
		private var _numSteps:int;
		private var _angleStep:Number = 0;
		private var _dirs:Vector.<Point> = new Vector.<Point>();
		
		private var _isInit:Boolean = false;
		
		private var _curStep:int;
		private var _asyncInProgress:Boolean;
		private var _asyncTimeLimit:int = 25;
		private var _numAsyncIters:int;
		
		
		public function AiDirections (numSteps:int) 
		{
			_numSteps = numSteps;
			_angleStep = (Math.PI + Math.PI) / numSteps;
		}
		
		/**
		 * Подготавливает массив направлений. Если подготовка уже была завершена или если запущен процесс асинхронной подготовки, создается исключение.
		 */
		public function prepareSync ():void
		{
			if (_isInit)
			{
				throw new Error ("Already prepared.");
			}
			denyIfAsync ();
			
			_dirs.length = 0;
			var angle:Number = 0;
			var num:int = _numSteps;
			for (var n:int = 0; n < num; n++)
			{
				angle = _angleStep * n;
				_dirs[n] = new Point (Math.cos (angle), Math.sin (angle));
				//angle += _angleStep;
			}
			
			_isInit = true;
		}
		
		/**
		 * Запускает процесс асинхронной подготовки массива направлений. Если подготовка уже была завершена или если запущен процесс асинхронной подготовки, создается исключение.
		 */
		public function prepareAsync ():void
		{
			if (_isInit)
			{
				throw new Error ("Already prepared.");
			}
			denyIfAsync ();
			
			_curStep = 0;
			_dirs.length = 0;
			_numAsyncIters = 1;
			_asyncInProgress = true;
			
			genNextOrExit ();
		}
		
		private function genNextOrExit ():void
		{
			// Exit.
			if (_curStep >= _numSteps)
			{
				_isInit = true;
				_asyncInProgress = false;
				dispatchEvent (new Event (Event.COMPLETE));
				trace ("AiDirections: prepared in " + _numAsyncIters + " iteration(s).");
				return;
			}
			
			// Continue.
			
			var startTime:int = getTimer ();
			
			var curAngle:Number = _curStep * _angleStep;
			_dirs[_curStep] = new Point (Math.cos (curAngle), Math.sin (curAngle));
			_curStep++;
			
			if (getTimer () - startTime >= _asyncTimeLimit)
			{
				_numAsyncIters++;
				setTimeout (genNextOrExit, 1);
			}
			else
			{
				genNextOrExit ();
			}
		}
		
		/**
		 * Возвращает направление с индексом index. Индекс, соответствующий углу, можно получить с помощью getIndexByAngle().
		 * @param	index Индекс искомого направления.
		 * @return Направление с индексом index.
		 */
		public final function getDirectionByIndex (index:int):Point
		{
			denyNonInitialized ();
			return _dirs[index];
		}
		
		/**
		 * Возвращает направление, соответствующее углу angle.
		 * @param	angle Угол, которому должно соответствовать искомое направление.
		 * @return Направление с углом angle.
		 */
		public final function getDirectionByAngle (angle:Number):Point
		{
			denyNonInitialized ();
			return _dirs[getIndexByAngle (angle)];
		}
		
		/**
		 * Возвращает случайное направление.
		 * @return Случайное направление, соответствующее углу [0, Math.PI * 2).
		 */
		public final function getRandomDirection ():Point
		{
			denyNonInitialized ();
			return _dirs[int (Math.random () * _numSteps)];
			//return _dirs[Math.floor (Math.random () * _numSteps)];
		}
		
		/**
		 * Возвращает индекс направления по углу, которому должно соответствовать это направление.
		 * @param	angle Угол направления, индекс которого нужно найти.
		 * @return Индекс направления во внутреннем массиве.
		 */
		[Inline]
		public final function getIndexByAngle (angle:Number):int
		{
			var index:int = Math.round (angle / _angleStep);
			if (index >= _numSteps)
			{
				index = 0;
			}
			return index;
		}
		
		[Inline]
		private final function denyNonInitialized ():void
		{
			if (!_isInit)
			{
				throw new Error ("Not initialized");
			}
		}
		
		[Inline]
		private final function denyIfAsync ():void
		{
			if (_asyncInProgress)
			{
				throw new Error ("Asynchronous preparing is in progress.");
			}
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		public final function get numSteps ():int { return _numSteps; }
		
	}

}