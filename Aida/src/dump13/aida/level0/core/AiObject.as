package dump13.aida.level0.core {
	/**
	 * ...
	 * @author dump13
	 */
	public class AiObject implements IAiNamed {
		
		// ======= CLONEABLE =======
		
		protected var _isUpdatable:Boolean = false;
		protected var _name:String;
		
		// ======= NON-CLONEABLE =======
		
		protected var _location:AiLocation;
		
		/// Нода связного списка для AiLocation::_objects.
		internal var _locObjectsNode:AiListNode;
		internal var _locUpdatableNode:AiListNode;
		/// Нода связного списка для AiLocation::_objectsToRemove. Создается для всех объектов.
		internal var _locObjToRemoveNode:AiListNode;
		/// Нода связного списка для AiLocation::_objectsToAdd. Создается для всех объектов.
		internal var _locObjToAddNode:AiListNode;
		
		/// Вызывать ли onRemoveFromLocation() этого объекта при удалении его из локации. Метод будет вызван перед удалением объекта, в начале AiLocation::removeObject().
		internal var _notifyOnRemove:Boolean;
		
		
		public function AiObject ()
		{
			_locObjectsNode = new AiListNode (this);
			if (_isUpdatable)
			{
				_locUpdatableNode = new AiListNode (this);
			}
			_locObjToRemoveNode = new AiListNode (this);
			_locObjToAddNode = new AiListNode (this);
		}
		
		public function destroy ():void
		{
			if (_location) throw new Error ("Object is in the Location, remove it first.");
			
			_locObjectsNode.destroy ();
			_locObjectsNode = null;
			
			if (_locUpdatableNode)
			{
				_locUpdatableNode.destroy ();
				_locUpdatableNode = null;
			}
			
			_locObjToRemoveNode.destroy ();
			_locObjToRemoveNode = null;
			
			_locObjToAddNode.destroy ();
			_locObjToAddNode = null;
		}
		
		/**
		 * Создает и возвращает копию этого объекта. Копия получает от текущего объекта только клонируемые свойства; служебные свойства, такие как ссылка на локацию, не копируются. Тем не менее, на клонируемом объекте может вызываться, например, метод для добавления вложенного визуала, который будет являться клоном визуала, добавленного в текущий объект.
		 * @return Копия текущего объекта, у которой клонируемые свойства установлены в те же значения, что и у текущего объекта, либо в клоны этих свойств (если свойства является сложным объектом).
		 */
		public function clone ():AiObject
		{
			var object:AiObject = new AiObject ();
			object.clonePropertiesFrom (this);
			return object;
		}
		
		/**
		 * Клонирует из объекта object те свойства, которые являются копируемыми.
		 * @param	object Объект, клонируемые свойства которого необходимо скопировать в текущий объект.
		 */
		protected function clonePropertiesFrom (object:AiObject):void
		{
			isUpdatable = object.isUpdatable;
			name = object.name;
		}
		
		internal function connect (location:AiLocation):void
		{
			_location = location;
		}
		
		internal function shutdown ():void
		{
			_location = null;
		}
		
		/** Метод, обновляющий объект. Запускается при каждом обновлении локации только для тех объектов, у которых <code>isUpdatable == true</code> Метод не принимает временного шага, но может использовать свойства объекта <code>Aida.timer</code>.
		 */
		public function updateDistant ():void
		{
			
		}
		
		/// Вызывается в начале AiLocation::removeObject(), если notifyOnRemove этого объекта установлен в true.
		internal function onRemoveFromLocation ():void
		{
			
		}
		
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		public function get location ():AiLocation { return _location; }
		
		public function get name ():String { return _name; }
		public function set name (value:String):void
		{
			if (_location && _name != null)
			{
				_location.unregisterObjectByName (this);
			}
			_name = value;
			if (_location && _name != null)
			{
				_location.registerObjectByName (this);
			}
		}
		
		public function get isUpdatable ():Boolean { return _isUpdatable; }
		public function set isUpdatable (value:Boolean):void
		{
			if (_isUpdatable == value)
			{
				return;
			}
			_isUpdatable = value;
			
			if (_isUpdatable)
			{
				_locUpdatableNode = new AiListNode (this);
				if (_location)
				{
					_location.addUpdatable (this);
				}
			}
			else
			{
				_location.removeUpdatable (this);
				_locUpdatableNode.destroy ();
				_locUpdatableNode = null;
			}
		}
		
	}

}