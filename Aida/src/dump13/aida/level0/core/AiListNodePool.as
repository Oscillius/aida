package dump13.aida.level0.core {
	/**
	 * Пробный класс для пула, который будет создавать и хранить только ноды связного списка AiListNode.
	 * @author dump13
	 */
	public class AiListNodePool {
		
		private var _nodes:Vector.<AiListNode>;
		/// Индекс, указывающий на последний элемент в пуле.
		private var _curIndex:int = -1;
		
		
		public function AiListNodePool ()
		{
			_nodes = new Vector.<AiListNode> ();
		}
		
		public function destroy ():void
		{
			_nodes.length = 0;
		}
		
		public function disposeNode (node:AiListNode):void
		{
			_nodes[++_curIndex] = node;
		}
		
		public function getNode ():AiListNode
		{
			var node:AiListNode;
			if (_curIndex < 0)
			{
				node = new AiListNode ();
				_nodes[++_curIndex] = node;
				return node;
			}
			
			node = _nodes[_curIndex];
			_nodes[_curIndex] = null;
			_curIndex--;
			
			return node;
		}
		
		/// [Temp]
		public function get vectorLength ():int { return _nodes.length; }
		
	}

}