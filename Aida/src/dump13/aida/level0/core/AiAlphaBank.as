package dump13.aida.level0.core {
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.getTimer;
	import flash.utils.setTimeout;
	/**
	 * Банк с полупрозрачными однотонными изображениями. Используется для "проявления" объектов.
	 * @author dump13
	 */
	public class AiAlphaBank extends EventDispatcher {
		
		/// Вектор с клипами. Нулевой и последний элементы вектора равны null, первый элемент соответствует первому ненулевому значению альфы, последний - наибольшему значению альфы меньше единицы.
		private var _clips:Vector.<AiClip> = new Vector.<AiClip> ();
		
		private var _isCreating:Boolean;
		private var _curStep:int;
		private var _asyncStartTime:int;
		private var _asyncTimeLimit:int = 25;
		
		
		public function AiAlphaBank () {
			
		}
		
		public function destroy ():void
		{
			if (_isCreating)
			{
				throw new Error ("The creation is in progress, can't destroy this object.");
			}
			
			var numClips:int = _clips.length;
			for (var n:int = 0; n < numClips; n++)
			{
				var clip:AiClip = _clips[n];
				if (!clip)
				{
					continue;
				}
				clip.destroy ();
			}
			_clips.length = 0;
		}
		
		/**
		 * Запускает процесс асинхронного создания клипов. По окончании будет разослано событие Event.COMPLETE.
		 */
		public function createAsync ():void
		{
			if (_isCreating)
			{
				throw new Error ("The creation process has been started already.");
			}
			_isCreating = true;
			
			_clips = new Vector.<AiClip>();
			_curStep = 0;
			_asyncStartTime = getTimer ();
			
			createNext ();
		}
		
		private function createNext ():void
		{
			var curAlpha:Number = _curStep * AiSettings.ALPHA_STEP;
			if (!_curStep)
			{
				_clips[_curStep] = null;
			}
			else if (curAlpha >= 1)
			{
				// end here
				_clips[_curStep] = null;
				_isCreating = false;
				dispatchEvent (new Event (Event.COMPLETE));
				return;
			}
			else
			{
				var size:Number = AiSettings.ALPHA_SQUARE_SIZE;
				var uintAlpha:uint = Math.round (curAlpha * 255);
				uintAlpha = (uintAlpha << 24) & 0xff000000;
				var bmd:BitmapData = new BitmapData (size, size, true, uintAlpha);
				_clips[_curStep] = new AiClip (bmd);
			}
			
			// Iterating.
			_curStep++;
			if (getTimer () - _asyncStartTime < _asyncTimeLimit)
			{
				createNext ();
			}
			else
			{
				_asyncStartTime = getTimer ();
				setTimeout (createNext, 1);
			}
		}
		
		/**
		 * Возвращает клип, соответствующий альфе alpha. Метод может вернуть значение null, если alpha == 0 или alpha == 1.
		 * @param	alpha Значение альфы, для которого необходимо найти клип.
		 * @return Клип с заливкой непрозрачности alpha либо null, если alpha == 0 или alpha == 1.
		 */
		public function getClipByAlpha (alpha:Number):AiClip
		{
			var index:int = Math.floor (alpha / AiSettings.ALPHA_STEP);
			return _clips[index];
		}
		
		/**
		 * Возвращает клип с индексом index. Этот индекс можно получить с помощью getIndexByAlpha(). Метод может вернуть значение null, если передан нулевой или последний индекс вектора.
		 * @param	index Индекс, полученный из альфы с помощью getIndexByAlpha().
		 * @return Клип с индексом index (в том числе null, если это нулевой или последний индекс вектора).
		 */
		public function getClipByIndex (index:int):AiClip
		{
			return _clips[index];
		}
		
		/**
		 * Возвращает индекс клипа в векторе _clips, соответствующий прозрачности alpha.
		 * @param	alpha Прозрачность, для которой необходимо найти индекс клипа.
		 * @return Индекс клипа в векторе _clips.
		 */
		public function getIndexByAlpha (alpha:Number):int
		{
			return Math.floor (alpha / AiSettings.ALPHA_STEP);
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		public function get isCreating ():Boolean { return _isCreating; }
		
		/// Число от нуля до единицы, сответствующее прогрессу подготовки клипов.
		public function get progress ():Number
		{
			return _curStep * AiSettings.ALPHA_STEP;
		}
		
	}

}