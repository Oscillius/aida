package dump13.aida.level0.core {
	import dump13.msl.utils.ColorUtils;
	import flash.geom.Point;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiPlayer extends AiBlob {
		
		private static const STROKE_COLOR:uint = ColorUtils.hslToRGB (2 / 3, .5, .15);
		private static const SHADE_COLOR:uint = ColorUtils.hslToRGB (2 / 3, .5, .6);
		private static const COLOR:uint = ColorUtils.hslToRGB (2 / 3, .5, .7);
		private static const GLOSS_COLOR:uint = ColorUtils.hslToRGB (2 / 3, .5, .9);
		
		private static const COLL_MASK:uint = AiCollisionCategory.SCENERY |
											  AiCollisionCategory.BOSS |
											  AiCollisionCategory.ENEMY;
		
		protected var _angle:Number = 0;
		protected var _guns:Vector.<AiGun> = new Vector.<AiGun> ();
		
		protected var _targetX:Number = 0;
		protected var _targetY:Number = 0;
		
		/// Движется ли персонаж вперед.
		private var _isMoving:Boolean;
		/// Ускорение, применяемое при движении.
		private var _acceleration:Number = 120;
		/// Изменение угла направления движения (по отношению к текущему направлению персонажа). При движении вправо использовать Math.PI / 2, при движении влево (-Math.PI / 2).
		private var _movingDeltaAngle:Number = 0;
		/// Влияние скорости игрока на скорость выпускаемых его пушками снарядов.
		public var bulletSpeedInheritanceCoef:Number = 0;
		
		
		public function AiPlayer ()
		{
			collCategory = AiCollisionCategory.PLAYER;
			collMask = COLL_MASK;
			
			isDynamic = true;
			radius = 30;
			_density = 1;
			gliding = .96;
			maxVelocity = 300;
			
			_strokeColor = STROKE_COLOR;
			_shadeColor = SHADE_COLOR;
			_color = COLOR;
			_glossColor = GLOSS_COLOR;
		}
		
		/// Уничтожение игрока приведет к отсоединению всех пушек (в том числе удалению их из локации) и их уничтожению.
		override public function destroy ():void
		{
			while (_guns.length)
			{
				var gun:AiGun = _guns[_guns.length - 1];
				detachGun (gun);
				gun.destroy ();
			}
			
			super.destroy ();
		}
		
		override public function update ():void
		{
			// Rotating with mouse.
			var mousePos:Point = Aida.instance.mouse.getGlobalCoords ();
			_angle = Math.atan2 (mousePos.y - y, mousePos.x - x);
			
			var mouseIsDown:Boolean = Aida.instance.mouse.isDown;
			
			// Moving forward.
			if (_isMoving)
			{
				var deltaTime:Number = Aida.instance.timer.deltaTime;
				var deltaVel:Number = deltaTime * _acceleration;
				var movingAngle:Number = _angle + _movingDeltaAngle;
				velocityX += Math.cos (movingAngle) * deltaVel;
				velocityY += Math.sin (movingAngle) * deltaVel;
			}
			
			super.update ();
			
			// Guns positioning.
			var cos:Number = Math.cos (_angle);
			var sin:Number = Math.sin (_angle);
			var numGuns:int = _guns.length;
			for (var n:int = 0; n < numGuns; n++)
			{
				var gun:AiGun = _guns[n];
				var sumAngle:Number = _angle+gun.slotAngle;
				var dist:Number = gun.slotDistance.getAbsolute (radius);
				//gun.setXY (x + dist * cos, y + dist * sin);
				gun.setXY (x + dist * Math.cos (sumAngle), y + dist * Math.sin (sumAngle));
				
				// Shooting.
				mouseIsDown? gun.startShooting () : gun.stopShooting ();
				
				// Aiming.
				_targetX = mousePos.x;
				_targetY = mousePos.y;
				
				// Updating the guns.
				gun.updateByPlayer ();
			}
			
			//super.update ();
		}
		
		public function move (movingDeltaAngle:Number = NaN):void
		{
			_isMoving = true;
			if (!isNaN (movingDeltaAngle))
			{
				_movingDeltaAngle = movingDeltaAngle;
			}
		}
		
		public function stop ():void
		{
			_isMoving = false;
		}
		
		/// Присоединяет пушку к этому игроку. Добавляет ее в _attachedEntities. Если игрок уже находится в локации, пушка будет добавлена в эту локацию.
		public function attachGun (gun:AiGun):void
		{
			if (gun._owner)
			{
				throw new Error ("Gun is already attached to some owner.");
			}
			gun._owner = this;
			gun.z = z - .1;
			_guns.push (gun);
			
			if (!_attachedEntities)
			{
				_attachedEntities = new Vector.<AiEntity> ();
			}
			_attachedEntities.push (gun);
			if (_location)
			{
				_location.addObject (gun);
			}
			
			// Включаем оповещение от локации об удалении пушки. Если пушка будет удалена из локации без ведома игрока, будет создано исключение.
			gun._notifyOnRemove = true;
		}
		
		/// Отсоединяет пушку от этого игрока. Удаляет ее из _attachedEntities. Если игрок находится в локации, пушка будет из нее удалена.
		public function detachGun (gun:AiGun):AiGun
		{
			if (gun._owner !== this)
			{
				throw new Error ("Gun is not attached to this Player.");
			}
			
			// Выключаем оповещения от локации об удалении пушки (чтобы не создавались исключения).
			gun._notifyOnRemove = false;
			
			var index:int = _attachedEntities.indexOf (gun);
			_attachedEntities.splice (index, 1);
			if (!_attachedEntities.length)
			{
				_attachedEntities = null;
			}
			if (_location)
			{
				_location.removeObject (gun);
				//_location.removeObjectDeffered (gun);
			}
			
			index = _guns.indexOf (gun);
			if (index < 0)
			{
				throw new Error ("Gun not found.");
			}
			_guns.splice (index, 1);
			gun._owner = null;
			
			return gun;
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/// Угол поворота игрока. Он меняется во время обновления и зависит от положения указателя мыши.
		public function get angle ():Number { return _angle; }
		
		/// Координата x точки, в которую будут целиться все пушки (при прицельной стрельбе), в системе координат локации.
		public function get targetX ():Number { return _targetX; }
		
		/// Координата y точки, в которую будут целиться все пушки (при прицельной стрельбе), в системе координат локации.
		public function get targetY ():Number { return _targetY; }
		
		/// Количество присоединенных пушек.
		public function get numGuns ():int { return _guns.length; }
		
		/// Вектор присоединенных пушек. Возвращается по ссылке.
		public function get guns ():Vector.<AiGun> { return _guns; }
		
	}

}