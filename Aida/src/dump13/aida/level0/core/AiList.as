package dump13.aida.level0.core {
	/**
	 * ...
	 * @author dump13
	 */
	public class AiList {
		
		private var _head:AiListNode;
		
		
		public function AiList () {
			
		}
		
		/**
		 * Очищает список, удаляя из него все элементы и при необходимости уничтожая сами элементы.
		 * @param destroyNodes Уничтожать ли удаляемые элементы. Ссылка на object уничтожаемого элемента будет занулена.
		 */
		public function clear (destroyNodes:Boolean = true):void
		{
			var curNode:AiListNode;
			var nextNode:AiListNode;
			
			curNode = _head;
			
			if (!destroyNodes)
			{
				while (curNode)
				{
					nextNode = curNode.next;
					curNode.next = null;
					// curNode.prev = null - не надо, потому что это "голова".
					if (nextNode)
					{
						nextNode.prev = null;
					}
					
					curNode._list = null;
					
					curNode = nextNode;
				}
			}
			else
			{
				while (curNode)
				{
					nextNode = curNode.next;
					curNode.next = null;
					// curNode.prev = null - не надо, потому что это "голова".
					if (nextNode)
					{
						nextNode.prev = null;
					}
					
					curNode._list = null;
					curNode.destroy ();
					
					curNode = nextNode;
				}
			}
			
			_head = null;
		}
		
		/**
		 * Добавляет элемент в начало этого списка, делая его головным. Если элемент уже состоит в этом списке, создается исключение.
		 * @param	node Элемент, который необходимо добавить в список.
		 */
		public function add (node:AiListNode):void
		{
			if (node._list)
			{
				throw new Error ("Node is already added to the List.");
			}
			node._list = this;
			
			if (!_head)
			{
				_head = node;
				node.prev = null;
				node.next = null;
				return;
			}
			
			node.prev = null;
			node.next = _head;
			_head.prev = node;
			_head = node;
		}
		
		/**
		 * Добавляет элемент в такое место списка, чтобы список оставался упорядоченным.
		 * @param	node Элемент, который необходимо добавить в список.
		 * @param	compare Функция вида <code>function compare (node1:AiListNode, node2:AiListNode):Number</code>, которая возвращает одно из следующих значений: 1) число меньше нуля, если node1 должен оказаться в списке перед node2; 2) число больше нуля, если node1 должен оказаться в списке после node2; 3) ноль, если порядок расположения node1 по отношению к node2 неважен.
		 */
		public function addSorted (node:AiListNode, compare:Function):void
		{
			if (node._list)
			{
				throw new Error ("Node is inside some List already.");
			}
			node._list = this;
			
			if (!_head)
			{
				_head = node;
				node.prev = null;
				node.next = null;
				return;
			}
			
			var curNode:AiListNode = _head;
			while (curNode)
			{
				var compareResult:Number = compare (node, curNode);
				
				// Если элемент node можно вставить перед curNode.
				if (compareResult <= 0)
				{
					var prevNode:AiListNode = curNode.prev;
					node.prev = prevNode;
					node.next = curNode;
					if (prevNode)
					{
						prevNode.next = node;
					}
					curNode.prev = node;
					
					if (_head === curNode)
					{
						_head = node;
					}
					
					break;
				}
				
				// Если элемент должен быть вставлен ближе к концу списка.
				
				var nextNode:AiListNode = curNode.next;
				if (!nextNode)
				{
					curNode.next = node;
					node.prev = curNode;
					node.next = null;
					
					break;
				}
				curNode = nextNode;
			}
		}
		
		/**
		 * Удаляет элемент <code>node</code> из этого списка. Свойства <code>prev</code> и <code>next</code> элемента не сбрасываются в <code>null</code>. Если элемент не состоит в этом списке, создается исключение.
		 * @param	node Элемент, который необходимо удалить.
		 */
		public function remove (node:AiListNode):void
		{
			if (node._list !== this)
			{
				throw new Error ("Node is not in this List.");
			}
			node._list = null;
			
			if (node === _head)
			{
				_head = node.next;
				if (_head)
				{
					_head.prev = null;
				}
				//node.next = null;
				//node.prev = null;
				return;
			}
			
			var nodePrev:AiListNode = node.prev;
			var nodeNext:AiListNode = node.next;
			if (nodePrev) nodePrev.next = nodeNext;
			if (nodeNext) nodeNext.prev = nodePrev;
			//node.prev = null;
			//node.next = null;
		}
		
		/**
		 * Добавляет элемент <code>node</code>, если он не содержится в каком-либо списке.
		 * @param	node Элемент, который необходимо добавить.
		 */
		public function addIfPossible (node:AiListNode):void
		{
			if (!node._list) add (node);
		}
		
		/**
		 * Добавляет элемент с помощью метода <code>addSorted()</code>, если он не содержится в каком-либо списке.
		 * @param	node Элемент, который нужно добавить.
		 * @param	compare Функция сравнения (см. addSorted()).
		 */
		public function addSortedIfPossible (node:AiListNode, compare:Function):void
		{
			if (!node._list) addSorted (node, compare);
		}
		
		/**
		 * Удаляет элемент <code>node</code>, если он содержится в этом списке.
		 * @param	node Элемент, который необходимо удалить.
		 */
		public function removeIfPossible (node:AiListNode):void
		{
			if (node._list === this) remove (node);
		}
		
		/**
		 * Возвращает <code>true</code>, если этот список содержит элемент <code>node</code> (метод проверяет свойство <code>AiListNode::_list</code>).
		 * @param	node Элемент, наличие которого в списке необходимо проверить.
		 * @return Значение <code>true</code>, если элемент содержится в списке, иначе <code>false</code>.
		 */
		public function has (node:AiListNode):Boolean
		{
			return node._list === this;
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/**
		 * Головной (начальный) элемент этого связного списка.
		 */
		public function get head ():AiListNode { return _head; }
		
	}

}