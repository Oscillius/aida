package dump13.aida.level0.core 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Rectangle;
	/**
	 * Визуал молнии. Принадлежит экземпляру молнии, но отрисовывается по владельцу той молнии.
	 * @author dump13
	 */
	public class AiLightningVisual extends AiVisual
	{
		private var _sprite:Sprite = new Sprite ();
		private var _matrix:Matrix = new Matrix ();
		
		/// Шаг искры - длина одного ее маленького отрезка.
		public var sparkStep:Number = 5;
		/// Максимальное смещение, применяемое к координатам концов маленьких отрезков искры.
		public var sparkOffset:Number = 10;
		
		
		public function AiLightningVisual () 
		{
			super ();
			// ...
		}
		
		override public function clone ():AiVisual
		{
			var visual:AiLightningVisual = new AiLightningVisual ();
			visual.clonePropertiesFrom (this);
			return visual;
		}
		
		override protected function clonePropertiesFrom (visual:AiVisual):void
		{
			super.clonePropertiesFrom (visual);
			
			var v:AiLightningVisual = visual as AiLightningVisual;
			sparkStep = v.sparkStep;
			sparkOffset = v.sparkOffset;
		}
		
		override internal function connectToEntity (entity:AiEntity):void
		{
			if (!(entity is AiLightning))
			{
				throw new Error ("Lightning Visual may be added only to a Lightning (including subclasses).");
			}
			super.connectToEntity (entity);
		}
		
		override public function redraw ():void
		{
			// Does nothing.
		}
		
		override public function render (camera:AiCamera):void
		{
			if (_entity && _entity.location)
			{
				var lightning:AiLightning = _entity as AiLightning;
				var lightningX:Number = lightning.x;
				var lightningY:Number = lightning.y;
				var owner:AiBlob = lightning.owner;
				var ownerRadius:Number = owner.radius;
				var cx:Number = camera.screenHalfWidth + owner.x - camera.xRound;
				var cy:Number = camera.screenHalfHeight + owner.y - camera.yRound;
				
				var g:Graphics = _sprite.graphics;
				g.clear ();
				g.lineStyle (3, 0x33eeee, .7);
				
				var targets:Vector.<AiBlob> = lightning._targets;
				var distances:Vector.<Number> = lightning._distances;
				var numSparks:int = lightning.numSparks;
				for (var n:int = 0; n < numSparks; n++)
				{
					var target:AiBlob = targets[n];
					if (!target)
					{
						break;
					}
					/// Расстояние между центром молнии и центром цели.
					var dist:Number = distances[n];
					var dx:Number = target.x - lightningX;
					var dy:Number = target.y - lightningY;
					// Орт направления к цели.
					var dirX:Number = dx / dist;
					var dirY:Number = dy / dist;
					
					// Расстояние от центра молнии до поверхности цели.
					var distToTargetSurf:Number = dist - target.radius;
					
					// Длина искры - расстояние от поверхности владельца до поверхности цели.
					var sparkLen:Number = dist - target.radius - ownerRadius;
					
					var numSteps:int = Math.floor (sparkLen / sparkStep) - 1;
					var stepDX:Number = dirX * sparkStep;
					var stepDY:Number = dirY * sparkStep;
					var curX:Number = dirX * ownerRadius;
					var curY:Number = dirY * ownerRadius;
					g.moveTo (curX, curY);
					for (var m:int = 0; m < numSteps; m++)
					{
						curX += stepDX;
						curY += stepDY;
						g.lineTo (curX + (Math.random () - .5) * sparkOffset, curY + (Math.random () - .5) * sparkOffset);
					}
					g.lineTo (dirX * distToTargetSurf, dirY * distToTargetSurf);
					
					//g.moveTo (dirX * ownerRadius, dirY * ownerRadius);
					//g.lineTo (dirX * distToTargetSurf, dirY * distToTargetSurf);
				}
				
				//_matrix.setTo (1, 0, 0, 1, cx, cy);
				_matrix.tx = cx;
				_matrix.ty = cy;
				camera.buffer.draw (_sprite, _matrix);
				// ...
			}
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		override public function get rect ():Rectangle
		{
			var lightning:AiLightning = _entity as AiLightning;
			if (!lightning)
			{
				return null;
			}
			var blob:AiBlob = lightning.owner;
			if (!blob)
			{
				return null;
			}
			var radius:Number = blob.radius + lightning.ownRadius;
			var diam:Number = radius + radius;
			return new Rectangle (blob.x - radius, blob.y - radius, diam, diam);
		}
		
		/**
		 * Это _entity, приведенный к AiLightning.
		 */
		[Inline]
		public final function get lightning ():AiLightning { return _entity as AiLightning; }
		
	}

}