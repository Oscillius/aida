package dump13.aida.level0.core {
	import flash.display.BitmapData;
	import flash.geom.Rectangle;
	/**
	 * Объект, хранящий инфу об изображении и его локальном центре.
	 * @author dump13
	 */
	public class AiClip {
		
		public var bitmapData:BitmapData;
		/// Координата x точки, заданной в локальных координатах битмапдаты, представляющей центр визуала. Обычно больше нуля.
		public var centerX:Number = 0;
		/// Координата y точки, заданной в локальных координатах битмапдаты, представляющей центр визуала. Обычно больше нуля.
		public var centerY:Number = 0;
		/// bitmapData.rect.
		public var rect:Rectangle;
		
		/**
		 * Конструктор клипа.
		 * @param	bitmapData Изображение клипа. Уничтожается вместе с клипом.
		 * @param	centerX Координата x центра (обычно положительное число). Если передано NaN, будет установлено в половину ширины изображения bitmapData.
		 * @param	centerY Координата y центра (обычно положительное число). Если передано NaN, будет установлено в половину высоты изображения bitmapData.
		 */
		public function AiClip (bitmapData:BitmapData, centerX:Number = NaN, centerY:Number = NaN)
		{
			this.bitmapData = bitmapData;
			this.centerX = !isNaN (centerX)? centerX : bitmapData.width * .5;
			this.centerY = !isNaN (centerY)? centerY : bitmapData.height * .5;
			
			rect = bitmapData.rect;
		}
		
		public function destroy ():void
		{
			bitmapData.dispose ();
			bitmapData = null;
			rect = null;
		}
		
	}

}