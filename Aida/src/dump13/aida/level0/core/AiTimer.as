package dump13.aida.level0.core {
	import flash.utils.getTimer;
	
	/**
	 * Глобальный таймер - помощник для обновления локации и объектов.
	 * @author dump13
	 */
	public class AiTimer {
		
		/// Максимальный временной шаг обновления.
		//public static const MAX_TIME_STEP:Number = 1 / 15;
		public static const MAX_TIME_STEP:Number = 1 / 30;
		
		/// Текущая итерация обновления.
		public var iteration:int = 0;
		/// Время последнего обновления в секундах.
		public var prevTime:Number = 0;
		/// Текущее время в секундах.
		public var curTime:Number = 0;
		/// Текущий шаг обновления в секундах. Максимальное значение, которое принимает это свойство, определяется значением AiTimer::MAX_TIME_STEP.
		public var deltaTime:Number = 0;
		/// Текущий шаг обновления (в секундах), возведенный в квадрат.
		public var deltaTimeSquared:Number = 0;
		
		
		public function AiTimer () {
			init ();
		}
		
		private function init ():void
		{
			iteration = 0;
			prevTime = getTimer () * 1e-3;
			curTime = prevTime;
		}
		
		/// Вызывать в начале обновления локации. Метод увеличивает текущую итерацию, устанавливает текущее время и шаг обновления.
		public function preUpdate ():void
		{
			iteration++;
			curTime = getTimer () * 1e-3;
			deltaTime = curTime - prevTime;
			if (deltaTime > MAX_TIME_STEP)
			{
				deltaTime = MAX_TIME_STEP;
			}
			
			// Constant time step (comment if unneeded).
			deltaTime = MAX_TIME_STEP;
			
			deltaTimeSquared = deltaTime * deltaTime;
		}
		
		/// Вызывать в конце обновления локации. Метод устанавливает время последнего обновления в значение текущего времени.
		public function postUpdate ():void
		{
			prevTime = curTime;
		}
		
	}

}