package dump13.aida.level0.core 
{
	import dump13.msl.types.Set;
	import flash.display.Graphics;
	import flash.geom.Rectangle;
	/**
	 * Это дерево квадрантов не будет поддерживать перерегистрацию объектов. Это значит, что при изменении координат сущности дерево не будет обновляться; вместо этого оно будет перестраиваться при каждом обновлении локации (так как на данный момент практически все динамические объекты имеют useVelocity = true и поэтому меняют свое положение). Смещение объекта при расчете столкновений не приведет к перерегистрации его в дереве, т.е. все объекты на момент расчета столкновений будут находиться в одном и том же месте; это может привести даже к ускорению расчета столкновений за счет отмены перерегистрации.
	 * @author dump13
	 */
	public class AiQuadTreeNode 
	{
		//private var _items:Set = new Set ();
		private var _items:Vector.<IAiBounded> = new Vector.<IAiBounded>();
		
		/// Прямоугольник, представляющий положение и размеры этой ноды.
		private var _rect:Rectangle;
		/// Половина размера этой ноды.
		private var _halfSize:Number = 0;
		/// Глубина этой ноды - ее уровень вложенности. Глубина может быть отрицательной.
		private var _depth:int;
		
		private var _rectTL:Rectangle;
		private var _rectTR:Rectangle;
		private var _rectBL:Rectangle;
		private var _rectBR:Rectangle;
		
		internal var _nodeTL:AiQuadTreeNode;
		internal var _nodeTR:AiQuadTreeNode;
		internal var _nodeBL:AiQuadTreeNode;
		internal var _nodeBR:AiQuadTreeNode;
		
		
		public function AiQuadTreeNode (x:Number, y:Number, size:Number, depth:int = 0) 
		{
			var hs:Number = size * .5;
			
			_rect = new Rectangle (x, y, size, size);
			_halfSize = hs;
			_depth = depth;
			
			_rectTL = new Rectangle (x, y, hs, hs);
			_rectTR = new Rectangle (x + hs, y, hs, hs);
			_rectBL = new Rectangle (x, y + hs, hs, hs);
			_rectBR = new Rectangle (x + hs, y + hs, hs, hs);
		}
		
		public static function createFromRect (rect:Rectangle, depth:int):AiQuadTreeNode
		{
			return new AiQuadTreeNode (rect.x, rect.y, rect.width, depth);
		}
		
		public function destroy ():void
		{
			if (_nodeTL) _nodeTL.destroy ();
			if (_nodeTR) _nodeTR.destroy ();
			if (_nodeBL) _nodeBL.destroy ();
			if (_nodeBR) _nodeBR.destroy ();
			_nodeTL = null;
			_nodeTR = null;
			_nodeBL = null;
			_nodeBR = null;
			
			//_items.clear ();
			_items.length = 0;
		}
		
		internal function addItem (item:IAiBounded, tree:AiQuadTree):Boolean
		{
			var itemRect:Rectangle = item.rect;
			
			// Если объект больше, чем текущая нода, пытаемся создать родительскую ноду.
			//if (!_rect.containsRect (itemRect))
			if (tree.rootNode===this && !_rect.containsRect (itemRect))
			{
				// Если достигнута минимальная глубина, создаем исключение.
				var prevDepth:int = _depth - 1;
				if (prevDepth < tree._minDepth)
				{
					throw new Error ("Minimum depth reached, the item cannot be placed in this tree.");
				}
				// Если эта нода не является корневой, то ситуации, требующей создания родительской ноды, возникать не должно.
				if (tree._rootNode !== this)
				{
					throw new Error ("Trying to create parent node from a non-root node.");
				}
				
				// Создаем родительскую ноду.
				var parent:AiQuadTreeNode;
				var size:Number = _rect.width;
				if (itemRect.x < _rect.x)
				{
					// To bottom right.
					if (itemRect.y < _rect.y)
					{
						parent = new AiQuadTreeNode (_rect.x - size, _rect.y - size, size + size, prevDepth);
						parent._nodeBR = this;
						//trace ("BR: " + item.rect.toString () + ", node: " + _rect.toString ());
					}
					// To top right.
					else
					{
						parent = new AiQuadTreeNode (_rect.x - size, _rect.y, size + size, prevDepth);
						parent._nodeTR = this;
						//trace ("TR: " + item.rect.toString () + ", node: " + _rect.toString ());
					}
				}
				else
				{
					// To bottom left.
					if (itemRect.y < _rect.y)
					{
						parent = new AiQuadTreeNode (_rect.x, _rect.y - size, size + size, prevDepth);
						parent._nodeBL = this;
						//trace ("BL: " + item.rect.toString () + ", node: " + _rect.toString ());
					}
					// To top left.
					else
					{
						parent = new AiQuadTreeNode (_rect.x, _rect.y, size + size, prevDepth);
						parent._nodeTL = this;
						//trace ("TL: " + item.rect.toString () + ", node: " + _rect.toString ());
					}
				}
				tree._rootNode = parent;
				
				// Теперь родительский объект является корневым, и мы можем рекурсивно поместить объект в него. (Если что, ошибка будет кинута из-под родительской ноды, здесь уже не проверяем.)
				parent.addItem (item, tree);
				//if (!parent.addItem (item, tree))
				//{
					//throw new Error ();
				//}
				
				return true;
			}
			
			// Помещаем объект в эту ноду или в одну из дочерних нод.
			
			// Если достигнута максимальная глубина деления дерева, добавляем айтем к этой ноде.
			var nextDepth:int = _depth + 1;
			if (nextDepth > tree._maxDepth)
			{
				_items.push (item);
				
				//if (!_items.add (item))
				//{
					//throw new Error ("The item is already in this node.");
				//}
				
				return true;
			}
			
			// Если ноду еще можно разделить на части, пытаемся добавить айтем к одной из дочерних нод.
			if (_rectTL.containsRect (itemRect))
			{
				if (!_nodeTL)
				{
					_nodeTL = AiQuadTreeNode.createFromRect (_rectTL, nextDepth);
				}
				if (_nodeTL.addItem (item, tree))
				{
					return true;
				}
			}
			if (_rectTR.containsRect (itemRect))
			{
				if (!_nodeTR)
				{
					_nodeTR = AiQuadTreeNode.createFromRect (_rectTR, nextDepth);
				}
				if (_nodeTR.addItem (item, tree))
				{
					return true;
				}
			}
			if (_rectBL.containsRect (itemRect))
			{
				if (!_nodeBL)
				{
					_nodeBL = AiQuadTreeNode.createFromRect (_rectBL, nextDepth);
				}
				if (_nodeBL.addItem (item, tree))
				{
					return true;
				}
			}
			if (_rectBR.containsRect (itemRect))
			{
				if (!_nodeBR)
				{
					_nodeBR = AiQuadTreeNode.createFromRect (_rectBR, nextDepth);
				}
				if (_nodeBR.addItem (item, tree))
				{
					return true;
				}
			}
			
			// Если ни в одну из дочерних нод айтем не помещается, добавляем его к этой ноде.
			_items.push (item);
			
			//if (!_items.add (item))
			//{
				//throw new Error ("The item is already in this node.");
			//}
			
			return true;
			
			
			// Иначе объект нельзя поместить в эту ноду.
			return false;
		}
		
		public function getItemsInRect (rect:Rectangle, result:Vector.<IAiBounded>):void
		{
			var i:Object;
			
			if (!_rect.intersects (rect))
			{
				return
			}
			
			//if (!result)
			//{
				//result = new Vector.<IAiBounded> ();
			//}
			
			// Проверяем айтемы, зарегистрированные в этой ноде.
			var numItems:int = _items.length;
			for (var n:int = 0; n < numItems; n++)
			{
				var bounded:IAiBounded = _items[n];
				var itemRect:Rectangle = bounded.rect;
				if (rect.intersects (itemRect))
				{
					result.push (bounded);
				}
			}
			
			//for (i in _items)
			//{
				//var bounded:IAiBounded = i as IAiBounded;
				//var itemRect:Rectangle = bounded.rect;
				//if (rect.intersects (itemRect))
				//{
					//result.push (bounded);
				//}
			//}
			
			// Ищу айтемы из дочерних нод.
			if (_nodeTL)
			{
				_nodeTL.getItemsInRect (rect, result);
			}
			if (_nodeTR)
			{
				_nodeTR.getItemsInRect (rect, result);
			}
			if (_nodeBL)
			{
				_nodeBL.getItemsInRect (rect, result);
			}
			if (_nodeBR)
			{
				_nodeBR.getItemsInRect (rect, result);
			}
			
			return;
		}
		
		internal function debugDraw (g:Graphics):void
		{
			g.drawRect (_rect.x, _rect.y, _rect.width, _rect.height);
			
			if (_nodeTL) _nodeTL.debugDraw (g);
			if (_nodeTR) _nodeTR.debugDraw (g);
			if (_nodeBL) _nodeBL.debugDraw (g);
			if (_nodeBR) _nodeBR.debugDraw (g);
		}
		
		public function get rect ():Rectangle { return _rect; }
		
		public function get depth ():int { return _depth; }
		
	}

}