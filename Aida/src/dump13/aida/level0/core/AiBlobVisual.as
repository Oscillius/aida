package dump13.aida.level0.core {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Matrix;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiBlobVisual extends AiVisual {
		
		private var _colorFlatClip:AiClip;
		private var _strokeFlatClip:AiClip;
		private var _shadeFlatClip:AiClip;
		private var _glossFlatClip:AiClip;
		private var _pointHelper:Point = new Point ();
		
		public static var numAppearing:int;
		
		
		public function AiBlobVisual () {
			
		}
		
		override public function clone ():AiVisual
		{
			var visual:AiBlobVisual = new AiBlobVisual ();
			visual.clonePropertiesFrom (this);
			return visual;
		}
		
		// Nothing to copy.
		//override protected function clonePropertiesFrom (visual:AiVisual):void
		//{
			//super.clonePropertiesFrom (visual);
		//}
		
		override internal function connectToEntity (entity:AiEntity):void {
			if (!(entity is AiBlob))
			{
				throw new Error ("BlobVisual may be added only to a Blob owner (including subclasses).");
			}
			super.connectToEntity (entity);
		}
		
		override public function redraw ():void
		{
			var blob:AiBlob = _entity as AiBlob;
			if (!blob)
			{
				return;
			}
			
			var flatBank:AiFlatBank = Aida.instance.flatBank;
			_colorFlatClip = flatBank.provideClipByColor (blob.color);
			_strokeFlatClip = flatBank.provideClipByColor (blob.strokeColor);
			_shadeFlatClip = flatBank.provideClipByColor (blob.shadeColor);
			_glossFlatClip = flatBank.provideClipByColor (blob.glossColor);
		}
		
		override public function render (camera:AiCamera):void {
			if (_entity && _entity.location)
			{
				var aida:Aida = Aida.instance;
				var cx:Number = camera.screenHalfWidth + _entity.x - camera.xRound;
				var cy:Number = camera.screenHalfHeight + _entity.y - camera.yRound;
				var blob:AiBlob = AiBlob (_entity);
				var radius:Number = blob.radius;
				if (radius >= AiSettings.CIRCLE_MAX_RADIUS - 1)
				{
					radius = AiSettings.CIRCLE_MAX_RADIUS - 1;
				}
				var circleBank:AiCircleBank = aida.circleBank;
				
				var clip:AiClip;
				
				if (!blob.isAppearing)
				//if (true)
				{
					clip = circleBank.getClipByRadius (radius + 1);
					_pointHelper.x = cx - clip.centerX;
					_pointHelper.y = cy - clip.centerY;
					camera.buffer.copyPixels (_strokeFlatClip.bitmapData, clip.rect, _pointHelper, clip.bitmapData, null, true);
					
					clip = circleBank.getClipByRadius (radius);
					_pointHelper.x = cx - clip.centerX;
					_pointHelper.y = cy - clip.centerY;
					camera.buffer.copyPixels (_shadeFlatClip.bitmapData, clip.rect, _pointHelper, clip.bitmapData, null, true);
					
					clip = circleBank.getClipByRadius (radius * .8);
					_pointHelper.x = cx + radius * .1 - clip.centerX;
					_pointHelper.y = cy - radius * .1 - clip.centerY;
					camera.buffer.copyPixels (_colorFlatClip.bitmapData, clip.rect, _pointHelper, clip.bitmapData, null, true);
					
					clip = circleBank.getClipByRadius (radius * .3);
					_pointHelper.x = cx + radius * .4 - clip.centerX;
					_pointHelper.y = cy - radius * .4 - clip.centerY;
					camera.buffer.copyPixels (_glossFlatClip.bitmapData, clip.rect, _pointHelper, clip.bitmapData, null, true);
				}
				else
				{
					numAppearing++;
					
					var alphaIndex:int = aida.alphaBank.getIndexByAlpha (1 - blob.timeToAppearing / blob.appearTime);
					if (!alphaIndex)
					{
						return;
					}
					var alphaClip:AiClip = aida.alphaBank.getClipByIndex (alphaIndex);
					if (!alphaClip)
					{
						throw new Error ("Blob is appearing but its alpha equals to 1 (blob's internal error).");
					}
					
					var preComp:AiClip = aida.preComp;
					var pcX:Number = preComp.centerX;
					var pcY:Number = preComp.centerY;
					
					clip = circleBank.getClipByRadius (radius + 1);
					_pointHelper.x = pcX - clip.centerX;
					_pointHelper.y = pcY - clip.centerY;
					preComp.bitmapData.copyPixels (_strokeFlatClip.bitmapData, clip.rect, _pointHelper, clip.bitmapData, null, true);
					
					clip = circleBank.getClipByRadius (radius);
					_pointHelper.x = pcX - clip.centerX;
					_pointHelper.y = pcY - clip.centerY;
					preComp.bitmapData.copyPixels (_shadeFlatClip.bitmapData, clip.rect, _pointHelper, clip.bitmapData, null, true);
					
					clip = circleBank.getClipByRadius (radius * .8);
					_pointHelper.x = pcX + radius * .1 - clip.centerX;
					_pointHelper.y = pcY - radius * .1 - clip.centerY;
					preComp.bitmapData.copyPixels (_colorFlatClip.bitmapData, clip.rect, _pointHelper, clip.bitmapData, null, true);
					
					clip = circleBank.getClipByRadius (radius * .3);
					_pointHelper.x = pcX + radius * .4 - clip.centerX;
					_pointHelper.y = pcY - radius * .4 - clip.centerY;
					preComp.bitmapData.copyPixels (_glossFlatClip.bitmapData, clip.rect, _pointHelper, clip.bitmapData, null, true);
					
					_pointHelper.x = cx - preComp.centerX;
					_pointHelper.y = cy - preComp.centerY;
					camera.buffer.copyPixels (preComp.bitmapData, preComp.bitmapData.rect, _pointHelper, alphaClip.bitmapData, null, true);
					
					preComp.bitmapData.fillRect (preComp.bitmapData.rect, 0x00000000);
				}
			}
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		override public function get rect ():Rectangle {
			var blob:AiBlob = _entity as AiBlob;
			if (blob)
			{
				var radius:Number = blob.radius;
				var diam:Number = radius + radius;
				return new Rectangle (blob.x - radius, blob.y - radius, diam, diam);
			}
			return null;
		}
		
	}

}