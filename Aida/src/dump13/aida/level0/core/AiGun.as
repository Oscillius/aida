package dump13.aida.level0.core {
	import dump13.msl.utils.ColorUtils;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiGun extends AiBlob {
		
		private static const STROKE_COLOR:uint = ColorUtils.hslToRGB (0 / 3, .5, .15);
		private static const SHADE_COLOR:uint = ColorUtils.hslToRGB (0 / 3, .5, .6);
		private static const COLOR:uint = ColorUtils.hslToRGB (0 / 3, .5, .7);
		private static const GLOSS_COLOR:uint = ColorUtils.hslToRGB (0 / 3, .5, .9);
		
		// Non-cloneable.
		internal var _owner:AiPlayer;
		
		// ======= CLONEABLE =======
		
		protected var _shotTime:Number = .2;
		protected var _isShooting:Boolean;
		protected var _timeToNextShot:Number = 0;
		protected var _shotIsReady:Boolean = true;
		
		protected var _slotDistance:AiLength = new AiLength (1, AiLength.RELATIVE);
		protected var _slotAngle:Number = 0;
		
		protected var _minRadius:Number = 3;
		protected var _maxRadius:Number = 6;
		
		/// Начальная скорость выпускаемого снаряда.
		protected var _bulletVelocity:Number = 150;
		protected var _bulletPrototype:AiBullet;
		
		
		public function AiGun ()
		{
			collCategory = AiCollisionCategory.DECOR;
			collMask = 0x00000000;
			
			isDynamic = true;
			
			_strokeColor = STROKE_COLOR;
			_shadeColor = SHADE_COLOR;
			_color = COLOR;
			_glossColor = GLOSS_COLOR;
			
			radius = 5;
			
			_bulletPrototype = new AiBullet ();
			_bulletPrototype.radius = 4;
			_bulletPrototype.shrinkSpeed = 1;
			_bulletPrototype.damageSpecs = new AiDamageSpecs (0, 10);
		}
		
		override public function clone ():AiObject
		{
			var gun:AiGun = new AiGun ();
			gun.clonePropertiesFrom (this);
			return gun;
		}
		
		override protected function clonePropertiesFrom (object:AiObject):void
		{
			super.clonePropertiesFrom (object);
			
			var gun:AiGun = AiGun (object);
			_shotTime = gun._shotTime;
			_isShooting = gun._isShooting;
			_timeToNextShot = gun._timeToNextShot;
			_shotIsReady = gun._shotIsReady;
			_slotDistance = gun._slotDistance.clone ();
			_slotAngle = gun._slotAngle;
			minRadius = gun._minRadius;
			maxRadius = gun._maxRadius;
			_bulletVelocity = gun._bulletVelocity;
		}
		
		/// При удалении пушки из локации без ведома игрока будет создаваться исключение. Единственный правильный способ удалить присоединенную к игроку пушку - попросить об этом игрока. Этот метод вызывается только для присоедиеннных пушек, у которых _notifyOnRemove установлен в true игроком.
		override internal function onRemoveFromLocation ():void
		{
			throw new Error ("Removing the gun without AiPlayer's permission.");
		}
		
		/// Такого происходить не должно, но оставлю пока этот метод. Предполагается, что пушка будет уничтожаться только по запросу владельца.
		override protected function onDead ():void 
		{
			if (_owner)
			{
				_owner.detachGun (this);
			}
			super.onDead ();
		}
		
		public function startShooting ():void
		{
			_isShooting = true;
		}
		
		public function stopShooting ():void
		{
			_isShooting = false;
		}
		
		/// Введение этого метода позволит гарантированно обновлять пушки после обновления блоба игрока.
		public function updateByPlayer ():void
		{
			if (!_shotIsReady)
			{
				_timeToNextShot -= Aida.instance.timer.deltaTime;
				if (_timeToNextShot <= 0)
				{
					_timeToNextShot = 0;
					_shotIsReady = true;
				}
				radius = _minRadius + (_maxRadius - _minRadius) * (1 - _timeToNextShot / _shotTime);
			}
			
			if (_isShooting)
			{
				if (_shotIsReady)
				{
					_shotIsReady = false;
					_timeToNextShot = _shotTime;
					
					// Make a shot here.
					var localX:Number = _x - _owner.x;
					var localY:Number = _y - _owner.y;
					var dx:Number = _owner.targetX - _x;
					var dy:Number = _owner.targetY - _y;
					// Если угол между отрезками "игрок - пушка" и "пушка - цель" является острым, то стреляем прицельно.
					if (localX * dx + localY * dy > 0)
					{
						var dist:Number = Math.sqrt (dx * dx + dy * dy);
						if (!dist)
						{
							dist = 1e-3;
						}
						var velMult:Number = _bulletVelocity / dist;
						//var velX:Number = dx * velMult + _owner.velocityX;
						//var velY:Number = dy * velMult + _owner.velocityY;
						var velX:Number = dx * velMult;
						var velY:Number = dy * velMult;
					}
					// Если угол между отрезками "игрок - пушка" и "пушка - цель" не является острым, то стреляем неприцельно.
					else
					{
						//dist = Math.sqrt (localX * localX + localY * localY);
						dist = _slotDistance.getAbsolute (_owner.radius);
						if (!dist)
						{
							dist = 1e-3;
						}
						velMult = _bulletVelocity / dist;
						velX = localX * velMult;
						velY = localY * velMult;
					}
					
					// Наследование скорости игрока.
					var inheritanceCoef:Number = _owner.bulletSpeedInheritanceCoef;
					if (inheritanceCoef)
					{
						if (inheritanceCoef)
						{
							velX += _owner.velocityX;
							velY += _owner.velocityY;
						}
						else
						{
							velX += _owner.velocityX * inheritanceCoef;
							velY += _owner.velocityY * inheritanceCoef;
						}
					}
					
					//var dx:Number = _owner.targetX - _x;
					//var dy:Number = _owner.targetY - _y;
					//var dist:Number = Math.sqrt (dx * dx + dy * dy);
					//if (!dist)
					//{
						//dist = 1e-3;
					//}
					//var velMult:Number = _bulletVelocity / dist;
					//var velX:Number = dx * velMult + _owner.velocityX;
					//var velY:Number = dy * velMult + _owner.velocityY;
					
					//var bullet:AiBullet = new AiBullet ();
					var bullet:AiBullet = _bulletPrototype.clone () as AiBullet;
					bullet.setXY (_x, _y);
					bullet.velocityX = velX;
					bullet.velocityY = velY;
					bullet.makeAppearing (.2);
					_location.addObjectDeferred (bullet);
				}
			}
		}
		
		private function updateRadius ():void
		{
			radius = _minRadius + (_maxRadius - _minRadius) * (1 - _timeToNextShot / _shotTime);
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/// Относительное расстояние от центра игрока до этой пушки. Возвращается по ссылке.
		public function get slotDistance ():AiLength { return _slotDistance; }
		
		/// Угол между осью X игрока и отрезком между этой пушкой и центром игрока.
		public function get slotAngle ():Number { return _slotAngle; }
		public function set slotAngle (value:Number):void
		{
			_slotAngle = value;
		}
		
		public function get minRadius ():Number { return _minRadius; }
		public function set minRadius (value:Number):void
		{
			_minRadius = value;
			updateRadius ();
		}
		
		public function get maxRadius ():Number { return _maxRadius; }
		public function set maxRadius (value:Number):void
		{
			_maxRadius = value;
			updateRadius ();
		}
		
	}

}