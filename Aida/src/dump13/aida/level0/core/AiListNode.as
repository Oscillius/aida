package dump13.aida.level0.core {
	/**
	 * ...
	 * @author dump13
	 */
	public class AiListNode {
		
		public var object:Object;
		public var prev:AiListNode;
		public var next:AiListNode;
		
		internal var _list:AiList;
		
		
		public function AiListNode (object:Object = null) {
			this.object = object;
		}
		
		public function destroy ():void
		{
			if (_list)
			{
				throw new Error ("Node is in the List, remove it first.");
			}
			object = null;
			prev = null;
			next = null;
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/**
		 * Связный список, в котором состоит этот элемент.
		 */
		public function get list ():AiList { return _list; }
		
	}

}