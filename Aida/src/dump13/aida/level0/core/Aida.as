package dump13.aida.level0.core {
	import flash.display.Bitmap;
	import flash.display.BitmapData;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	/**
	 * 
	 * @author dump13
	 */
	public class Aida {
		
		public static var instance:Aida;
		
		public var host:Sprite;
		public var stage:Stage;
		
		public var width:int;
		public var height:int;
		
		public var camera:AiCamera;
		private var _screen:Bitmap;
		public var timer:AiTimer;
		public var mouse:AiMouse;
		
		public var circleBank:AiCircleBank;
		public var flatBank:AiFlatBank;
		public var alphaBank:AiAlphaBank;
		/// Клип, используемый для прекомпозиции визуалов блобов. Его размер зависит от AiSettings::CIRCLE_MAX_RADIUS. Предполагается, что битмапдата клипа перед началом отрисовки визуала является чистой (полностью прозрачной).
		public var preComp:AiClip;
		public var directions:AiDirections;
		
		private var _onComplete:Function;
		
		
		public function Aida (host:Sprite)
		{
			this.host = host;
			stage = host.stage;
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;
			
			width = stage.stageWidth;
			height = stage.stageHeight;
			
			camera = new AiCamera (stage.stageWidth, stage.stageHeight);
			_screen = camera.createScreen ();
			host.addChild (_screen);
			
			var preCompHalfSize:int = AiSettings.CIRCLE_MAX_RADIUS;
			var preCompSize:int = preCompHalfSize + preCompHalfSize;
			var preCompBMD:BitmapData = new BitmapData (preCompSize, preCompSize, true, 0x00000000);
			preComp = new AiClip (preCompBMD, preCompHalfSize, preCompHalfSize);
			
			Aida.instance = this;
		}
		
		/**
		 * Запускает процесс асинхронной подготовки ресурсов (синхронное создание банков).
		 * @param	onComplete Метод, который будет вызван по завершении подготовки. Если передано значение null, создается исключение.
		 */
		public function prepare (onComplete:Function):void
		{
			if (!Boolean (onComplete))
			{
				throw new Error ("The 'onComplete' parameter should not be null.");
			}
			_onComplete = onComplete;
			circleBank = new AiCircleBank ();
			circleBank.addEventListener (Event.COMPLETE, onCircleBankComplete);
			circleBank.createAsync ();
		}
		
		private function onCircleBankComplete (e:Event):void
		{
			circleBank.removeEventListener (Event.COMPLETE, onCircleBankComplete);
			
			alphaBank = new AiAlphaBank ();
			alphaBank.addEventListener (Event.COMPLETE, onAlphaBankComplete);
			alphaBank.createAsync ();
		}
		
		private function onAlphaBankComplete (e:Event):void
		{
			alphaBank.removeEventListener (Event.COMPLETE, onAlphaBankComplete);
			
			directions = new AiDirections (AiSettings.NUM_DIRECTIONS);
			directions.addEventListener (Event.COMPLETE, onDirectionsComplete);
			directions.prepareAsync ();
		}
		
		private function onDirectionsComplete (e:Event):void
		{
			directions.removeEventListener (Event.COMPLETE, onDirectionsComplete);
			
			flatBank = new AiFlatBank ();
			timer = new AiTimer ();
			mouse = new AiMouse ();
			
			_onComplete ();
		}
		
		// =======================================================
		// ======================== UTILS ========================
		// =======================================================
		
		[Inline]
		public static function clampAngle (angle:Number):Number
		{
			return Math.atan2 (Math.sin (angle), Math.cos (angle));
		}
		
	}

}