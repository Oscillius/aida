package dump13.aida.level0.core {
	/**
	 * Объект длины. Длина может быть абсолютной и относительной. У абсолютной длины свойство value представляет само значение длины, у относительной длины свойство value определяет долю от номинальной длины.
	 * @author dump13
	 */
	public class AiLength {
		
		public static const ABSOLUTE:int = 0;
		public static const RELATIVE:int = 1;
		
		/// Величина длины.
		public var value:Number = 0;
		/// Тип объекта длины - константа этого класса.
		public var type:int = ABSOLUTE;
		
		
		public function AiLength (value:Number, type:int = -1)
		{
			this.value = value;
			this.type = type < 0? ABSOLUTE : type;
		}
		
		public function clone ():AiLength
		{
			return new AiLength (value, type);
		}
		
		/**
		 * Возвращает абсолютное значение длины. Для абсолютной длины метод вернет значение value, для относительной - значение value, умноженное на unit.
		 * @param	unit Номинальное значение, по отношению к которому установлено значение этой относительной длины.
		 * @return Абсолютное значение длины.
		 */
		public function getAbsolute (unit:Number):Number {
			if (type == ABSOLUTE) {
				return value;
			}
			if (type == RELATIVE) {
				return value * unit;
			}
			throw new Error ("Unknown type.");
			return 0;
		}
		
	}

}