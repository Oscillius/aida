package dump13.aida.level0.core {
	import dump13.msl.types.Set;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiNameRegistry {
		private var _objectsByNames:Object;
		
		
		public function AiNameRegistry () {
			_objectsByNames = { };
		}
		
		public function destroy ():void {
			_objectsByNames = { };
		}
		
		/// [From Pusher]. Регистрирует объект по имени. Если существует только один объект с таким именем, он заносится в _objectsByNames напрямую; если объект с таким именем уже зарегистрирован (или зарегистрировано несколько объектов), то они хранятся в сете (Set). Объекты с именем, равным <code>null</code>, не регистрируются.
		public function registerObject (object:IAiNamed):void {
			var name:String = object.name;
			if (name == null) {
				return;
			}
			
			var item:Object = _objectsByNames[name];
			if (!item) {
				_objectsByNames[name] = object;
				return;
			}
			var setItem:Set = item as Set;
			if (setItem) {
				setItem.add (object);
				return;
			}
			var objItem:IAiNamed = item as IAiNamed;
			if (!objItem) {
				throw new Error ("Unknown item in _objectsByNames.");
			}
			setItem = new Set ();
			setItem.add (objItem);
			setItem.add (object);
			_objectsByNames[name] = setItem;
		}
		
		/// [From Pusher]. Удаляет регистрацию объекта по имени. См. registerObjectByName(). Если в локации было два объекта с таким именеи и один из них удаляется, набор (Set) будет удалён, а объект зарегистрируется напрямую. Объекты с именем, равным <code>null</code>, не регистрируются.
		public function unregisterObject (object:IAiNamed):void {
			var name:String = object.name;
			if (name == null) {
				return;
			}
			
			var item:Object = _objectsByNames[name];
			if (!item) {
				throw new Error ("Object not found.");
			}
			var objItem:IAiNamed = item as IAiNamed;
			if (objItem) {
				delete _objectsByNames[name];
				return;
			}
			var setItem:Set = item as Set;
			if (!setItem) {
				throw new Error ("Internal error.");
			}
			setItem.remove (object);
			if (!setItem.length) {
				delete _objectsByNames[name];
			}
		}
		
		/// [From Pusher]. Возвращает объект по его имени. Если зарегистрировано несколько объектов с таким именем, метод вернёт первый попавшийся.
		public function getObjectByName (name:String):IAiNamed {
			if (name == null) {
				throw new Error ("Bad name.");
			}
			var item:Object = _objectsByNames[name];
			if (!item) {
				return null;
			}
			var objItem:IAiNamed = item as IAiNamed;
			if (objItem) {
				return objItem;
			}
			var setItem:Set = item as Set;
			if (!setItem) {
				throw new Error ("Internal error.");
			}
			return IAiNamed (setItem.peek ());
		}
		
		/// [From Pusher]. Возвращает набор (Set) из объектов, имя которых равно <code>name</code>. Если не найдено ни одного объекта, возвращаемый набор будет пустым.
		public function getMultipleObjectsByName (name:String):Set {
			if (name == null) {
				throw new Error ("Bad name.");
			}
			var item:Object = _objectsByNames[name];
			if (!item) {
				return new Set ();
			}
			var objItem:IAiNamed = item as IAiNamed;
			if (objItem) {
				var result:Set = new Set ();
				result.add (objItem);
				return result;
			}
			var setItem:Set = item as Set;
			if (!setItem) {
				throw new Error ("Internal error.");
			}
			return setItem.clone ();
		}
		
	}

}