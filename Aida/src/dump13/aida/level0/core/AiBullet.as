package dump13.aida.level0.core {
	import dump13.msl.utils.ColorUtils;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiBullet extends AiBlob {
		
		private static const COLL_MASK:uint = AiCollisionCategory.SCENERY | AiCollisionCategory.BOSS | AiCollisionCategory.ENEMY | AiCollisionCategory.SPLIT;
		private static const KAMIKAZE_MASK:uint = AiCollisionCategory.SCENERY | AiCollisionCategory.BOSS | AiCollisionCategory.ENEMY | AiCollisionCategory.SPLIT;
		
		private static const STROKE_COLOR:uint = ColorUtils.hslToRGB (0 / 3, .75, .15);
		private static const SHADE_COLOR:uint = ColorUtils.hslToRGB (0 / 3, .75, .6);
		private static const COLOR:uint = ColorUtils.hslToRGB (0 / 3, .5, .8);
		private static const GLOSS_COLOR:uint = ColorUtils.hslToRGB (0 / 3, .75, .9);
		
		
		public function AiBullet ()
		{
			collCategory = AiCollisionCategory.BULLET;
			collMask = COLL_MASK;
			kamikazeMask = KAMIKAZE_MASK;
			
			isDynamic = true;
			//_directDamageCoef = .5;
			//_splitDamageCoef = .5;
			_damageSpecs = new AiDamageSpecs ();
			
			_strokeColor = STROKE_COLOR;
			_shadeColor = SHADE_COLOR;
			_color = COLOR;
			_glossColor = GLOSS_COLOR;
		}
		
		override public function clone ():AiObject
		{
			var bullet:AiBullet = new AiBullet ();
			bullet.clonePropertiesFrom (this);
			return bullet;
		}
		
	}

}