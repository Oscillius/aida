package dump13.aida.level0.core {
	import flash.geom.Point;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiBlob extends AiEntity implements IAiCircle {
		
		public static const SPLIT_COLL_MASK:uint = AiCollisionCategory.SCENERY | AiCollisionCategory.PLAYER | AiCollisionCategory.BOSS | AiCollisionCategory.ENEMY | AiCollisionCategory.BULLET;
		
		// ======= CLONEABLE =======
		
		protected var _radius:Number = 10;
		protected var _density:Number = 1;
		protected var _elasticity:Number = .9;
		protected var _mass:Number = 0;
		// (NON-CLONEABLE)
		protected var _massIsDirty:Boolean = true;
		
		protected var _strokeColor:uint = 0x707070;
		protected var _shadeColor:uint = 0xaaaaaa;
		protected var _color:uint = 0xcccccc;
		protected var _glossColor:uint = 0xeeeeee;
		
		private var _isAppearing:Boolean = false;
		/// Время от рождения блоба до появления его визуала.
		private var _appearTime:Number = 0;
		/// Время до полного появления визуала блоба.
		private var _timeToAppearing:Number = 0;
		
		/// Скорость уменьшения радиуса.
		public var shrinkSpeed:Number = 0;
		/// Коэффициент скольжения.
		public var gliding:Number = .991;
		/// Коэффициент, определяющий влияние скорости камикадзе на прирост скорости осколка. При значении, равном единице, осколок после создания движется навстречу снаряду, с такой же по модулю скоростью, как и у него. При нулевом значении осколок не отскакивает в сторону снаряда.
		public var splitBounceCoef:Number = .3;
		public var splitAmbienceCoef:Number = .3;
		/// У блобов с kamikazeMask, отличным от нуля: коэффициент влияния скорости камикадзе на скорость жертвы и ее осколков.
		protected var _velocityAffectCoef:Number = .02;
		/// Характеристики повреждений, наносимых этим блобом. Свойство может быть установлено в null, но у блобов с kamikazeMask, отличным от нуля, оно должно являться существующим объектом AiDamageSpecs.
		protected var _damageSpecs:AiDamageSpecs;
		
		// ======= NON-CLONEABLE =======
		
		/// Молния, присоединенная к этому блобу.
		protected var _lightning:AiLightning;
		
		/// [Helper].
		protected var _rect:Rectangle = new Rectangle ();
		protected var _rectIsDirty:Boolean = true;
		
		/// [Helper]. Если false, то _massIsDirty не будет устанавливаться в true при изменении радиуса. Используется в сеттере радиуса.
		private var _enableMassAutoDirty:Boolean = true;
		
		
		public function AiBlob () {
			//visual = new AiBlobVisual ();
			setVisual (new AiBlobVisual ());
		}
		
		override public function clone ():AiObject
		{
			var blob:AiBlob = new AiBlob ();
			blob.clonePropertiesFrom (this);
			return blob;
		}
		
		override protected function clonePropertiesFrom (object:AiObject):void
		{
			super.clonePropertiesFrom (object);
			
			var blob:AiBlob = AiBlob (object);
			radius = blob.radius;
			density = blob.density;
			elasticity = blob.elasticity;
			mass = blob.mass;
			
			_strokeColor = blob._strokeColor;
			_shadeColor = blob._shadeColor;
			_color = blob._color;
			_glossColor = blob._glossColor;
			
			if (blob.timeToAppearing) makeAppearing (blob.timeToAppearing);
			
			shrinkSpeed = blob.shrinkSpeed;
			gliding = blob.gliding;
			splitBounceCoef = blob.splitBounceCoef;
			splitAmbienceCoef = blob.splitAmbienceCoef;
			velocityAffectCoef = blob.velocityAffectCoef;
			
			var blobDamageSpecs:AiDamageSpecs = blob._damageSpecs;
			if (blobDamageSpecs) _damageSpecs = blobDamageSpecs.clone ();
		}
		
		public function cloneToSplit ():AiBlob
		{
			var blob:AiBlob = AiBlob (clone ());
			if (!blob.isDynamic)
			{
				blob.isDynamic = true;
				blob.collCategory = AiCollisionCategory.SPLIT;
				blob.collMask = SPLIT_COLL_MASK;
				blob.shrinkSpeed = 1;
				//blob.splitBounceCoef = 0;
				//blob.splitAmbienceCoef = .3;
				
				// Test.
				//blob.elasticity = .7;
				//blob.maxVelocity = 100;
			}
			return blob;
		}
		
		override public function update ():void
		{
			// Появление.
			if (_isAppearing)
			{
				_timeToAppearing -= Aida.instance.timer.deltaTime;
				if (_timeToAppearing <= 0)
				{
					_isAppearing = false;
					_timeToAppearing = 0;
				}
			}
			
			// Растворение.
			if (shrinkSpeed)
			{
				radius -= Aida.instance.timer.deltaTime * shrinkSpeed;
			}
			
			// Применение сопротивления среды.
			if (_isDynamic && gliding != 1 && (velocityX || velocityY))
			{
				velocityX *= gliding;
				velocityY *= gliding;
			}
			//if (_isDynamic && (velocityX || velocityY))
			//{
				//var dragCoef:Number = .01;
				//var velSquared:Number = velocityX * velocityX + velocityY * velocityY;
				//var diam:Number = radius;
				//diam += diam;
				//var dragForce:Number = dragCoef * velSquared * diam;
				//var deltaVel:Number = (dragForce / mass) * Aida.instance.timer.deltaTime;
				//var velLen:Number = Math.sqrt (velSquared);
				//if (deltaVel > velLen) deltaVel = velLen;
				//var newVelLen:Number = velLen - deltaVel;
				//var ratio:Number = newVelLen / velLen;
				//velocityX *= ratio;
				//velocityY *= ratio;
			//}
			
			super.update ();
		}
		
		/**
		 * Применяет раскалывающий урон и возвращает массив полученных частей (включая этот блоб). Если урон достаточен для того, чтобы расколоть блоб на максимально допустимое количество частей, а размер одной части получается меньше допустимого значения, то этот блоб будет отложенно уничтожен. Метод не воздействует на блоб-камикадзе, назначать ему новую массу - задача локации.
		 * Созданным осколкам назначаются новые скорости. Их ускорение делится на две составляющие: отскок и рассеяние. Отскок зависит от скорости камикадзе и коэффициента отскока, а также от того, на сколько частей раскалывается блоб: если раскалывание происходит более чем на две части, отскока происходить не будет; если части две, то отскок будет уменьшаться с увеличением массы меньшего осколка (от единицы до нуля). Рассеяние - это простое применение к осколкам случайных мгновенных ускорений, которые могут быть направлены в любую сторону. Модуль ускорения зависит от скорости камикадзе и коэффициента рассеяния.
		 * @param	damage		Раскалывающий урон, который необходимо нанести этому блобу. Если он больше, чем половина массы этого блоба, то будет происходить деление на более чем две части. Если количество частей больше, чем AiSettings.MAX_SPLIT_PARTS, а размер одной части меньше, чем AiSettings.MIN_SPLIT_RADIUS, то блоб будет отложенно удален ("распылен").
		 * @param	kamikaze	Блоб-камикадзе, который наносит урон. Например, снаряд по отношению к врагу является камикадзе.
		 * @return Массив осколков, получившихся при раскалывании. Содержит этот блоб.
		 */
		public function split (damage:Number, kamikaze:AiBlob):Vector.<AiBlob>
		{
			var parts:Vector.<AiBlob> = new Vector.<AiBlob> ();
			parts[0] = this;
			
			// Направление от этого блоба к камикадзе, который его раскалывает.
			var kamiDirX:Number = kamikaze.x - _x;
			var kamiDirY:Number = kamikaze.y - _y;
			var kamiDirLen:Number = Math.sqrt (kamiDirX * kamiDirX + kamiDirY * kamiDirY);
			kamiDirX /= kamiDirLen;
			kamiDirY /= kamiDirLen;
			
			// Направление, вдоль которого раздвигаются осколки.
			var splitDirX:Number = kamiDirX;
			var splitDirY:Number = kamiDirY;
			
			var lastSplit:Boolean;
			var numPartsLeft:int = AiSettings.MAX_SPLIT_PARTS;
			
			while (damage > 0)
			{
				// Поворачиваем направление раскалывания на 90 градусов.
				var temp:Number = splitDirX;
				splitDirX = -splitDirY;
				splitDirY = temp;
				
				// Является ли смещение актуальным. В начале каждого цикла сбрасываем в false.
				var offsetDefined:Boolean = false;
				
				var firstPart:AiBlob = parts[0];
				
				var numParts:int = parts.length;
				for (var n:int = 0; n < numParts; n++)
				{
					var curBlob:AiBlob = parts[n];
					var curMass:Number = curBlob.mass;
					var curHalfMass:Number = curMass * .5;
					
					// Создаем новый осколок, помещаем его в вектор и добавляем в локацию.
					var part:AiBlob = cloneToSplit ();
					parts.push (part);
					_location.addObjectDeferred (part);
					
					// Если раскалывающего урона хватает только на то, чтобы создать один осколок.
					if (damage <= curHalfMass)
					{
						mass -= damage;
						part.mass = damage;
						damage = 0;
						
						// Последний осколок размещается в том же месте, где был снаряд.
						// OLD.
						part.setXY (kamikaze.x, kamikaze.y);
						// NEW.
						//var lastSplitDist:Number = radius + part.radius;
						//part.setXY (_x + kamiDirX * lastSplitDist, _y + kamiDirY * lastSplitDist);
						
						lastSplit = true;
					}
					// Если раскалывающего урона достаточно, чтобы расколоть текущий блоб надвое.
					else
					{
						mass = curHalfMass;
						part.mass = curHalfMass;
						damage -= curHalfMass;
						
						// Определяем смещение, если оно еще не определено.
						if (!offsetDefined)
						{
							offsetDefined = true;
							//var offsetLen:Number = firstPart.radius * .4;
							var offsetLen:Number = firstPart.radius * Math.SQRT1_2;
							var offsetX:Number = offsetLen * splitDirX;
							var offsetY:Number = offsetLen * splitDirY;
						}
						
						setXY (_x + offsetX, _y + offsetY);
						part.setXY (part.x - offsetX, part.y - offsetY);
					}
					
					// Velocities.
					numPartsLeft--;
					if (numPartsLeft <= 0)
					{
						lastSplit = true;
						damage = 0;
					}
					
					if (lastSplit)
					{
						break;
					}
				}
			}
			
			// Velocities.
			numParts = parts.length;
			var kamiVelX:Number = kamikaze.velocityX;
			var kamiVelY:Number = kamikaze.velocityY;
			var kamiVelLen:Number = Math.sqrt (kamiVelX * kamiVelX + kamiVelY * kamiVelY);
			
			// Нахожу дополнительный коэффициент для отскока, учитывающий массы.
			var splitBounceMassCoef:Number = 0;
			if (splitBounceCoef)
			{
				if (numParts > 2)
				{
					splitBounceMassCoef = 0;
				}
				else
				{
					var baseMass:Number = parts[0].mass;
					splitBounceMassCoef = 1 - parts[1].mass / baseMass;
					if (splitBounceMassCoef <= 0)
					{
						splitBounceMassCoef = 0;
					}
				}
				var splitBounceCoefDamped:Number = splitBounceCoef * splitBounceMassCoef;
			}
			
			// Применяю отскок.
			if (splitBounceCoefDamped)
			{
				var splitBounceVelLen:Number = kamiVelLen * splitBounceCoefDamped;
				// From 1, not 0 (zero was buggy).
				for (n = 1; n < numParts; n++)
				{
					part = parts[n];
					part.velocityX += kamiDirX * splitBounceVelLen;
					part.velocityY += kamiDirY * splitBounceVelLen;
				}
			}
			// Применяю рассеяние.
			if (splitAmbienceCoef)
			{
				var splitAmbienceVelLen:Number = kamiVelLen * splitAmbienceCoef;
				var directions:AiDirections = Aida.instance.directions;
				// From 1, not 0 (zero was buggy).
				for (n = 1; n < numParts; n++)
				{
					part = parts[n];
					var randomDir:Point = directions.getRandomDirection ();
					part.velocityX += randomDir.x * splitAmbienceVelLen;
					part.velocityY += randomDir.y * splitAmbienceVelLen;
					//part.velocityX += (Math.random () - .5) * splitAmbienceVelLen;
					//part.velocityY += (Math.random () - .5) * splitAmbienceVelLen;
				}
			}
			// Применяю влияние скорости камикадзе.
			/*var velAffectCoef:Number = kamikaze.velocityAffectCoef;
			
			var kamiMass:Number = kamikaze.mass;	// v1
			if (velAffectCoef && kamiMass)			// v1
			//if (velAffectCoef)					// v0
			{
				var velAffectUnit:Number = velAffectCoef / kamiMass;	// v1
				for (n = 1; n < numParts; n++)
				{
					part = parts[n];
					// v0.
					//part.velocityX += kamiVelX * velAffectCoef;
					//part.velocityY += kamiVelY * velAffectCoef;
					
					// v1.
					var velAffectMultForCurrent:Number = part.mass * velAffectUnit;
					part.velocityX += kamiVelX * velAffectMultForCurrent
					part.velocityY += kamiVelY * velAffectMultForCurrent;
				}
			}*/
			
			return parts;
		}
		
		/// Присоединяет молнию к этому блобу. Блоб не должен содержать других молний, а присоединяемая молния не должна быть в составе других блобов. Молния будет добавлена в локацию вместе с этим блобом.
		public function attachLightning (lightning:AiLightning):void
		{
			if (lightning._owner)
			{
				throw new Error ("Lightning is already attached to some blob.");
			}
			if (_lightning)
			{
				throw new Error ("Some lightning is attached to this blob, detach it first.");
			}
			_lightning = lightning;
			lightning._owner = this;
			
			if (!_attachedEntities)
			{
				_attachedEntities = new Vector.<AiEntity> ();
			}
			_attachedEntities.push (lightning);
			if (_location)
			{
				_location.addObject (lightning);
			}
		}
		
		/// Отсоединяет молнию от этого блоба. Отсоединяемая молния удаляется из локации и уничтожается (локацией).
		public function detachLightning (lightning:AiLightning):void
		{
			if (lightning._owner !== this)
			{
				throw new Error ("Lightning is not attached to this blob.");
			}
			
			var index:int = _attachedEntities.indexOf (lightning);
			_attachedEntities.splice (index, 1);
			if (!_attachedEntities.length)
			{
				_attachedEntities = null;
			}
			if (_location)
			{
				_location.removeObject (lightning);
			}
			
			_lightning._owner = null;
			_lightning = null;
		}
		
		/// При изменении координат x или y помечаем _rect как неактуальный.
		override protected function onXYChange ():void
		{
			_rectIsDirty = true;
			super.onXYChange ();
		}
		
		/// Рассчитывает радиус по массе блоба.
		[Inline]
		public final function getRadiusByMass (mass:Number):Number
		{
			return Math.sqrt (mass / (Math.PI * _density));
		}
		
		/// Вызывается, когда масса блоба становится равна нулю.
		protected function onDead ():void
		{
			if (_location) _location.removeObjectDeffered (this);
		}
		
		/// Заставляет блоб проявляться в течение времени appearTime. По умолчанию блобы видны с самого начала, с помощью этого метода можно изменить их поведение. Метод работает только для динамических блобов, при вызове на статике будет создано исключение.
		[Inline]
		public final function makeAppearing (appearTime:Number):void
		{
			if (!_isDynamic)
			{
				throw new Error ("This blob is static.");
			}
			_appearTime = appearTime;
			_timeToAppearing = appearTime;
			_isAppearing = true;
		}
		
		/// Возвращает массу для расчета столкновений. У динамических объектов она равна mass (используется геттер), у статичных - Number.POSITIVE_INFINITY.
		[Inline]
		public final function getMassForPhysics ():Number
		{
			return _isDynamic? mass : Number.POSITIVE_INFINITY;
		}
		
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		[Inline]
		override public final function get rect ():Rectangle
		{
			if (_rectIsDirty)
			{
				_rectIsDirty = false;
				var diam:Number = _radius + _radius;
				_rect.x = _x - _radius;
				_rect.y = _y - _radius;
				_rect.width = diam;
				_rect.height = diam;
			}
			return _rect.clone ();
		}
		
		/**
		 * Радиус блоба. При установке нового значения масса блоба будет пересчитана (laisy), а сам блоб и его визуал будут перерегистрированы в сетках локации. Так как обновление массы будет происходить только по запросу, то установка нулевого радиуса является исключительным случаем: масса будет установлена в нулевое значение и будет вызван метод onDead().
		 */
		[Inline]
		public final function get radius ():Number { return _radius; }
		public final function set radius (value:Number):void
		{
			// Исключительный случай.
			if (value <= 0)
			{
				_mass = 0;
				_massIsDirty = false;
				onDead ();
				return;
			}
			
			_radius = value;
			_rectIsDirty = true;
			
			if (_visual)
			{
				_visual._needsRedraw = true;
			}
			// Перерегистрируем блоб в сетках для физики и визуалов.
			if (_location)
			{
				_location.reregisterEntityInPhysicsGrid (this);
				if (_visual)
				{
					_location.reregisterVisualInGrid (_visual);
				}
			}
			
			if (_enableMassAutoDirty)
			{
				_massIsDirty = true;
			}
		}
		
		/**
		 * Масса блоба. Рассчитывается из его радиуса и плотности. Когда масса становится равна нулю, блоб "умирает" и отложенно удаляется из локации. Статичные блобы не имеют бесконечную массу (она считается так же, как и для динамических блобов); массу для расчета столкновений можно узнать с помощью getMassForPhysics().
		 */
		[Inline]
		public final function get mass ():Number
		{
			if (_massIsDirty)
			{
				_massIsDirty = false;
				_mass = Math.PI * _radius * _radius * _density;
			}
			return _mass;
		}
		public final function set mass (value:Number):void
		{
			if (value < 0)
			{
				value = 0;
			}
			_mass = value;
			
			_enableMassAutoDirty = false;
			radius = getRadiusByMass (value);	// Setter.
			_enableMassAutoDirty = true;
			
			if (_mass <= 0)
			{
				onDead ();
			}
		}
		
		/**
		 * Плотность материала блоба.
		 */
		[Inline]
		public final function get density ():Number { return _density; }
		public final function set density (value:Number):void {
			_density = value;
		}
		
		/**
		 * Коэффициент упругости блоба.
		 */
		[Inline]
		public final function get elasticity ():Number { return _elasticity; }
		public final function set elasticity (value:Number):void
		{
			_elasticity = value;
		}
		
		/**
		 * Основной цвет блоба.
		 */
		[Inline]
		public final function get color ():uint { return _color; }
		public final function set color (value:uint):void
		{
			_color = value;
			if (_visual)
			{
				_visual._needsRedraw = true;
			}
		}
		
		[Inline]
		public final function get strokeColor ():uint { return _strokeColor; }
		
		[Inline]
		public final function get shadeColor ():uint { return _shadeColor; }
		
		[Inline]
		public final function get glossColor ():uint { return _glossColor; }
		
		/// Запущен ли процесс проявления блоба. Чтобы заставить блоб проявляться со временем, используй makeAppearing().
		[Inline]
		public final function get isAppearing ():Boolean { return _isAppearing; }
		
		/// Время от рождения блоба до его полного проявления. Чтобы заставить блоб проявляться со временем, используй makeAppearing().
		[Inline]
		public final function get appearTime ():Number { return _appearTime; }
		
		/// Время, остающееся до полного появления визуала блоба. Чтобы заставить блоб проявляться со временем, используй makeAppearing().
		[Inline]
		public final function get timeToAppearing ():Number { return _timeToAppearing; }
		
		/// Характеристики повреждения блоба. Объект возвращается и устанавливается по ссылке. Может быть равен null, но у блобов с kamikazeMask, отличным от нуля, должен являться экземпляром AiDamageSpecs.
		[Inline]
		public final function get damageSpecs ():AiDamageSpecs { return _damageSpecs; }
		public final function set damageSpecs (value:AiDamageSpecs):void
		{
			_damageSpecs = value;
		}
		
		/**
		 *  Это visual, приведенный к AiBlobVisual.
		 */
		[Inline]
		public final function get blobVisual ():AiBlobVisual { return AiBlobVisual (_visual); }
		
		/// У камикадзе: коэффициент влияния скорости камикадзе на скорость жертвы и ее осколков.
		[Inline]
		public final function get velocityAffectCoef ():Number { return _velocityAffectCoef; }
		public final function set velocityAffectCoef (value:Number):void
		{
			if (isNaN (value)) throw new Error ("NaN encountered.");
			if (value < 0) value = 0;
			_velocityAffectCoef = value;
		}
		
		/// Молния, присоединенная к этому блобу, либо null, если к блобу молния не присоединена.
		[Inline]
		public final function get lightning ():AiLightning { return _lightning; }
		
	}

}