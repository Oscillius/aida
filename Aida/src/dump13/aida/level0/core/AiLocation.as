package dump13.aida.level0.core {
	import dump13.msl.types.Set;
	import dump13.msl.utils.IGrid;
	import dump13.msl.utils.OGrid;
	import dump13.msl.utils.OGridSmart;
	import flash.display.Sprite;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiLocation {
		
		/// Список всех объектов локации.
		private var _objects:AiList = new AiList ();
		/// Список объектов, подлежащих обновлению.
		private var _updatables:AiList = new AiList ();
		/// Список визуалов, добавленных на экран. Сортируется по уменьшению координаты z сущности-владельца (визуалы в списке располагаются от дальнего к ближнему).
		private var _visualsOnScreen:AiList = new AiList ();
		/// Список объектов, которых необходимо отложенно удалить.
		private var _objectsToRemove:AiList = new AiList ();
		/// Список объектов, которых необходимо отложенно удалить.
		private var _objectsToAdd:AiList = new AiList ();
		/// Идет ли в данный момент процесс отложенного удаления блобов.
		private var _defRemovingInProgress:Boolean;
		/// Идет ли в данный момент процесс отложенного добавления блобов.
		private var _defAddingInProgress:Boolean;
		/// Идет ли в данный момент фаза обновления. Фаза обновления включает в себя работу метода update() до фазы отложенного удаления и добавления объектов. Если true, то метод removeObject() вызовет removeObjectDeferred() (то же с addObject()).
		private var _updatingInProgress:Boolean;
		
		private var _nameRegistry:AiNameRegistry = new AiNameRegistry ();
		
		/// Оптимизационная сетка для регистрации статических сущностей по их ограничивающим прямоугольникам.
		private var _staticsGrid:OGrid/*.<AiEntity>*/;
		/// Сетка для регистрации динамических сущностей.
		private var _dynamicsGrid:IGrid/*.<AiEntity>*/;
		/// [Helper]. Область, внутри которой будут рассчитываться столкновения. Задается в координатах локации. Размеры задаются в конструкторе, а координаты обновляются в update() с учетом положения камеры.
		private var _physicsArea:Rectangle;
		/// [Helper]. Набор динамических сущностей, обновляемых в текущей итерации. Устанавливается в методе updateEntitiesBehaviors().
		private var _curDynamics:Set;
		/// [Helper]. Набор динамических сущностей, которые будут обновлены с помощью postUpdate() в конце текущей итерации. Заполняется в методе updateEntitiesBehaviors().
		private var _entitiesToPostUpdate:Set = new Set ();
		/// Сетка для регистрации визуалов.
		private var _visualsGrid:OGrid/*.<AiVisual>*/;
		
		/// Блоб игрока, добавленный в локацию.
		private var _player:AiPlayer;
		
		
		public function AiLocation ()
		{
			_staticsGrid = new OGrid (AiSettings.STATICS_GRID_CELL_SIZE);
			
			if (AiSettings.PHYSICS_USES_SMART_GRID)
			{
				//_dynamicsGrid = new OGridSmart (AiSettings.STATICS_GRID_CELL_SIZE, AiSettings.STATICS_GRID_CELL_SIZE / 4, AiSettings.STATICS_GRID_CELL_SIZE / 4, 10);
				_dynamicsGrid = new OGridSmart (96, 6, 6, 4);
			}
			else
			{
				_dynamicsGrid = new OGrid (AiSettings.DYNAMICS_GRID_CELL_SIZE);
			}
			
			_visualsGrid = new OGrid (AiSettings.VISUALS_GRID_CELL_SIZE);
			
			var physicsAreaSize:Number = AiSettings.PHYSICS_AREA_HALF_SIZE;
			physicsAreaSize += physicsAreaSize;
			_physicsArea = new Rectangle (0, 0, physicsAreaSize, physicsAreaSize);
		}
		
		public function destroy ():void
		{
			while (_objects.head)
			{
				var obj:AiObject = AiObject (_objects.head.object);
				removeObject (obj);
				obj.destroy ();
			}
			_curDynamics.clear ();
			_entitiesToPostUpdate.clear ();
		}
		
		
		/**
		 * Добавляет Объект в эту Локацию. Если передано значение null, создается исключение. Если объект состоит в какой-либо Локации (в том числе в этой), создается исключение. При удалении объекта он удаляется из списков _objectsToRemove и _objectsToAdd (если только не идет процесс отложенного удаления или добавления объектов).
		 * Если в данный момент идет фаза обновления (см. _updatingInProgress), то этот метод вызовет addObjectDeferred(), и объект будет помещен в список на отложенное добавление.
		 * @param	object Объект, который необходимо добавить в Локацию.
		 */
		public function addObject (object:AiObject):void
		{
			if (!object)
			{
				throw new Error ("The 'object' parameter should not be null.");
			}
			if (object.location)
			{
				throw new Error ("Object is in some Location already.");
			}
			
			if (_updatingInProgress)
			{
				addObjectDeferred (object);
				return;
			}
			
			object.connect (this);
			
			_objects.add (object._locObjectsNode);
			if (object.isUpdatable)
			{
				_updatables.add (object._locUpdatableNode);
			}
			if (!_defRemovingInProgress)
			{
				_objectsToRemove.removeIfPossible (object._locObjToRemoveNode);
			}
			if (!_defAddingInProgress)
			{
				_objectsToAdd.removeIfPossible (object._locObjToAddNode);
			}
			
			registerObjectByName (object);
			
			// ENTITIES.
			
			var entity:AiEntity = object as AiEntity;
			if (entity)
			{
				// Physics.
				registerEntityInPhysicsGrid (entity);
				//_staticsGrid.addItemByRealRect (entity, entity.rect);
				
				// Visual.
				var visual:AiVisual = entity.visual;
				if (visual)
				{
					addEntityVisual (visual);
				}
				
				// Attached entities.
				var attached:Vector.<AiEntity> = entity._attachedEntities;
				if (attached)
				{
					var numAttached:int = attached.length;
					for (var n:int = 0; n < numAttached; n++)
					{
						addObject (attached[n]);
					}
				}
				
				// Player.
				var player:AiPlayer = entity as AiPlayer;
				if (player)
				{
					_player = player;
				}
			}
		}
		
		/**
		 * Удаляет Объект из Локации. Если объект находится в списке на отложенное удаление, он будет удален из этого списка (аналогично - для списка на отложенное добавление), но только если в данный момент не идут процессы отложенного удаления и добавления.
		 * Если в данный момент идет фаза обновления (см. _updatingInProgress), то этот метод вызовет removeObjectDeferred(), и объект будет помещен в список на отложенное удаление.
		 * @param	object Объект, который необходимо удалить из Локации.
		 */
		internal function removeObject (object:AiObject):void
		{
			if (!object)
			{
				throw new Error ("The 'object' parameter should not be null.");
			}
			if (object.location !== this)
			{
				throw new Error ("Object is not in this Location.");
			}
			
			if (_updatingInProgress)
			{
				removeObjectDeffered (object);
				return;
			}
			
			// Уведомляем объект об удалении.
			if (object._notifyOnRemove)
			{
				object.onRemoveFromLocation ();
			}
			
			// ENTITIES.
			
			var entity:AiEntity = object as AiEntity;
			if (entity)
			{
				// Physics.
				unregisterEntityInPhysicsGrid (entity);
				//_staticsGrid.removeItem (entity);
				
				// Visual.
				var visual:AiVisual = entity.visual;
				if (visual)
				{
					removeEntityVisual (visual);
				}
				
				// Attached entities.
				var attached:Vector.<AiEntity> = entity._attachedEntities;
				if (attached)
				{
					var numAttached:int = attached.length;
					for (var n:int = 0; n < numAttached; n++)
					{
						removeObject (attached[n]);
					}
				}
				
				// Player.
				if (_player && object === _player)
				{
					_player = null;
				}
				
				// Post-updatable entities.
				_entitiesToPostUpdate.remove (entity);
			}
			
			// OBJECTS.
			unregisterObjectByName (object);
			
			if (!_defRemovingInProgress)
			{
				_objectsToRemove.removeIfPossible (object._locObjToRemoveNode);
			}
			if (!_defAddingInProgress)
			{
				_objectsToAdd.removeIfPossible (object._locObjToAddNode);
			}
			
			if (object.isUpdatable)
			{
				_updatables.remove (object._locUpdatableNode);
			}
			_objects.remove (object._locObjectsNode);
			
			object.shutdown ();
		}
		
		/**
		 * Помещает объект в список на отложенное удаление и уничтожение. Объект будет удален и уничтожен по завершении обновления локации. Если объект находится в списке на отложенное добавление, он будет из него удален.
		 * @param	object Объект, который необходимо удалить и уничтожить.
		 */
		public function removeObjectDeffered (object:AiObject):void
		{
			if (!object)
			{
				throw new Error ("The 'object' parameter should not be null.");
			}
			if (object.location !== this)
			{
				throw new Error ("Object is not in this Location.");
			}
			_objectsToRemove.addIfPossible (object._locObjToRemoveNode);
			_objectsToAdd.removeIfPossible (object._locObjToAddNode);
		}
		
		/**
		 * Помещает объект в список на отложенное добавление. Объект будет добавлен по завершении обновления локации. Если объект находится в списке на отложенное удаление, он будет оттуда удален.
		 * @param	object Объект, который необходимо удалить.
		 */
		public function addObjectDeferred (object:AiObject):void
		{
			if (!object)
			{
				throw new Error ("The 'object' parameter should not be null.");
			}
			if (object.location)
			{
				throw new Error ("Object is already added to some location.");
			}
			_objectsToAdd.addIfPossible (object._locObjToAddNode);
			_objectsToRemove.removeIfPossible (object._locObjToRemoveNode);
		}
		
		/**
		 * Обновляет Локацию. Перед обновлением следует вызвать Aida.timer.preUpdate(), после обновления - Aida.timer.postUpdate(). Требуется переписать логику обновляемых объектов (см. лог для Aida v0.0.2).
		 */
		public function update ():void
		{
			var i:Object;
			var visual:AiVisual;
			var nextNode:AiListNode;
			var object:AiObject;
			
			_updatingInProgress = true;
			
			var camera:AiCamera = Aida.instance.camera;
			
			// Обновляем объекты из _updatables. Даже если эта схема будет внедрена, она неправильная (объекты, попадающие в область обновления физики, будут обновляться дважды).
			var curNode:AiListNode = _updatables.head;
			while (curNode)
			{
				AiObject (curNode.object).updateDistant ();
				curNode = curNode.next;
			}
			
			// ========= Updating the behaviors ==========
			
			updateEntitiesBehaviors ();
			
			// ========= Physics =========
			
			performPhysics ();
			
			// ========== Post updating ===========
			
			for (i in _entitiesToPostUpdate)
			{
				AiEntity (i).postUpdate ();
			}
			
			// Updating phase is over.
			_updatingInProgress = false;
			
			// ========= Removing objects =========
			
			_defRemovingInProgress = true;
			
			//curNode = _objectsToRemove.head;
			//while (curNode)
			//{
				//removeObject (AiObject (curNode.object));
				//curNode = curNode.next;
			//}
			
			curNode = _objectsToRemove.head;
			while (curNode)
			{
				nextNode = curNode.next;
				object = AiObject (curNode.object);
				removeObject (object);
				_objectsToRemove.remove (curNode);
				object.destroy ();
				
				curNode = nextNode;
			}
			
			//curNode = _objectsToRemove.head;
			//while (curNode)
			//{
				//removeObject (AiObject (curNode.object));
				//curNode = curNode.next;
			//}
			//_objectsToRemove.clear (false);
			
			_defRemovingInProgress = false;
			
			// ========== Adding objects ==========
			
			_defAddingInProgress = true;
			
			curNode = _objectsToAdd.head;
			while (curNode)
			{
				nextNode = curNode.next;
				addObject (AiObject (curNode.object));
				_objectsToAdd.remove (curNode);
				
				curNode = nextNode;
			}
			
			//curNode = _objectsToAdd.head;
			//while (curNode)
			//{
				//addObject (AiObject (curNode.object));
				//curNode = curNode.next;
			//}
			//_objectsToAdd.clear (false);
			
			_defAddingInProgress = false;
			
			// ========= Визуалы =========
			
			// Берем визуалы из области, заданной _camera.area.
			var visuals:Set = _visualsGrid.getItemsInRect (_visualsGrid.realToIntRect (camera.area));
			
			// Удаляем с экрана те визуалы, которые есть в списке _visualsOnScreen, но которых нет в сете visuals.
			curNode = _visualsOnScreen.head;
			while (curNode)
			{
				visual = AiVisual (curNode.object);
				if (!visuals.has (visual))
				{
					_visualsOnScreen.remove (curNode);
					//_host.removeChild (visual.sprite);
				}
				curNode = curNode.next;
			}
			
			// Добавляем на экран те визуалы, которые есть в сете visuals, но которых нет в списке _visualsOnScreen, и попутно обновляем все визуалы, которые необходимо.
			//var visualsDeltaX:Number = camera.screenWidth * .5 - camera.x;
			//var visualsDeltaY:Number = camera.screenHeight * .5 - camera.y;
			var visualsDeltaX:Number = camera.screenHalfWidth - camera.x;
			var visualsDeltaY:Number = camera.screenHalfHeight - camera.y;
			for (i in visuals)
			{
				visual = AiVisual (i);
				
				curNode = visual._locVisualsOnScreenNode;
				if (!_visualsOnScreen.has (curNode))
				{
					_visualsOnScreen.addSorted (curNode, compareVisualsByZ);
					// add to screen...
					if (curNode.next)
					{
						var nextVisual:AiVisual = AiVisual (curNode.next.object);
						//_host.addChildAt (visual.sprite, _host.getChildIndex (nextVisual.sprite));
					}
					else
					{
						//_host.addChild (visual.sprite);
					}
				}
				
				// Redraw.
				if (visual._needsRedraw)
				{
					visual._needsRedraw = false;
					visual.redraw ();
				}
				// Reposition.
				//visual.sprite.x = visual.entity.x + visualsDeltaX;
				//visual.sprite.y = visual.entity.y + visualsDeltaY;
			}
			
			// Rendering.
			camera.lock ();
			camera.fill ();
			
			curNode = _visualsOnScreen.head;
			while (curNode)
			{
				AiVisual (curNode.object).render (camera);
				curNode = curNode.next;
			}
			
			camera.unlock ();
		}
		
		/// Определяет область обновления физики (и поведения), достает из сетки набор _curDynamics и обновляет сущности в этом наборе. Также заполняет набор _entitiesToPostUpdate, которые будут обновлены с помощью postUpdate() в конце текущей итерации.
		private function updateEntitiesBehaviors ():void
		{
			// Пока что (или всегда) обновляться будет область вокруг камеры.
			var camera:AiCamera = Aida.instance.camera;
			var areaHS:Number = AiSettings.PHYSICS_AREA_HALF_SIZE;
			_physicsArea.x = camera.x - areaHS;
			_physicsArea.y = camera.y - areaHS;
			var dynamics:Set = _dynamicsGrid.getItemsInRealRect (_physicsArea);
			//var dynamics:Set = _curDynamics;
			
			_entitiesToPostUpdate.clear ();
			
			for (var i:Object in dynamics)
			{
				var entity:AiEntity = AiEntity (i);
				entity.update ();
				
				if (entity.enablePostUpdate)
				{
					_entitiesToPostUpdate.add (entity);
				}
			}
			
			// Для использования в performPhysics().
			_curDynamics = dynamics;
		}
		
		private function performPhysics ():void
		{
			var i:Object;
			var j:Object;
			
			// Перенесено в updateEntitiesBehaviors().
			//var camera:AiCamera = Aida.instance.camera;
			//var areaHS:Number = AiSettings.PHYSICS_AREA_HALF_SIZE;
			//_physicsArea.x = camera.x - areaHS;
			//_physicsArea.y = camera.y - areaHS;
			//var dynamics:Set = _dynamicsGrid.getItemsInRealRect (_physicsArea);
			var dynamics:Set = _curDynamics;
			
			for (i in dynamics)
			{
				var dyn:AiEntity = AiEntity (i);
				var dynBlob:AiBlob = dyn as AiBlob;
				if (!dynBlob)
				{
					continue;
				}
				// Если масса блоба равна нулю, то он, вероятно, был уничтожен при предыдущих столкновениях.
				if (!dynBlob.mass)
				{
					continue;
				}
				var dynCollCategory:uint = dyn.collCategory;
				var dynCollMask:uint = dyn.collMask;
				
				// Я отказываюсь от флага isGhost, вместо него буду использовать нулевую маску столкновений.
				if (!dynCollMask)
				{
					continue;
				}
				
				var nbs:Set = _dynamicsGrid.getNeighbors (dynBlob);
				var dynRect:Rectangle = dyn.rect;
				_staticsGrid.getItemsInRealRect (dynRect, nbs);
				// Теперь nbs содержит и статичных, и динамических соседей сущности dyn.
				
				var dynRadius:Number = dynBlob.radius;
				
				for (j in nbs)
				{
					var nb:AiEntity = AiEntity (j);
					var nbBlob:AiBlob = nb as AiBlob;
					if (!nbBlob)
					{
						continue;
					}
					// Если масса соседа нулевая, то он был уничтожен при предыдущем столкновении (в этом же апдейте).
					if (!nbBlob.mass)
					{
						continue;
					}
					
					// Добавить проверку по типам (если в принципе не сталкиваются, пропускаем).
					//if ((dynCollMask & nb.collCategory) == 0 || (nb.collMask & dynCollCategory) == 0)
					if ((dynCollMask & nb.collCategory) == 0 && (nb.collMask & dynCollCategory) == 0)
					//if ((dynCollMask & nb.collCategory) == 0)
					{
						continue;
					}
					// ...
					
					// Широкая фаза: если баунды сущностей не пересекаются, то пропускаем их.
					// Можно проверить, есть ли толк от такой проверки, в отдельном тесте.
					if (!dynRect.intersects (nb.rect))
					{
						continue;
					}
					
					// Узкая фаза.
					var nbRadius:Number = nbBlob.radius;
					var dx:Number = nb.x - dyn.x;
					var dy:Number = nb.y - dyn.y;
					var distSq:Number = dx * dx + dy * dy;
					var dist:Number = Math.sqrt (distSq);
					var penetr:Number = dynRadius + nbRadius - dist;
					
					//if (penetr <= 0)
					if (penetr < 0)
					{
						continue;
					}
					
					// Если объекты проникают друг в друга.
					// Обработка столкновений будет производиться "на месте" - без создания дополнительных объектов контактов. Не совсем правильная модель, но она упростит мне работу.
					
					// Если объекты не сталкиваются по принципу камикадзе-урона. Если один из объектов заявляет об участии в камикадзе-взаимодействии, взаимодействие будет происходить.
					var dynIsKamikaze:Boolean = Boolean (dyn.kamikazeMask & nb.collCategory);
					var nbIsKamikaze:Boolean = Boolean (nb.kamikazeMask & dyn.collCategory);
					if (!dynIsKamikaze && !nbIsKamikaze)
					{
						var dynMass:Number = dynBlob.getMassForPhysics ();
						var nbMass:Number = nbBlob.getMassForPhysics ();
						var massSum:Number = dynMass + nbMass;
						var massRatio:Number = dynMass / massSum;
						var massRatioCompl:Number = 1 - massRatio;
						
						// Для начала просто растолкну сущности - без изменения скоростей, учета импульсов и т.д.
						var pushRatio:Number = penetr / dist;
						var pushX:Number = dx * pushRatio;
						var pushY:Number = dy * pushRatio;
						dyn.x -= pushX * massRatioCompl;
						dyn.y -= pushY * massRatioCompl;
						nb.x += pushX * massRatio;
						nb.y += pushY * massRatio;
						
						
						// ========== Назначаю новые скорости ==========
						
						// Орт столкновения. Длина равна единице. Направлен от первого тела ко второму (dyn -> nb).
						var collOrthX:Number = dx / dist;
						var collOrthY:Number = dy / dist;
						// Относительная скорость - скорость движения второго тела по отношению к первому.
						var relVelX:Number = nb.velocityX - dyn.velocityX;
						var relVelY:Number = nb.velocityY - dyn.velocityY;
						
						// Проекция относительной скорости на орт столкновения.
						var relVelToColl:Number = relVelX * collOrthX + relVelY * collOrthY;
						// Если скорость сближения и орт столкновения направлены в одну сторону (образуют острый или прямой угол), пропускаем пару.
						if (relVelToColl >= 0)
						{
							continue;
						}
						// Относительная скорость, которая будет уменьшена с учетом коэффициентов упругости обоих тел. На это значение в сумме изменятся проекции скоростей тел на орт столкновения. (Умножил ее на два, чтобы coll-составляющие векторов скорости отражались, а не обнулялись.)
						var pushVelLen:Number = 2 * relVelToColl * dynBlob.elasticity * nbBlob.elasticity;
						
						// Дельта-скорость для первого тела.
						var pushVel1:Number = pushVelLen * massRatioCompl;
						dyn.velocityX += collOrthX * pushVel1;
						dyn.velocityY += collOrthY * pushVel1;
						//dyn.velocityX += 2 * collOrthX * pushVel1;
						//dyn.velocityY += 2 * collOrthY * pushVel1;
						
						// Дельта-скорость для второго тела.
						//var pushVel2:Number = pushVelLen * massRatio;
						var pushVel2:Number = pushVelLen - pushVel1;
						nb.velocityX -= collOrthX * pushVel2;
						nb.velocityY -= collOrthY * pushVel2;
					}
					
					// Если сущности сталкиваются по принципу камикадзе-урона.
					// =============== DAMAGING ===============
					else
					{
						var kami:AiEntity;
						var kamiBlob:AiBlob;
						var victim:AiEntity;
						var victimBlob:AiBlob;
						if (dynIsKamikaze && nbIsKamikaze)
						{
							throw new Error ("The 'kamikaze - kamikaze' interactions are not supported.");
						}
						if (dynIsKamikaze)
						{
							kami = dyn;
							kamiBlob = dynBlob;
							victim = nb;
							victimBlob = nbBlob;
						}
						else
						{
							kami = nb;
							kamiBlob = nbBlob;
							victim = dyn;
							victimBlob = dynBlob;
						}
						
						// (New).
						var damageSpecs:AiDamageSpecs = kamiBlob.damageSpecs;
						
						// Здесь масса блоба-жертвы условно делится на две части: массу под прямой урон и массу под раскалывающий урон.
						var victimMass:Number = victimBlob.mass;
						var victimDirectMass:Number = victimMass * damageSpecs.directRatio;
						var victimSplitMass:Number = victimMass - victimDirectMass;
						
						// Рассчитываю потенциальный урон, который может нанести блоб-камикадзе.
						var kamiMass:Number = kamiBlob.mass;
						var directDamage:Number = kamiMass * damageSpecs.directCoef;
						var splitDamage:Number = kamiMass * damageSpecs.splitCoef;
						
						// Теперь сравниваю условные (direct- и split-) массы блоба-жертвы с потенциальными уронами блоба-камикадзе.
						// Если хотя бы одна из условных масс блоба-жертвы окажется достаточной для его выживания, то блоб-камикадзе будет уничтожен. В противном случае уничтожен будет блоб-жертва, а масса камикадзе уменьшится.
						
						// Direct.
						// Если direct-часть массы жертвы больше прямого урона, то жертва выживет.
						//var victimDirSurvives:Boolean = victimDirectMass > directDamage;
						var victimDirSurvives:Boolean = directDamage && (victimDirectMass > directDamage);
						
						// Split.
						// Выживет ли сплит-часть блоба.
						var victimSplitSurvives:Boolean;
						// Полумасса сплит-части блоба.
						var halfSplitVMass:Number = victimSplitMass * .5;
						// Уровень раскалывания (степень двойки).
						var splitLevel:Number = splitDamage / halfSplitVMass;
						
						// Если раскалывающий урон разделит жертву на максимальное количество частей.
						if (splitLevel > AiSettings.MAX_SPLIT_LEVEL)
						{
							var splitPartMass:Number = victimSplitMass / AiSettings.MAX_SPLIT_PARTS;
							var splitPartRadius:Number = victimBlob.getRadiusByMass (splitPartMass);
							// Если при раскалывании получаются слишком мелкие кусочки.
							if (splitPartRadius < AiSettings.MIN_SPLIT_RADIUS)
							{
								victimSplitSurvives = false;
							}
							else
							{
								victimSplitSurvives = true;
							}
						}
						else
						{
							victimSplitSurvives = true;
						}
						
						// Выживет ли камикадзе.
						var kamiSurvives:Boolean = !(victimDirSurvives || victimSplitSurvives);
						if (kamiSurvives)
						{
							victimBlob.mass = 0;
							
							// bullet mass? ...
							// Уменьшаю массу камикадзе в соответствии с прямым уроном, который он нанес.
							if (damageSpecs.directCoef)
							{
								kamiBlob.mass -= victimDirectMass / damageSpecs.directCoef;
							}
							// Уменьшаю массу камикадзе в соответствии с раскалывающим уроном, который он нанес.
							if (damageSpecs.splitCoef)
							{
								kamiBlob.mass -= victimSplitMass / damageSpecs.splitCoef;
							}
						}
						else
						{
							kamiBlob.mass = 0;
							
							// victim? ...
							// Применяю к жертве весь прямой урон камикадзе.
							//victimBlob.mass -= victimDirSurvives? directDamage : victimDirectMass;
							if (directDamage)
							{
								victimBlob.mass -= directDamage;
							}
							// Применяю к жертве весь раскалывающий урон камикадзе.
							if (splitDamage)
							{
								var splints:Vector.<AiBlob> = victimBlob.split (splitDamage, kamiBlob);
							}
							// Применяю velocityAffectCoef к блобу жертвы.
							var velAffectCoef:Number = kamiBlob.velocityAffectCoef;
							var kamiVelX:Number = kamiBlob.velocityX;
							var kamiVelY:Number = kamiBlob.velocityY;
							if (velAffectCoef && kamiMass)
							{
								var velAffectUnit:Number = velAffectCoef * kamiMass;
								if (!splints)
								{
									splints = new Vector.<AiBlob> ();
									splints[0] = victimBlob;
								}
								var numSplints:int = splints.length;
								for (var n:int = 0; n < numSplints; n++)
								{
									var splint:AiBlob = splints[n];
									var splintMass:Number = splint.mass;
									if (!splintMass)
									{
										continue;
									}
									var velAffectMultForSplint:Number = velAffectUnit / splintMass;
									splint.velocityX += velAffectMultForSplint * kamiVelX;
									splint.velocityY += velAffectMultForSplint * kamiVelY;
								}
							}
						}
					}
					
				}
			}
			
			// Для использования в updateEntitiesByBehaviors()
			//_curDynamics = dynamics;
		}
		
		/**
		 * Возвращает набор статичных сущностей, попадающих в область прямоугольника realRect, заданного в системе координат локации.
		 * @param	realRect Прямоугольник, внутри которого нужно вести поиск. Задается в координатах локации.
		 * @param	accurateSearch Вести ли дополнительный поиск среди найденных сущностей на предмет попадания их ограничивающих прямоугольников в область поиска. Значение по умолчанию false, т.е. обычно поиск ведется грубо, по ячейкам сетки.
		 * @param	result Набор, в который необходимо поместить найденные сущности. В начале поиска не очищается. Если передано значение null, будет создан новый набор.
		 * @return Набор, содержащий статичные сущности внутри прямоугольника realRect.
		 */
		public function getStaticsInRealRect (realRect:Rectangle, accurateSearch:Boolean = false, result:Set = null):Set
		{
			if (!accurateSearch)
			{
				return _staticsGrid.getItemsInRealRect (realRect, result);
			}
			if (!result)
			{
				result = new Set ();
			}
			var entities:Set = _staticsGrid.getItemsInRealRect (realRect, result);
			for (var i:Object in entities)
			{
				var entity:AiEntity = i as AiEntity;
				if (realRect.intersects (entity.rect))
				{
					result.add (entity);
				}
			}
			return result;
		}
		
		/**
		 * Возвращает набор динамических сущностей, попадающих в область прямоугольника realRect, заданного в системе координат локации.
		 * @param	realRect Прямоугольник, внутри которого нужно вести поиск. Задается в координатах локации.
		 * @param	accurateSearch Вести ли дополнительный поиск среди найденных сущностей на предмет попадания их ограничивающих прямоугольников в область поиска. Значение по умолчанию false, т.е. обычно поиск ведется грубо, по ячейкам сетки.
		 * @param	result Набор, в который необходимо поместить найденные сущности. В начале поиска не очищается. Если передано значение null, будет создан новый набор.
		 * @return Набор, содержащий динамические сущности внутри прямоугольника realRect.
		 */
		public function getDynamicsInRealRect (realRect:Rectangle, accurateSearch:Boolean = false, result:Set = null):Set
		{
			if (!accurateSearch)
			{
				return _dynamicsGrid.getItemsInRealRect (realRect, result);
			}
			if (!result)
			{
				result = new Set ();
			}
			var entities:Set = _dynamicsGrid.getItemsInRealRect (realRect, result);
			for (var i:Object in entities)
			{
				var entity:AiEntity = i as AiEntity;
				if (realRect.intersects (entity.rect))
				{
					result.add (entity);
				}
			}
			return result;
		}
		
		/**
		 * Возвращает набор сущностей (и статику, и динамику), попадающих в область прямоугольника realRect, заданного в системе координат локации.
		 * @param	realRect Прямоугольник, внутри которого нужно вести поиск. Задается в координатах локации.
		 * @param	accurateSearch Вести ли дополнительный поиск среди найденных сущностей на предмет попадания их ограничивающих прямоугольников в область поиска. Значение по умолчанию false, т.е. обычно поиск ведется грубо, по ячейкам сетки.
		 * @param	result Набор, в который необходимо поместить найденные сущности. В начале поиска не очищается. Если передано значение null, будет создан новый набор.
		 * @return Набор, содержащий сущности (статичные и динамические) внутри прямоугольника realRect.
		 */
		public function getEntitiesInRealRect (realRect:Rectangle, accurateSearch:Boolean = false, result:Set = null):Set
		{
			result = getStaticsInRealRect (realRect, accurateSearch, result);
			return getDynamicsInRealRect (realRect, accurateSearch, result);
		}
		
		
		internal function registerObjectByName (object:AiObject):void
		{
			_nameRegistry.registerObject (object);
		}
		
		internal function unregisterObjectByName (object:AiObject):void
		{
			_nameRegistry.unregisterObject (object);
		}
		
		public function getObjectByName (name:String):AiObject
		{
			return AiObject (_nameRegistry.getObjectByName (name));
		}
		
		public function getMultipleObjectsByName (name:String):Set
		{
			return _nameRegistry.getMultipleObjectsByName (name);
		}
		
		
		internal function addUpdatable (object:AiObject):void
		{
			_updatables.add (object._locUpdatableNode);
		}
		
		internal function removeUpdatable (object:AiObject):void
		{
			_updatables.remove (object._locUpdatableNode);
		}
		
		
		/// Вызывать, когда enablePostUpdate сущности устанавливается в false.
		internal function onPostUpdatableIsFalse (entity:AiEntity):void
		{
			_entitiesToPostUpdate.remove (entity);
		}
		
		
		internal function registerEntityInPhysicsGrid (entity:AiEntity):void
		{
			var grid:IGrid = entity.isDynamic? _dynamicsGrid : _staticsGrid;
			grid.addItemByRealRect (entity, entity.rect);
		}
		
		internal function unregisterEntityInPhysicsGrid (entity:AiEntity):void
		{
			var grid:IGrid = entity.isDynamic? _dynamicsGrid : _staticsGrid;
			grid.removeItem (entity);
		}
		
		internal function reregisterEntityInPhysicsGrid (entity:AiEntity):void
		{
			var grid:IGrid = entity.isDynamic? _dynamicsGrid : _staticsGrid;
			grid.removeItem (entity);
			grid.addItemByRealRect (entity, entity.rect);
		}
		
		internal function onEntityZChange (entity:AiEntity):void
		{
			var visual:AiVisual = entity.visual;
			if (visual)
			{
				_visualsOnScreen.removeIfPossible (visual._locVisualsOnScreenNode);
			}
		}
		
		/// Функция для упорядочивания визуалов по уменьшению координаты z их сущностей-владельцев.
		private function compareVisualsByZ (node1:AiListNode, node2:AiListNode):Number
		{
			if (AiVisual (node1.object).entity.z >= AiVisual (node2.object).entity.z)
			{
				return -1;
			}
			return 1;
		}
		
		
		/// [Not used]
		internal function registerVisualInGrid (visual:AiVisual):void
		{
			_visualsGrid.addItemByRealRect (visual, visual.rect);
		}
		
		/// [Not used]
		internal function unregisterVisualInGrid (visual:AiVisual):void
		{
			_visualsGrid.removeItem (visual);
		}
		
		/// [Not used]
		internal function reregisterVisualInGrid (visual:AiVisual):void
		{
			_visualsGrid.removeItem (visual);
			_visualsGrid.addItemByRealRect (visual, visual.rect);
		}
		
		
		internal function addEntityVisual (visual:AiVisual):void
		{
			_visualsGrid.addItemByRealRect (visual, visual.rect);
		}
		
		internal function removeEntityVisual (visual:AiVisual):void
		{
			_visualsGrid.removeItem (visual);
			_visualsOnScreen.removeIfPossible (visual._locVisualsOnScreenNode);
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		public function get objects ():AiList { return _objects; }
		
		/// [Slow]
		public function get numObjects ():int
		{
			var curNode:AiListNode = _objects.head;
			var quantity:int = 0;
			while (curNode)
			{
				quantity++;
				curNode = curNode.next;
			}
			return quantity;
		}
		
		/// Ссылка на игрока, добавленного в локацию.
		public function get player ():AiPlayer { return _player; }
		
	}

}