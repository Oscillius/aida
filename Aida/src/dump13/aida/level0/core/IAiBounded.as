package dump13.aida.level0.core 
{
	import flash.geom.Rectangle;
	
	/**
	 * Интерфейс объекта, обладающего ограничивающим прямоугольником.
	 * @author dump13
	 */
	public interface IAiBounded 
	{
		/// Ограничивающий прямоугольник объекта.
		function get rect ():Rectangle;
	}
	
}