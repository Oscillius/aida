package dump13.aida.level0.core 
{
	import dump13.msl.types.Set;
	import flash.geom.Rectangle;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiLightning extends AiEntity
	{
		public static const DYNAMICS_AND_STATICS:int = 0;
		public static const STATICS_ONLY:int = 1;
		public static const DYNAMICS_ONLY:int = 2;
		
		public static const ENEMY_TARGETS:uint = AiCollisionCategory.BOSS | AiCollisionCategory.ENEMY;
		public static const FRIENDLY_TARGETS:uint = AiCollisionCategory.PLAYER | AiCollisionCategory.BULLET;
		
		internal var _owner:AiBlob;
		
		private var _ownRadius:Number = 100;
		private var _numSparks:int = 5;
		private var _damagePerSpark:Number = 20;
		
		/// Типы целей по типу их регистрации в физических сетках: динамика и статика, только динамика или только статика. Это свойство главенствует над targetsMask: например, если молния действует только на динамику, то она не сможет поражать статичные объекты (AiCollisionCategory.SCENERY), даже если их тип указан в targetsMask. По умолчанию молния действует и на динамику, и на статику, но в целях оптимизации свойство можно менять.
		public var targetsByRegistration:int = DYNAMICS_AND_STATICS;
		/// Какие цели поражает молния.
		public var targetsMask:uint;
		
		/// [Helper]. Расстояния до целей, расположенные в порядке возрастания. Заполняются в postUpdate(). Для отрисовки визуала молнии. Следует использовать только первые numSparks элементов, потому что вектор может быть длиннее.
		internal var _distances:Vector.<Number> = new Vector.<Number> ();
		/// [Helper]. Цели, расположенные в порядке возрастания расстояния до них. Заполняются в postUpdate(). Для отрисовки визуала молнии. Следует использовать только первые numSparks элементов, потому что вектор может быть длиннее.
		internal var _targets:Vector.<AiBlob> = new Vector.<AiBlob> ();
		
		
		public function AiLightning () 
		{
			isDynamic = true;
			collCategory = AiCollisionCategory.DECOR;
			//collMask = 0x00000000;
			targetsMask = ENEMY_TARGETS;
			
			useVelocity = false;
			useMaxVelocity = false;
			
			setVisual (new AiLightningVisual ());
			
			enablePostUpdate = true;
		}
		
		/**
		 * Перед уничтожением молния должна быть отсоединена от блоба-владельца.
		 */
		override public function destroy ():void
		{
			if (_owner)
			{
				throw new Error ("Lightning is connected to an owner, disconnect it first.");
			}
			
			super.destroy ();
		}
		
		override public function clone ():AiObject
		{
			var lightning:AiLightning = new AiLightning ();
			lightning.clonePropertiesFrom (this);
			return lightning;
		}
		
		override protected function clonePropertiesFrom (object:AiObject):void 
		{
			super.clonePropertiesFrom (object);
			
			var lightning:AiLightning = object as AiLightning;
			ownRadius = lightning.ownRadius;
			numSparks = lightning.numSparks;
			damagePerSpark = lightning.damagePerSpark;
		}
		
		/// Для молнии этот метод переопределен в заглушку. В postUpdate() используется super.update().
		override public function update ():void
		{
			// Does nothing in lightning.
		}
		
		/// Основной функционал молнии. Молния должна обновляться после большинства остальных объектов, чтобы правильно отрисовываться.
		override public function postUpdate ():void
		{
			var i:Object;
			var n:int, na:int;
			var m:int, ma:int;
			
			if (!_owner || !_location)
			{
				return;
			}
			
			var deltaTime:Number = Aida.instance.timer.deltaTime;
			
			// Снэппим к владельцу.
			setXY (_owner.x, _owner.y);
			z = _owner.z + .2;
			
			// Подготавливаем вектора расстояний и целей.
			var distances:Vector.<Number> = _distances;
			var targets:Vector.<AiBlob> = _targets;
			na = _numSparks;
			for (n = 0; n < na; n++)
			{
				_targets[n] = null;
				_distances[n] = Number.POSITIVE_INFINITY;
			}
			
			// Bounds.
			var fullRadius:Number = _owner.radius + _ownRadius;
			var diam:Number = fullRadius + fullRadius;
			var bounds:Rectangle = new Rectangle (_owner.x - fullRadius, _owner.y - fullRadius, diam, diam);
			
			// Neighbors.
			var nbs:Set;
			switch (targetsByRegistration)
			{
				case DYNAMICS_AND_STATICS: nbs = _location.getEntitiesInRealRect (bounds); break;
				case DYNAMICS_ONLY:		   nbs = _location.getDynamicsInRealRect (bounds); break;
				case STATICS_ONLY:		   nbs = _location.getStaticsInRealRect (bounds); break;
				default:				   throw new Error ("Unknown 'targetsByRegistration' value.");
			}
			for (i in nbs)
			{
				var blob:AiBlob = i as AiBlob;
				if (!blob)
				{
					continue;
				}
				// Проверка по типу цели.
				if (!Boolean (targetsMask & blob.collCategory))
				{
					continue;
				}
				// Широкая фаза, по баундам.
				if (!bounds.intersects (blob.rect))
				{
					continue;
				}
				var dx:Number = blob.x - _x;
				var dy:Number = blob.y - _y;
				var dist:Number = Math.sqrt (dx * dx + dy * dy) - blob.radius;
				// Если цель за пределами радиуса поражения.
				if (dist > fullRadius)
				{
					continue;
				}
				
				// Узкая фаза.
				na = _numSparks;
				ma = na - 1;
				// Это дополнительная проверка: если все элементы массивов уже заполнены, а блоб дальше, чем самый дальний target, найденный на этот момент, то переходим к следующему блобу (не перебираем массив лишние _numSparks раз). Можно не использовать эту проверку для малого количества искр.
				if (dist > distances[ma])
				{
					continue;
				}
				// Вставляем расстояние и цель в нужное место в массивах (если она окажется достаточно близко).
				for (n = 0; n < na; n++)
				{
					var comparedDist:Number = _distances[n];
					if (dist < comparedDist)
					{
						// Сдвигаем правые части массивов вправо на один элемент.
						for (m = ma; m > n; m--)
						{
							distances[m] = distances[m - 1];
							targets[m] = targets[m - 1];
						}
						// Вставляем текущие расстояние и цель в массивы.
						distances[n] = dist;
						targets[n] = blob;
						
						break;
					}
				}
			}
			
			// Применяем урон.
			na = _numSparks;
			for (n = 0; n < na; n++)
			{
				var target:AiBlob = _targets[n];
				if (!target)
				{
					break;
				}
				target.mass -= _damagePerSpark * deltaTime;
			}
			
			// Здесь вызываю штатный update() сущности, потому что у молнии он переопределен в заглушку.
			super.update ();
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/**
		 * Ограничивающий прямоугольник в системе координат локации; используется для регистрации в сетке динамических объектов локации. Если AiSettings.OPTIMIZE_LIGHTNINGS_PHYS_RECT == true, метод вернет "неправильный" баунд, так что использовать этот метод для определения действительных размеров объекта нельзя.
		 */
		override public function get rect ():Rectangle
		{
			var optimize:Boolean = AiSettings.OPTIMIZE_LIGHTNINGS_PHYS_RECT;
			if (optimize || !_owner)
			{
				return new Rectangle (0, 0, 1, 1);
			}
			var r:Number = _owner.radius + _ownRadius;
			var diam:Number = r + r;
			return new Rectangle (_owner.x - r, _owner.y - r, diam, diam);
		}
		
		/**
		 * Блоб-владелец, которому принадлежит эта молния.
		 */
		[Inline]
		public final function get owner ():AiBlob { return _owner; }
		
		/**
		 * Это visual, приведенный к AiLightningVisual.
		 */
		[Inline]
		public final function get lightningVisual ():AiLightningVisual { return AiLightningVisual (_visual); }
		
		/**
		 * Собственный радиус молнии. Чтобы узнать полный радиус области, в пределах которой действует молния, нужно сложить ее собственный радиус с радиусом блоба-владельца.
		 */
		[Inline]
		public final function get ownRadius ():Number { return _ownRadius; }
		public final function set ownRadius (value:Number):void
		{
			_ownRadius = value;
			// _visual всегда будет нуждаться в перерисовке, так что с _needsRedraw не возимся.
		}
		
		/**
		 * Количество искр в составе этой молнии.
		 */
		[Inline]
		public final function get numSparks ():int { return _numSparks; }
		public final function set numSparks (value:int):void
		{
			_numSparks = value;
		}
		
		/**
		 * Урон, наносимый в секунду одной искрой.
		 */
		[Inline]
		public final function get damagePerSpark ():Number { return _damagePerSpark; }
		public final function set damagePerSpark (value:Number):void
		{
			_damagePerSpark = value;
		}
		
		/**
		 * Полный радиус молнии - сумма радиуса блоба-владельца и собственного радиуса этой молнии.
		 */
		[Inline]
		public final function get fullRadius ():Number
		{
			if (!_owner)
			{
				return _ownRadius;
			}
			return _ownRadius + owner.radius;
		}
		
	}

}