package dump13.aida.level0.core {
	/**
	 * ...
	 * @author dump13
	 */
	public class AiCollisionCategory {
		
		public static const DECOR:uint   = 0x00000000;
		public static const SCENERY:uint = 0x00000001;
		public static const PLAYER:uint  = 0x00000002;
		public static const BOSS:uint    = 0x00000004;
		public static const BULLET:uint  = 0x00000008;
		public static const ENEMY:uint   = 0x00000010;
		/// Осколки от статики.
		public static const SPLIT:uint	 = 0x00000020;
		
		
		public function AiCollisionCategory () {
			
		}
		
	}

}