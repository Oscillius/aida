package dump13.aida.level0.core {
	
	/**
	 * ...
	 * @author dump13
	 */
	public interface IAiNamed {
		function get name ():String;
		function set name (value:String):void;
	}
	
}