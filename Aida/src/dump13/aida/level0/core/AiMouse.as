package dump13.aida.level0.core {
	import flash.events.MouseEvent;
	import flash.geom.Point;
	/**
	 * ...
	 * @author dump13
	 */
	public class AiMouse {
		
		private var _isDown:Boolean;
		
		
		public function AiMouse () {
			init ();
		}
		
		public function destroy ():void
		{
			Aida.instance.stage.removeEventListener (MouseEvent.MOUSE_DOWN, onMouseDown);
			Aida.instance.stage.removeEventListener (MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private function init ():void
		{
			Aida.instance.stage.addEventListener (MouseEvent.MOUSE_DOWN, onMouseDown);
			Aida.instance.stage.addEventListener (MouseEvent.MOUSE_UP, onMouseUp);
		}
		
		private function onMouseDown (e:MouseEvent):void
		{
			_isDown = true;
		}
		
		private function onMouseUp (e:MouseEvent):void
		{
			_isDown = false;
		}
		
		/// Возвращает координаты указателя мыши в системе координат локации.
		public function getGlobalCoords ():Point
		{
			var camera:AiCamera = Aida.instance.camera;
			var gx:Number = Aida.instance.stage.mouseX - camera.screenHalfWidth + camera.x;
			var gy:Number = Aida.instance.stage.mouseY - camera.screenHalfHeight + camera.y;
			
			//var gx:Number = Aida.instance.stage.mouseX - camera.screenHalfWidth + camera.xRound;
			//var gy:Number = Aida.instance.stage.mouseY - camera.screenHalfHeight + camera.yRound;
			
			return new Point (gx, gy);
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/// Нажата ли левая кнопка мыши.
		public function get isDown ():Boolean { return _isDown; }
		
	}

}