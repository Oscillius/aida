package dump13.aida.level0.core {
	import flash.display.BitmapData;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.utils.Dictionary;
	import flash.utils.setTimeout;
	/**
	 * Банк с одноцветными изображениями. На данный момент используется синхронное получение клипов без предварительной подготовки.
	 * @author dump13
	 */
	public class AiFlatBank extends EventDispatcher {
		
		/// Словарь, хранящий либо задания на подготовку изображения, либо сами изображения. В качестве ключа используется uint-овый цвет.
		private var _clips:Dictionary/*.<uint : AiClip | Boolean>*/= new Dictionary ();
		
		private var _isCreating:Boolean;
		private var _traversal:Vector.<uint>;
		private var _curIndex:int = 0;
		
		
		public function AiFlatBank() {
			
		}
		
		public function destroy ():void
		{
			if (_isCreating)
			{
				throw new Error ("The bank is being created.");
			}
			_traversal.length = 0;
			for (var i:Object in _clips)
			{
				delete _clips[i];
			}
		}
		
		public function addTask (color:uint):void
		{
			if (_clips[color] != undefined)
			{
				throw new Error ("Task already added and may be processed.");
			}
			_clips[color] = true;
		}
		
		public function createAsync ():void
		{
			if (_isCreating)
			{
				throw new Error ("Already being created.");
			}
			_isCreating = true;
			
			_traversal = new Vector.<uint> ();
			for (var i:Object in _clips)
			{
				_traversal.push (uint (i));
			}
			_curIndex = 0;
			
			createNext ();
		}
		
		private function createNext ():void
		{
			if (_curIndex < _traversal.length)
			{
				var color:uint = _traversal[_curIndex];
				var size:int = AiSettings.FLAT_SQUARE_SIZE;
				var bmd:BitmapData = new BitmapData (size, size, true, 0xff000000 | color);
				_clips[color] = new AiClip (bmd);
				
				_curIndex++;
				setTimeout (createNext, 1);
			}
			else
			{
				_isCreating = false;
				_curIndex = 0;
				_traversal.length = 0;
				dispatchEvent (new Event (Event.COMPLETE));
			}
		}
		
		/// [Deprecated]. Возвращает клип с цветом color, который был до этого создан. Если клип не найден, создается исключение.
		public function getClipByColor (color:uint):AiClip
		{
			var clip:AiClip = _clips[color] as AiClip;
			if (!clip)
			{
				throw new Error ("Clip not found.");
			}
			return clip;
		}
		
		/// Синхронный метод, который возвращает существующий клип с цветом color или создает новый (если существующего не найдено) и возвращает его.
		public function provideClipByColor (color:uint):AiClip
		{
			var clip:AiClip = _clips[color] as AiClip;
			if (!clip)
			{
				var size:int = AiSettings.FLAT_SQUARE_SIZE;
				var bmd:BitmapData = new BitmapData (size, size, true, 0xff000000 | color);
				clip = new AiClip (bmd);
				_clips[color] = clip;
			}
			return clip;
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		public function get isCreating ():Boolean { return _isCreating; }
		
		public function get progress ():Number
		{
			return _curIndex / _traversal.length;
		}
		
	}

}