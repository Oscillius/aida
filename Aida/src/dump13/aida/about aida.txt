=================================================
================== Aida v0.0.0 ==================
=================================================

Бродилка. Логическое развитие ДефБолла.
Особенности.
- Отдельные локации (не открытый мир).
- ГГ путешествует, а не стоит на месте.
- Развитие ГГ.
- Очень простой ИИ врагов.

После сравнения с ДефБоллом возник вопрос: должны ли взаимодействовать враги? Пока ответ в том, что бывают Bosses и Foes (возможно, переименую). Bosses сталкиваются друг с другом. Foes ведут себя так же, как и вражеские Stones в ДефБолле, т.е. не сталкиваются.
Так как осколков будет очень много, взаимодействовать друг с другом они не должны. Однако теперь появляются стены, а с ними осколки взаимодействовать должны. Кроме того, раньше осколки притягивались к центру экрана, а сейчас они будут "искать" ГГ.

Стены могут порождать врагов.
Ядовитые стены.
Ядовитые облака.
Разрушаемые стены-тайники.
Флагами (01000000) можно задавать не только столкновения, но и, например, повреждения. Правда, это повреждения определённых типов (прямое, раскалывающее, отравляющее), поэтому и групп флагов будет много (либо их можно будет добавлять вручную - как отдельные правила).

=================================================
================== Aida v0.0.1 ==================
=================================================

От первой версии остались только AiNameRegistry и INamed.
- Реализован связный список AiList, состоящий из нодов AiListNode. Ноды содержат кастомный объект object:Object.
- INamed переименован в IAiNamed.
- Простейший объектный пул для AiListNode не удался (либо тест написан неудачно), так что я отказываюсь от его использования в этом проекте.
- Возникла проблема со связными списками. Объекты AiObject ничего не знали о нодах списка, с которыми они ассоциированы, поэтому удаление объекта из локации не являлось бы быстрой операцией. Решение: 1) объект сам создает ноду списка (нода зануляется в AiObject::destroy()); 2) локация использует готовую ноду объекта для помещения его в связный список; 3) для других связных списков, в которых будет участвовать объект, будут создаваться соответствующие свойства этого объекта; 4) ноды объекта для других списков создаются в том случае, если он действительно должен располагаться в этих списках (т.е. по значению соответствующего свойства объекта).
- Добавлены AiObject::isUpdatable:Boolean, AiObject::_locUpdatableNode:AiListNode, AiLocation::_updatables:AiList.

=================================================
================== Aida v0.0.2 ==================
=================================================

В этой версии ввожу Сущность AiEntity, наследующуюся от AiObject. Сущность имеет обязательные координаты (x, y, z), по которым она будет регистрироваться в сетках. Возможно, я введу свойство AiEntity::isVisible:Boolean, установка которого в false позволит не использовать координаты Сущности и не регистрировать ее в сетках.
- Добавлен AiLocation::_entitiesByZ:AiList. Сущности в списке сортируются по убыванию z (чем ближе к камере, тем дальше по списку).

Теперь нужно добавить объекты, которые будут отображаться на экране.
Добавляю класс визуала. Он будет содержать по крайней мере служебную инфу.
Пришла мысль, что идея с обновляемыми Объектами плохая (в том виде, в каком она сейчас реализована). Если Объектов в Локации много, то каждый кадр придется проходить по всему списку updatable-объектов. Ведь сетки я придумывал как раз для того, чтобы брать только те объекты, которые необходимо обновить. Либо я ввожу дополнительные обновляемые компоненты (как визуал) и обновляю каждый шаг уже их, либо изменяю _updatables Локации на _objectsToUpdate. В этом случае длина соответствующих списков сократится.
Решено не вводить менеджер визуалов, вместо этого отдать все под управление локации. Есть две операции, связанные с визуалами: регистрация в сетке и добавление спрайта визуала в дисплей-лист.
Возникла проблема: по z сортируются сущности, но не все из них могут содержать визуалы. При определении визуала с меньшей координатой z нужно искать те сущности из сортированного списка, у которых есть визуал (это медленнее). Решения два
  - можно ввести список визуалов _visualsByZ, а список сущностей хранить в несортированном виде.
  - можно оставить все как есть и заниматься поиском сущностей с визуалами. Это решение рассчитано на то, что подавляющее большинство сущностей будут иметь визуалы.
Я прибегаю ко второму решению.
Сделать AiEntity::onZChange, AiEntity::onXYChange.

=================================================
================== Aida v0.0.3 ==================
=================================================

Предыдущая версия осталась неоттестированной, в этой версии я добавлю классы Блоба и Визуала Блоба и проверю, как все работает.
AiVisual::_needsReposition на практике оказывается бессмысленным - камера почти постоянно будет в движении, поэтому обновлять положения придется у всех спрайтов, попадающих в область видимости.
Процесс добавления спрайтов при добавлении визуалов в локацию неверен. В локации будет находиться гораздо больше визуалов, чем будет видно на экране, поэтому и спрайты необходимо добавлять и удалять с экрана по запросу, при каждом обновлении локации.

=================================================
================== Aida v0.0.4 ==================
=================================================

Задачи на эту версию.
- Изменить процесс добавления спрайтов на экран и сортировки визуалов.
- Убрать флаг AiVisual::_needsReposition и все, что с ним связано (каждый кадр положения визуалов, попадающих в область видимости, будут обновляться).

Сделано.
- Удалил AiLocation::_entitiesByZ. Сущности теперь не хранятся ни в каком списке.
- Теперь спрайты визуалов не добавляются к хосту при добавлении сущностей. AiEntity::_locEntitiesByZNode удален.
- Добавлен AiVisual::_locVisualsOnScreenNode.
- Удален AiVisual::_needsReposition. AiLocation и AiEntity изменены.
- Теперь спрайты визуалов добавляются к хосту локации с использованием _visualsGrid:OGrid и _visualsOnScreen:AiList.

=================================================
================== Aida v0.0.5 ==================
=================================================

Задача на версию - реализовать рендеринг на блиттинге и сравнить его по производительности с простым добавлением спрайтов на экран. Тормоза со спрайтами могут происходить просто из-за движения мыши и генерации ивентов. Очевидно, этого происходить не будет, если на экране будет только одна битмапа.
Есть два способа реализовать блиттинг - с кэшированием и без него. Кэширование - с помощью BitmapData::copyPixels(), без кэширования - с помощью BitmapData::draw().
- Добавлена Aida, которая обеспечивает инициализацию игры и глобальный доступ к часто используемым свойствам.
- Добавлены AiCircleBank и AiFlatBank, подготавливающие изображения для круглых масок и одноцветных заливок, соответственно.
- Добавлен AiClip - изображение с координатами центра, используемое классами Банков с одной стороны и классами Визуалов - с другой.
- AiBlobVisual::redraw() теперь не обновляет графику спрайта (она бесполезна), а получает из банков объекты, необходимые для рендеринга. Так должно работать чуть быстрее, потому что цвета блоба практически не будут меняться после его создания, и у визуала не будет необходимости заниматься получением клипов банка каждый апдейт.
- Добавлены статичные свойства AiSettings::CIRCLE_MAX_RADIUS, AiSettings::CIRCLE_RADIUS_STEP и AiSettings::FLAT_SQUARE_SIZE.
- Внедрен другой способ рисования кругов с обводкой. Сначала рисуется круг цвета обводки (чуть большего диаметра), затем круг цвета заливки. В классе AiBlobVisual реализована пробная декорация блобов кругами разных диаметров (хардкодед), однако, вместе с уменьшением количества используемой памяти я получаю в разы более медленный рендеринг (заливка + обводка - в два раза, с кругом блика - примерно в три раза).

=================================================
================== Aida v0.0.6 ==================
=================================================

С предыдущей версии тянется хвост, который я откладываю на потом. Локации не помешало бы владеть набором (или объектом) требований, которые будут использоваться при ее подготовке. Например, требования могут содержать материалы, используемые локацией; по материалам можно определить, клипы каких цветов необходимо создать в банке AiFlatBank.

Задача на эту версию - начать физическую часть движка.
- Я по-прежнему делю блобы на две больших группы: статику и динамику. Это позволит отсечь множество лишних проверок при определении столкновений и обходить только динамику. Поначалу меня сбивал с толку вопрос о том, почему именно происходит такое деление. Первая очевидная мысль в том, что статичные объекты не сталкиваются друг с другом; однако возникал контраргумент: ведь и динамические объекты одной группы могут не взаимодействовать.
Тем не менее, деление на статику и динамику полезно. Дело в том, что в большинстве случаев динамические объекты не смогут проходить через статичные. Статика служит границей уровня и является неплохим инструментом в геймдизе. Я могу определить недосягаемые для игрока (пока?) области, как и недосягаемые для его врагов.
- AiLocation::_host удален.
- Добавлены AiLocation::_objectsToRemove:AiList, AiLocation::removeObjectDeferred(), AiObject::_locObjToRemoveNode. Метод AiLocation::update() дописан.

Провел небольшой тест по рендерингу. Сравнивались разные варианты отрисовки в AiBlobVisual. Вот результаты, взятые из AiTest004AidaVisuals (радиус блобов равен 20 пикселям):
	// 4 comps - 30 ms per 1000 objects.
	// 2 comps (fill + stroke) - 22 ms per 1000 objects.
	// Fill only - 15 ms per 1000 objects.
Тысяча объектов на экране при отсутствии физики и вообще всего, кроме рендеринга, видится мне допустимым пределом. Даже если не использовать многопроходный рендеринг и кэшировать в память не маски, а готовые спрайты блобов (на каждый материал по набору), то тысяча объектов будет отнимать половину процессорного времени (см. "Fill only" - последний результат). Конечно, нужно провести такие же тесты с меньшим размером блобов.
Вот резуальтаты второго теста, с радиусом блобов 10 пикселей:
	// 4 comps - 18 ms per 1000 objects.
	// 2 comps (fill + stroke) - 13...14 ms per 1000 objects.
	// Fill only - 10 ms per 1000 objects.

К доработке.
- Нашел использование AiLocation::reregisterEntityInPhysicsGrid() в AiEntity и в его наследнике AiBlob. Перерегистрация в сетке происходит при назначении новых координат и при изменении радиуса. И то, и другое будет происходить каждый кадр с большинством динамических блобов, и перерегистрация будет происходить как минимум дважды. Следует добавить "тихую" установку координат и радиуса с последующей ручной перерегистрацией сущности.

- AiLocation::onEntityZChange() дописан. Сущность просто удаляется из списка объектов, присутствующих на экране (при следующем обновлении локации она будет в него добавлена, если необходимо).

- Начал писать физику и понял, что сущности не обязаны иметь круглую форму. Ввожу IAiCircle, который будут реализовывать круглые сущности (блобы, взрывы). Я планирую сделать взаимодействие типа "круг - круг" единственно возможным в игре, но интерфейс ввожу про запас (так как ввел промежуточный класс сущностей, а не сразу перешел к блобам).
- Решил не хранить в сущностях набор соседей, с которыми она уже столкнулась (в текущем обновлении локации). До этого хотел использовать его при определении контактов, чтобы избежать обработки подобных контактов. Сейчас я буду производить обработку столкновений "на месте"; следовательно, сущности после обработки окажутся в новых положениях и, возможно, будут сталкиваться уже с другими объектами. Эта модель не является "правильной" и зависит от порядка обхода объектов (который будет случайным), но, во-первых, она упростит мне работу, а во-вторых, для игровых целей сойдет.
- Сомнение вызывает старая модель повреждений. Должны ли вражеские блобы уничтожаться при столкновении? В каких-то случаях так и должно происходить. Но что если у протагониста будет скилл отталкивания блоба (в повреждение переводится только часть массы врага, а сам враг остается жив, но отталкивается)? И что делать, когда оба блоба имеют такое свойство?
- Сначала сделаю случай, когда объекты просто сталкиваются, не повреждая друг друга. Безо всяких условий, пока что так будет происходить всегда (для сталкивающихся объектов).
- Логика обновляемых (updatable) объектов изменится. Пожалуй, этот список остается на всякий случай - для тех объектов, которых необходимо обновлять даже в том случае, если они выходят за пределы области обновления физики.
- AiObject::update() -> AiObject::updateDistant().
- Добавлен AiEntity::update(). Будет вызываться для ДИНАМИЧЕСКИХ сущностей в области обновления физики. Статика не обновляется.

=================================================
================== Aida v0.0.7 ==================
=================================================

В предыдущей версии была сделана физика столкновений.
Задача на эту версию - внедрить модель повреждений.
- Прямой и раскалывающий урон.
- Камикадзе- и не-камикадзе-урон.
- Влияние снаряда на скорость движения блоба.
- Shrinking.

Камикадзе-урон - это такое взаимодействие между блобами, при котором хотя бы один из них гарантированно будет уничтожен. Снаряд при столкновении с массивным врагом "умрет", отняв от врага часть массы, равную его прямому урону, и нанеся ему весь свой раскалывающий урон. При столкновении с легким врагом снаряд уменьшится в массе, а враг будет уничтожен.
Не-камикадзе-урон - это урон, наносимый блобами друг другу, при котором оба из них могут остаться в живых. Пример - столкновение игрока и босса; возможно, я захочу дать игроку второй шанс, поэтому от столкновения с боссом, превосходящим игрока по массе, он (игрок) не умрет. Не-камикадзе-урон - это уже не простой размен масс.

Сделаю появляющиеся блобы. Способов как минимум два. Первый - основанный на текущей модели рендеринга: появляющийся блоб рисуется сначала на отдельную битмапу, а та уже перекидывается на экран. Где хранится эта битмапа, я пока не знаю, но она должна быть общей для всех визуалов блобов. Второй метод предполагает другой метод кэширования и рендеринга блобов (по одному банку на каждый цветовой набор блоба). Попробую сначала сделать первый способ, а потом, в целях оптимизации, доберусь и до второго.
- Добавлен AiAlphaBank - банк, хранящий квадрантые изображения, залитые полупрозрачными цветами. Они используются для проявления блобов.
- Добавлен Aida::preComp:AiClip. Этот клип служит предварительным буфером для рендеринга полупрозрачных визуалов.
- AiBlobVisual::render() дописан: теперь полупрозрачные визуалы рисуются в клип прекомпозиции, а непрозрачные - напрямую в буфер камеры.

=================================================
================== Aida v0.0.8 ==================
=================================================

Из хвостов у меня осталась реализация другой модели кэширования и рендеринга. В этой версии я могу попробовать сделать наконец ее либо продолжить дописывать игровую логику - добавить игрока, пушки и снаряды, а вместе с ними и модель повреждений.
- Добавлен AiGun.
- Добавлены AiPlayer::attachGun(), AiPlayer::detachGun(). Эти методы всего лишь говорят Игроку, что нужно обновлять положение пушки, но НЕ добавляют пушку в локацию.
- Возникла проблема несвоевременного обновления положения пушки. Решить можно, либо поменяв расположение super.update() в AiPlayer::update(), либо поменяв логику обновления Игрока (для этого нужно добавить в класс локации список объектов, которые должны быть обновлены после физики - с помощью другого метода).
  - Переместил super.update() в другое место AiPlayer::update(). Это несколько сглаживает проблему, но не решает ее. Пушка при движении блоба игрока располагается там, где нужно, однако при соударениях слегка смещается. Это происходит потому, что объекты обновляются ДО расчета столкновений, а рендеринг происходит после расчета столкновений; сначала пушка ставится в нужное место рядом с блобом игрока, затем блоб игрока смещается из-за столкновения с другим объектом, а потом происходит рендеринг блоба в новом положении (после соударения) и пушки в старом положении (до соударения). На глаз неточности заметны не очень сильно, так что откладываю решение на потом.
- Добавлены снаряды. Игрок научился стрелять прицельно и неприцельно в зависимости от положения мыши.
- Отложенное удаление и добавление объектов:
  - добавлены AiLocation::_defRemovingInProgress и AiLocation::defAddingInProgress. Методы addObject() и removeObject() теперь удаляют ноды объектов из списков на _objectsToRemove и _objectsToAdd только когда процесс отложенного добавления (удаления) не запущен;
  - теперь при удалении объекты уничтожаются.
- Исправлена ошибка: теперь при удалении объекта из локации его визуал удаляется из списка _visualsOnScreen.
- Теперь сеттер AiBlob::radius не допускает установку отрицательных значений (из-за этого блобы с отрицательным радиусом не удалялись из локации). При установке нулевого значения масса сразу же устанавливается в ноль, и вызывается onDead().

=================================================
================== Aida v0.0.9 ==================
=================================================

В этой версии добавлю повреждения от снарядов. Стоит принять во внимание, что при столкновении снаряда и повреждаемого объекта скорость последнего должна изменяться в зависимости от bullet.velocityAffectCoef (не меняться вовсе или меняться на величину меньшую, чем обычно). Снаряд при столкновении с объектом тоже не должен отскакивать назад.
- AiLocation::update() больше не использует IAiCircle, вместо этого сущности кастуются к AiBlob (ввиду того, что интерфейс вряд ли будет использоваться в дальнейшем - все будет состоять из блобов).

- Gliding или сопротивление среды?
- Снаряды должны разлетаться по-другому.
- Дерево вместо сеток?

// Сила сопротивления считается так: X0 = world.dragCoef * (body.vel ^ 2) * body.square.
// В 2D-контексте body.square принимается равным диаметру тела.

=================================================
================== Aida v0.0.10 =================
=================================================

TODOs (ближайшие).
- [Done] Добавить velocityAffectCoef.
- [Done] Можно сделать прирост скоростей при раскалывании зависящим от isDynamic блоба (статика дает "быстрые" встречные осколки, динамика - либо не летящие против снаряда, либо летящие с меньшей скоростью, либо летящие вразброс). Можно сделать универсальный коэффициент, определяющий степень влияния скорости снаряда на скорость осколка.
- [Fixed] Осколки от статики получают слишком большую (как казалось, начальную) скорость. Все из-за того, что они сталкиваются с близлежащей статикой. Можно реализовать список исключений по скоростям на уровне обработки столкновений; все объекты в этом списке будут участвовать в столкновениях, но не будут получать "правильного" прироста скоростей.
- [Fixed, partially] Осколки динамического блоба (который в тесте) летят в сторону снаряда (так и задумывалось, конечно). Выглядит неестественно. Нужно либо твикать настройки самого блоба, либо назначать скорости осколков с учетом массы "целого" куска (чем меньше отношение массы осколка к массе самой большой части, тем меньше будет отскок).
- [Solved] Теперь осколки динамического блоба движутся в одном направлении, если имело место деление на более чем две части. Для начала я попробую отказаться от splitAmbienceCoef и привязать его комплементарно к splitBounceMassCoef. Тем не менее, от него еще может быть польза в виде дополнительных твиков, и нужно подумать, каким образом used defined коэффициент будет связан со splitBounceMassCoef. Проблему решил простым назначением splitAmbienceCoef = .3 (или .5) всем блобам по умолчанию. Таким образом, динамические блобы теперь более реалистично разлетаются на кусочки. При желании статике я смогу переназначить этот коэффициент. Split-коэффициенты остались независимыми.
- [Solved] Реализовать свой random() с randomSeed. (Выдернуто из чужой библиотеки.)
- [Done] Массив со случайными направлениями для общего пользования. Это набор ортов, направление которых кратно какому-то шагу; Math.random() используется для получения случайного индекса.
- [Solved] Проблема с AiEntity::set visual(). С одной стороны, visual может назначаться при создании подкласса сущности, с другой стороны - любой пользователь может назначить сущности другой визуал. Как быть - клонировать визуал или нет?

- Изменил максимальную скорость AiPlayer и его gliding, а также его ускорения в классе теста. Очевидно, при прокачке можно будет улучшать параметр вроде "управляемости", которая, в свою очередь, будет определять преднастроенные триады "ускорение - максимальная скорость - глайдинг".
- В AiBlob добавлены splitBounceCoef и splitAmbienceCoef. Методы AiBlob::clonePropertiesFrom() и AiBlob::cloneToSplit() исправлены и дополнены.
- Небольшое изменение в тесте (createScene()).
- AiLocation: добавлены getStaticsInRealRect(), getDynamicsInRealRect(), getEntitiesInRealRect().
- Пробую играться с maxVelocity и elasticity осколка при клонировании (cloneToSplit()). Не помогает.
- Пробую размещать последний осколок не в место снаряда, а куда-то еще (подальше от центра блоба). Ожиданий не оправдал.
- Может, проблема в том, что я меняю скорость статичного блоба, и со временем она накапливается?
- AiBlob::split() исправлен: теперь скорость нулевой части не изменяется (до этого у статичных блобов она накапливалась, "разгоняя" дочерние осколки).
- Опять размещаю осколок в том месте, где был снаряд (не рассчитываю все по радиусам).
- Правлю раскалывание для динамики. Схема такая: нахожу минимальную или среднюю массу осколка (можно приблизительно посчитать по количеству частей), нахожу ее отношение к массе блоба-базы (это parts[0]) и вычитаю полученное из единицы. Если получилось значение меньше нуля, то ограничиваю его нулем. Умножаю полученный коэффициент на коэффициент отскока осколка.
- Добавил splitBounceMassCoef в часть метода AiBlob::split(), отвечающую за назначение скоростей.
- Оптимизирую расчет splitBounceMassCoef.
- Изменил cloneToSplit(): теперь осколки не получают новых split-коэффициентов.
- Дополнил методы AiLocation: getStaticsInRealRect(), getDynamicsInRealRect(), getEntitiesInRealRect(). Теперь они умеют производить дополнительный поиск по баундам сущностей.
- Добавляю velocityAffectCoef.
- AiBlob::split() теперь возвращает массив осколков. Обновление оказалось бесполезным, но оставлю как есть.
- В AiBlob добавил функционал, связанный с velocityAffectCoef. Назначение скоростей происходит прямо в split(). Сделал это по ошибке; если снаряд будет обладать только прямым повреждением, то метод раскалывания вызываться не будет, как и назначение скоростей.
- Функционал, связанный с velocityAffectCoef, переехал в AiLocation::performPhysics().
- Inline-тест.
- Добавлен AiDirections; Aida хранит его подготовленный экземпляр. Теперь AiBlob::split() использует Aida.instance.directions.

=================================================
================== Aida v0.0.11 =================
=================================================

- Молния. Есть два варианта: с кэшированием и без. Без кэширования может быть тормознее, но гораздо проще (как я сейчас вижу). С кэшированием я создаю набор банков AiLineBank с разной длиной линии, а при отрисовке использую отрезки разной длины. При этом, вероятнее всего, кончик искры не сойдется с поверхностью блоба-жертвы, и его придется урезать с помощью круговой маски, взятой из другого банка. Такое же урезание можно использовать при отрисовке самой цепочки искры. И все равно я не уверен, что такой подход даст приемлемые результаты: концы отрезков будут смотреться обрубленными.
  - Был в мыслях и третий вариант - использовать setPixel(), но выигрыш по производительности в этом случае видится мне сомнительным. И он далеко не проще обычного спрайтового метода.
  - Спрайтовый метод тоже имеет подварианты. Можно рисовать спрайт с помощью draw() в битмапдату, а можно накладывать его поверх изображения, т.е. сверху буфера камеры. Последнее, возможно, и будет лучше по производительности (и то я не уверен, что лучше, а не хуже), но имеет недостатки: гробится принцип z-сортировки (и, соответственно, отрисовки), его нужно дополнительно встраивать в уже существующую систему рендеринга, положить что-то сверху молнии - значит запихать поверх всего этого еще один слой.
  - Останавливаюсь на самом обычном спрайте, который рисуется в битмапдату. Если по производительности меня что-то не устроит, буду думать дальше.
  - Каждый блоб сможет владеть объектом молнии, но только одним (как визуалом). При уничтожении блоб должен уничтожить присоединенный блоб молнии.
    - Эффекты клонируемые и неклонируемые?
  - Молния должна добавляться в локацию вместе с блобом-владельцем.
  - Придумал механизм такого добавления. Объект получает массив вложенных объектов, которые нужно добавить в локацию вместе с ним. Это не совсем его потомки в моем привычном понимании, потому что они не наследуют его трансформацию (хоть и могут делать это в updateBehavior()); но некоторые удобства это привнесет - для пушек и молний. Приступаю.
  - AiEntity::clonePropertiesFrom(): визуал будет клонироваться, но перед этим старый визуал должен быть уничтожен. И все это только для поддержки свободной установки визуала после создания подкласса сущности (отменено). Другой подход состоял в том, чтобы запретить многократную установку визуалов, тогда при клонировании создавать копию визуала уже не пришлось бы - тем более что визуалы на данный момент не хранят в себе каких-то дополнительных свойств, за которые можно было бы беспокоиться; они больше похожи на объекты поведения, хранящие фиксированный функционал, основанный на свойствах сущности-владельца. (Применил последний подход.)
  - Теперь нужно реализовать postUpdate() для молнии. Пока что просто буду снэппить молнию к блобу-владельцу.
    - Можно переопределить update() молнии, сделав его пустым, а в postUpdate() использовать super.update().
  - Теперь визуал молнии должен рисовать какую-нибудь простую нелогичную штуку.
  - Молния работает, визуал рисует прямые линии.
  - Делаю нормальную отрисовку молний, чтобы было похоже на искры.
- [Mistake] Вместо рассылки событий onObjectRemove из локации сделаю AiObject::notifyOnRemove:Boolean. Это более простое и эффективное решение, чем реализация событий.
    - Флаг и метод объекта добавил, removeObject() локации дополнил, но использовать для автоматического отсоединения пушки не буду. Отсоединение вызывает removeObject() локации и превращает все в бесконечный цикл. Возиться с этим не буду. Вместо этого внесу ясность: присоединенные объекты (и пушки, и молнии) не должны удаляться из локации вручную; вместо этого необходимо удалять из локации родительский объект.
	- onRemoveFromLocation пушки и молнии будет кидать исключение, если объект присоединен к владельцу.

Progress.
- Поставил AiObject::isUpdatable = false по умолчанию. Протестил - ничего не изменилось, все работает так же. isUpdatable влияет на запуск AiObject::updateDistant(), так что это лишняя нагрузка.
- Добавлены AiLightning и AiLightningVisual.
- Добавлен AiSettings::OPTIMIZE_LIGHTNINGS_PHYS_RECT. Хотя это плохая затея, можно было и без этого.
- AiEntity::set visual () удален, вместо него добавлены AiEntity::setVisual() и AiEntity::clearVisual(). Методы должны вызываться только при создании и уничтожении сущности, соответственно, однако защиты от их неправильного использования я не поставил, чтобы не лечить и не калечить производительность.
- AiEntity::destroy() дописан с использованием setVisual() и clearVisual().
- Конструктор AiBlob дописан с использованием setVisual().
- AiEntity::clonePropertiesFrom() теперь не клонирует визуал.
- AiEntity::clonePropertiesFrom(): настройки максимальной скорости теперь клонируются.
- AiEntity: добавлен _attachedEntities. AiLocation::addObject() и AiLocation::removeObject() теперь добавляет в локацию (удаляет из нее) присоединенные сущности.
- AiPlayer::attachGun() и AiPlayer::detachGun() дописаны. Теперь эти методы добавляют присоединенную сущность в локацию и в _attachedEntities. AiTest004AidaVisuals изменен.
- AiPlayer::detachGun() теперь использует internal AiLocation::removeObject() вместо AiLocation::removeObjectDeferred(). AiPlayer::destroy() не только отсоединяет пушки, но и уничтожает их.
- AiBlob: добавлены _lightning и методы attachLightning() и detachLightning().
- Добавлены AiEntity::enablePostUpdate и postUpdate(). AiLocation::updateEntitiesBehaviors() при подготовке набора обновляемых заполняет набор _entitiesToPostUpdate:Set (добавлен). Метод destroy() локации очищает этот набор. AiLocation::removeObject() удаляет сущность из набора _entitiesToPostUpdate.
- Добавлены AiEntity::set enablePostUpdate() и AiLocation::onPostUpdatableIsFalse().
- AiLightning::update() переопределен в заглушку, а postUpdate() содержит функционал.
- Молния снэппится к владельцу в postUpdate().
- Добавлен AiLightning::targetsMask.
- Добавлен AiLocation::updatingInProgress. Он устанавливается в true в начале update() и сбрасывается в false перед отложенным добавлением и удалением объектов.
- При AiLocation::updatingInProgress == true метод AiLocation::addObject() заменяет себя на addObjectDeferred() (то же с removeObject()).
- AiLightning: добавлена проверка по targetsMask.
- AiGun::onDead() теперь вызывает AiPlayer::detachGun().
- AiLightningVisual::render() теперь рисует искры, а не прямые отрезки.
- AiObject: добавлены _notifyOnRemove:Boolean и onRemoveFromLocation(). Метод AiLocation::removeObject() дополнен.
- AiPlayer: attachGun() и detachGun() используют AiObject::_notifyOnRemove для включения и выключения ошибочного удаления пушки из локации.
  
TODOs (ближайшие). Список перенесен из версии 0.0.10 за исключением выполненных тасков.
- [Done] Молния: графика и поведение.
- [Solved] Проблема с AiEntity::set visual(). С одной стороны, visual может назначаться при создании подкласса сущности, с другой стороны - любой пользователь может назначить сущности другой визуал. Как быть - клонировать визуал или нет?
- [Done] Ввести приоритеты обновления объектов. Это нужно для обновления молнии: она должна располагаться там, где находятся объекты после обновления. Обновление молнии после всех остальных объектов гарантирует, что ее искры будут рисоваться и работать правильно. Кроме того, я избавлюсь от включения проверки на наличие молнии в AiBlob::update().
  - [Partially] Вместо приоритетов добавлю AiEntity::enablePostUpdate:Boolean.
  - [Done] Сделать аксессоры для enablePostUpdate.
  - [Done?] При удалении сущности из локации она должна удаляться также и из _entitiesToPostUpdate.
- [Done] Баг: при уничтожении пушки та не детачится с игрока.
- [Done] AiLocation::removeObject() должен использовать removeObjectDeferred(), если мгоновенное удаление невозможно.
- [Done, dif way] Добавить слушатель сущности onRemove. При удалении сущности из локации она будет иметь возможность детачить себя с владельца (блоба для молнии, игрока для пушки). На данный момент решение сделано с помощью переопределения onDead() пушки.

=================================================
================== Aida v0.0.12 =================
=================================================

TODOs (ближайшие). Список перенесен из версии 0.0.11 за исключением выполненных тасков.
- [Done] Движение AiPlayer в сторону направления главной пушки.
- [Done, canceled] Quadtrees вместо OGrid.

Current.
- Добавил к AiPlayer _isMoving, _acceleration и методы move() и stop(). Дополнил update(), теперь игрок может двигаться вперед.
- Дописываю тест.
- Пробую направлять врагов к блобу игрока.
  - Добавляю локации ссылку на игрока. Меняю addObject(), removeObject().
- Как выяснилось, проблема также в производительности при определении столкновений, поэтому в этой версии нужно реализовать Quadtree.
- Квадродерево: пробую исключить проверку "слишком большого объекта" при движении вглубь дерева. Она нужна только для рутовых нод.
- Пробую заменить Сет на Вектор. Быстрее не стало.
- Резюмирую.
  - Из наблюдений сделал вывод: квадродерево в моей реализации оказывается медленнее сетки OGrid, имеющейся на данный момент. Думаю, даже если я сяду серьезно оптимизировать квад-дерево, я разве что доведу его производительность до уровня сетки. Наверное, проблема квадродерева в том, что на поиск объектов, лежащих внутри прямоугольника, уходит слишком много проверок на пересечение баундов - по доброй пачке на каждый уровень дерева. При добавлении объекта на каждом уровне дерева будет производиться (навскидку) в среднем две с половиной ((4 + 1) / 2) проверки; если дерево шестиуровневое (а в условиях большой локации уровней будет больше), то на добавление одного объекта уйдет 15 проверок.
  - Создание квадродерева в тесте с тысячей объектов занимает около 10 мс, в то время как время создания сетки определялось как нулевое.
  - Даже при плотном скоплении объектов в одном месте дерево ведет себя не лучше сетки.
  - В следующих версиях можно дописать класс сетки, чтобы она была способна подразделять себя.

Progress.
- Добавил к AiPlayer _isMoving, _acceleration и методы move() и stop(). Дополнил update(), теперь игрок может двигаться вперед.
- AiTest004AidaVisuals дополнен.
- Добавлен AiEnemy.
- AiLocation: добавлен _player:AiPlayer, дописаны addObject() и removeObject().
- AiEnemy::update() переопределен: теперь враг всегда стремится к игроку.
- Добавлены классы для квадродерева и соответствующий тест.

=================================================
================== Aida v0.0.13 =================
=================================================

TODOs (ближайшие). Список перенесен из версии 0.0.12 за исключением выполненных тасков.
- [Done] Subdividable OGrid.
- [Done] Разные размеры сеток для статики и динамики (в локации и AiSettings).

Current.
- OGrid: пробую подразделять при слишком большом количестве объектов.
  - Придется ввести класс ячейки. Сейчас используется Set, и искать каждый раз в нем дочернюю сетку мне не захочется.
  - Начинаю OGridSmart, чтобы потом можно было сравнивать производительности разных вариаций сеток.
    - Теперь кроме int-прямоугольника объекта придется хранить еще и real-прямоугольник. Без этого перерегистрация во вложенную сетку окажется невозможной.
	- Правда, можно выделить набор "маленьких объектов", для которых стоит хранить realRect. Все остальные объекты не будут "проваливаться" на нижний уровень сетки и останутся в наборе объектов ячейки в любом случае.
- Возникает ошибка при перерегистрации объекта (скорее всего, крупного, а не мелкого).
  - Сейчас умная сетка всегда будет содержать набор _items.
  - Ошибка исправлена.
- Слишком медленный поиск (из-за того, что getItemsInRealRect()) сетки и collectItemsInRealRect() ячейки не учитывают размер прямоугольника для поиска).
  - Меняю OGridCell::collectItemsInRealRect().

Progress.
- OGrid::getAllItems() переименован в collectAllItems(). Теперь он может принимать набор result.
- Добавлены OGridSmart и OGridCell. Тест для квадродерева дописан. Новая сетка в некоторых случаях способна показывать лучшие результаты, в каких-то - худшие. Все зависит от ситуации и настроек сетки.
- Классы сеток дополнены, добавлен интерфейс IGrid.
- Исправлена ошибка регистрации объекта в OGridCell.
- OGridCell::collectItemsInRealRect() теперь принимает параметр accurateSearch.
- OGridSmart: добавлено свойство _smallRectSize. Метод getItemsInRealRect() использует его, чтобы производить грубый или аккуратный поиск в зависимости от того, каковы размеры области поиска.

=================================================
================== Aida v0.0.14 =================
=================================================

TODOs (ближайшие). Список перенесен из версии 0.0.12 за исключением выполненных тасков.
- [Done] Влияние игрока на скорость снарядов: полная или нулевая.
- Распределенный по времени расчет ИИ врагов.
- Поведение врагов.
- Привязать камеру к игроку (добавить контроллер?).
- Перенести логику управления персонажем из класса теста в класс контроллера.
- Не-камикадзе урон.
- Генераторы, в том числе прямые отрезки, кривые и деревья.
- Кисти с автоназначением z, изменяемым радиусом, цветом, плотностью, эластичностью и другими параметрами блобов.
- Эффекты: постоянные, временные, при повреждении, при столкновении с блобом определенного класса.
  - Клонируемые и не клонируемые. Первые клонируются вместе с блобом-владельцем, т.е. достаются его потомкам (осколкам, например) по наследству. Последние действуют только на главный блоб.
- Взрывы: графика и поведение.
- На данный момент на скорость осколка не очень хорошо влияет рассеяние; вместо простого прибавления к скорости случайного вектора хочется использовать что-то вроде поворота вектора. Для быстрой реализации я планирую использовать вектор с предрассчитанными направлениями.
- Частички для красоты.
- Пройтись по clone() и clonePropertiesFrom() у всех классов и обновить их.
- AiLocation::removeObjectDeferred() уничтожает объект после удаления. Опционально можно было бы и не уничтожать. Пока потребности в этом нет.
  - Решение - добавить флаг AiObject::destroyAfterDeletion.
- velocityAffectCoef: добавить вилку - должно ли мгновенное ускорение объекта зависеть от его массы или не должно (AiSettings, AiBlob::split()).
- Добавить дополнительные свойства AiCamera для оптимизации рендеринга (см. локальные переменные cx и cy в AiBlobVisual::render()).
- Инлайнить внутренние методы локации.
- Можно улучшить AiDirections: использовать PairNumbers (еще не написанный) вместо Point.
- Исправить: скорость нулевого осколка никак не меняется (лучше сделать проверку isDynamic).
- Преграды для молнии (она не должна действовать на цели за стенкой, например).
- Улучшить погоню врагов за игроком.
  - Враги всегда ускоряются в сторону игрока. Ввести расстояние обнаружения? (Возможно, для боссов это и нужно сделать.)
  - Простое ускорение к точке: не учитывается ни скорость врага, ни скорость игрока, поэтому враг не может нападать "по-умному".
- Погоня врагов за игроком (2).
  - Проблемы.
    - Большая кучность: со временем становится сложно определить, сколько врагов гонятся за игроком (они оказываются в одном месте).
	- Низкая производительность: каждый враг рассчитывает расстояние до игрока, затем - собственное ускорение.
  - Возможное решение.
    - Делить врагов на группы. Процесс деления может быть таким.
	  - взять всех врагов внутри области обновления (в текущем шаге локации);
	  - взять первого врага и сделать его лидером новой группы;
	  - найти врагов вокруг лидера (т.е. внутри квадрата);
	  - найти следующего врага, не входящего ни в одну группу и повторить все для него.
	  Определение групп выполнять не каждый кадр.
	- Расчет направления каждым лидером тоже производить не каждый кадр.

Current.
- Наследование скорости снаряда от игрока-владельца пушки.
- [Later] Тест производительности предпросчитанных арктангенсов.
- [Later] Пока попробую сделать просто распределенный по времени расчет ИИ врагов.

Progress.
- AiPlayer: добавлен bulletSpeedInheritanceCoef. AiGun::updateByPlayer() теперь использует этот коэффициент, чтобы определять, насколько сильно зависит ее скорость от скорости игрока.