package dump13.msl.physics.simple {
	import flash.geom.Rectangle;
	
	use namespace smpphys;
	/**
	 * Физическое представление axis aligned прямоугольника, всегда статичное.
	 * Свойства _x и _y используются движком; пользователю доступны лишь локальные смещения прямоугольника в системе координат владельца _owner (и размеры прямоугольника).
	 * @author dump13
	 */
	public class AARectRep extends PhysicRep {
		private var _localX:Number = 0;
		private var _localY:Number = 0;
		private var _x:Number = 0;
		private var _y:Number = 0;
		private var _xSize:Number = 0;
		private var _ySize:Number = 0;
		
		/**
		 * Конструктор axis aligned прямоугольника.
		 * @param	localX Локальная (заданная в системе координат объекта-владельца) координата <code>x</code> физпредставления.
		 * @param	localY Локальная (заданная в системе координат объекта-владельца) координата <code>y</code> физпредставления.
		 * @param	xSize Размер прямоугольника по оси <code>x</code>.
		 * @param	ySize Размер прямоугольника по оси <code>y</code>.
		 */
		public function AARectRep (localX:Number, localY:Number, xSize:Number, ySize:Number) {
			_localX = localX;
			_localY = localY;
			_xSize = xSize;
			_ySize = ySize;
		}
		
		/**
		 * Заставляет этот прямоугольник определить новые координаты <code>x</code> и <code>y</code> по соответствующим координатам своего владельца (если тот назначен).
		 */
		override public function snapToOwner ():void {
			if (!_owner) return;
			_x = _localX + _owner.x;
			_y = _localY + _owner.y;
			reregister ();
		}
		
		/**
		 * Метод создаёт исключение, запрещая таким образом изменять координаты владельца. AA-прямоугольник является статичным, поэтому его глобальные координаты нельзя менять, а необходимость влиять на координаты владельца отпадает.
		 */
		override public function affectOwner ():void {
			throw new Error ("It's not allowed to change AARectRep's coords, so there should be no need to affect owner.");
		}
		
		// =======================================================
		// ===================== ACCESSORS =======================
		// =======================================================
		
		/**
		 * Ограничивающий прямоугольник.
		 */
		override public function get rect ():Rectangle { return new Rectangle (_x, _y, _xSize, _ySize); }
		
		/**
		 * Является ли объект статичным (значение всегда <code>true</code>, AA-прямоугольник статичен).
		 */
		override public function get isStatic ():Boolean { return true; }
		
		/**
		 * Глобальная (заданная в системе координат локации) координата <code>x</code> физпредставления. Глобальные координаты физпредставления определяется по координатам его владельца и локальному смещению данного физпредставления в системе координат владельца.
		 */
		public function get x ():Number { return _x; }
		
		/**
		 * Глобальная (заданная в системе координат локации) координата <code>y</code> физпредставления. Глобальные координаты физпредставления определяется по координатам его владельца и локальному смещению данного физпредставления в системе координат владельца.
		 */
		public function get y ():Number { return _y; }
		
		/**
		 * Локальная (заданная в системе координат объекта-владельца) координата <code>x</code> физпредставления.
		 */
		public function get localX ():Number { return _localX; }
		public function set localX (value:Number):void {
			_localX = value;
			snapToOwner ();
		}
		
		/**
		 * Локальная (заданная в системе координат объекта-владельца) координата <code>y</code> физпредставления.
		 */
		public function get localY ():Number { return _localY; }
		public function set localY (value:Number):void {
			_localY = value;
			snapToOwner ();
		}
		
		/**
		 * Размер AA-прямоугольника по оси <code>x</code>.
		 */
		public function get xSize ():Number { return _xSize; }
		public function set xSize (value:Number):void {
			_xSize = value;
			reregister ();
		}
		
		/**
		 * Размер AA-прямоугольника по оси <code>y</code>.
		 */
		public function get ySize ():Number { return _ySize; }
		public function set ySize (value:Number):void {
			_ySize = value;
			reregister ();
		}
		
	}

}