package dump13.msl.physics.simple {
	
	/**
	 * Интерфейс владельца физического представления - объекта, использующего функционал <code>PhysicRep</code>.
	 * @author dump13
	 */
	public interface IPhysicsOwner {
		/**
		 * Координата <code>x</code> владельца физического представления.
		 */
		function get x ():Number;
		function set x (value:Number):void;
		
		/**
		 * Координата <code>y</code> владельца физического представления.
		 */
		function get y ():Number;
		function set y (value:Number):void;
		
		/**
		 * Устанавливает координаты этого объекта (владельца физпредставления), не производя обновление координат самого физпредставления. Параметры, переданные как <code>NaN</code>, не устанавливаются.
		 * @param	x Значение, в которое необходимо установить координату <code>x</code> владельца физпредставления.
		 * @param	y Значение, в которое необходимо установить координату <code>y</code> владельца физпредставления.
		 * @param	z Значение, в которое необходимо установить координату <code>z</code> владельца физпредставления.
		 */
		function setCoordsSilent (x:Number = NaN, y:Number = NaN, z:Number = NaN):void;
	}
	
}