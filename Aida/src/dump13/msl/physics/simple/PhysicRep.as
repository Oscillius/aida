package dump13.msl.physics.simple {
	import flash.geom.Rectangle;
	
	use namespace smpphys;
	
	/**
	 * Физическое представление.
	 * @author dump13
	 */
	public class PhysicRep {
		protected var _manager:PhysicsManager;
		protected var _owner:IPhysicsOwner;
		
		/**
		 * Имя физпредставления.
		 */
		public var name:String;
		
		/**
		 * Должен ли менеджер физики генерировать оповещения о столкновениях с этим объектом. Информация о столкновении хранится в физических представлениях (участниках столкновения) - подклассах этого базового класса.
		 */
		public var enableNotifications:Boolean = false;
		
		
		/**
		 * Конструктор физического представления.
		 */
		public function PhysicRep () {
			
		}
		
		/**
		 * Подключает физпредставление к менеджеру физики.
		 * @param	manager Менеджер физики, к которому необходимо подключить текущее физпредставление.
		 */
		smpphys function connectToManager (manager:PhysicsManager):void {
			_manager = manager;
		}
		
		/**
		 * Отключает физпредставление от менеджера физики, к которому он в данный момент подключен.
		 */
		smpphys function shutdownFromManager ():void {
			_manager = null;
		}
		
		/**
		 * Подключает физпредставление к объекту-владельцу.
		 * @param	owner Владелец, к которому необходимо подключить текущее физпредставление.
		 */
		public function connect (owner:IPhysicsOwner):void {
			_owner = owner;
			//snapToOwner ();
		}
		
		/**
		 * Отключает физпредставление от объекта-владельца, к которому оно в данный момент подключено.
		 */
		public function shutdown ():void {
			_owner = null;
		}
		
		/**
		 * Заставляет владельца текущего физпредставления (<code>owner</code>) принять новое положение, определяемое этим физпредставлением. Вызывать, например, из менеджера физики.
		 */
		public function affectOwner ():void {
			throw new Error ("Not implemented.");
		}
		
		/**
		 * Заставляет текущее физпредставление определить своё новое положение по положению своего владельца <code>owner</code>.
		 */
		public function snapToOwner ():void {
			throw new Error ("Not implemented.");
		}
		
		/**
		 * Выполняет перерегистрацию данного физпредставления в менеджере физики, если тот найден.
		 */
		protected function reregister ():void {
			if (_manager) {
				_manager.reregister (this);
			}
		}
		
		// =======================================================
		// ===================== ACCESSORS =======================
		// =======================================================
		
		/**
		 * Ограничивающий прямоугольник физпредставления. Геттер должен быть переопределён в потомках.
		 */
		public function get rect ():Rectangle { throw new Error ("Not implemented."); return null; }
		
		/**
		 * Является ли физпредставление статичным. Если <code>false</code>, то физпредставление динамическое и при столкновениях оно будет перемещаться.
		 */
		public function get isStatic ():Boolean { throw new Error ("Not implemented."); return false; }
		
		/**
		 * Объект-владелец текущего физпредставления.
		 */
		public function get owner ():IPhysicsOwner { return _owner; }
		
	}

}