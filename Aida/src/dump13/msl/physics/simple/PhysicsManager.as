package dump13.msl.physics.simple {
	/**
	 * Базовый класс менеджера физики. Определяет интерфейс для всех подобных менеджеров физики.
	 * @author dump13
	 */
	public class PhysicsManager {
		
		/**
		 * Конструктор менеджера физики.
		 */
		public function PhysicsManager () {
			
		}
		
		/**
		 * Добавляет физпредставление в этот менеджер физики. Метод должен быть переопределён в субклассах.
		 * @param	rep Физическое представление, которое необходимо добавить.
		 */
		public function add (rep:PhysicRep):void {
			throw new Error ("Abstract method.");
		}
		
		/**
		 * Удаляет физпредставление из этого менеджера физики. Метод должен быть переопределён в субклассах.
		 * @param	rep Физическое представление, которое необходимо удалить.
		 */
		public function remove (rep:PhysicRep):void {
			throw new Error ("Abstract method.");
		}
		
		/**
		 * Очищает менеджер физики, удаляя из него все физические представления. Метод должен быть переопределён в субклассах.
		 */
		public function clear ():void {
			throw new Error ("Abstract method.");
		}
		
		/**
		 * Производит перерегистрацию физического представления в этом менеджере физики. Метод должен быть переопределён в субклассах.
		 * @param	rep Физическое представление, которое необходимо перерегистрировать.
		 */
		public function reregister (rep:PhysicRep):void {
			throw new Error ("Abstract method.");
		}
		
		/**
		 * Обновляет менеджер физики - находит пересекающиеся физпредставления и расталкивает их, если необходимо. Метод должен быть переопределён в субклассах.
		 */
		public function performPhysics ():void {
			throw new Error ("Abstract method.");
		}
		
	}

}