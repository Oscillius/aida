package dump13.msl.physics.simple {
	import flash.geom.Rectangle;
	
	use namespace smpphys;
	/**
	 * Физпредставление круглого объекта, всегда является динамическим.
	 * Свойства _x и _y используются менеджером физики и недоступны для внешнего использования. Предполагается, что круг расположен в центре объекта, так что извне можно задать только его радиус и массу.
	 * @author dump13
	 */
	public class CircleRep extends PhysicRep {
		private var _x:Number = 0;
		private var _y:Number = 0;
		private var _radius:Number = 0;
		private var _mass:Number = 0;
		
		private var _collStartX:Number = 0;
		private var _collStartY:Number = 0;
		private var _collShiftX:Number = 0;
		private var _collShiftY:Number = 0;
		
		
		/**
		 * Конструктор круглого физпредставления.
		 * @param	radius Радиус физпредставления.
		 * @param	mass Масса физпредставления. Влияет на степень расталкивания при столкновениях с другими динамическими телами.
		 */
		public function CircleRep (radius:Number, mass:Number = 1) {
			_radius = radius;
			_mass = mass;
		}
		
		/**
		 * Устанавливает координаты объекта-владельца (если он назначен) в значения, равные координатам текущего физпредставления.
		 */
		override public function affectOwner ():void {
			if (!_owner) return;
			_owner.setCoordsSilent (_x, _y);
		}
		
		/**
		 * Устанавливает глобальные координаты текущего физпредставления в значения, равные координатам объекта-владельца (если тот имеется).
		 */
		override public function snapToOwner ():void {
			if (!_owner) return;
			_x = _owner.x;
			_y = _owner.y;
			reregister ();
		}
		
		/**
		 * [For internal usage]. Устанавливает глобальные координаты физпредставления, а вместе с ними и координаты объекта-владельца. Используется только менеджером физики.
		 * @param	x Значение, в которое нужно установить координату <code>x</code> физпредставления и его владельца.
		 * @param	y Значение, в которое нужно установить координату <code>y</code> физпредставления и его владельца.
		 */
		smpphys function setCoords (x:Number, y:Number):void {
			_x = x;
			_y = y;
			reregister ();
			// don't forget to update owner's coords
			affectOwner ();
		}
		
		// =========================================================
		// ============== COLLISIONS NOTIFICATIONS =================
		// =========================================================
		
		/**
		 * [For internal usage]. Передаёт физпредставлению информацию о столкновении. Внутри представления она не используется, но внешние слушатели столкновений, использующие менеджер физики, могут получить её посредством геттеров этого физпредставления.
		 * @param	startX Координата <code>x</code> круга до начала столкновения.
		 * @param	startY Координата <code>y</code> круга до начала столкновения.
		 * @param	shiftX Смещение круга по оси <code>x</code>, произошедшее при расталкивании.
		 * @param	shiftY Смещение круга по оси <code>y</code>, произошедшее при расталкивании.
		 */
		smpphys function setCollisionInfo (startX:Number, startY:Number, shiftX:Number, shiftY:Number):void {
			_collStartX = startX;
			_collStartY = startY;
			_collShiftX = shiftX;
			_collShiftY = shiftY;
		}
		
		// =======================================================
		// ===================== ACCESSORS =======================
		// =======================================================
		
		/**
		 * Ограничивающий прямоугольник круглого физпредставления. Определяется его глобальными координатами и радиусом.
		 */
		override public function get rect ():Rectangle {
			var d:Number = _radius + _radius;
			return new Rectangle (_x - _radius, _y - _radius, d, d);
		}
		
		/**
		 * Является ли физпредставление статичным (всегда <code>false</code>, круглые физпредставления - динамические).
		 */
		override public function get isStatic ():Boolean { return false; }
		
		/**
		 * Глобальная координата <code>x</code> круга, определяется положением его владельца.
		 */
		public function get x ():Number { return _x; }
		
		/**
		 * Глобальная координата <code>y</code> круга, определяется положением его владельца.
		 */
		public function get y ():Number { return _y; }
		
		/**
		 * Радиус круга. Установка нового значения приведёт к перерегистрации круга в менеджере физики.
		 */
		public function get radius ():Number { return _radius; }
		public function set radius (value:Number):void {
			_radius = value;
			reregister ();
		}
		
		/**
		 * Масса круга. Влияет на степень расталкивания, применяемого к этому объекту при столкновениях.
		 */
		public function get mass ():Number { return _mass; }
		public function set mass (value:Number):void {
			_mass = value;
		}
		
		/**
		 * Координата <code>x</code> круга до начала столкновения. Свойство становится актуально перед тем, как менеджер начинает оповещать слушателей о столкновении.
		 */
		public function get collStartX ():Number { return _collStartX; }
		/**
		 * Координата <code>y</code> круга до начала столкновения. Свойство становится актуально перед тем, как менеджер начинает оповещать слушателей о столкновении.
		 */
		public function get collStartY ():Number { return _collStartY; }
		/**
		 * Смещение круга по оси <code>x</code>, произошедшее при расталкивании. Свойство становится актуально перед тем, как менеджер начинает оповещать слушателей о столкновении.
		 */
		public function get collShiftX ():Number { return _collShiftX; }
		/**
		 * Смещение круга по оси <code>y</code>, произошедшее при расталкивании. Свойство становится актуально перед тем, как менеджер начинает оповещать слушателей о столкновении.
		 */
		public function get collShiftY ():Number { return _collShiftY; }
		
	}

}