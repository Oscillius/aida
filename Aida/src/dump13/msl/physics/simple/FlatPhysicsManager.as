package dump13.msl.physics.simple {
	import dump13.msl.types.Set;
	import dump13.msl.utils.Grid;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	
	use namespace smpphys;
	/**
	 * Менеджер плоской физики, оперирующий только двумя физпредставлениями: статичным axis aligned прямоугольником <code>AARectRep</code> (из таких объектов делаются сами игровые уровни) и динамическим кругом <code>CircleRep</code> (подходит для персонажей).
	 * 
	 * <p>Статичные и динамические объекты регистрируются в двух отдельных оптимизационных сетках; благодаря этому при определении столкновений рассматриваются только пары объектов типа "динамика - динамика" или "динамика - статика" (статичные объекты на столкновения между собой не проверяются).</p>
	 * <p>Физическое пространство менеджера делится на квадратные ячейки; физические представления регистрируются в этих ячейках по своим ограничивающим прямоугольникам.</p>
	 * <p>В Pusher v0.0.3 добавлена простая реализация оповещений о столкновениях. Оповещения происходят только при столкновениях с объектами, чьи свойства <code>enableNotifications</code> установлены в <code>true</code>; при оповещениях срабатывают методы-слушатели, которые были зарегистрированы в менеджере физики с помощью <code>addCollisionListener()</code>. Методы-слушатели не должны принимать обязательных аргументов. Вместо того, чтобы рассылать информацию о столкновении в виде отдельного объекта (например, события), менеджер физики сохраняет эту информацию в своём экземпляре и в экземплярах физических представлений, участвовавших в столкновении.</p>
	 * @author dump13
	 */
	public class FlatPhysicsManager extends PhysicsManager {
		private var _staticsGrid:Grid;
		private var _dynamicsGrid:Grid;
		private var _dynamicsSet:Set = new Set ();
		
		/**
		 * @private
		 * [Added in Pusher v0.0.3]. Список методов-слушателей, срабатывающих при столкновении объектов. Это методы, не принимающие аргументов.
		 */
		private var _listeners:Vector.<Function> = new Vector.<Function> ();
		private var _collRepFirst:PhysicRep;
		private var _collRepSecond:PhysicRep;
		
		
		/**
		 * Конструктор менеджера плоской физики. Для удобства для создания экземпляра менеджера можно воспользоваться методом <code>makeByRealSizes()</code>.
		 * @param	intSizeX Размер физического пространства по оси <code>x</code>, в ячейках.
		 * @param	intSizeY Размер физического пространства по оси <code>y</code>, в ячейках.
		 * @param	cellSize Действительный размер ячейки в условных единицах (например, в метрах).
		 */
		public function FlatPhysicsManager (intSizeX:int, intSizeY:int, cellSize:Number) {
			_staticsGrid = new Grid (intSizeX, intSizeY, cellSize, 0, 0);
			_dynamicsGrid = new Grid (intSizeX, intSizeY, cellSize, 0, 0);
			_staticsGrid.name = "staticsGrid";
			_dynamicsGrid.name = "dynamicsGrid";
		}
		
		/**
		 * Создаёт и возвращает новый менеджер плоской физики, используя размеры физического пространства и требуемый размер ячейки.
		 * @param	realSizeX Действительный размер физического пространства по оси <code>x</code>, в условных единицах (например, в метрах).
		 * @param	realSizeY Действительный размер физического пространства по оси <code>y</code>, в условных единицах (например, в метрах).
		 * @param	cellSize Размер квадратных ячеек, на которые будет поделено физическое пространство.
		 * @return Новый экземпляр менеджера плоской физики.
		 */
		public static function makeByRealSizes (realSizeX:Number, realSizeY:Number, cellSize:Number):FlatPhysicsManager {
			var ix:int = Math.floor (realSizeX / cellSize);
			var iy:int = Math.floor (realSizeY / cellSize);
			if (ix * cellSize < realSizeX) ix++;
			if (iy * cellSize < realSizeY) iy++;
			return new FlatPhysicsManager (ix, iy, cellSize);
		}
		
		/**
		 * Добавляет физическое представление к менеджеру физики.
		 * @param	rep Физпредставление, которое необходимо добавить.
		 */
		override public function add (rep:PhysicRep):void {
			rep.connectToManager (this);
			
			if (!rep.isStatic) _dynamicsSet.add (rep);
			var g:Grid = rep.isStatic? _staticsGrid : _dynamicsGrid;
			g.addItem (rep, g.realToIntRect (rep.rect));
		}
		
		/**
		 * Удаляет физическое представление из менеджера физики.
		 * @param	rep Физпредставление, которое необходимо удалить.
		 */
		override public function remove (rep:PhysicRep):void {
			if (!rep.isStatic) _dynamicsSet.remove (rep);
			var g:Grid = rep.isStatic? _staticsGrid : _dynamicsGrid;
			g.removeItem (rep);
			
			rep.shutdownFromManager ();
		}
		
		/**
		 * Удаляет из менеджера все физические представления.
		 */
		override public function clear ():void {
			var i:Object;
			// Удаляем все статичные объекты.
			var reps:Set = _staticsGrid.getAllItems ();
			for (i in reps) remove (PhysicRep (i));
			// Удаляем всю динамику.
			//reps = _dynamicsSet.clone ();		// Это если не будет работать.
			reps = _dynamicsSet;
			for (i in reps) remove (PhysicRep (i));
		}
		
		/**
		 * Производит перерегистрацию физического представления в менеджере. Перерегистрация производится, только если физпредставление уже зарегистрировано.
		 * @param	rep Физпредставление, которое необходимо перерегистрировать.
		 */
		override public function reregister (rep:PhysicRep):void {
			var g:Grid = rep.isStatic? _staticsGrid : _dynamicsGrid;
			if (!g.hasItem (rep)) return;
			g.removeItem (rep);
			g.addItem (rep, g.realToIntRect (rep.rect));
		}
		
		/**
		 * Обновляет менеджер физики - находит пересекающиеся физпредставления и расталкивает их, если необходимо.
		 */
		override public function performPhysics ():void {
			var i:*, j:*;
			var nbs:Set;
			
			for (i in _dynamicsSet) {
				var curcircle:CircleRep = i as CircleRep;
				if (!curcircle) throw new Error ("Unknown dynamics.");
				
				// v1.
				//var rect:Rectangle = _dynamicsGrid.getItemIntRect (i);
				//var largeRect:Rectangle = new Rectangle (rect.x - 1, rect.y - 1, rect.width + 2, rect.height + 2);
				//nbs = _dynamicsGrid.getItemsInRect (largeRect);
				//nbs.remove (i);
				
				// v0.
				performPhysicsFor (curcircle);
				/*nbs = _dynamicsGrid.getNeighbours (i);
				for (j in nbs) {
					var nbcircle:CircleRep = j as CircleRep;
					if (!nbcircle) throw new Error ("Unknown dynamics.");
					collideCircles (curcircle, nbcircle);
				}
				
				nbs = _staticsGrid.getItemsInRect (_dynamicsGrid.getItemIntRect (i));
				for (j in nbs) {
					if (j is AARectRep) {
						var nbrect:AARectRep = j as AARectRep;
						collideCircleAndRect (curcircle, nbrect);
					}
				}*/
			}
		}
		
		/**
		 * [Added in Pusher v0.0.2]. Находит пересечения с соседями только для физпредставления <code>dyn</code>.
		 * @param	dyn Динамическое физпредставление круга.
		 */
		public function performPhysicsFor (dyn:CircleRep):void {
			var i:Object;
			var nbs:Set;
			
			nbs = _dynamicsGrid.getNeighbours (dyn);
			for (i in nbs) {
				var nbcircle:CircleRep = i as CircleRep;
				if (!nbcircle) throw new Error ("Unknown dynamics.");
				collideCircles (dyn, nbcircle);
			}
			
			nbs = _staticsGrid.getItemsInRect (_dynamicsGrid.getItemIntRect (dyn));
			for (i in nbs) {
				var nbrect:AARectRep = i as AARectRep;
				if (nbrect) {
					collideCircleAndRect (dyn, nbrect);
				}
			}
		}
		
		// =========================================================
		// ============== COLLISIONS NOTIFICATIONS =================
		// =========================================================
		
		/**
		 * Регистрирует слушатель столкновений в менеджере физики. Метод-слушатель не должен принимать обязательных аргументов.
		 * @param	listener Метод-слушатель, который будет срабатывать при возникновении столкновений.
		 */
		public function addCollisionListener (listener:Function):void {
			if (_listeners.indexOf (listener) >= 0) {
				throw new Error ("Listener already registered.");
			}
			_listeners.push (listener);
		}
		
		/**
		 * Удаляет слушатель столкновений из менеджера физики.
		 * @param	listener Метод-слушатель, регистрацию которого необходимо снять.
		 */
		public function removeCollisionListener (listener:Function):void {
			var index:int = _listeners.indexOf (listener);
			if (index < 0) {
				throw new Error ("Listener not found.");
			}
			_listeners.splice (index, 1);
		}
		
		/**
		 * Удаляет все слушатели столкновений из менеджера физики.
		 */
		public function clearCollisionListeners ():void {
			_listeners.length = 0;
		}
		
		/**
		 * Возвращает <code>true</code>, если указанный слушатель зарегистрирован в менеджере физики.
		 * @param	listener Метод-слушатель, наличие которого в менеджере физики необходимо проверить.
		 * @return <code>true</code>, если слушатель зарегистрирован, иначе - <code>false</code>.
		 */
		public function hasCollisionListener (listener:Function):Boolean {
			return _listeners.indexOf (listener) >= 0;
		}
		
		// =========================================================
		// =================== RESIZING GRIDS ======================
		// =========================================================
		
		/**
		 * Создаёт новые оптимизационные сетки с указанными размерами и производит перерегистрацию объектов в них.
		 * @param	intSizeX Новый размер оптимизационных сеток по оси <code>x</code>.
		 * @param	intSizeY Новый размер оптимизационных сеток по оси <code>y</code>.
		 * @param	cellSize Новый размер ячеек сеток.
		 */
		public function resize (intSizeX:int, intSizeY:int, cellSize:Number):void {
			var i:*;
			var staticsSet:Set = _staticsGrid.getItemsInRect (_staticsGrid.intRect);
			var dynamicsSet:Set = _dynamicsSet.clone ();
			_dynamicsSet.clear ();
			_staticsGrid.clear ();
			_dynamicsGrid.clear ();
			_staticsGrid = new Grid (intSizeX, intSizeY, cellSize, 0, 0);
			_dynamicsGrid = new Grid (intSizeX, intSizeY, cellSize, 0, 0);
			for (i in staticsSet) add (PhysicRep (i));
			for (i in dynamicsSet) add (PhysicRep (i));
		}
		
		// =========================================================
		// ============ CIRCLE & RECTANGLE COLLISION ===============
		// =========================================================
		
		/**
		 * @private
		 * Определяет, пересекаются ли окружность и прямоугольник; если они пересекаются, расталкивает их.
		 * @param	circle Круг.
		 * @param	rect AA-прямоугольник.
		 */
		private function collideCircleAndRect (circle:CircleRep, rect:AARectRep):void {
			var offset:Point = circleAndRectCollision (circle, rect);
			if (offset) {
				// Storing the collision info if necessary.
				var numListeners:int = _listeners.length;
				if (numListeners) {
					var cx:Number = circle.x;
					var cy:Number = circle.y;
				}
				// Setting the coords.
				circle.setCoords (circle.x + offset.x, circle.y + offset.y);
				// Notifying about collision.
				if (numListeners) {
					_collRepFirst = circle;
					_collRepSecond = rect;
					circle.setCollisionInfo (cx, cy, offset.x, offset.y);
					for (var n:int = 0; n < numListeners; n++) {
						_listeners[n]();
					}
				}
			}
		}
		
		/**
		 * @private
		 * Определяет, столкнулись ли окружность и прямоугольник. Если столкновение произошло, возвращает смещение круга, иначе - <code>null</code>.
		 * @param	circle Круг, который нужно проверить на столкновения.
		 * @param	rect AA-прямоугольник, который нужно проверить на столкновения.
		 * @return Смещение круга либо <code>null</code>, если физпредставления не пересекаются.
		 */
		private function circleAndRectCollision (circle:CircleRep, rect:AARectRep):Point {
			if (!rect) throw new Error ("Block is null.");
			var rad:Number = circle.radius;
			var cx:Number = circle.x;
			var cy:Number = circle.y;
			var rleft:Number = rect.x;
			var rtop:Number = rect.y;
			var rright:Number = rleft + rect.xSize;
			var rbottom:Number = rtop + rect.ySize;
			var cleft:Number = cx - rad;
			var cright:Number = cx + rad;
			var ctop:Number = cy - rad;
			var cbottom:Number = cy + rad;
			// Эта проверка не нужна, потом надо ее убрать.
			if (cright <= rleft || cleft >=rright || cbottom < rtop || ctop >= rbottom) return null;
			// Проверка ниже обязательна.
			var penetr:Number = 0;
			if (cx < rleft) {
				if (cy < rtop) {
					return circleAndRectCornerCollision (cx, cy, rleft, rtop, rad);
				}
				if (cy > rbottom) {
					return circleAndRectCornerCollision (cx, cy, rleft, rbottom, rad);
				}
				penetr = cx + rad - rleft;
				if (penetr < 0) return null;
				return new Point ( -penetr, 0);
			}
			if (cx > rright) {
				if (cy < rtop) {
					return circleAndRectCornerCollision (cx, cy, rright, rtop, rad);
				}
				if (cy > rbottom) {
					return circleAndRectCornerCollision (cx, cy, rright, rbottom, rad);
				}
				penetr = rright + rad - cx;
				if (penetr < 0) return null;
				return new Point (penetr, 0);
			}
			if (cy < rtop) {
				penetr = cy + rad - rtop;
				if (penetr < 0) return null;
				return new Point (0, -penetr);
			}
			penetr = rbottom + rad - cy;
			if (penetr < 0) return null;
			return new Point (0, penetr);
		}
		
		/**
		 * @private
		 * Возвращает смещение круга при столкновении с вершиной прямоугольника или <code>null</code>, если столкновения нет.
		 * @return Смещение, которое необходимо применить к кругу, или <code>null</code>, если пересечения нет.
		 */
		private function circleAndRectCornerCollision (circleX:Number, circleY:Number, cornerX:Number, cornerY:Number, rad:Number):Point {
			var dx:Number = cornerX - circleX;
			var dy:Number = cornerY - circleY;
			var sqdist:Number = dx * dx + dy * dy;
			var sqrad:Number = rad * rad;
			if (sqdist >= sqrad) return null;
			var dist:Number = Math.sqrt (sqdist);
			var ratio:Number = (rad - dist) / dist;
			return new Point ( -dx * ratio, -dy * ratio);
		}
		
		// =========================================================
		// ============== CIRCLE & CIRCLE COLLISION ================
		// =========================================================
		
		private function collideCircles (c0:CircleRep, c1:CircleRep):void {
			var dx:Number = c1.x - c0.x;
			var dy:Number = c1.y - c0.y;
			var sumrad:Number = c1.radius + c0.radius;
			var dist:Number = Math.sqrt (dx * dx + dy * dy);
			var penetr:Number = sumrad - dist;
			if (penetr > 0) {
				var ratio:Number = penetr / dist;
				var xx:Number = dx * ratio;
				var yy:Number = dy * ratio;
				var t1:Number = c0.mass / (c0.mass + c1.mass);
				var xx1:Number = xx * t1;
				var yy1:Number = yy * t1;
				var xx0:Number = xx1 - xx;
				var yy0:Number = yy1 - yy;
				// Remembering collision info if necessary.
				var numListeners:int = _listeners.length;
				if (numListeners) {
					var sx0:Number = c0.x;
					var sy0:Number = c0.y;
					var sx1:Number = c1.x;
					var sy1:Number = c1.y;
				}
				// Setting the coords.
				c0.setCoords (c0.x + xx0, c0.y + yy0);
				c1.setCoords (c1.x + xx1, c1.y + yy1);
				// Notifying the circles about collision.
				if (numListeners) {
					_collRepFirst = c0;
					_collRepSecond = c1;
					c0.setCollisionInfo (sx0, sy0, xx0, yy0);
					c1.setCollisionInfo (sx1, sy1, xx1, yy1);
					for (var n:int = 0; n < numListeners; n++) {
						_listeners[n]();
					}
				}
			}
		}
		
		// =========================================================
		// ======================= HELPERS =========================
		// =========================================================
		
		/**
		 * Возвращает набор динамических физпредставлений, находящихся внутри переданного прямоугольника с действительными координатами.
		 * @param	realRect Прямоугольник с действительными координатами и размерами (например, указанными в метрах), определяющий область, в которой следует вести поиск динамических физпредставлений.
		 * @return Множество динамических физпредставлений, оказавшихся внутри прямоугольника <code>realRect</code>.
		 */
		public function getDynamicsInRealRect (realRect:Rectangle):Set {
			return _dynamicsGrid.getItemsInRect (_dynamicsGrid.realToIntRect (realRect));
		}
		
		/**
		 * [Added in Pusher v0.0.4]. Возвращает набор статичных физпредставлений, находящихся внутри переданного прямоугольника с действительными координатами.
		 * @param	realRect Прямоугольник с действительными координатами и размерами (например, указанными в метрах), определяющий область, в которой следует вести поиск статичных физпредставлений.
		 * @return Множество статичных физпредставлений, оказавшихся внутри прямоугольника <code>realRect</code>.
		 */
		public function getStaticsInRealRect (realRect:Rectangle):Set {
			return _staticsGrid.getItemsInRect (_staticsGrid.realToIntRect (realRect));
		}
		
		/**
		 * [Added in Pusher v0.0.4]. Возвращает набор всех физпредставлений, находящихся внутри переданного прямоугольника с действительными координатами.
		 * @param	realRect Прямоугольник с действительными координатами и размерами (например, указанными в метрах), определяющий область, в которой следует вести поиск физпредставлений.
		 * @return Множество физпредставлений, оказавшихся внутри прямоугольника <code>realRect</code>.
		 */
		public function getRepsInRealRect (realRect:Rectangle):Set {
			var intRect:Rectangle = _staticsGrid.realToIntRect (realRect);
			var result:Set = _staticsGrid.getItemsInRect (intRect);
			result.unite (_dynamicsGrid.getItemsInRect (intRect));
			return result;
		}
		
		/**
		 * Возвращает прямоугольник с действительными координатами, ограничивающий окружность с центром в <code>(centerX, centerY)</code> и радиусом <code>radius</code>.
		 * @param	centerX Координата <code>x</code> центра окружности.
		 * @param	centerY Координата <code>y</code> центра окружности.
		 * @param	radius Радиус окружности.
		 * @return
		 */
		public function getCircleRealBounds (centerX:Number, centerY:Number, radius:Number):Rectangle {
			return new Rectangle (centerX - radius, centerY - radius, radius += radius, radius);
		}
		
		// =======================================================
		// ===================== ACCESSORS =======================
		// =======================================================
		
		/**
		 * Размер физического пространства по оси <code>x</code>, выраженный в ячейках оптимизационной сетки.
		 */
		public function get intSizeX ():int { return _staticsGrid.intWidth; }
		/**
		 * Размер физического пространства по оси <code>y</code>, выраженный в ячейках оптимизационной сетки.
		 */
		public function get intSizeY ():int { return _staticsGrid.intHeight; }
		/**
		 * Размер ячейки оптимизационной сетки.
		 */
		public function get cellSize ():Number { return _staticsGrid.cellSize; }
		
		/**
		 * Первое физпредставление, участвующее в столкновении. Этот геттер всегда возвращает динамическое физпредставление. Например, если произошло столкновение динамики и статики, по этому геттеру можно будет получить динамическое представление. Это свойство можно использовать в методах-слушателях столкновений, добавленных в менеджер с помощью <code>addCollisionListener()</code>.
		 */
		public function get collRepFirst ():PhysicRep { return _collRepFirst; }
		/**
		 * Второге физпредставление, участвующее в столкновении. При столкновении динамики и статики этот геттер всегда будет возвращать статическое физпредставление. Это свойство можно использовать в методах-слушателях столкновений, добавленных в менеджер с помощью <code>addCollisionListener()</code>.
		 */
		public function get collRepSecond ():PhysicRep { return _collRepSecond; }
		
	}

}