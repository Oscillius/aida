package dump13.msl.utils {
	import dump13.msl.types.Map;
	import dump13.msl.types.PairInt;
	import dump13.msl.types.Set;
	import flash.geom.Rectangle;
	/**
	 * Оптимизационная квадратная сетка, поддерживающая полупространства с отрицательными координатами.
	 * @author dump13
	 */
	public class OGrid implements IGrid {
		private var _cellSize:Number = 0;
		private var _items:Object/*.<String : Set>*/ = new Object ();
		private var _rectsByItems:Map/*<Object : Rectangle>*/ = new Map ();
		
		
		public function OGrid (cellSize:Number) {
			_cellSize = cellSize;
		}
		
		/**
		 * Очищает сетку, удаляя из неё все зарегистрированные объекты.
		 */
		[Inline]
		public final function clear ():void {
			for (var cellID:String in _items) {
				var itemsSet:Set = _items[cellID] as Set;
				if (itemsSet) {
					itemsSet.clear ();
				}
				delete _items[cellID];
			}
			
			_rectsByItems.clear ();
		}
		
		/**
		 * Преобразует действительные координаты точки в целочисленные координаты ячейки, содержащей эту точку. Если ячейка выходит за допустимые пределы (они определяются величинами <code>int.MIN_VALUE</code> и <code>int.MAX_VALUE</code>), создаётся исключение.
		 * @param	realX Действительная координата x точки.
		 * @param	realY Действительная координата y точки.
		 * @return Экземпляр <code>PairInt</code>, у которого свойство <code>first</code> соответствует координате <code>x</code> искомой ячейки, а свойство <code>second</code> - её координате <code>y</code>.
		 */
		[Inline]
		public final function realToInt (realX:Number, realY:Number):PairInt {
			var inumX:Number = Math.floor (realX / _cellSize);
			var inumY:Number = Math.floor (realY / _cellSize);
			if (inumX < int.MIN_VALUE || inumX > int.MAX_VALUE || inumY < int.MIN_VALUE || inumY > int.MAX_VALUE) {
				throw new RangeError ("Given point is out of range.");
			}
			return new PairInt (inumX, inumY);
		}
		
		/**
		 * Преобразует действительный прямоугольник объекта, находящегося в сетке, в целочисленный прямоугольник (в координатах сетки). Величины <code>left</code> и <code>top</code> возвращенного прямоугольника являются координатами ячейки, в которой находится левый верхний угол объекта. Величины <code>width</code> и <code>height</code> указывают ширину и высоту объекта в ячейках; <code>right</code> и <code>bottom</code> - полосы ячеек, в которых объект уже НЕ находится.
		 * @param	realRect Прямоугольник (с действительными координатами), описанный вокруг объекта.
		 * @return Ограничивающий прямоугольник объекта, <code>topLeft</code>, <code>width</code> и <code>height</code> которого указаны в ячейках.
		 */
		[Inline]
		public final function realToIntRect (realRect:Rectangle):Rectangle {
			var inumX:Number = Math.floor (realRect.x / _cellSize);
			var inumY:Number = Math.floor (realRect.y / _cellSize);
			var inumR:Number = Math.floor (realRect.right / _cellSize) + 1;
			var inumB:Number = Math.floor (realRect.bottom / _cellSize) + 1;
			if (inumX < int.MIN_VALUE || inumX > int.MAX_VALUE || inumY < int.MIN_VALUE || inumY > int.MAX_VALUE || inumR < int.MIN_VALUE || inumR > int.MAX_VALUE || inumB < int.MIN_VALUE || inumB > int.MAX_VALUE) {
				throw new RangeError ("Given rectangle is out of range.");
			}
			return new Rectangle (inumX, inumY, inumR - inumX, inumB - inumY);
		}
		
		/**
		 * Возвращает <code>true</code>, если указанные целочисленные координаты ячейки не выходят за пределы допустимых значений (т.е. если ячейка лежит в пределах этой сетки).
		 * @param	intX Целочисленная координата <code>x</code> ячейки.
		 * @param	intY Целочисленная координата <code>y</code> ячейки.
		 * @return <code>true</code>, если координаты ячейки находятся в позволенном диапазоне.
		 */
		[Inline]
		public final function cellIsInRange (intX:Number, intY:Number):Boolean {
			return intX >= int.MIN_VALUE && intX <= int.MAX_VALUE && intY >= int.MIN_VALUE && intY <= int.MAX_VALUE;
		}
		
		/**
		 * Возвращает ограничивающий прямоугольник с действительными координатами для указанной ячейки.
		 * @param	intX Целочисленная координата <code>x</code> ячейки.
		 * @param	intY Целочисленная координата <code>y</code> ячейки.
		 * @return Целочисленный ограничивающий прямоугольник ячейки.
		 */
		[Inline]
		public final function getCellRealRect (intX:int, intY:int):Rectangle {
			var realX:Number = intX * _cellSize;
			var realY:Number = intY * _cellSize;
			return new Rectangle (realX, realY, _cellSize, _cellSize);
		}
		
		/**
		 * Регистрирует объект <code>item</code> в сетке. Если этот объект уже зарегистрирован, создаётся исключение.
		 * @param	item Объект, который необходимо зарегистрировать.
		 * @param	itemIntRect Целочисленный ограничивающий прямоугольник объекта <code>item</code> в этой сетке. Он может быть получен с помощью метода сетки <code>realToIntRect()</code>.
		 */
		public final function addItem (item:Object, itemIntRect:Rectangle):void {
			if (hasItem (item)) {
				throw new Error ("Item is already added.");
			}
			if (!cellIsInRange (itemIntRect.x, itemIntRect.y) || !cellIsInRange (itemIntRect.right, itemIntRect.bottom)) {
				throw new RangeError ("Item's rectangle is out of range.");
			}
			
			_rectsByItems.add (item, itemIntRect);
			
			var ix:int = itemIntRect.x;
			var iy:int = itemIntRect.y;
			var ir:int = itemIntRect.right;
			var ib:int = itemIntRect.bottom;
			
			for (var xx:int = ix; xx < ir; xx++) {
				for (var yy:int = iy; yy < ib; yy++) {
					var cellID:String = getCellID (xx, yy);
					var s:Set = _items[cellID] as Set;
					if (!s) {
						s = new Set ();
						_items[cellID] = s;
					}
					if (!s.add (item)) {
						throw new Error ("Internal error.");
					}
				}
			}
		}
		
		/**
		 * Регистрирует объект <code>item</code> по его ограничивающему прямоугольнику, заданному в действительных координатах (не в координатах сетки).
		 * @param	item Регистрируемый объект.
		 * @param	realRect Действительный ограничивающий прямоугольник регистрируемого объекта.
		 */
		public final function addItemByRealRect (item:Object, realRect:Rectangle):void {
			addItem (item, realToIntRect (realRect));
		}
		
		/**
		 * Удаляет объект <code>item</code> из сетки.
		 * @param	item Объект, который необходимо удалить.
		 */
		public final function removeItem (item:Object):void {
			var itemIntRect:Rectangle = _rectsByItems[item] as Rectangle;
			if (!itemIntRect) {
				throw new ReferenceError ("Item " + item + " not found.");
			}
			_rectsByItems.remove (item);
			
			var ix:int = itemIntRect.x;
			var iy:int = itemIntRect.y;
			var ir:int = itemIntRect.right;
			var ib:int = itemIntRect.bottom;
			
			for (var xx:int = ix; xx < ir; xx++) {
				for (var yy:int = iy; yy < ib; yy++) {
					var cellID:String = getCellID (xx, yy);
					var s:Set = _items[cellID] as Set;
					if (!s) {
						throw new Error ("Internal error: cell's set not found.");
					}
					if (!s.remove (item)) {
						throw new Error ("Internal error: item is not registered in the cell's set.");
					}
					if (!s.length) {
						delete _items[cellID];
					}
				}
			}
		}
		
		/**
		 * Возвращает целочисленный ограничивающий прямоугольник, по которому айтем <code>item</code> зарегистрирован в этой сетке. Если айтем не зарегистрирован в сетке, создаётся исключение.
		 * @param	item Зарегистрированный в сетке объект, ограничивающий прямоугольник которого необходимо получить.
		 * @return Экземпляр <code>Rectangle</code>, представляющий целочисленный ограничивающий прямоугольник объекта <code>item</code>.
		 */
		[Inline]
		public final function getItemIntRect (item:Object):Rectangle {
			var rect:Rectangle = _rectsByItems[item] as Rectangle;
			if (!rect) {
				throw new ReferenceError ("Item " + rect + " not found.");
			}
			return rect;
		}
		
		/**
		 * Возвращает <code>true</code>, если сетка содержит зарегистрированный объект <code>item</code> (иначе - <code>false</code>).
		 * @param	item Объект, наличие которого необходимо проверить.
		 * @return <code>true</code>, если сетка содержит зарегистрированный объект <code>item</code> и <code>false</code> в противном случае.
		 */
		[Inline]
		public final function hasItem (item:Object):Boolean {
			return Boolean (_rectsByItems[item]);
		}
		
		/**
		 * Возвращает набор объектов, зарегистрированных в ячейке (<code>intX, intY</code>), или <code>null</code>, если в ячейке не зарегистрировано ни одного объекта.
		 * @param	intX Целочисленная координата <code>x</code> ячейки.
		 * @param	intY Целочисленная координата <code>y</code> ячейки.
		 * @return Экземпляр <code>Set</code>, содержащий все зарегистрированные в ячейке объекты.
		 */
		[Inline]
		public final function getCellItems (intX:int, intY:int):Set {
			return _items[getCellID (intX, intY)] as Set;
		}
		
		/**
		 * Находит объекты, чьи ограничивающие прямоугольники пересекаются с прямоугольником <code>intRect</code>, и помещает их в набор <code>result</code>, либо создаёт новый набор с такими объектами. Если переданный прямоугольник выходит за допустимые пределы, создаётся исключение.
		 * @param	intRect Прямоугольник с целочисленными координатами, внутри которого ведётся поиск.
		 * @param	result Набор, в который будут помещаться найденные объекты. Перед началом поиска этот набор не очищается, так что он будет дополнен. Если передано значение <code>null</code>, будет создан новый набор.
		 * @return Экземпляр <code>Set</code>, содержащий все найденные объекты.
		 */
		public final function getItemsInRect (intRect:Rectangle, result:Set = null):Set {
			if (!result) {
				result = new Set ();
			}
			
			if (!cellIsInRange (intRect.x, intRect.y) || !cellIsInRange (intRect.right, intRect.bottom)) {
				throw new RangeError ("The intRect rectangle is out of range.");
			}
			
			var ix:int = intRect.x;
			var iy:int = intRect.y;
			var ir:int = intRect.right;
			var ib:int = intRect.bottom;
			
			for (var xx:int = ix; xx < ir; xx++) {
				for (var yy:int = iy; yy < ib; yy++) {
					var cellID:String = getCellID (xx, yy);
					var s:Set = _items[cellID] as Set;
					if (!s) {
						continue;
					}
					result.unite (s);
				}
			}
			
			return result;
		}
		
		/**
		 * Подобен методу <code>getItemsInRect()</code>, но принимает в качестве аргумента прямоугольник, заданный в действительных координатах (не в координатах этой сетки).
		 * @param	realRect Прямоугольник с действительными координатами и размерами, внутри которого ведется поиск.
		 * @param	result Набор, в который будут помещены найденные объекты. Перед началом поиска он не очищается. Если передано значение <code>null</code>, создается новый набор.
		 * @return Экземпляр <code>Set</code>, содержащий все найденные объекты.
		 */
		public final function getItemsInRealRect (realRect:Rectangle, result:Set = null):Set {
			return getItemsInRect (realToIntRect (realRect), result);
		}
		
		/**
		 * Возвращает набор айтемов, расположенных в тех же ячейках сетки, что и айтем <code>item</code>, за исключением самого этого айтема.
		 * @param	item Объект, вокруг которого вести поиск соседей.
		 * @param	result Набор, в который необходимо поместить найденных соседей. Перед началом поиска он не очищается. Если передано значение <code>null</code>, создается новый набор.
		 * @return Экземпляр <code>Set</code>, содержащий соседей айтема <code>item</code>.
		 */
		public final function getNeighbors (item:Object, result:Set = null):Set {
			var itemRect:Rectangle = getItemIntRect (item);
			var nbs:Set = getItemsInRect (itemRect, result);
			nbs.remove (item);
			return nbs;
		}
		
		/**
		 * [Changed in Aida 0.0.13] Возвращает набор всех объектов, зарегистрированных в этой сетке.
		 * @param  result Набор, в который будут помещены все объекты этой сетки. Набор в начале метода не очищается. Если передано значение <code>null</code>, будет создан новый набор.
		 * @return Экземпляр <code>Set</code>, содержащий все объекты сетки.
		 */
		public final function collectAllItems (result:Set = null):Set
		{
			if (!result)
			{
				result = new Set ();
			}
			for (var i:Object in _rectsByItems)
			{
				result.add (i);
			}
			return result;
			
			//var result:Set = new Set ();
			//
			//for each (var i:Object in _items) {
				//var cellItems:Set = i as Set;
				//result.unite (cellItems);
			//}
			//
			//return result;
		}
		
		/**
		 * @private
		 */
		[Inline]
		private final function getCellID (intX:int, intY:int):String {
			return "c" + intX.toString (16) + "x" + intY.toString (16);
		}
		
		/**
		 * Размер ячейки сетки (действительное значение).
		 */
		[Inline]
		public final function get cellSize ():Number { return _cellSize; }
		
		/**
		 * Количество объектов, зарегистрированных в этой сетке.
		 */
		[Inline]
		public final function get numItems ():int { return _rectsByItems.length; }
		
	}

}