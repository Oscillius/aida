package dump13.msl.utils {
	/**
	 * Простой класс для генерации псевдослучайных чисел.
	 * @author dump13
	 */
	public class Randomizer {
		/**
		 * Переменная для генерации.
		 */
		public var a:uint;
		/**
		 * Переменная для генерации.
		 */
		public var b:uint;
		/**
		 * Переменная для генерации.
		 */
		public var c:uint;
		/**
		 * Сид, используемый для определения следующего значения в псевдослучайной последовательности.
		 */
		public var seed:uint;
		
		/**
		 * Константа, определяющая тип рандомайзера по умолчанию.
		 */
		public static const TYPE_0:uint = 0;
		
		/**
		 * Конструктор рандомайзера.
		 * @param	type Тип рандомайзера, определяющий используемые значения <code>a</code>, <code>b</code> и <code>c</code>. Поддерживается только один тип - Randomizer.TYPE_0.
		 * @param	startSeed Начальный сид, определяющий следующее сгенерированное значение.
		 */
		public function Randomizer (type:uint = TYPE_0, startSeed:uint = 0) {
			switch (type) {
				case Randomizer.TYPE_0:
					a = 1686629717;
					b = 907633385;
					c = 0xffffffff;
					break;
				default:
					throw new Error ("Unknown randomizer type (" + type + ").");
					break;
			}
			seed = startSeed;
		}
		
		/**
		 * Задаёт начальный сид <code>s</code>, от которого будет производиться генерация, и генерирует следующее за ним псевдослучайное значение от 0x00000000 до 0xFFFFFFFF. Текущий сид <code>seed</code> устанавливается в только что сгенерированное значение.
		 * @param	s Начальный сид, по которому вычислять генерируемое значение.
		 * @return Новое псевдослучайное число от 0x00000000 до 0xFFFFFFFF, основанное на сиде <code>s</code>.
		 */
		public function gen (s:uint):uint {
			seed = s;
			seed = (seed * a + b) % c;
			return seed;
		}
		
		/**
		 * Генерирует следующее псевдослучайное число от 0x00000000 до 0xFFFFFFFF, основанное на текущем значении <code>seed</code>. Текущее значение <code>seed</code> устанавливается в только что сгенерированное значение. Для генерации псевдослучайных последовательностей, содержащих значения от 0x00000000 до 0xFFFFFFFF, достаточно раз за разом вызывать этот метод, не меняя текущий <code>seed</code>.
		 * @return Новое псевдослучайное число от 0x00000000 до 0xFFFFFFFF, основанное на текущем сиде <code>seed</code>.
		 */
		public function genNext ():Number {
			seed = (seed * a + b) % c;
			return seed;
		}
		
		/**
		 * Генерирует псевдослучайное число, лежащее в диапазоне [0, 1). Генерация основана на текущем значении <code>seed</code>. Возвращаемое значение является результатом деления полученного беззнакового псевдослучайного числа на 0xFFFFFFFF.
		 * @return Новое псевдослучайное число от нуля до единицы, основанное на текущем сиде <code>seed</code>.
		 */
		public function genNextFloat ():Number {
			seed = (seed * a + b) % c;
			return seed / 0xFFFFFFFF;
		}
		
		/**
		 * Генерирует псевдослучайную последовательность, состоящую из целых беззнаковых чисел.
		 * @param	startSeed Начальный сид, от которого будет производиться последовательность.
		 * @param	length Количество чисел в последовательности.
		 * @return Вектор, содержащий псевдослучайную последовательность.
		 */
		public function genSequence (startSeed:uint, length:uint):Vector.<uint> {
			var v:Vector.<uint> = new Vector.<uint> ();
			for (var n:uint = 0, seed:uint = startSeed; n < length; n++) {
				seed = (seed * a + b) % c;
				v.push (seed);
			}
			return v;
		}
		
	}

}