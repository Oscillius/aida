package dump13.msl.utils {
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import dump13.msl.types.Map;
	import dump13.msl.types.PairInt;
	import dump13.msl.types.Set;
	/**
	 * Оптимизационная квадратная сетка, регистрирующая объекты по их ограничивающим прямоугольникам, переведённым в систему координат этой сетки.
	 * @author dump13
	 */
	public class Grid {
		protected var _intWidth:int;
		protected var _intHeight:int;
		protected var _cellSize:Number;
		protected var _realX:Number;
		protected var _realY:Number;
		protected var _realWidth:Number;
		protected var _realHeight:Number;
		protected var _cellItems:Array/*.<Array.<Set.<IGridItem>>>*/;
		protected var _columnLengths:Vector.<int>;
		protected var _itemRects:Map/*.<* : Rectangle>*/;
		public var name:String = "";
		
		/**
		 * Конструктор оптимизационной сетки.
		 * @param	intWidth Ширина сетки, в ячейках.
		 * @param	intHeight Высота сетки, в ячейках.
		 * @param	cellSize Размер ячейки (действительное число).
		 * @param	realX [Deprecated]. Действительное смещение сетки по оси X.
		 * @param	realY [Deprecated]. Действительное смещение сетки по оси Y.
		 */
		public function Grid (intWidth:int, intHeight:int, cellSize:Number, realX:Number, realY:Number) {
			_intWidth = intWidth;
			_intHeight = intHeight;
			_cellSize = cellSize;
			_realX = realX;
			_realY = realY;
			_realWidth = _cellSize * _intWidth;
			_realHeight = _cellSize * _intHeight;
			_cellItems = new Array ();
			_columnLengths = new Vector.<int> (_intWidth);
			_itemRects = new Map ();
		}
		
		/**
		 * Клонирует текущую сетку. Если задан параметр <code>itemsMap</code>, производит регистрацию его значений (values) во вновь созданной сетке. (В этом методе должна вылетать ошибка при использовании itemsMap).
		 * @param	itemsMap itemsMap Ассоциативный массив, в котором в качестве ключей используются объекты текущей сетки, а в качестве значений - соответствующие им объекты новой сетки. Если параметр не задан, регистрации объектов в новой сетке не производится.
		 * @return Копия текущей сетки.
		 */
		public function clone (itemsMap:Map = null):Grid {
			var newgrid:Grid = new Grid (_intWidth, _intHeight, _cellSize, _realX, _realY);
			if (itemsMap) {
				newgrid._columnLengths = _columnLengths.slice ();
				var n:int, na:int = _cellItems.length;
				for (n = 0; n < na; n++) {
					var colItems:Array = _cellItems[n];
					if (colItems) {
						var newColItems:Array = newgrid[n] = new Array ();		// here's the mistake, newgrid[n].
						var m:int, ma:int = colItems.length;
						for (m = 0; m < ma; m++) {
							var cellset:Set = colItems[m];
							if (cellset) {
								var newCellSet:Set = newColItems[m] = new Set ();
								for (var i:* in cellset) {
									newCellSet.add (itemsMap[i]);
								}
							}
						}
					}
				}
				var newItemRects:Map = newgrid._itemRects;
				for (i in _itemRects) {
					newItemRects.add (itemsMap[i], _itemRects[i]);
				}
			}
			return newgrid;
		}
		
		/**
		 * Удаляет все объекты из ячеек сетки.
		 */
		public function clear ():void {
			var n:int, na:int = _cellItems.length;
			for (n = 0; n < na; n++) {
				var colItems:Array = _cellItems[n];
				if (colItems) {
					var m:int, ma:int = colItems.length;
					for (m = 0; m < ma; m++) {
						var cellset:Set = colItems[m];
						if (cellset) {
							cellset.clear ();
						}
					}
					colItems.length = 0;
				}
			}
			_cellItems.length = 0;
			na = _columnLengths.length;
			for (n = 0; n < na; n++) { _columnLengths[n] = 0; }
			_itemRects.clear ();
		}
		
		/**
		 * [Deprecated]. Создает сетку по максимальным границам объекта и требуемому размеру ячейки.
		 * @param	objRect Максимально возможные границы объекта в собственной системе координат.
		 * @param	cellSize Требуемый размер ячейки ожидаемой сетки.
		 * @return Новый экземпляр сетки.
		 */
		public static function makeByCellSize (objRect:Rectangle, cellSize:Number):Grid {
			var delta:Number = cellSize * .99999;
			var iwidth:int = (objRect.width + delta) / cellSize;
			var iheight:int = (objRect.height + delta) / cellSize;
			return new Grid (iwidth, iheight, cellSize, objRect.left, objRect.top);
		}
		
		/**
		 * [Deprecated]. Создает сетку по максимальным границам объекта и требуемой длине сетки в ячейке (берется максимальный размер).
		 * @param	objRect Максимально возможные границы объекта в собственной системе координат.
		 * @param	intLength Разрешение сетки по большей стороне (длина в ячейках).
		 * @return
		 */
		public static function makeByIntLength (objRect:Rectangle, intLength:int):Grid {
			var objWidth:Number = objRect.width;
			var objHeight:Number = objRect.height;
			var cellsize:Number;
			if (objWidth >= objHeight) {
				cellsize = objWidth / intLength;
				return new Grid (intLength, (objHeight + cellsize * .99999) / cellsize, cellsize, objRect.left, objRect.top);
			}
			cellsize = objHeight / intLength;
			return new Grid ((objWidth + cellsize * .99999) / cellsize, intLength, cellsize, objRect.left, objRect.top);
		}
		
		/**
		 * Преобразует действительные координаты точки в координаты ячейки, содержащей эту точку.
		 * @param	realPoint
		 * @return Пара из int, у которой <code>first</code> соответствует целочисленной координате <code>x</code> ячейки, а <code>second</code> - целочисленной координате <code>y</code>.
		 */
		public function realToInt (realPoint:Point):PairInt {
			var xx:int = (realPoint.x - _realX) / _cellSize;
			var yy:int = (realPoint.y - _realY) / _cellSize;
			return new PairInt (xx, yy);
		}
		
		/**
		 * Преобразует действительный прямоугольник объекта, находящегося в сетке, в целочисленный прямоугольник (в координатах сетки). Величины <code>left</code> и <code>top</code> возвращенного прямоугольника являются координатами ячейки, в которой находится левый верхний угол объекта. Величины <code>width</code> и <code>height</code> указывают ширину и высоту объекта в ячейках; <code>right</code> и <code>bottom</code> - полосы ячеек, в которых объект уже НЕ находится.
		 * @param	realRect Прямоугольник (с действительными координатами), описанный вокруг объекта.
		 * @return Ограничивающий прямоугольник объекта, <code>topLeft</code>, <code>width</code> и <code>height</code> которого указаны в ячейках.
		 */
		public function realToIntRect (realRect:Rectangle):Rectangle {
			var ileft:int = (realRect.left - _realX) / _cellSize;
			var itop:int = (realRect.top - _realY) / _cellSize;
			var iright:int = (realRect.right - _realX) / _cellSize + 1;		// Так как это недосягаемый для объекта предел.
			var ibottom:int = (realRect.bottom - _realY) / _cellSize + 1;	// Same
			var iwidth:int = iright - ileft;
			var iheight:int = ibottom - itop;
			return new Rectangle (ileft, itop, iwidth, iheight);
		}
		
		/**
		 * Возвращает действительный прямоугольник ячейки с координатами <code>(intX, intY)</code>.
		 * @param	intX Целочисленная координата X ячейки.
		 * @param	intY Целочисленная координата Y ячейки.
		 * @return Прямоугольник ячейки (с действительными координатами и размерами).
		 */
		public function getCellRect (intX:int, intY:int):Rectangle {
			var rectLeft:Number = _cellSize * intX + _realX;
			var rectTop:Number = _cellSize * intY + _realY;
			return new Rectangle (rectLeft, rectTop, _cellSize, _cellSize);
		}
		
		/**
		 * Возвращает <code>true</code>, если координаты ячейки с указанными координатами не выходят за пределы данной сетки.
		 * @param	intX Целочисленная координата X ячейки.
		 * @param	intY Целочисленная координата Y ячейки.
		 * @return Находится ли ячейка в пределах текущей сетки.
		 */
		public function cellIsInside (intX:int, intY:int):Boolean {
			return (intX < _intWidth && intY < _intHeight);
		}
		
		/**
		 * Возвращает множество объектов, зарегистрированных в ячейке, или null, если в ячейке не зарегистрировано ни одного объекта.
		 * @param	intX Целочисленная координата X ячейки.
		 * @param	intY Целочисленная координата Y ячейки.
		 * @return Объект <code>Set</code>, содержащий все объекты, зарегистрированные в указанной ячейке сетки.
		 */
		public function getCellItems (intX:int, intY:int):Set {
			var col:Array = _cellItems[intX];
			if (!col) { return null; }
			return col[intY];
		}
		
		/**
		 * Возвращает прямоугольник объекта в системе координат текущей сетки (с целочисленными координатами). Если объект не зарегистрирован в сетке, создается исключение.
		 * @param	item Объект, ограничивающий прямоугольник которого необходимо найти.
		 * @return Ограничивающий прямоугольник объекта.
		 */
		public function getItemIntRect (item:*):Rectangle {
			var i:*= _itemRects[item];
			if (i == undefined) { throw new Error ("Item not found."); }
			return Rectangle (i).clone ();
		}
		
		/**
		 * Регистрирует объект в ячейках, определяемых пересечением прямоугольника <code>itemGridRect</code> и собственного прямоугольника сетки. Возвращает <code>true</code>, если объект выходит за пределы текущей сетки.
		 * @param	item Объект, который необходимо зарегистрировать.
		 * @param	itemGridRect Ограничивающий прямоугольник объекта в системе координат текущей сетки (с целочисленными координатами).
		 * @return Выходит ли прямоугольник объекта за пределы прямоугольника сетки.
		 */
		public function addItem (item:*, itemGridRect:Rectangle):Boolean {
			var n:int, m:int;
			//item.parentalGridRect = itemGridRect;
			var curIntRect:Rectangle = intRect;
			var irect:Rectangle, outside:Boolean;
			if (curIntRect.containsRect (itemGridRect)) { irect = itemGridRect; outside = false; }
			else { irect = curIntRect.intersection (itemGridRect); outside = true; }
			if (_itemRects.hasKey (item)) { throw new Error ("Trying to add an item that is already added."); }
			_itemRects.add (item, irect);
			var ileft:int = irect.left; 	var itop:int = irect.top;
			var iright:int = irect.right; 	var ibottom:int = irect.bottom;
			for (n = ileft; n < iright; n++) {
				var col:Array = _cellItems[n];
				if (!col) { col = new Array (); _cellItems[n] = col; }
				for (m = itop; m < ibottom; m++) {
					var cellset:Set = col[m];
					if (!cellset) {
						cellset = new Set ();
						col[m] = cellset;
						_columnLengths[n]++;
					}
					//cellset.add (item);
					if (!cellset.add (item)) { throw new Error ("Internal error: item already exists."); }	/// TODO: replace with line above.
				}
			}
			return outside;
		}
		
		/**
		 * Удаляет объект из ячеек, в которых он зарегистрирован. Если объект не зарегистрирован в сетке, создаётся исключение.
		 * @param	item Объект, который необходимо удалить из сетки.
		 */
		public function removeItem (item:*):void {
			var n:int, m:int;
			var irect:Rectangle = _itemRects[item];
			if (!irect) { throw new Error ("Trying to remove an item that is absent."); }
			var ileft:int = irect.left;		var itop:int = irect.top;
			var iright:int = irect.right;	var ibottom:int = irect.bottom;
			//if (!_itemRects.remove (item)) { throw new Error ("Trying to remove an item that is absent."); }
			_itemRects.remove (item);
			for (n = ileft; n < iright; n++) {
				var col:Array = _cellItems[n];
				for (m = itop; m < ibottom; m++) {
					var cellset:Set = col[m];
					//cellset.remove (item);
					if (!cellset.remove (item)) { throw new Error ("Internal error: item is absent."); }	/// TODO: replace with line above.
					if (!cellset.length) { delete col[m]; _columnLengths[n]--; }
				}
				if (!_columnLengths[n]) { delete _cellItems[n]; }
				if (_columnLengths[n] < 0) { throw new Error ("Internal error: negative column length."); }		/// TODO: remove after dev.
			}
		}
		
		/**
		 * Возвращает <code>true</code>, если объект <code>item</code> содержится в данной сетке.
		 * @param	item Объект, наличие которого в сетке необходимо определить.
		 * @return Зарегистрирован ли объект в сетке.
		 */
		public function hasItem (item:*):Boolean {
			return _itemRects.hasKey (item);
		}
		
		/**
		 * Возвращает прямоугольник объекта <code>item</code> в системе координат сетки (целочисленный).
		 * @param	item Объект, ограничивающий прямоугольник которого требуется получить.
		 * @return Ограничивающий прямоугольник объекта.
		 */
		public function getItemRect (item:*):Rectangle {
			return Rectangle (_itemRects[item]).clone ();
		}
		
		/**
		 * Возвращает множество объектов, находящихся в тех же ячейках, что и объект <code>item</code> (за исключением самого объекта <code>item</code>).
		 * @param	item Объект, для которого необходимо найти соседей.
		 * @return Множество соседей объекта <code>item</code>.
		 */
		public function getNeighbours (item:*):Set {
			var result:Set = new Set ();
			var irect:Rectangle = _itemRects[item];
			var n:int, m:int;
			var ileft:int = irect.left, iright:int = irect.right;
			var itop:int = irect.top, ibottom:int = irect.bottom;
			for (n = ileft; n < iright; n++) {
				var col:Array = _cellItems[n];
				for (m = itop; m < ibottom; m++) {
					var cellset:Set = col[m];
					if (!cellset) { throw new Error ("Internal error: can\'n find the item."); }
					for (var i:* in cellset) {
						result.add (i);
					}
				}
			}
			result.remove (item);
			return result;
		}
		
		/**
		 * Возвращает множество объектов, находящихся в ячейках, ограниченных прямоугольником rect (с целочисленными координатами). Для получения прямоугольника нужно воспользоваться методом <code>realToIntRect()</code> (правая и нижняя границы должны быть на единицу больше).
		 * @param	rect Целочисленный прямоугольник, содержащий ячейки (за исключением правой и нижней границ), в которых следует вести поиск.
		 * @return Множество объектов, находящихся в заданной области.
		 */
		public function getItemsInRect (rect:Rectangle):Set {
			var s:Set = new Set ();
			rect = intRect.intersection (rect);
			var left:Number = rect.left, right:Number = rect.right;
			var top:Number = rect.top, bottom:Number = rect.bottom;
			for (var n:int = left; n < right; n++) {
				var col:Array = _cellItems[n];
				if (col) {
					for (var m:int = top; m < bottom; m++) {
						var cellset:Set = col[m];
						if (cellset) {
							for (var i:* in cellset) { s.add (i); }
						}
						//else { throw new Error ("Empty set in cell."); }
					}
				}
			}
			return s;
		}
		
		/**
		 * Возвращает набор всех объектов, зарегистрированных в сетке.
		 * @return Множество всех объектов.
		 */
		public function getAllItems ():Set {
			var result:Set = new Set ();
			for (var i:Object in _itemRects) {
				result.add (i);
			}
			return result;
		}
		
		/**
		 * Создаёт спрайт, отображающий количество объектов, зарегистрированных в разных ячейках сетки. Более заполненные ячейки отображаются более жёлтыми.
		 * @param	maxCount Максимальное количество объектов, которое может быть зарегистрировано в одной ячейке (принимается за 100% заполненность ячейки). Если действительно количество объектов, зарегистрированных в ячейке, превышает это значение, заполненность ограничивается 100%.
		 * @param	alpha Прозрачность спрайта.
		 * @return Спрайт, демонстрирующий заполненность ячеек.
		 */
		public function toSprite (maxCount:int = 255, alpha:Number = .5):Sprite {
			var sp:Sprite = new Sprite ();
			var gr:Graphics = sp.graphics;
			for (var n:int = 0; n < _intWidth; n++) {
				for (var m:int = 0; m < _intHeight; m++) {
					var cellset:Set = getCellItems (n, m);
					var density:Number, color:uint;
					if (cellset) {
						var setlen:int = cellset.length;
						if (setlen >= maxCount) { density = 1; }
						else { density = setlen / maxCount; }
					}
					else { density = 0; }
					if (density) { color = 0xaaaa00 | (0xaa * (1 - density)); }
					else 		 { color = 0xaaaaaa; }
					var currect:Rectangle = getCellRect (n, m);
					gr.lineStyle (0, 0x666666, alpha);
					gr.beginFill (color, alpha);
					gr.drawRect (currect.left, currect.top, currect.width, currect.height);
					gr.endFill ();
				}
			}
			return sp;
		}
		
		// ACCESSORS
		
		/**
		 * Ширина сетки, в ячейках.
		 */
		public function get intWidth ():int { return _intWidth; }
		/**
		 * Высота сетки, в ячейках.
		 */
		public function get intHeight ():int { return _intHeight; }
		/**
		 * Прямоугольник с левой верхней точкой в нуле и с длиной и шириной текущей сетки, указанной в ячейках.
		 */
		public function get intRect ():Rectangle { return new Rectangle (0, 0, _intWidth, _intHeight); }
		/**
		 * [Deprecated]. Смещение сетки по оси X (действительное число).
		 */
		public function get realX ():Number { return _realX; }
		/**
		 * [Deprecated]. Смещение сетки по оси Y (действительное число).
		 */
		public function get realY ():Number { return _realY; }
		/**
		 * Размер ячейки сетки (действительное число).
		 */
		public function get cellSize():Number { return _cellSize; }
		
		/**
		 * Действительная ширина сетки (<code>intWidth * cellSize</code>).
		 */
		public function get realWidth ():Number { return _realWidth; }
		/**
		 * Действительная высота сетки (<code>intHeight * cellSize</code>).
		 */
		public function get realHeight ():Number { return _realHeight; }
		
	}

}