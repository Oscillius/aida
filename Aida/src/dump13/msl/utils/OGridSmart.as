package dump13.msl.utils 
{
	import dump13.msl.types.Map;
	import dump13.msl.types.PairInt;
	import dump13.msl.types.Set;
	import flash.geom.Rectangle;
	
	/**
	 * Класс "умной" сетки, при необходимости подразделяющей свои ячейки с помощью внутренних сеток. Ячейки не делятся рекурсивно, т.е. существует только один уровень вложенности.
	 * @author dump13
	 */
	public class OGridSmart implements IGrid
	{
		internal var _cellSize:Number = 0;
		/// Размер ячейки внутренних сеток (вложенных в ячейки этой сетки).
		internal var _internalCellSize:Number = 0;
		/// Object (String : OGridCell). Ячейки, хранящиеся по ключам-идентификаторам, и содержащие, в свою очередь, набор зарегистрированных айтемов.
		private var _cells:Object/*.<String : OGridCell>*/ = new Object ();
		/// int-прямоугольники айтемов.
		private var _rectsByItems:Map/*<Object : Rectangle>*/ = new Map ();
		/// real-прямоугольники айтемов (только для small-айтемов).
		private var _realRectsByItems:Map/*<Object : Rectangle>*/ = new Map ();
		
		// [Deprecated]. Является ли сетка вложенной. Я планирую ввести только один уровень вложенности, поэтому сетки будут только двух уровней - корневая и вложенные.
		//private var _isInserted:Boolean = false;
		/// Размер "малых объектов". Объект будет считаться малым, если оба его размера меньше или равны этому значению. Очевидно, это свойство целесообразно сделать приблизительно равным размеру ячейки вложенной сетки. Только малые объекты могут быть зарегистрированы во вложенной сетке, и только для них следует хранить real-прямоугольник.
		private var _smallItemSize:Number = 0;
		/// Количество зарегистрированных объектов ячейки, при превышении которого происходит подразделение этой ячейки и регистрация мелких объектов во вложенной сетке. Таким образом, ячейка может содержать _maxItems объектов и при этом не быть подразделена.
		internal var _maxItems:int;
		/// Размер, которым должен обладать real-прямоугольник, чтобы поиск по нему был аккуратным. Если хотя бы один из размеров прямоугольника будет больше этого значения, поиск будет грубым.
		private var _smallRectSize:Number = 0;
		
		
		/**
		 * Конструктор "умной" сетки.
		 * @param	cellSize Размер ячейки этой сетки.
		 * @param	internalCellSize Размер ячейки внутренних сеток. Должен быть меньше, чем <code>cellSize</code>.
		 * @param	smallItemSize Размер, при котором объект еще считается малым. Если один из размеров баунда объекта превышает это значение, объект не является малым и не может быть зарегистрирован во внутренней сетке ячейки.
		 * @param	maxItems Количество объектов ячейки (и малых, и обычных), при котором эта ячейка еще не подразделяется.
		 */
		public function OGridSmart (cellSize:Number, internalCellSize:Number, smallItemSize:Number, maxItems:int)
		{
			if (cellSize <= 0 || internalCellSize <= 0)
			{
				throw new RangeError ("Neither common nor internal cell size may be lower than or equal to zero.");
			}
			if (internalCellSize >= cellSize)
			{
				throw new Error ("Internal grid's cell size should be lower than this grid cell size.");
			}
			if (smallItemSize <= 0)
			{
				throw new RangeError ("Small item size should be greater than zero.");
			}
			if (maxItems < 1)
			{
				throw new RangeError ("Number of items causing cell to subdivide should be greater than 1 (so maximal number of items that cell can hold without subdivision should be at least 1). Pass at least '1' as 'maxItems'");
			}
			
			_cellSize = cellSize;
			_internalCellSize = internalCellSize;
			_smallItemSize = smallItemSize;
			_maxItems = maxItems;
			
			//_smallRectSize = _smallItemSize * 2;
			_smallRectSize = _internalCellSize * 2;
		}
		
		/**
		 * Очищает сетку, удаляя из неё все зарегистрированные объекты.
		 */
		[Inline]
		public final function clear ():void {
			for (var cellID:String in _cells) {
				var itemsCell:OGridCell = _cells[cellID] as OGridCell;
				if (itemsCell) {
					itemsCell.clear ();
				}
				delete _cells[cellID];
			}
			
			_rectsByItems.clear ();
			_realRectsByItems.clear ();
		}
		
		/**
		 * Преобразует действительные координаты точки в целочисленные координаты ячейки, содержащей эту точку. Если ячейка выходит за допустимые пределы (они определяются величинами <code>int.MIN_VALUE</code> и <code>int.MAX_VALUE</code>), создаётся исключение.
		 * @param	realX Действительная координата x точки.
		 * @param	realY Действительная координата y точки.
		 * @return Экземпляр <code>PairInt</code>, у которого свойство <code>first</code> соответствует координате <code>x</code> искомой ячейки, а свойство <code>second</code> - её координате <code>y</code>.
		 */
		[Inline]
		public final function realToInt (realX:Number, realY:Number):PairInt {
			var inumX:Number = Math.floor (realX / _cellSize);
			var inumY:Number = Math.floor (realY / _cellSize);
			if (inumX < int.MIN_VALUE || inumX > int.MAX_VALUE || inumY < int.MIN_VALUE || inumY > int.MAX_VALUE) {
				throw new RangeError ("Given point is out of range.");
			}
			return new PairInt (inumX, inumY);
		}
		
		/**
		 * Преобразует действительный прямоугольник объекта, находящегося в сетке, в целочисленный прямоугольник (в координатах сетки). Величины <code>left</code> и <code>top</code> возвращенного прямоугольника являются координатами ячейки, в которой находится левый верхний угол объекта. Величины <code>width</code> и <code>height</code> указывают ширину и высоту объекта в ячейках; <code>right</code> и <code>bottom</code> - полосы ячеек, в которых объект уже НЕ находится.
		 * @param	realRect Прямоугольник (с действительными координатами), описанный вокруг объекта.
		 * @return Ограничивающий прямоугольник объекта, <code>topLeft</code>, <code>width</code> и <code>height</code> которого указаны в ячейках.
		 */
		[Inline]
		public final function realToIntRect (realRect:Rectangle):Rectangle {
			var inumX:Number = Math.floor (realRect.x / _cellSize);
			var inumY:Number = Math.floor (realRect.y / _cellSize);
			var inumR:Number = Math.floor (realRect.right / _cellSize) + 1;
			var inumB:Number = Math.floor (realRect.bottom / _cellSize) + 1;
			if (inumX < int.MIN_VALUE || inumX > int.MAX_VALUE || inumY < int.MIN_VALUE || inumY > int.MAX_VALUE || inumR < int.MIN_VALUE || inumR > int.MAX_VALUE || inumB < int.MIN_VALUE || inumB > int.MAX_VALUE) {
				throw new RangeError ("Given rectangle is out of range.");
			}
			return new Rectangle (inumX, inumY, inumR - inumX, inumB - inumY);
		}
		
		/**
		 * Возвращает <code>true</code>, если указанные целочисленные координаты ячейки не выходят за пределы допустимых значений (т.е. если ячейка лежит в пределах этой сетки).
		 * @param	intX Целочисленная координата <code>x</code> ячейки.
		 * @param	intY Целочисленная координата <code>y</code> ячейки.
		 * @return <code>true</code>, если координаты ячейки находятся в позволенном диапазоне.
		 */
		[Inline]
		public final function cellIsInRange (intX:Number, intY:Number):Boolean {
			return intX >= int.MIN_VALUE && intX <= int.MAX_VALUE && intY >= int.MIN_VALUE && intY <= int.MAX_VALUE;
		}
		
		/**
		 * Возвращает ограничивающий прямоугольник с действительными координатами для указанной ячейки.
		 * @param	intX Целочисленная координата <code>x</code> ячейки.
		 * @param	intY Целочисленная координата <code>y</code> ячейки.
		 * @return Целочисленный ограничивающий прямоугольник ячейки.
		 */
		[Inline]
		public final function getCellRealRect (intX:int, intY:int):Rectangle {
			var realX:Number = intX * _cellSize;
			var realY:Number = intY * _cellSize;
			return new Rectangle (realX, realY, _cellSize, _cellSize);
		}
		
		/**
		 * Регистрирует объект <code>item</code> по его ограничивающему прямоугольнику, заданному в действительных координатах (не в координатах сетки).
		 * @param	item Регистрируемый объект.
		 * @param	realRect Действительный ограничивающий прямоугольник регистрируемого объекта.
		 */
		public final function addItemByRealRect (item:Object, realRect:Rectangle):void
		{
			if (hasItem (item))
			{
				throw new Error ("Item is already added.");
			}
			
			// Если объект является малым, сохраняем его realRect.
			var isSmall:Boolean;
			if (realRect.width <= _smallItemSize && realRect.height <= _smallItemSize)
			{
				isSmall = true;
				_realRectsByItems.add (item, realRect);
			}
			
			// int-прямоугольник сохраняем в любом случае.
			var itemIntRect:Rectangle = realToIntRect (realRect);
			if (!cellIsInRange (itemIntRect.x, itemIntRect.y) || !cellIsInRange (itemIntRect.right, itemIntRect.bottom))
			{
				throw new RangeError ("Item's rectangle is out of range.");
			}
			_rectsByItems.add (item, itemIntRect);
			
			var ix:int = itemIntRect.x;
			var iy:int = itemIntRect.y;
			var ir:int = itemIntRect.right;
			var ib:int = itemIntRect.bottom;
			
			for (var xx:int = ix; xx < ir; xx++) {
				for (var yy:int = iy; yy < ib; yy++) {
					var cellID:String = getCellID (xx, yy);
					
					var cell:OGridCell = _cells[cellID] as OGridCell;
					if (!cell)
					{
						cell = new OGridCell ();
						_cells[cellID] = cell;
					}
					cell.addItem (item, this, isSmall? realRect : null);
				}
			}
		}
		
		/**
		 * Удаляет объект <code>item</code> из сетки.
		 * @param	item Объект, который необходимо удалить.
		 */
		public final function removeItem (item:Object):void
		{
			// Определяем, является ли объект малым. У малого объекта удаляем real-прямоугольник.
			var realRect:Rectangle = _realRectsByItems[item] as Rectangle;
			var isSmall:Boolean = Boolean (realRect);
			if (isSmall)
			{
				_realRectsByItems.remove (item);
			}
			
			// Удаляем int-прямоугольник.
			var itemIntRect:Rectangle = _rectsByItems[item] as Rectangle;
			if (!itemIntRect) {
				throw new ReferenceError ("Item " + item + " not found.");
			}
			_rectsByItems.remove (item);
			
			// Удаляем айтем из всех ячеек.
			var ix:int = itemIntRect.x;
			var iy:int = itemIntRect.y;
			var ir:int = itemIntRect.right;
			var ib:int = itemIntRect.bottom;
			
			for (var xx:int = ix; xx < ir; xx++)
			{
				for (var yy:int = iy; yy < ib; yy++)
				{
					var cellID:String = getCellID (xx, yy);
					var cell:OGridCell = _cells[cellID] as OGridCell;
					if (!cell)
					{
						throw new Error ("Internal error: cell not found.");
					}
					cell.removeItem (item, this, isSmall);
					if (!cell.numItems)
					{
						delete _cells[cellID];
					}
				}
			}
		}
		
		/**
		 * Возвращает целочисленный ограничивающий прямоугольник, по которому айтем <code>item</code> зарегистрирован в этой сетке. Если айтем не зарегистрирован в сетке, создаётся исключение.
		 * @param	item Зарегистрированный в сетке объект, ограничивающий прямоугольник которого необходимо получить.
		 * @return Экземпляр <code>Rectangle</code>, представляющий целочисленный ограничивающий прямоугольник объекта <code>item</code>.
		 */
		[Inline]
		public final function getItemIntRect (item:Object):Rectangle {
			var rect:Rectangle = _rectsByItems[item] as Rectangle;
			if (!rect) {
				throw new ReferenceError ("Item " + item + " not found.");
			}
			return rect;
		}
		
		/**
		 * Возвращает ограничивающий прямоугольник с действительными координатами, по которому малый айтем <code>item</code> зарегистрирован в этой сетке. Если айтем не является малым или не регистрировался в этой сетке, метод возвращает null.
		 * @param	item Айтем, для которого находится ограничивающий прямоугольник.
		 * @return Ограничивающий прямоугольник с действительными координатами либо null, если айтем не является малым или если он не регистрировался в этой сетке.
		 */
		[Inline]
		public final function getItemRealRect (item:Object):Rectangle
		{
			return _realRectsByItems[item] as Rectangle;
		}
		
		/**
		 * Возвращает <code>true</code>, если сетка содержит зарегистрированный объект <code>item</code> (иначе - <code>false</code>).
		 * @param	item Объект, наличие которого необходимо проверить.
		 * @return <code>true</code>, если сетка содержит зарегистрированный объект <code>item</code> и <code>false</code> в противном случае.
		 */
		[Inline]
		public final function hasItem (item:Object):Boolean {
			return Boolean (_rectsByItems[item]);
		}
		
		/**
		 * [Not used] Является ли объект малым. Объект должен быть зарегистрирован в сетке, иначе метод вернет <code>false</code> в любом случае.
		 * @param	item Объект, который необходимо проверить.
		 * @return <code>true</code>, если объект <code>item</code> зарегистрирован в сетке и является малым, <code>false</code> в противном случае.
		 */
		[Inline]
		public final function itemIsSmall (item:Object):Boolean
		{
			return Boolean (_realRectsByItems[item]);
		}
		
		/**
		 * Находит объекты, зарегистрированные в ячейках, пересекающихся с int-прямоугольником <code>intRect</code>, и помещает их в набор <code>result</code>, либо создаёт новый набор с такими объектами. Если переданный прямоугольник выходит за допустимые пределы, создаётся исключение.
		 * @param	intRect Прямоугольник с целочисленными координатами, внутри которого ведётся (грубый) поиск.
		 * @param	result Набор, в который будут помещаться найденные объекты. Перед началом поиска этот набор не очищается, так что он будет дополнен. Если передано значение <code>null</code>, будет создан новый набор.
		 * @return Экземпляр <code>Set</code>, содержащий все найденные объекты.
		 */
		public final function getItemsInRect (intRect:Rectangle, result:Set = null):Set
		{
			if (!result) {
				result = new Set ();
			}
			
			if (!cellIsInRange (intRect.x, intRect.y) || !cellIsInRange (intRect.right, intRect.bottom)) {
				throw new RangeError ("The intRect rectangle is out of range.");
			}
			
			var ix:int = intRect.x;
			var iy:int = intRect.y;
			var ir:int = intRect.right;
			var ib:int = intRect.bottom;
			
			for (var xx:int = ix; xx < ir; xx++)
			{
				for (var yy:int = iy; yy < ib; yy++)
				{
					var cellID:String = getCellID (xx, yy);
					var cell:OGridCell = _cells[cellID];
					if (!cell)
					{
						continue;
					}
					cell.collectItems (result);
				}
			}
			
			return result;
		}
		
		/**
		 * Помещает в набор <code>result</code> (или во вновь созданный набор, если <code>result == null</code>) те объекты, у которых ограничивающий прямоугольник может пересекаться с real-прямоугольником <code>realRect</code>. Это приблизительный поиск, не ищущий пересечения баундов объектов с прямоугольником поиска. Вместо этого поиск ведется по ячейкам, пересекающимся с <code>realRect</code>: объекты, не являющиеся малыми, достаются из ячеек в полном составе, а малые объекты (если в ячейке такие содержатся) ищутся с помощью вложенной сетки ячейки, но опять же "грубо", с учетом ячеек этой сетки.
		 * Набор <code>result</code> перед поиском не очищается.
		 * @param	realRect Прямоугольник с действительными координатами, внутри которого ведется приблизительный поиск.
		 * @param	result Набор, в который следует помещать результаты поиска, или <code>null</code>, если нужно создать новый набор.
		 * @return Набор <code>result</code>, дополненный найденными объектами, или новый экземпляр <code>Set</code>, содержащий найденные объекты.
		 */
		public final function getItemsInRealRect (realRect:Rectangle, result:Set = null):Set
		{
			if (!result) {
				result = new Set ();
			}
			
			var intRect:Rectangle = realToIntRect (realRect);
			
			if (!cellIsInRange (intRect.x, intRect.y) || !cellIsInRange (intRect.right, intRect.bottom)) {
				throw new RangeError ("The intRect rectangle is out of range.");
			}
			
			var ix:int = intRect.x;
			var iy:int = intRect.y;
			var ir:int = intRect.right;
			var ib:int = intRect.bottom;
			
			// Если real-прямоугольник достаточно мал, используем аккуратный поиск. В остальных случаях поиск не будет производиться по ячейкам внутренней сетки.
			var accurateSearch:Boolean = realRect.width <= _smallRectSize && realRect.height <= _smallRectSize;
			
			for (var xx:int = ix; xx < ir; xx++)
			{
				for (var yy:int = iy; yy < ib; yy++)
				{
					var cellID:String = getCellID (xx, yy);
					var cell:OGridCell = _cells[cellID];
					if (!cell)
					{
						continue;
					}
					cell.collectItemsInRealRect (realRect, result, accurateSearch);
				}
			}
			
			return result;
		}
		
		/**
		 * Возвращает набор айтемов, расположенных в тех же ячейках сетки, что и айтем <code>item</code>, за исключением самого этого айтема.
		 * @param	item Объект, вокруг которого вести поиск соседей.
		 * @param	result Набор, в который необходимо поместить найденных соседей. Перед началом поиска он не очищается, однако объект <code>item</code> по завершении поиска будет из него удален. Если передано значение <code>null</code>, создается новый набор.
		 * @return Экземпляр <code>Set</code>, содержащий соседей айтема <code>item</code>.
		 */
		public final function getNeighbors (item:Object, result:Set = null):Set
		{
			if (!result)
			{
				result = new Set ();
			}
			var realRect:Rectangle = getItemRealRect (item);
			if (realRect)
			{
				getItemsInRealRect (realRect, result);
			}
			else
			{
				getItemsInRect (getItemIntRect (item), result);
			}
			// Удаляем item из результатов поиска.
			result.remove (item);
			return result;
		}
		
		/**
		 * Возвращает набор всех объектов, зарегистрированных в этой сетке.
		 * @param  result Набор, в который будут помещены все объекты этой сетки. Набор в начале метода не очищается. Если передано значение <code>null</code>, будет создан новый набор.
		 * @return Экземпляр <code>Set</code>, содержащий все объекты сетки.
		 */
		public final function collectAllItems (result:Set = null):Set
		{
			if (!result)
			{
				result = new Set ();
			}
			for (var i:Object in _rectsByItems)
			{
				result.add (i);
			}
			return result;
		}
		
		/**
		 * @private
		 */
		[Inline]
		private final function getCellID (intX:int, intY:int):String {
			return "c" + intX.toString (16) + "x" + intY.toString (16);
		}
		
		/**
		 * Размер ячейки сетки (действительное значение).
		 */
		[Inline]
		public final function get cellSize ():Number { return _cellSize; }
		
		/**
		 * Количество объектов, зарегистрированных в этой сетке.
		 */
		[Inline]
		public final function get numItems ():int { return _rectsByItems.length + _realRectsByItems.length; }
		
	}

}