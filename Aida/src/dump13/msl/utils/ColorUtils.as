package dump13.msl.utils {
	/**
	 * Класс с утилитами для работы с цветом.
	 * @author dump13
	 */
	public class ColorUtils {
		
		/**
		 * Конструктор. Нет необходимости его использовать.
		 */
		public function ColorUtils () {
			
		}
		
		/**
		 * Переводит HSL-цвет в RGB-цвет. Все параметры - от нуля до единицы.
		 * @param	hue Оттенок цвета.
		 * @param	satur Насыщенность цвета.
		 * @param	light Яркость цвета.
		 * @return Беззнаковое целое, содержащее RGB-цвет.
		 */
		public static function hslToRGB (hue:Number, satur:Number, light:Number):uint {
			var red:uint;
			var green:uint;
			var blue:uint;
			var currentValue:uint;
			
			var lightInt:int;
			var temp1:Number, temp2:Number, temp3:Number;
			var temp4:uint;
			currentValue = currentValue & 0xff000000;
			if (satur == 0) {
				lightInt = int (255 * light);
				red = lightInt;
				green = lightInt;
				blue = lightInt;
				return red << 16 | green << 8 | blue;
				//return;								// return here
			}
			if (light < .5) { temp2 = light * (1 + satur); }
			else { temp2 = light + satur - light * satur; }
			temp1 = 2 * light - temp2;
			// for three colors
			var color:Number;
			for (var n:int = 0; n < 3; n++) {
				switch (n) {
					case 0: temp3 = hue + 1 / 3; break;
					case 1: temp3 = hue; break;
					case 2: temp3 = hue-1 / 3;
				}
				if (temp3 < 0) { temp3 += 1; }
				if (temp3 > 1) { temp3 -= 1; }
				if (6 * temp3 < 1) { color = temp1 + (temp2 - temp1) * 6 * temp3; }
				else {
					if (2 * temp3 < 1) { color = temp2; }
					else {
						if (3 * temp3 < 2) { color = temp1 + (temp2 - temp1) * ((2 / 3) - temp3) * 6; }
						else {color = temp1;}
					}
				}
				temp4 = uint (color * 255);
				switch (n) {
					case 0:
						red = temp4;
						currentValue = currentValue | (temp4 << 16);
						break;
					case 1:
						green = temp4;
						currentValue = currentValue | (temp4 << 8);
						break;
					case 2:
						blue = temp4;
						currentValue = currentValue | temp4;
				}
			}
			return currentValue;
		}
		
	}

}