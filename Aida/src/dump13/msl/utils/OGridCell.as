package dump13.msl.utils 
{
	import dump13.msl.types.Set;
	import flash.geom.Rectangle;
	/**
	 * Ячейка сетки OGrid может содержать набор объектов, зарегистрированный ПРЯМО НА НЕЙ, и/или дочернюю сетку OGrid, которая создаётся в том случае, если в наборе регистрируется слишком большое количество мелких объектов.
	 * @author dump13
	 */
	public class OGridCell 
	{
		/// Набор, содержащий объекты, зарегистрированные ИМЕННО В ЭТОЙ ЯЧЕЙКЕ (не во вложенной сетке). Может быть равен null.
		private var _items:Set = new Set ();
		private var _internalGrid:OGrid;
		
		
		public function OGridCell ()
		{
			
		}
		
		/**
		 * Очищает ячейку - удаляет все объекты из нее и из вложенной сетки (если она существует).
		 */
		public function clear ():void
		{
			_items.clear ();
			if (_internalGrid)
			{
				_internalGrid.clear ();
			}
		}
		
		
		/**
		 * Добавляет айтем в ячейку. Если айтем является малым, методу необходимо передать его действительный прямоугольник; если айтем не является малым, realRect должен быть равен null. Когда айтемов в ячейке становится больше, чем grid.maxItems, ячейка создает внутреннюю сетку OGrid и регистрирует в ней все свои МАЛЫЕ объекты.
		 * @param	item Айтем, который нужно зарегистрировать.
		 * @param	grid Сетка, которой принадлежит эта ячейка.
		 * @param	realRect Если айтем является малым, то real-прямоугольник айтема. Иначе null.
		 */
		internal function addItem (item:Object, grid:OGridSmart, realRect:Rectangle = null):void
		{
			// Если айтемов недостаточно для того, чтобы подразделить ячейку.
			if (numItems < grid._maxItems)
			{
				if (!_items.add (item))
				{
					throw new Error ("Item is already added to cell.");
				}
				return;
			}
			
			// Когда объектов становится слишком много, внутренняя сетка создается в любом случае, даже если малых объектов нет.
			if (!_internalGrid)
			{
				_internalGrid = new OGrid (grid._internalCellSize);
				
				// Перекидываем объекты из _items в _internalGrid. Эта операция производится для уже зарегистрированных в сетке grid айтемов, так что real-прямоугольники малых айтемов должны существовать.
				for (var i:Object in _items)
				{
					var curRealRect:Rectangle = grid.getItemRealRect (i);
					if (!curRealRect)
					{
						continue;
					}
					_items.remove (i);
					_internalGrid.addItemByRealRect (i, curRealRect);
				}
			}
			
			// Добавляем item куда нужно.
			if (realRect)
			{
				_internalGrid.addItemByRealRect (item, realRect);
				return;
			}
			if (!_items.add (item))
			{
				throw new Error ("Internal error: item is already registered in this cell.");
			}
		}
		
		/**
		 * Удаляет айтем из ячейки.
		 * @param	item Айтем, который нужно удалить.
		 * @param	grid Сетка, которой принадлежит эта ячейка.
		 * @param	isSmall Является ли айтем малым. Используется для того, чтобы метод не проводил второй раз проверку, которая уже была проведена в родительской сетке.
		 */
		internal function removeItem (item:Object, grid:OGridSmart, isSmall:Boolean):void
		{
			// Удаляю айтем оттуда, откуда следует.
			if (_internalGrid && isSmall)
			{
				_internalGrid.removeItem (item);
			}
			else
			{
				if (!_items.remove (item))
				{
					throw new Error ("Internal error: item not found.");
				}
			}
			
			// Remove 'this' then.
			if (_internalGrid && this.numItems <= grid._maxItems)
			{
				_internalGrid.collectAllItems (_items);
				_internalGrid.clear ();
				_internalGrid = null;
			}
		}
		
		/**
		 * Собирает все объекты ячейки в набор result. Набор перед поиском не очищается. Если экземпляр набора не передан, создается новый набор.
		 * @param	result Набор, в который будут помещаться объекты ячейки, либо null, если нужно создать новый набор.
		 * @return Набор, содержащий все объекты ячейки.
		 */
		public final function collectItems (result:Set = null):Set
		{
			result.unite (_items);
			if (_internalGrid)
			{
				_internalGrid.collectAllItems (result);
			}
			return result;
		}
		
		/**
		 * Помещает в набор result ВСЕ объекты, зарегистрированные в собственном наборе _items ячейки, и малые объекты внутренней сетки _internalGrid, находящиеся в ячейках, пересекающихся с прямоугольником realRect. Таким образом, производится грубый поиск по ячейкам внутренней сетки (без учета баундов самих объектов).
		 * @param	realRect	real-прямоугольник, внутри которого ведется поиск. В результаты поиска попадут абсолютно все объекты ячейки, не являющиеся МАЛЫМИ.
		 * @param	result 		Все большие объекты этой ячейки и часть малых объектов, попадающих в ячейки, пересекающиеся с прямоугольником realRect.
		 * @param	accurateSearch	Производить ли аккуратный поиск по внутреннему дереву (если оно существует) или собирать все его малые айтемы.
		 */
		public final function collectItemsInRealRect (realRect:Rectangle, result:Set, accurateSearch:Boolean):void
		{
			result.unite (_items);
			if (_internalGrid)
			{
				if (accurateSearch)
				{
					_internalGrid.getItemsInRealRect (realRect, result);
				}
				else
				{
					_internalGrid.collectAllItems (result);
				}
			}
		}
		
		/**
		 * Возвращает true, если айтем добавлен в эту сетку.
		 * @param	item Айтем, наличие которого необходимо проверить.
		 */
		[Inline]
		public final function hasItem (item:Object):Boolean
		{
			if (_internalGrid && _internalGrid.hasItem (item))
			{
				return true;
			}
			return _items.has (item);
		}
		
		/**
		 * Количество объектов, зарегистрированных в этой ячейке.
		 */
		[Inline]
		public final function get numItems ():int
		{
			return _internalGrid? _internalGrid.numItems + _items.length : _items.length;
		}
		
	}

}