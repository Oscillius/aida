package dump13.msl.pathfinding {
	/**
	 * Связный список, состоящий из ячеек <code>AStarCell</code>. Используется для представления пути.
	 * @author dump13
	 */
	public class AStarSet {
		private var _first:AStarCell;
		private var _length:int = 0;
		
		/**
		 * Конструктор связного списка.
		 */
		public function AStarSet () {
			
		}
		
		/**
		 * Добавляет элемент <code>cell</code> в список, в необходимую позицию (так, чтобы элементы в списке упорядочивались по возрастанию <code>f</code>, то есть чтобы в начале находились элементы с меньшим <code>f</code>).
		 * @param	cell Ячейка, которую необходимо добавить в связный список.
		 */
		public function add (cell:AStarCell):void {
			_length++;
			
			// No elements in list.
			if (!_first) {
				_first = cell;
				cell.next = null;
				return;
			}
			// Replacing the first element with 'cell'.
			if (cell.f < _first.f) {
				cell.next = _first;
				_first = cell;
				return;
			}
			// The 'cell' element won't be the first, going deeper.
			var cur:AStarCell = _first.next;
			var prev:AStarCell = _first;
			while (cur) {
				if (cell.f < cur.f) {
					prev.next = cell;
					cell.next = cur;
					break;
				}
				
				prev = cur;
				cur = cur.next;
			}
			// if we're at the end of the set, then push the 'cell' element there.
			if (!cur) {
				prev.next = cell;
				cell.next = null;
			}
		}
		
		/**
		 * Добавляет элемент в начало списка. При использовании этого метода вместо метода <code>add()</code> получается обычный, несортированный связный список.
		 * @param	cell Ячейка, которую необходимо добавить в начало списка.
		 */
		public function addUnsorted (cell:AStarCell):void {
			if (!_first) {
				_first = cell;
				cell.next = null;
				return;
			}
			cell.next = _first;
			_first = cell;
		}
		
		/**
		 * Находит и удаляет элемент <code>cell</code> из списка. Если элемент не найден, создаётся исключение.
		 * @param	cell Элемент, который необходимо удалить из списка.
		 */
		public function remove (cell:AStarCell):void {
			if (!_first) throw new Error ("Set is empty.");
			_length--;
			
			// Removing the first element.
			if (_first === cell) {
				_first = _first.next;
				cell.next = null;
				return;
			}
			var cur:AStarCell = _first.next;
			var prev:AStarCell = _first;
			while (cur) {
				if (cur === cell) {
					prev.next = cur.next;
					cur.next = null;
					return;
				}
				
				prev = cur;
				cur = cur.next;
			}
			// Element 'cell' is missing.
			throw new Error ("Element was not found.");
		}
		
		/**
		 * Удаляет первый элемент из списка. В случае сортированного списка первый элемент является самым выгодным с точки зрения эвристики (его <code>f</code> является наименьшим по сравнению с остальными элементами).
		 * @return Удалённый элемент.
		 */
		public function removeFirst ():AStarCell {
			var result:AStarCell = _first;
			if (!result) throw new Error ("Set is empty.");
			remove (result);
			return result;
		}
		
		/**
		 * Возвращает <code>true</code>, если список содержит элемент <code>cell</code>.
		 * @param	cell Ячейка, наличие которой необходимо проверить.
		 * @return <code>true</code>, если список содержит элемент <code>cell</code>.
		 */
		public function has (cell:AStarCell):Boolean {
			var cur:AStarCell = _first;
			while (cur) {
				if (cur === cell) return true;
				cur = cur.next;
			}
			return false;
		}
		
		/**
		 * Разъединяет все элементы в списке и зануляет ссылку на первый элемент.
		 */
		public function destroy ():void {
			var prev:AStarCell = _first;
			var cur:AStarCell;
			if (prev) cur = prev.next;
			while (cur) {
				prev.next = null;
				prev = cur;
				cur = cur.next;
			}
			_first = null;
			_length = 0;
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/**
		 * Количество элементов в связном списке.
		 */
		public function get length ():int { return _length; }
		
	}

}