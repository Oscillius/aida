package dump13.msl.pathfinding {
	/**
	 * Квадратная ячейка, используемая как составной элемент поля для поиска пути.
	 * @author dump13
	 */
	public class AStarCell {
		/**
		 * Целочисленная координата <code>x</code> ячейки.
		 */
		public var x:int;
		/**
		 * Целочисленная координата <code>y</code> ячейки.
		 */
		public var y:int;
		/**
		 * Линейный индекс ячейки (определяется как <code>y &#042; gridWidth + x</code>).
		 */
		public var index:int;
		/**
		 * Сумма <code>g + h</code> - стоимости пути от начальной ячейки до текущей и эвристической оценки расстояния до цели.
		 */
		public var f:Number = 0;
		/**
		 * Стоимость пути от начальной ячейки до текущей.
		 */
		public var g:Number = 0;
		/**
		 * Эвристика, оценивающая расстояние от этой ячейки до целевой ячейки.
		 */
		public var h:Number = 0;
		/**
		 * Проходимость этой ячейки.
		 */
		public var passable:Boolean = true;
		
		
		/**
		 * Следующий элемент в списке.
		 */
		public var next:AStarCell;
		/**
		 * Родитель этого элемента, используется при поиске пути.
		 */
		public var parent:AStarCell;
		
		/**
		 * Конструктор ячейки.
		 * @param	x Целочисленная координата <code>x</code> ячейки.
		 * @param	y Целочисленная координата <code>y</code> ячейки.
		 * @param	index Линейный индекс ячейки.
		 */
		public function AStarCell (x:int, y:int, index:int) {
			this.x = x;
			this.y = y;
			this.index = index;
		}
		
	}

}