package dump13.msl.pathfinding.test {
	import dump13.msl.pathfinding.AStar;
	import dump13.msl.pathfinding.AStarCell;
	import dump13.msl.pathfinding.AStarField;
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.ui.Keyboard;
	/**
	 * @private
	 * LMB - изменить проходимость ячейки. Shift+LMB - установить начальную ячейку. Space+LMB - установить целевую ячейку.
	 * @author dump13
	 */
	public class PFTest extends Sprite {
		private var _squares:Vector.<Sprite>;
		private var _field:AStarField;
		private var _aStar:AStar;
		private var _size:int;
		private var _len:int;
		
		private var _start:AStarCell;
		private var _end:AStarCell;
		private var _startSprite:Sprite;
		private var _endSprite:Sprite;
		
		private var _steps:Vector.<Sprite> = new Vector.<Sprite> ();
		
		private var _sKeyDown:Boolean;
		private var _eKeyDown:Boolean;
		
		public function PFTest () {
			_size = 40;
			_len = 12.5;
			
			if (stage) init ();
			else addEventListener (Event.ADDED_TO_STAGE, init);
		}
		
		private function init (e:Event = null):void {
			removeEventListener (Event.ADDED_TO_STAGE, init);
			
			stage.addEventListener (KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener (KeyboardEvent.KEY_UP, onKeyUp);
			
			_squares = new Vector.<Sprite> ();
			for (var yy:int = 0; yy < _size; yy++) {
				for (var xx:int = 0; xx < _size; xx++) {
					_squares.push (createSquare (xx, yy));
				}
			}
			
			_field = new AStarField (_size, _size);
			_aStar = new AStar ();
		}
		
		private function createSquare (x:int, y:int):Sprite {
			var s:Sprite = new Sprite ();
			redrawSquare (s, true);
			s.x = x * _len;
			s.y = y * _len;
			addChild (s);
			//s.addEventListener (MouseEvent.CLICK, onSquareClick);
			s.addEventListener (MouseEvent.MOUSE_DOWN, onSquareClick);
			return s;
		}
		
		private function redrawSquare (s:Sprite, passable:Boolean, start:Boolean = false, end:Boolean = false):void {
			var lineColor:uint = 0;
			if (start || end) lineColor = passable? 0xcccccc : 0x333333;
			
			var color:uint = passable? 0xcccccc : 0x333333;
			if (start) color = 0x33cc33;
			if (end) color = 0xcc3333;
			
			var g:Graphics = s.graphics;
			g.clear ();
			g.lineStyle (2, 0, 1);
			g.beginFill (color, 1);
			g.drawRect (0, 0, _len, _len);
			g.endFill ();
		}
		
		private function updatePath ():void {
			var n:int, na:int;
			
			if (!_start || !_end) return;
			
			na = _steps.length;
			for (n = 0; n < na; n++) removeChild (_steps[n]);
			_steps.length = 0;
			
			var path:Vector.<AStarCell> = _aStar.findPath (_field, _start, _end);
			if (!path) return;
			
			na = path.length;
			for (n = 0; n < na; n++) {
				var cell:AStarCell = path[n];
				
				var step:Sprite = new Sprite ();
				var g:Graphics = step.graphics;
				g.lineStyle (2, 0, 1);
				g.beginFill (0x3333cc, 1);
				g.drawCircle (0, 0, _len * .25);
				g.endFill ();
				step.x = (cell.x + .5) * _len;
				step.y = (cell.y + .5) * _len;
				step.mouseEnabled = false;
				
				_steps.push (step);
				addChild (step);
			}
		}
		
		private function onSquareClick (e:MouseEvent):void {
			var s:Sprite = Sprite (e.target);
			var index:int = _squares.indexOf (s);
			var cell:AStarCell = _field.getCellByIndex (index);
			
			if (!_sKeyDown && !_eKeyDown) {
				cell.passable = !cell.passable;
				redrawSquare (s, cell.passable, cell === _start, cell === _end);
				updatePath ();
			}
			if (_sKeyDown) {
				if (cell === _end) return;
				
				if (_start) redrawSquare (_startSprite, _start.passable, false, false);
				_start = cell;
				_startSprite = s;
				redrawSquare (s, cell.passable, cell === _start, cell === _end);
				updatePath ();
			}
			if (_eKeyDown) {
				if (cell === _start) return;
				
				if (_end) redrawSquare (_endSprite, _end.passable, false, false);
				_end = cell;
				_endSprite = s;
				redrawSquare (s, cell.passable, cell === _start, cell === _end);
				updatePath ();
			}
		}
		
		private function onKeyDown (e:KeyboardEvent):void {
			switch (e.keyCode) {
				case Keyboard.SHIFT:
					_sKeyDown = true;
					break;
				case Keyboard.SPACE:
					_eKeyDown = true;
					break;
			}
		}
		
		private function onKeyUp (e:KeyboardEvent):void {
			switch (e.keyCode) {
				case Keyboard.SHIFT:
					_sKeyDown = false;
					break;
				case Keyboard.SPACE:
					_eKeyDown = false;
					break;
			}
		}
		
	}

}