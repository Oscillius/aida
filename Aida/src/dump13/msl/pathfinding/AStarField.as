package dump13.msl.pathfinding {
	import flash.geom.Point;
	/**
	 * Поле проходимостей, используемое при определении кратчайшего пути.
	 * @author dump13
	 */
	public class AStarField {
		private var _width:int;
		private var _height:int;
		private var _intSquare:int;
		
		private var _cells:Vector.<AStarCell>;
		
		/**
		 * Конструктор поля проходимостей. Если параметр <code>passabilities</code> не задан, все ячейки поля будут проходимыми.
		 * @param	width Ширина поля, в ячейках.
		 * @param	height Высота поля, в ячейках.
		 * @param	passabilities Вектор проходимостей, каждый i-й элемент которого равен <code>true</code>, если ячейка с линейным индексом i является проходимой. Линейный индекс ячейки определяется её координатами и шириной поля: <code>cell.index = cell.y &#042; field.width + cell.x</code>. Вектор должен иметь <code>width &#042; height</code> элементов.
		 */
		public function AStarField (width:int, height:int, passabilities:Vector.<Boolean> = null) {
			_width = width;
			_height = height;
			_intSquare = width * height;
			
			makeCells ();
			if (passabilities) setPassabilitiesFromVector (passabilities);
		}
		
		private function makeCells ():void {
			_cells = new Vector.<AStarCell> (_intSquare);
			var na:int = _intSquare;
			var xx:int = 0;
			var yy:int = 0;
			var xmax:int = _width;
			for (var n:int = 0; n < na; n++) {
				var cell:AStarCell = new AStarCell (xx, yy, n);
				cell.passable = true;
				_cells[n] = cell;
				
				if (++xx >= xmax) {
					xx = 0;
					yy++;
				}
			}
		}
		
		/**
		 * Вызывает <code>disconnectCells()</code> и удаляет все ячейки из вектора.
		 */
		public function destroy ():void {
			disconnectCells ();
			_cells.length = 0;
		}
		
		/**
		 * Зануляет ссылки ячеек друг на друга. Позволяет убедиться, что все связные списки, созданные алгоритмом поиска пути, и найденные возможные пути (также являющиеся цепочками из ячеек) уничтожены.
		 */
		public function disconnectCells ():void {
			var na:int = _cells.length;
			for (var n:int = 0; n < na; n++) {
				var cell:AStarCell = _cells[n];
				cell.next = null;
				cell.parent = null;
			}
			_cells.length = 0;
		}
		
		/**
		 * Сбрасывает <code>f</code>, <code>g</code> и <code>h</code> всех ячеек в 0. Зануляет <code>parent</code> и <code>next</code> для каждой ячейки. Вызывать перед новым поиском кратчайшего пути, чтобы уничтожить уже существующие цепочки.
		 */
		public function resetAllCells ():void {
			var na:int = _cells.length;
			for (var n:int = 0; n < na; n++) {
				var cell:AStarCell = _cells[n];
				cell.f = cell.g = cell.h = 0;
				cell.parent = null;
				cell.next = null;
			}
		}
		
		/**
		 * Устанавливает проходимость ячеек из вектора <code>passabilities</code>. Ячейке с координатами <code>(x, y)</code> при ширине поля <code>width</code> соответствует элемент вектора с индексом <code>y &#042; width + x</code>. Если длина вектора не равна количеству ячеек поля, создаётся исключение.
		 * @param	passabilities Вектор с проходимостями ячеек.
		 */
		public function setPassabilitiesFromVector (passabilities:Vector.<Boolean>):void {
			var na:int = _intSquare;
			if (passabilities.length != na) {
				throw new Error ("The 'passabilities' vector has wrong length (" + passabilities.length + " instead of " + _intSquare);
			}
			for (var n:int = 0; n < na; n++) {
				_cells[n].passable = passabilities[n];
			}
		}
		
		/**
		 * Устанавливает проходимости ячеек, расположенных внутри прямоугольника. Координаты и размеры этого прямоугольника - целочисленные. Если <code>xSize = 1</code> и <code>ySize = 1</code>, будет изменена проходимость одной ячейки.
		 * @param	x Координата <code>x</code> левого верхнего угла прямоугольника.
		 * @param	y Координата <code>y</code> левого верхнего угла прямоугольника.
		 * @param	xSize Ширина прямоугольника.
		 * @param	ySize Высота прямоугольника.
		 * @param	passable Устанавливаемая проходимость ячейки.
		 */
		public function setPassabilitiesRect (x:int, y:int, xSize:int, ySize:int, passable:Boolean):void {
			if (xSize <= 0 || ySize <= 0) return;
			var maxX:int = x + xSize;
			var maxY:int = y + ySize;
			for (var curX:int = x; curX < maxX; curX++) {
				for (var curY:int = y; curY < maxY; curY++) {
					getCellByCoords (curX, curY).passable = passable;
					//trace ("Passability of [" + curX + ", " + curY + "] was set to " + passable);
				}
			}
		}
		
		/**
		 * Возвращает экземпляр ячейки по её координатам.
		 * @param	x Координата <code>x</code> ячейки.
		 * @param	y Координата <code>y</code> ячейки.
		 * @return Экземпляр ячейки.
		 */
		public function getCellByCoords (x:int, y:int):AStarCell {
			return _cells[getIndexByCoords (x, y)];
		}
		
		/**
		 * Возвращает экземпляр ячейки по её линейному индексу.
		 * @param	index Линейный индекс ячейки.
		 * @return Экземпляр ячейки.
		 */
		public function getCellByIndex (index:int):AStarCell {
			return _cells[index];
		}
		
		/**
		 * Помещает индексы ячеек, расположенных рядом с ячейкой <code>(x, y)</code>, в вектор <code>output</code>. Найденные индексы будут помещены в начало вектора, остальные его элементы (если соседей меньше восьми) будут установлены в -1 (при использовании полученного вектора можно обходить его до первой встреченной -1).
		 * @param	x Координата <code>x</code> ячейки, для которой находятся индексы соседей.
		 * @param	y Координата <code>y</code> ячейки, для которой находятся индексы соседей.
		 * @param	output  Вектор, в который будут помещены индексы искомых ячеек. Желательно сделать его фиксированным и состоящим из восьми элементов. По окончании работы метода все найденные индексы будут помещены в начальную часть вектора; если их меньше девяти, то оставшимся элементам будут присвоены значения, равные -1.
		 */
		public function getNeighborsIndices (x:int, y:int, output:Vector.<int>):void {
			var xi:int = x - 1;
			var yi:int = y - 1;
			var xa:int = x + 1;
			var ya:int = y + 1;
			if (xi < 0) xi = 0;
			if (yi < 0) yi = 0;
			if (xa >= _width) xa = _width - 1;
			if (ya >= _height) ya = _height - 1;
			
			var index:int = 0;
			for (var xx:int = xi; xx <= xa; xx++) {
				for (var yy:int = yi; yy <= ya; yy++) {
					if (xx != x && yy != y) output[index++] = getIndexByCoords (xx, yy);
				}
			}
			for (; index < 8; index++) output[index] = -1;
		}
		
		/**
		 * Записывает координаты ячеек, расположенных рядом с ячейкой <code>(x, y)</code>, в соответствующие свойства объектов <code>Point</code> из вектора <code>output</code>. Этот вектор уже должен содержать восемь элементов, не равных <code>null</code>. Если найденных ячеек меньше восьми, координаты <code>x</code> последующих элементов вектора <code>output</code> устанавливаются в -1.
		 * @param	x Координата <code>x</code> ячейки, рядом с которой следует вести поиск.
		 * @param	y Координата <code>y</code> ячейки, рядом с которой следует вести поиск.
		 * @param	output Вектор, состоящий из восьми экземпляров <code>Point</code>. В него помещается результат.
		 */
		public function getNeighborsCoords (x:int, y:int, output:Vector.<Point>):void {
			var xi:int = x - 1;
			var yi:int = y - 1;
			var xa:int = x + 1;
			var ya:int = y + 1;
			if (xi < 0) xi = 0;
			if (yi < 0) yi = 0;
			if (xa >= _width) xa = _width - 1;
			if (ya >= _height) ya = _height - 1;
			
			var index:int = 0;
			for (var xx:int = xi; xx <= xa; xx++) {
				for (var yy:int = yi; yy <= ya; yy++) {
					if (xx != x && yy != y) {
						var point:Point = output[index++];
						point.x = xx;
						point.y = yy;
					}
				}
			}
			for (; index < 8; index++) output[index].x = -1;
		}
		
		/**
		 * Помещает в вектор <code>output</code> экземпляры ячеек, соседствующих с ячейкой <code>cell</code> и подходящих в качестве смежных при проверке пути. Для этого ячейка-сосед должна быть проходимой и, кроме того, по пути к ней из ячейки <code>cell</code> нельзя срезать углы. Если подходящих ячеек меньше восьми, оставшиеся элементы вектора <code>output</code> устанавливаются в <code>null</code>.
		 * @param	cell Ячейка, вокруг которой следует вести поиск.
		 * @param	output Вектор из ячеек, в который помещаются результаты поиска.
		 */
		public function getPassableNeighbors (cell:AStarCell, output:Vector.<AStarCell>):void {
			var xi:int = cell.x - 1;
			var yi:int = cell.y - 1;
			var xa:int = cell.x + 1;
			var ya:int = cell.y + 1;
			var leftValid:Boolean = xi >= 0;
			var rightValid:Boolean = xa < _width;
			var topValid:Boolean = yi >= 0;
			var bottomValid:Boolean = ya < _height;
			
			var index:int = cell.index;
			var curIndex:int;
			
			if (leftValid) 		var left:AStarCell = _cells[index - 1];
			if (rightValid) 	var right:AStarCell = _cells[index + 1];
			
			if (topValid) {
				curIndex = index - _width;
				var top:AStarCell = _cells[curIndex];
				if (leftValid) 	var topLeft:AStarCell = _cells[curIndex - 1];
				if (rightValid) var topRight:AStarCell = _cells[curIndex + 1];
			}
			
			if (bottomValid) {
				curIndex = index + _width;
				var bottom:AStarCell = _cells[curIndex];
				if (leftValid) 	var bottomLeft:AStarCell = _cells[curIndex - 1];
				if (rightValid) var bottomRight:AStarCell = _cells[curIndex + 1];
			}
			
			if (left && !left.passable) {
				topLeft = null;
				bottomLeft = null;
			}
			if (right && !right.passable) {
				topRight = null;
				bottomRight = null;
			}
			if (top && !top.passable) {
				topLeft = null;
				topRight = null;
			}
			if (bottom && !bottom.passable) {
				bottomLeft = null;
				bottomRight = null;
			}
			
			index = 0;
			if (topLeft && topLeft.passable) output[index++] = topLeft;
			if (topRight && topRight.passable) output[index++] = topRight;
			if (bottomLeft && bottomLeft.passable) output[index++] = bottomLeft;
			if (bottomRight && bottomRight.passable) output[index++] = bottomRight;
			/*if (topLeft) output[index++] = topLeft;
			if (topRight) output[index++] = topRight;
			if (bottomLeft) output[index++] = bottomLeft;
			if (bottomRight) output[index++] = bottomRight;*/
			
			if (left && left.passable) output[index++] = left;
			if (right && right.passable) output[index++] = right;
			if (top && top.passable) output[index++] = top;
			if (bottom && bottom.passable) output[index++] = bottom;
			
			for (; index < 8; index++) output[index] = null;
		}
		
		/**
		 * Путь <code>g</code>, который необходимо пройти от ячейки <code>(x0, y0)</code> до ячейки <code>(x1, y1)</code>. Ячейки должны быть соседними (и могут располагаться по диагонали относительно друг друга). (Введена временная проверка).
		 * @param	x0 Координата <code>x</code> первой ячейки.
		 * @param	y0 Координата <code>y</code> первой ячейки.
		 * @param	x1 Координата <code>x</code> второй ячейки.
		 * @param	y1 Координата <code>y</code> второй ячейки.
		 * @return Путь <code>g</code>.
		 */
		public function getGBetweenNeighbors (x0:int, y0:int, x1:int, y1:int):Number {
			var dx:int = x1 - x0;
			var dy:int = y1 - y0;
			
			// Temporary check.
			if (dx < -1 || dx > 1 || dy < -1 || dy > 1) {
				throw new Error ("Cells are not neighbors.");
			}
			
			if (dx && dy) return 1.41421356;
			return 1;
		}
		
		// =====================================================
		// ===================== INDICES =======================
		// =====================================================
		
		/**
		 * Возвращает линейный индекс ячейки по её координатам.
		 * @param	x Координата <code>x</code> ячейки.
		 * @param	y Координата <code>y</code> ячейки.
		 * @return Индекс ячейки с указанными координатами.
		 */
		public function getIndexByCoords (x:int, y:int):int {
			return y * _width + x;
		}
		
		/**
		 * Возвращает координаты ячейки по её линейному индексу.
		 * @param	index Индекс ячейки.
		 * @return Координаты ячейки.
		 */
		public function getCoordsByIndex (index:int):Point {
			var yy:int = Math.floor (index / _width);
			//return new Point (index - yy, yy);
			return new Point (index - yy * _width, yy);
		}
		
		/**
		 * Возвращает <code>true</code>, если переданные координаты валидны (то есть если ячейка с такими координатами находится внутри поля).
		 * @param	x Координата <code>x</code> ячейки.
		 * @param	y Координата <code>y</code> ячейки.
		 * @return <code>true</code>, если координаты валидны.
		 */
		public function cellIsInside (x:int, y:int):Boolean {
			return x >= 0 && x < _width && y >= 0 && y < _height;
		}
		
		/**
		 * Ограничивает координаты ячейки размерами поля.
		 * @param	coords Координаты предполагаемой ячейки, которые необходимо ограничить.
		 */
		public function constrainCoords (coords:Point):void {
			if (coords.x < 0) coords.x = 0;
			else if (coords.x >= _width) coords.x = _width - 1;
			if (coords.y < 0) coords.y = 0;
			else if (coords.y >= _height) coords.y = _height - 1;
		}
		
	}

}