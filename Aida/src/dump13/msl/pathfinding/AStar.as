package dump13.msl.pathfinding {
	import dump13.msl.types.Set;
	/**
	 * Объект, выполняющий поиск кратчайшего пути в квадратной сетке по алгоритму A-звезда. Проходимости ячеек сетки задаются булевыми величинами. Предполагается, что можно совершать только горизонтальные или вертикальные перемещения из ячейки в ячейку (срезать углы нельзя).
	 * @author dump13
	 */
	public class AStar {
		
		/**
		 * Конструктор А-звезды.
		 */
		public function AStar() {
			
		}
		
		/**
		 * Выполняет поиск кратчайшего пути.
		 * @param	field Поле проходимостей.
		 * @param	start Ячейка, соответствующая началу пути.
		 * @param	goal Целевая ячейка.
		 * @return Вектор, состоящий из ячеек, представляющих найденный путь, или <code>null</code>, если путь не найден. Ячейки в векторе располагаются в порядке от целевой ячейки к стартовой (то есть от конца пути к его началу).
		 */
		public function findPath (field:AStarField, start:AStarCell, goal:AStarCell):Vector.<AStarCell> {
			field.resetAllCells ();		// Похоже, помогло :)
			
			//var closedSet:AStarSet = new AStarSet ();
			var closedSet:Set = new Set ();
			var openedSet:AStarSet = new AStarSet ();
			openedSet.add (start);
			
			start.g = 0;
			start.h = getHeuristicsDistance (start, goal);
			start.f = start.g + start.h;
			
			var nbs:Vector.<AStarCell> = new Vector.<AStarCell> (8, true);
			
			while (openedSet.length) {
				var xx:AStarCell = openedSet.removeFirst ();
				if (xx === goal) {
					var path:Vector.<AStarCell> = reconstructPath (goal);
					openedSet.destroy ();
					//closedSet.destroy ();
					closedSet.clear ();
					return path;
				}
				closedSet.add (xx);
				
				field.getPassableNeighbors (xx, nbs);
				for (var n:int = 0; n < 8; n++) {
					var yy:AStarCell = nbs[n];
					if (!yy) break;
					
					if (!closedSet.has (yy)) {
						var tentG:Number = xx.g + field.getGBetweenNeighbors (xx.x, xx.y, yy.x, yy.y);
						
						var tentBetter:Boolean;
						if (!openedSet.has (yy)) {
							openedSet.add (yy);
							tentBetter = true;
						}
						else {
							tentBetter = tentG < yy.g;
						}
						
						if (tentBetter) {
							yy.parent = xx;
							yy.g = tentG;
							yy.h = getHeuristicsDistance (yy, goal);
							yy.f = yy.g + yy.h;
							
							openedSet.remove (yy);
							openedSet.add (yy);
						}
					}
				}
			}
			
			return null;
		}
		
		private function getHeuristicsDistance (current:AStarCell, goal:AStarCell):Number {
			var dx:int = goal.x - current.x;
			var dy:int = goal.y - current.y;
			if (dx < 0) dx = -dx;
			if (dy < 0) dy = -dy;
			return dx + dy;
		}
		
		private function reconstructPath (goal:AStarCell):Vector.<AStarCell> {
			var path:Vector.<AStarCell> = new Vector.<AStarCell> ();
			var cur:AStarCell = goal;
			while (cur) {
				path.push (cur);
				cur = cur.parent;
			}
			return path;
		}
		
	}

}