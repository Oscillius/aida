package dump13.msl.types {
	import flash.utils.Dictionary;
	/**
	 * <p>Last time modified for: MSL v0.5.</p>
	 * <p>Карта - ассоциативный массив, наследующийся от <code>Dictionary</code> и хранящий пары "ключ - значение". В качестве ключа может быть использована ссылка на объект. С одним и тем же ключом можно ассоциировать только единственное значение.</p>
	 * @author dump13
	 */
	public class Map extends Dictionary {
		/**
		 * @private
		 */
		protected var _length:uint = 0;
		
		/**
		 * Конструктор ассоциативного массива.
		 * @param	weakKeys Использовать ли в карте слабые ссылки.
		 */
		public function Map (weakKeys:Boolean = false) {
			super (weakKeys);
		}
		
		/**
		 * Создаёт и возвращает копию текущего ассоциативного массива.
		 * @return Копия текущей карты.
		 */
		public function clone ():Map {
			var map:Map = new Map ();
			for (var key:Object in this) {
				map.add (key, this[key]);
			}
			return map;
		}
		
		/**
		 * Очищает ассоциативный массив, удаляя из него все пары "ключ-значение".
		 */
		public function clear ():void {
			for (var key:Object in this) {
				delete this[key];
			}
			_length = 0;
		}
		
		/**
		 * Добавляет пару "ключ - значение". Если с ключом <code>key</code> уже ассоциировано какое-то значение, оно будет заменено на новое значение <code>value</code>.
		 * @param	key Объект, используемый в качестве ключа.
		 * @param	value Объект-значение, ассоциируемый с ключом <code>key</code>.
		 */
		public function add (key:Object, value:Object):void {
			if (this[key] == undefined) _length++;
			this[key] = value;
		}
		
		/**
		 * Удаляет пару "ключ - значение". Если пара не найдена, метод вернёт <code>false</code> (иначе - <code>true</code>).
		 * @param	key Ключ, по которому будет искаться удаляемая пара.
		 * @return Значение <code>true</code>, если пара была найдена и успешно удалена, или <code>false</code>, если пара не была найдена.
		 */
		public function remove (key:Object):Boolean {
			if (this[key] != undefined) {
				delete this[key];
				_length--;
				return true;
			}
			return false;
		}
		
		/**
		 * Удаляет из ассоциативного массива случайную пару и возвращает значение (<code>value</code>) этой пары (ключ не возвращается). Если массив пуст, метод ничего не возвращает.
		 * @return Удалённая из ассоциативного массива пара либо <code>null</code>, если массив оказался пуст.
		 */
		public function pop ():* {
			var i:Object, temp:Object;
			if (_length == 0) { return; }
			for (i in this) {
				temp = this[i];
				delete this[i];
				_length--;
				return temp;
			}
		}
		
		/**
		 * Возвращает случайную пару, содержащуюся в ассоциативном массиве (сам массив не модифицируется), либо <code>null</code>, если массив является пустым.
		 * @return Первая найденная пара "ключ - значение" либо <code>null</code>, если массив пуст.
		 */
		public function peek ():* {
			for (var i:Object in this) {
				return i;
			}
			return;
		}
		
		/**
		 * Определяет, содержит ли ассоциативный массив пару с указанным ключом <code>key</code>. Результат вызова этого метода аналогичен результату выполнения <code>myMap[key] != undefined</code>.
		 * @param	key Проверяемый ключ.
		 * @return Содержит ли текущий массив переданный ключ.
		 */
		public function hasKey (key:Object):Boolean {
			return (this[key] != undefined);
		}
		
		/**
		 * Проходит по всем парам ассоциативного массива до тех пор, пока не встретит пару, содержащую значение <code>value</code>; в этом случае возвращает <code>true</code>. Если такой пары не нашлось, возвращает <code>false</code>.
		 * @param	value Значение, которое должна содержать искомая пара.
		 * @return Содержит ли текущий массив указанное значение.
		 */
		public function hasValue (value:Object):Boolean {
			var i:Object;
			for each (i in this) {
				if (i === value) {
					return true;
				}
			}
			return false;
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/**
		 * Количество пар "ключ - значение", хранящееся в данном ассоциативном массиве.
		 */
		public function get length ():uint {
			return _length;
		}
		
	}

}