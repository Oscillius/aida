package dump13.msl.types {
	import flash.utils.ByteArray;
	/**
	 * <p>Last time modified for: MSL v0.5.</p>
	 * <p>Битовый массив расширяет функционал байтового массива, используя композицию. Он имеет методы для доступа к отдельным битам потока и позволяет производить "плотную" запись данных. Как запись, так и чтение производятся с помощью воображаемого курсора, помещенного внутрь массива над определенным битом. Текущая позиция курсора описывается с помощью двух значений - базы <code>posBase</code> и смещения <code>posShift</code>. Тем не менее, можно посчитать и абсолютное положение курсора, то есть выразить его в битах:</p>
	 * <p><code>position = posBase &#42; 8 + posShift</code></p>
	 * <p>База - это текущий байт, а смещение - это текущий бит, расположенный внутри текущего байта. Подобным образом (с помощью базы <code>lenBase</code> и смещения <code>lenShift</code>) определяется и длина массива. При этом <code>lenBase</code> определяется как количество полностью заполненных байт (а не как действительная длина тела <code>body</code>). Для определения действительной длины массива (занимаемого им в памяти объема) нужно пользоваться соответствующим геттером.</p>
	 * @author dump13
	 */
	public class BitArray {
		/**
		 * Тело битового массива - байтовый массив.
		 */
		protected var _body:ByteArray;
		/**
		 * База длины. Количество полностью заполненных байт (базовая длина массива). При длине массива в 18 бит она равна двум (16 бит = 2 байта, и 2 бита в остатке).
		 */
		protected var _lenBase:uint;
		/**
		 * Смещение длины. Размер последнего байта массива, в битах (0...7).
		 */
		protected var _lenShift:int;
		/**
		 * База курсора. Позиция текущего байта (того, в который/из которого происходит запись/чтение). Свойство аналогично ByteArray.position. Данный байт содержит текущий бит.
		 */
		protected var _posBase:uint;
		/**
		 * Смещение курсора. Положение текущего бита внутри текущего байта (0...7).
		 */
		protected var _posShift:int;
		/**
		 * Переменная, 8 младших бит которой хранят байт, над которым производятся текущие операции. Будет полезен, если в один и тот же байт последовательно записываются короткие типы данных. Если не определен, устанавливается в -1.
		 */
		protected var _cacheByte:int;
		
		/**
		 * @private
		 */
		public static const RIGHT_MASKS:Vector.<int> = Vector.<int> ([0, 1, 3, 7, 15, 31, 63, 127, 255, 511, 1023, 2047, 4095, 8191, 16383, 32767, 65535, 131071, 262143, 524287, 1048575, 2097151, 4194303, 8388607, 16777215, 33554431, 67108863, 134217727, 268435455, 536870911, 1073741823, 2147483647, 4294967295]);
		/**
		 * @private
		 */
		public static const LEFT_MASKS:Vector.<int> = Vector.<int> ([0, 128, 192, 224, 240, 248, 252, 254, 255]);
		/**
		 * @private
		 */
		public static const POW_OF_TWO:Vector.<uint> = Vector.<uint> ([1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072, 262144, 524288, 1048576, 2097152, 4194304, 8388608, 16777216, 33554432, 67108864, 134217728, 268435456, 536870912, 1073741824, 2147483648]);
		
		
		/**
		 * Конструктор битового массива.
		 */
		public function BitArray () {
			_body = new ByteArray ();
			_lenBase = 0;
			_lenShift = 0;
			_posBase = 0;
			_posShift = 0;
			_cacheByte = -1;
		}
		
		/**
		 * Очищает битовый массив и устанавливает позицию курсора в 0 : 0.
		 */
		public function clear ():void {
			_body.clear ();
			_posBase = 0;
			_posShift = 0;
			_lenBase = 0;
			_lenShift = 0;
			_cacheByte = -1;
		}
		
		/**
		 * Устанавливает текущую позицию курсора. Курсор определяет байт (база) и бит (смещение), в который будет производится запись или из которого будет производиться чтение.
		 * @param	base База курсора.
		 * @param	shift Смещение курсора.
		 */
		public function setPosition (base:uint, shift:int):void {
			if (base > _lenBase || (base == _lenBase && shift > _lenShift)) {
				throw new Error ("Trying to set wrong position.");
			}
			_posBase = base;
			_posShift = shift;
			_body.position = _posBase;		// Устанавливаем положение для рабочего байтового массива
			_cacheByte = -1;
		}
		
		/**
		 * Устанавливает смещение текущей позиции курсора в битах (внутри текущего байта).
		 * @param	shift Смещение текущей позиции курсора, в битах (0...7).
		 */
		public function setPositionShift (shift:int):void {
			if (_posBase == _lenBase && shift > _lenShift) {
				throw new Error ("Trying to set wrong position shift.");
			}
			_posShift = shift;
		}
		
		/**
		 * Создает битовый массив из байтового с учетом переданных данных о его точной длине. У нового битового массива курсор будет установлен в позицию (0 : 0).
		 * @param	byteArray Байтовый массив, из которого необходимо создать битовый массив.
		 * @param	lengthBase База длины создаваемого битового массива.
		 * @param	lengthShift Смещение длины создаваемого битового массива.
		 * @return Новый битовый массив, использующий в качестве тела <code>byteArray</code> и имеющий длину <code>lengthBase * 8 + lengthShift</code> бит.
		 */
		public static function construct (byteArray:ByteArray, lengthBase:uint, lengthShift:int):BitArray {
			var ba:BitArray = new BitArray ();
			ba._body = byteArray;
			ba._lenBase = lengthBase;
			ba._lenShift = lengthShift;
			ba.setPosition (0, 0);
			return ba;
		}
		
		// =======================================================
		// ======================= WRITING =======================
		// =======================================================
		
		/**
		 * Записывает величину <code>data</code> размером в <code>size</code> бит, начиная с текущей позиции курсора <code>(posBase : posShift)</code>. Если размер <code>size</code> меньше 32 бит, учитываются только младшие байты. Первыми записываются старшие биты объекта <code>data</code>, последними - младшие (правые).
		 * Данный метод поддерживает форматы, имеющие размер не более 32 бит.
		 * @param	data Объект <code>uint</code>, младшие (или все) биты которого содержат данные, предназначенные для записи в текущий битовый массив.
		 * @param	size Количество бит объекта <code>data</code>, которые необходимо записать. Значение от 1 до 32.
		 */
		public function writeData (data:uint, size:uint):void {
			var n:int, m:int;
			var leftSize:int, midSize:int, rightSize:int;		// Размер правой части - в байтах, а не в битах.
			var dif:int;
			var emask:int, wmask:int;					// Existing data mask, Written data mask.
			var esize:int;								// Размер маски для существующих данных.
			if (!_posShift) {							// ЕСЛИ левая часть отсутствует.
				if (size == 8) {						// ЕСЛИ размер записываемой величины совпадает с размером кэша.
					_body.writeByte (data);				// Остальные 24 старших бита игнорируются.
					_posBase++;
					if (_lenBase < _posBase) {			// При необходимости устанавливаем новую базовую длину массива.
						_lenBase = _posBase;
						_lenShift = 0;
					}
					_cacheByte = -1;					// Курсор устанавливается над следующим байтом - кэш не определен.
					return;								// ВЫХОД.
				}
				if (size < 8) {							// ЕСЛИ присутствует только правая часть (_lenBase меняться не будет).
					esize = 8 - size;					// Количество бит в кэше (справа), которое нужно сохранить неизмененными.
					//wmask = LEFT_MASKS[size];
					emask = RIGHT_MASKS[esize];
					if (_posBase < _body.length) {				// ЕСЛИ не конец файла (нужно прочитать хотя бы один байт).
						if (_cacheByte == -1) {					// ЕСЛИ кэш не определен.
							_cacheByte = _body.readUnsignedByte ();
							_body.position--;
						}
						_cacheByte = (data << esize) | (_cacheByte & emask);
						_body.writeByte (_cacheByte);
						_body.position--;						// Записали неполный байт - указатель тела вернуть на место.
						_posShift = size;						// Базовое положение указателя остается без изменений.
						if (_lenBase == _posBase && _lenShift < _posShift) {
							_lenShift = _posShift;				// Обновляем при необходимости смещение длины.
						}
					}
					else {										// ЕСЛИ указатель находится над концом файла.
						_cacheByte = data << esize;
						_body.writeByte (_cacheByte);			// Просто записываем в тело новый байт с величиной.
						_body.position--;						// Записан неполный байт.
						_posShift = size;						// Смещаем указатель на size бит вправо, текущий байт остается тем же.
						_lenShift = _posShift;					// Т.к. конец файла, _lenBase = _posBase и _lenShift=0.
																// Если бы _lenShift был ненулевым, это означало бы, что в тело уже
																// что-то записывали.
					}
					return;								// ВЫХОД.
				}
				// Левая часть отсутствует, могут присутствовать средняя и правая части.
				midSize = size >> 3;					// Находим размер средней части (в байтах). В данном случае он ненулевой.
				rightSize = size - (midSize << 3);		// Находим размер правой части (в битах).
				if (midSize) {							// ЕСЛИ центральная часть существует.
					m = midSize-1;
					for (n = 0; n < midSize; n++ ) {		// Смещаем значение на (8 * m + rightSize) бит вправо и берем 8 младших бит.
						_body.writeByte ((data >> ((m << 3) + rightSize)) & 255);		// Здесь пишем полные байты (смещаться назад не нужно).
						m--;
					}
					_posBase += midSize;					// Положение указателя (в байтах) смещается на длину средней части.
															// _posShift = 0 уже установлено в условии.
					if (_lenBase < _posBase) { 				// (_lenBase == _posBase && _lenShift < _posShift) выполняется автоматом.
						_lenBase = _posBase;				// Общая длина обновится, только если будут записаны новые байты.
						_lenShift = 0;
					}
				}
				if (rightSize) {						// ЕСЛИ правая часть присутствует (только если была центральная часть).
														// Известно, что _posShift = 0.
					wmask = LEFT_MASKS[rightSize];			// Будет использоваться для комбинации (не для извлечения из data). 11100000
					esize = 8 - rightSize;					// Определяем, сколько бит имеющихся данных нужно сохранить.
					if (_posBase < _body.length) {			// ЕСЛИ мы можем прочитать один байт из тела (для кэша).
						_cacheByte = _body.readUnsignedByte ();
						_body.position--;
						emask = RIGHT_MASKS[esize];			// Находим маску для для существующих данных. 00011111
						_cacheByte = ((data << esize) & wmask) | (_cacheByte & emask);
						_body.writeByte (_cacheByte);
						_body.position--;					// Т.к. правая часть не может быть полной (максимум 7 бит).
						_posShift = rightSize;				// _posShift был равен нулю, поэтому всяко увеличится.
						if (_lenBase == _posBase && _lenShift < _posShift) {	// Может оказаться, что мы прочитали в кэш последний байт
							_lenShift = _posShift;								// тела, поэтому смещение длины может измениться.
						}
					}
					else {									// ЕСЛИ указатель в конце файла (кэш не прочитать). _lenBase = _posBase.
						_cacheByte = (data << esize);		// Так как сдвигаем влево, маска не нужна (справа нули).
						_body.writeByte (_cacheByte);
						_body.position--;
						_posShift = rightSize;
						if (_lenShift < _posShift) {		// Обновляем смещение длины (базовая длина обновлена сверху).
							_lenShift = _posShift;			// Дополнительных условий не вводим, т.к. _lenBase = _posBase.
						}
					}
				}			// ПРОВЕРЕНО :)
				else {									// ЕСЛИ правой части нет.
					_cacheByte = -1;						// Кэш неизвестен.
															// После записи средней части _posShift = 0, а общая длина уже проверена.
				}
				return;										// ВЫХОД.
			}
			else {										// ЕСЛИ левая часть присутствует (_posShift!=0).
														// В этом случае также невозможна ситуация, когда размер тела не совпадает
														// со значением _lenBase (мы всегда можем прочитать кэш).
				leftSize = 8 - _posShift;				// Максимальный размер левой части.
				dif = leftSize - size;
				if (dif >= 0) {							// ЕСЛИ записываемая величина поместится в текущий байт (только левая часть).
					wmask = RIGHT_MASKS[size] << dif;	// Маска для записываемой величины.
					if (_cacheByte == -1) {
						_cacheByte = _body.readUnsignedByte ();
						_body.position --;
					}
					_cacheByte = ((data << dif) & wmask) | (_cacheByte & (~wmask));
					_body.writeByte (_cacheByte);		// Смотри следующий ELSE.
					_posShift += size;					// В данном случае достаточно переместить сдвиг указателя на размер новой записи.
					if (_posShift == 8) {				// За исключением этого случая.
						_posShift = 0;
						_posBase ++;
						_cacheByte = -1;				// Так как базовое положение указателя изменилось, кэш не определен.
						if (_lenBase < _posBase) {		// Так как переходим в новый байт, базовая длина может измениться.
							_lenBase = _posBase;
							_lenShift = 0;
						}
					}
					else {
						_body.position --;				// Если есть хотя бы один свободный бит в текущем байте, откатываем указатель назад.
					}
					if (_lenBase == _posBase && _lenShift < _posShift) {	// Проверку _lenBase >= _posBase сделали сверху.
						_lenShift = _posShift;
					}
					return;								// ВЫХОД.
				}
				/***************************************** Случай, когда левая часть - не единственная. *****************************/
				dif = -dif;								// Теперь dif - сумма размеров средней и правой части.
				midSize = dif >> 3;						// Размер средней части в байтах.
				rightSize = dif - (midSize << 3);		// Размер правой части в битах.
				if (_cacheByte == -1) {
					_cacheByte = _body.readUnsignedByte ();
					_body.position--;
				}
				wmask = RIGHT_MASKS[leftSize];								// Считаем и записываем левую часть.
				_cacheByte = ((data >> dif) & wmask) | (_cacheByte & (~wmask));
				_body.writeByte (_cacheByte);								// Левая часть заполнила байт полностью (отката нет).
				_posBase++;													// Увеличиваем базовое положение на единицу.
				_posShift = 0;
				if (midSize) {												// ЕСЛИ средняя часть присутствует.
					m = midSize-1;											// Считаем и записываем среднюю часть.
					for (n = 0; n < midSize; n++ ) {
						_body.writeByte (data >> ((m << 3) + rightSize));	// Отката нет.
						m--;
					}
					_posBase += midSize;									// Увеличиваем базовое положение на длину средней части.
																			// _posShift = 0 - установлен сверху.
					_cacheByte = -1;										// Если правой части нет, кэш будет неопределен.
				}
				if (rightSize) {											// ЕСЛИ правая часть присутствует.
					esize = 8 - rightSize;									// Сколько имеющихся бит справа нужно сохранить.
					wmask = LEFT_MASKS[rightSize];
					emask = RIGHT_MASKS[esize];
					if (_posBase < _body.length) {							// Определяем кэш.
						_cacheByte = _body.readUnsignedByte ();
						_body.position--;
					}
					else {
						_cacheByte = 0;
					}
					_cacheByte = ((data << esize) & wmask) | (_cacheByte & emask);	// Если правая часть есть, кэш будет таким.
					_body.writeByte (_cacheByte);
					_body.position--;										// Правая часть не может занимать полный байт - откат.
					_posShift = rightSize;									// При нулевой правой части _posShift все равно 0.
				}
				if (_lenBase < _posBase || (_lenBase == _posBase && _lenShift < _posShift)) {	// Проверяем общую длину.
					_lenBase = _posBase;
					_lenShift = _posShift;
				}
			}
		}
		
		/// Записывает в битовый массив один бит, равный нулю, если data == false, и единице, если data == true.
		/**
		 * Записывает в текущий битовый массив один бит, равный нулю, если <code>data == false</code>, и единице, если <code>data == true</code>.
		 * @param	data Булево значение, которое необходимо записать.
		 */
		public function writeBoolean (data:Boolean):void {
			writeData (uint (data), 1);
		}
		
		/**
		 * Записывает в битовый массив 32-битное число с плавающей точкой, содержащееся в <code>data</code>. Точность представления при записи снижается с double precision до single precision.
		 * @param	data Объект <code>Number</code>, содержащий записываемое значение.
		 */
		public function writeFloat (data:Number):void {
			var tempArray:ByteArray = new ByteArray ();
			var tempUint:uint;
			tempArray.writeFloat (data);
			tempArray.position = 0;
			tempUint = tempArray.readUnsignedInt ();
			writeData (tempUint, 32);
		}
		
		/**
		 * Записывает в битовый массив 64-битное число с плавающей точкой, содержащееся в <code>data</code>. Точность представления при записи не снижается (я надеюсь :/ ).
		 * @param	data Объект <code>Number</code>, содержащий записываемое значение.
		 */
		public function writeDouble (data:Number):void {
			var tempArray:ByteArray = new ByteArray ();
			var tempUint:uint;
			tempArray.writeDouble (data);
			tempArray.position = 0;
			tempUint = tempArray.readUnsignedInt ();
			writeData (tempUint, 32);
			tempUint = tempArray.readUnsignedInt ();
			writeData (tempUint, 32);
		}
		
		/**
		 * Записывает в текущий битовый массив строку.
		 * @param	data Строка, которую необходимо записать.
		 * @param	lengthBits Количество бит, которое будет потрачено на указание длины строки (длина строки указывается в байтах).
		 */
		public function writeString (data:String, lengthBits:int = 16):void {
			if (!data) {
				writeData (0, lengthBits);					// Если передана пустая строка, просто записываем нулевую длину и выходим.
				return;
			}
			var tempArray:ByteArray = new ByteArray ();
			tempArray.writeUTFBytes (data);
			writeData (tempArray.length, lengthBits);		// Иначе записываем длину строки (в символах, то есть байтах).
			var bitArray:BitArray = construct (tempArray, tempArray.length, 0);
			writeBits (bitArray);							// Помещаем в текущий битовый массив саму строку.
		}
		
		/**
		 * Полностью записывает переданный битовый массив.
		 * @param	data Битовый массив, который необходимо записать в текущий массив.
		 */
		public function writeBits (data:BitArray):void {
			var prevPosBase:uint = data.posBase;			// Запоминаем текущее положение указателя переданного массива.
			var prevPosShift:int = data.posShift;
			data.setPosition (0, 0);						// Устанавливаем указатель переданного массива в самое начало.
			var remBase:uint = data.lenBase;
			var remShift:int = data.lenShift;
			var temp32:uint;
			while (remBase >= 4) {							// Если возможно, начинаем читать по четыре байта.
				temp32 = data.readData (32);
				writeData (temp32, 32);						// И записывать в текущий массив.
				remBase -= 4;
			}
			var remBits:int = (remBase << 3) + remShift;	// Когда в исходнике остается меньше 4 байт, считаем общее число оставшихся бит.
			if (remBits) {
				var temp:uint = data.readData (remBits);
				writeData (temp, remBits);
			}
			data.setPosition (prevPosBase, prevPosShift);	// Возвращаем указатель в исходное положение.
		}
		
		// =======================================================
		// ======================= READING =======================
		// =======================================================
		
		/**
		 * Читает из текущего битового массива значение размером <code>size</code> бит (но не более 32 бит) и возвращает это значение. При чтении позиция курсора смещается вправо, к концу битового массива.
		 * @param	size Количество бит, которое нужно прочитать - значение от 1 до 32.
		 * @return Объект <code>uint</code>, младшие (или все) биты которого содержат прочитанное значение.
		 */
		public function readData (size:int):uint {
			var n:int, m:int;
			var bitlen:int;
			var leftSize:int, midSize:int, rightSize:int;
			var emask:int;											// Existing data mask.
			var dif:int;
			var data:uint = 0;										// То, что будем возвращать.
			var dbase:int = _lenBase - _posBase;
			var dshift:int = _lenShift - _posShift;
			var str:String;
			if (dbase < 8) {										// Чтобы bitlen не переполнился.
				bitlen = (dbase << 3) + dshift;
				if (bitlen < size) {
					str = "Data format is too wide.";
					str += "\nSize transmitting: " + size;
					str += "\nPosition: [" + _posBase+", "+_posShift+"]; ";
					str += "Length: [" + _lenBase + ", " + _lenShift + "]";
					str += "\nDifference: [" + dbase + ", " + dshift + "]; ";
					str += "Bit length: " + bitlen + ".";
					throw new Error (str);
				}
			}
			if (_posShift == 0) {									// ЕСЛИ левая часть отсутствует.
				if (size == 8) {									// ЕСЛИ мы собираемся читать полный байт.
					data = _body.readUnsignedByte ();
					_posBase++;
					return data;
				}
				if (size < 8) {										// ЕСЛИ после прочтения останутся неиспользованные биты.
					data = (_body.readUnsignedByte () >>> (8 - size));
					_body.position--;
					_posShift = size;
					return data;
				}
				/********************* Случай, когда присутствует средняя и, возможно, правая часть *********************/
				midSize = size >>> 3;								// Размер центральной части.
				rightSize = size & 7;								// Размер правой части.
				if (midSize) {										// ЕСЛИ центральная часть присутствует.
					m = size - 8;
					for (n = 0; n < midSize; n++ ) {
						data = data | (_body.readUnsignedByte () << m);		// Читаем полный байт (отката нет).
						m -= 8;
					}
					_posBase += midSize;
				}
				if (rightSize) {									// ЕСЛИ правая часть присутствует.
					data = data | ((_body.readUnsignedByte () >> (8 - rightSize)) & RIGHT_MASKS[rightSize]);
					_body.position--;								// Читаем неполный байт.
					_posShift = rightSize;
				}
				return data;
			}
			leftSize = 8 - _posShift;								// Максимальный размер левой части.
			dif = leftSize - size;									// Количество бит справа, которые останутся нетронутыми.
			if (dif >= 0) {											// ЕСЛИ читаемое значение помещается в текущем байте.
				emask = RIGHT_MASKS[size];
				data = (_body.readUnsignedByte () >> dif) & emask;			// Значение выровнено по правому краю.
				if (dif) {											// ЕСЛИ в текущем байте еще остались непрочитанные биты.
					_body.position--;								// Откатываем указатель тела назад.
					_posShift += size;								// Сдвигаем курсор вправо.
				}
				else {												// ЕСЛИ все биты в текущем байте прочитаны.
					_posBase++;										// В следующий раз будем читать из нового байта.
					_posShift = 0;									// И с его левого края.
				}
				return data;
			}
			/**************** Случай, когда левая часть - не единственная ********************/
			dif = -dif;												// dif теперь равен сумме средней и правой частей.
			midSize = dif >> 3;										// Размер средней части в байтах.
			rightSize = dif - (midSize << 3);						// Размер правой части в битах.
			emask = RIGHT_MASKS[leftSize];
			data = (_body.readUnsignedByte () & emask) << dif;		// Величину сдвигаем влево, чтобы оставить место для ср. и пр. частей.
			_posBase++;
			_posShift = 0;
			if (midSize) {											// Если присутствует центральная часть.
				m = dif - 8;
				for (n = 0; n < midSize; n++ ) {
					data = data | (body.readUnsignedByte () << m);
					m -= 8;
				}
				_posBase += midSize;
			}
			if (rightSize) {										// ЕСЛИ правая часть присутствует.
				emask = RIGHT_MASKS[rightSize];
				data = data | ((_body.readUnsignedByte () >> (8 - rightSize)) & emask);
				_body.position--;									// Чтение правой части оставит непрочитанные биты в текущем байте.
				_posShift = rightSize;
			}
			return data;
		}
		
		/**
		 * Читает флаг (один бит шириной) и возвращает <code>true</code>, если он равен единице, и <code>false</code>, если нулю.
		 * @return Булево значение, соответствующее прочитанному биту.
		 */
		public function readBoolean ():Boolean {
			return Boolean (readData (1));
		}
		
		/**
		 * Читает и возвращает 32-разрядное число с плавающей точкой.
		 * @return Single-precision float.
		 */
		public function readFloat ():Number {
			var tempArray:ByteArray = new ByteArray ();
			var tempUint:uint = readData (32);
			tempArray.writeUnsignedInt (tempUint);
			tempArray.position = 0;
			return tempArray.readFloat ();
		}
		
		/**
		 * Читает и возвращает 64-разрядное число с плавающей точкой.
		 * @return Double-precision float.
		 */
		public function readDouble ():Number {
			var tempArray:ByteArray = new ByteArray ();
			var tempUint:uint = readData (32);
			tempArray.writeUnsignedInt (tempUint);
			tempUint = readData (32);
			tempArray.writeUnsignedInt (tempUint);
			tempArray.position = 0;
			return tempArray.readDouble ();
		}
		
		/**
		 * Читает строку из текущего битового массива.
		 * @param	lengthBits Количество бит, которое было потрачено на указание длины сохраняемой строки (длина указывается в байтах).
		 * @return Извлеченная строка либо null, если прочитанная строка является пустой.
		 */
		public function readString (lengthBits:int = 16):String {
			var len:int = readData (lengthBits);
			if (!len) { return null; }
			var bitArray:BitArray = readBits (len, 0);
			return bitArray._body.readUTFBytes (len);
		}
		
		/**
		 * Извлекает битовый массив указанной величины. Позиция курсора при чтении смещается вправо.
		 * @param	lengthBase База длины создаваемого битового массива.
		 * @param	lengthShift Смещение длины создаваемого битового массива.
		 * @return Новый битовый массив с точной длиной <code>lengthBase * 8 + lengthShift</code> бит.
		 */
		public function readBits (lengthBase:uint, lengthShift:int):BitArray {
			var ba:BitArray = new BitArray ();
			var remBase:uint = lengthBase;
			var remShift:int = lengthShift;
			var temp32:uint;
			while (remBase >= 4) {
				temp32 = readData (32);
				ba.writeData (temp32, 32);
				remBase -= 4;
			}
			var remBits:int = (remBase << 3) + remShift;
			if (remBits) {
				var temp:uint = readData (remBits);
				ba.writeData (temp, remBits);
			}
			ba.setPosition (0, 0);
			return ba;
		}
		
		// =======================================================
		// ======================== UTILS ========================
		// =======================================================
		
		/**
		 * Преобразует байтовый массив особого формата в битовый массив, содержащий абсолютно те же данные (в том числе и свою точную длину). Предполагается, что первые 35 бит байтового массива содержат длину битового массива, хранящегося в нём (32 бита на базу и 3 бита на смещение). Указатель возвращаемого битового массива устанавливается в позицию (4, 3).
		 * Для того, чтобы получить такой байтовый массив, создайте битовый массив и запишите в его начало 35 бит:
		 * <code>var bitArray:BitArray = new BitArray ();
		 * bitArray.writeData (35, 0);</code>
		 * После того, как вы сохраните в массив все данные, вернитесь в его начало и запишите его длину:
		 * <code>bitArray.setPosition (0, 0);
		 * bitArray.writeData (bitArray.lengthBase, 32);
		 * bitArray.writeData (bitArray.lengthShift, 3);
		 * var byteArray:ByteArray = bitArray.body;</code>
		 * Чтобы начать читать полезные данные (не включающие длину) из начала массива, указатель необходимо установить в (4, 3):
		 * <code>// reading data from some other place...
		 * bitArray.setPosition (4, 3);
		 * // reading data from beginning...</code>
		 * @param	byteArray Байтовый массив, первые 35 бит которого содержат данные о длине битового массива, хранящегося в нём.
		 * @return Новый битовый массив, который был записан в байтовом массиве.
		 */
		public static function bytesToBits (byteArray:ByteArray):BitArray {
			byteArray.position = 0;
			var baLenBase:uint = byteArray.readUnsignedInt ();
			var baLenShift:int = int ((byteArray.readUnsignedByte () >> 5) & 7);
			byteArray.position = 0;
			var bitArray:BitArray = BitArray.construct (byteArray, baLenBase, baLenShift);
			bitArray.setPosition (4, 3);
			return bitArray;
		}
		
		/**
		 * Преобразует целое число со знаком в целое без знака, смещая знаковый бит в соответствующую позицию.
		 * @param	value Целое число со знаком, которое необходимо преобразовать.
		 * @param	size Количество бит в возвращаемом значении, которые будут представлять величину, включая и знаковый бит.
		 * @return Запакованный <code>uint</code>, хранящий значащие биты из <code>value</code> (справа) и знаковый бит (слева от группы бит, представляющих значение).
		 */
		public static function intToUint (value:int, size:int):uint {
			if (size == 1) { throw new Error ("Wrong size of data (must be at least 2), "+size+" was transmitted."); }
			var neg:Boolean = Boolean (value & 2147483648);			// true, если число отрицательное.
			if (!neg) { return (value & RIGHT_MASKS[size]); }
			//if (!neg) { return value; }
			return (POW_OF_TWO[size-1] | (value & RIGHT_MASKS[size-1]));
		}
		
		/**
		 * Распаковывает беззнаковое значение, полученное с помощью intToUint(), перемещая знаковый бит в 31-ю позицию.
		 * @param	value Запакованный <code>uint</code>, хранящий в себе значение со знаковым битом, которое необходимо преобразовать в соответствующий <code>int</code>, пригодный для использования.
		 * @param	size Предполагаемый размер величины (включая знак), помещенной в <code>value</code>.
		 * @return Целое число со знаком - распакованное значение.
		 */
		public static function uintToInt (value:uint, size:int):int {
			if (size == 1) { throw new Error ("Wrong size of int (must be at least 2), "+size+" was transmitted."); }
			var neg:Boolean = Boolean (value & POW_OF_TWO[size-1]);
			if (!neg) { return (value & RIGHT_MASKS[size]); }
			return (4294967295 ^ RIGHT_MASKS[size]) | (value & RIGHT_MASKS[size]);
		}
		
		/**
		 * Определяет количество бит, которые потребуются для записи значения, лежащего в указанном диапазоне, и наличие знакового бита. Количество бит вычисляется с учетом знакового бита.
		 * @param	minValue Минимально допустимое значение.
		 * @param	maxValue Максимально допустимое значение.
		 * @return Пара целых значений со знаком - количество бит формата, включая знаковый бит, если он имеется (<code>first</code>), и наличие знаковой единицы (<code>second</code>).
		 */
		public static function defineFormat (minValue:int, maxValue:int):PairInt {
			var cont:Boolean;
			var pos:int;
			var size0:int = 0;					// При положительном minValue соответствующий размер будет равен нулю.
			var size1:int = 0;
			var size:int;
			var isSigned:Boolean = false;
			if (minValue < ( -1)) {				// ЕСЛИ значение меньше -1, ищем самый старший нуль.
				cont = true;
				pos = 31;
				while (cont) {
					pos--;						// Здесь получится 30 (31-я позиция для знакового бита).
					if (!Boolean (minValue & BitArray.POW_OF_TWO[pos])) {
						cont = false;
					}
				}
				size0 = pos + 2;				// Если старший нуль в позиции №2 (третий справа), то
												// необходимы 4 бита (3 для числа + 1 знака).
				isSigned = true;
			}
			if (minValue == -1) {
				size0 = 2;						// Пусть dec -1 записывается как binary 11
												// Тогда для записи числа [-2, 1] хватит двух бит.
				isSigned = true;
			}
			// Если макс. значение меньше нуля, то оно все равно не будет занимать больше места, чем мин. значение.
			if (maxValue > 0) {					// ЕСЛИ макс. значение больше нуля, ищем самую старшую единицу.
				cont = true;
				pos = 32;
				while (cont) {
					pos--;						// В первый раз получится 31 (самый старший бит).
					if (Boolean (maxValue & BitArray.POW_OF_TWO[pos])) {
						cont = false;
					}
				}
				size1 = pos + 1;
				if (isSigned) { size1++; }		// Если существует отрицательный диапазон, на знак тратится лишний бит.
			}
			size = (size0 > size1)? size0 : size1;
			if (!size) { size = 1; }
			return new PairInt (size, int (isSigned));
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/**
		 * Тело битового массива - байтовый массив.
		 */
		public function get body ():ByteArray { return _body; }
		
		/**
		 * База длины. Количество полностью заполненных байт (базовая длина массива). При длине массива в 19 бит она равна двум (16 бит = 2 байта, и 3 бита в остатке).
		 */
		public function get lenBase ():uint { return _lenBase; }
		
		/**
		 * Смещение длины. Размер последнего байта массива, в битах (0...7).
		 */
		public function get lenShift ():int { return _lenShift; }
		
		/**
		 * База курсора. Позиция текущего байта (того, в который/из которого происходит запись/чтение). Свойство аналогично ByteArray.position. Данный байт содержит текущий бит.
		 */
		public function get posBase ():uint { return _posBase; }
		
		/**
		 * Смещение курсора. Положение текущего бита внутри текущего байта (0...7).
		 */
		public function get posShift ():int { return _posShift; }
		
		/**
		 * Длина массива в битах.
		 */
		public function get bitLength ():Number { return Number (_lenBase) * 8 + Number (_lenShift); }
		
	}

}