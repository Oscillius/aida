package dump13.msl.types {
	import flash.utils.Dictionary;
	/**
	 * <p>Last time modified for: MSL v0.5.</p>
	 * <p>Множество, унаследованное от <code>Dictionary</code>. Все объекты множества хранятся в качестве ключей <code>Dictionary</code>, а в качестве значений используется <code>true</code>. Каждый проход (с помощью for..in) по одному и тому же неизменённому множеству будет давать разные результаты (объекты при обходах будут располагаться в разной последовательности). Каждый проход с помощью <code>for each..in</code>, очевидно, будет возвращать группу значений <code>true</code>.</p>
	 * @author dump13
	 */
	public class Set extends Dictionary {
		/**
		 * @private
		 */
		private var _length:uint = 0;
		
		/**
		 * Конструктор множества.
		 * @param	weakKeys Использовать ли слабые ссылки.
		 */
		public function Set (weakKeys:Boolean = false) {
			super (weakKeys);
		}
		
		/**
		 * Создаёт и возвращает копию текущего множества, состоящую из тех же объектов.
		 * @param	weakKeys Использовать ли в создаваемой копии слабые ссылки.
		 * @return Копия текущего множества.
		 */
		public function clone (weakKeys:Boolean = false):Set {
			var s:Set = new Set (weakKeys);
			for (var element:Object in this) {
				s[element] = true;
			}
			s._length = _length;
			return s;
		}
		
		/**
		 * Очищает текущее множество - удаляет из него все объекты.
		 */
		public function clear ():void {
			for (var element:Object in this) {
				delete this[element];
			}
			_length = 0;
		}
		
		/**
		 * Добавляет объект в множество. Если этот объект уже содержится в множестве, метод вернёт <code>false</code> (иначе - <code>true</code>).
		 * @param	element Объект, который необходимо добавить в текущее множество.
		 * @return Значение <code>true</code>, если объект ещё не содержался в множестве и был успешно добавлен, или <code>false</code>, если объект уже присутствовал.
		 */
		public function add (element:Object):Boolean {
			if (this[element] == undefined) {
				this[element] = true;
				_length++;
				return true;
			}
			return false;
		}
		
		/**
		 * Удаляет объект из множества. Если объект не содержится в данном множестве, метод вернёт <code>false</code> (иначе - <code>true</code>).
		 * @param	element Объект, который необходимо удалить из текущего множества.
		 * @return Значение <code>true</code>, если объект содержался в множестве и был успешно удалён, или <code>false</code>, если объект не был найден.
		 */
		public function remove (element:Object):Boolean {
			if (this[element] != undefined) {
				delete this[element];
				_length--;
				return true;
			}
			return false;
		}
		
		/**
		 * Удаляет из множества случайный объект и возвращает его. Если множество не содержит объектов, метод вернёт <code>false</code>.
		 * @return Удалённый объект или <code>false</code>, если множество было пустым.
		 */
		public function pop ():Object {
			if (_length == 0) { return false; }
			for (var element:Object in this) {
				_length--;
				delete this[element];
				return element;
			}
			return false;
		}
		
		/**
		 * Возвращает случайный объект (в отличие от метода <code>pop()</code>, не удаляя его из множества). Если множество не содержит объектов, метод вернёт значение <code>false</code>.
		 * @return Случайный объект множества или <code>false</code>, если множество являлось пустым.
		 */
		public function peek ():Object {
			for (var element:Object in this) {
				return element;
			}
			return false;
		}
		
		/**
		 * Проверяет, содержится ли объект <code>element</code> в множестве.
		 * @param	element Элемент, наличие которого необходимо проверить.
		 * @return Содержится ли элемент <code>element</code> в текущем множестве.
		 */
		public function has (element:Object):Boolean {
			return (this[element] != undefined);
		}
		
		/**
		 * Возвращает true, если текущее множество состоит из тех же элементов, что и множество <code>s</code>.
		 * @param	s Множество, которое необходимо сравнить с текущим.
		 * @return true, если текущее множество состоит из тех же элементов, что и множество <code>s</code>.
		 */
		public function equals (s:Set):Boolean {
			if (_length != s._length) { return false; }
			for (var i:* in this) {
				//if (!s.has (i)) { return false; }
				if (s[i] == undefined) { return false; }
			}
			return true;
		}
		
		/**
		 * Объединяет текущее множество с множеством <code>withSet</code>. Последнее остается без изменений.
		 * @param	withSet Множество, с которым необходимо объединить текущее множество.
		 */
		public function unite (withSet:Set):void {
			for (var element:Object in withSet) {
				add (element);
			}
		}
		
		/**
		 * Вычитает множество <code>aSet</code> из текущего множества.
		 * @param	aSet Вычитаемое множество.
		 */
		public function subtract (aSet:Set):void {
			for (var element:Object in aSet) {
				remove (element);
			}
		}
		
		/**
		 * Статичный метод, создающий новое множество и помещающий в него все объекты из множеств <code>a</code> и <code>b</code>. Исходные множества остаются без изменений.
		 * @param	a Первое множество-слагаемое.
		 * @param	b Второе множество-слагаемое.
		 * @param	weakKeys Использовать ли слабые ссылки в создаваемой копии.
		 * @return Новое множество, содержащее в себе все объекты из множеств <code>a</code> и <code>b</code>.
		 */
		public static function union (a:Set, b:Set, weakKeys:Boolean = false):Set {
			var c:Set = a.clone (weakKeys);
			for (var element:Object in b) {
				c.add (element);
			}
			return c;
		}
		
		/**
		 * Статичный метод, создающий новое множество, являющееся разностью множеств <code>a</code> и <code>b</code>. Оно содержит все элементы, имеющиеся в множестве <code>a</code>, но не содержащиеся в множестве <code>b</code>.
		 * @param	a Уменьшаемое множество (остаётся без изменений).
		 * @param	b Вычитаемое множество (остаётся без изменений).
		 * @param	weakKeys Использовать ли слабые ссылки в создаваемой копии.
		 * @return Новое множество, являющееся разностью множеств <code>a</code> и <code>b</code>.
		 */
		public static function difference (a:Set, b:Set, weakKeys:Boolean = false):Set {
			var c:Set = a.clone (weakKeys);
			for (var element:Object in b) {
				c.remove (element);
			}
			return c;
		}
		
		/**
		 * Статичный метод, создающий новое множество, являющееся пересечением множеств <code>a</code> и <code>b</code>. Оно содержит элементы, содержащиеся и в множестве <code>a</code>, и в множестве <code>b</code>.
		 * @param	a Первое множество.
		 * @param	b Второе множество.
		 * @param	weakKeys Использовать ли слабые ссылки в создаваемой копии.
		 * @return Новое множество, являющееся пересечением множеств <code>a</code> и <code>b</code>.
		 */
		public static function intersection (a:Set, b:Set, weakKeys:Boolean = false):Set {
			var c:Set=new Set (weakKeys);
			var element:Object;
			for (element in a) {
				if (b.has (element)) {
					c.add (element);
				}
			}
			for (element in b) {
				if (a.has (element)) {
					c.add (element);
				}
			}
			return c;
		}
		
		/**
		 * Создаёт и возвращает строковое представление множества, перечисляющее все его элементы в виде "Set (element0, element1..., elementN)".
		 * @return Строковое представление множества.
		 */
		public function toString ():String {
			var s:String = "Set (";
			for (var i:* in this) {
				s += Object (i).toString ();
				s += ", ";
			}
			if (length) { s = s.substr (0, s.length - 2); }
			s += ")";
			return s;
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/**
		 * Количество элементов, содержащихся в множестве.
		 */
		public function get length ():uint {
			return _length;
		}
		
	}

}