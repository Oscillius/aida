package dump13.msl.types {
	/**
	 * <p>Last time modified for: MSL v0.5.</p>
	 * <p>Пара int-значений.</p>
	 * @author dump13
	 */
	public class PairInt {
		/**
		 * Первое int-значение пары.
		 */
		public var first:int;
		/**
		 * Второе int-значение пары.
		 */
		public var second:int;
		
		/**
		 * Конструктор.
		 * @param	first Первое int-значение пары.
		 * @param	second Второе int-значение пары.
		 */
		public function PairInt (first:int, second:int) {
			this.first = first;
			this.second = second;
		}
		
		/**
		 * Создаёт и возвращает копию текущей int-пары с теми же значениями.
		 * @return Копия текущей int-пары.
		 */
		public function clone ():PairInt {
			return new PairInt (first, second);
		}
		
		/**
		 * Копирует значения <code>first</code> и <code>second</code> из пары <code>pair</code> в текущую пару.
		 * @param	pair int-пара, из которой нужно скопировать значения.
		 */
		public function copy (pair:PairInt):void {
			first = pair.first;
			second = pair.second;
		}
		
		/**
		 * Сравнивает значения <code>first</code> и <code>second</code> текущей пары с соответствующими значениями переданной пары <code>pair</code>.
		 * @param	pair Пара, с которой необходимо произвести сравнение.
		 * @return Значение <code>true</code>, если пары идентичны; иначе - <code>false</code>.
		 */
		public function equals (pair:PairInt):Boolean {
			return first == pair.first && second == pair.second;
		}
		
		/**
		 * Создаёт и возвращает строковое представление текущей int-пары.
		 * @return Строковое представление.
		 */
		public function toString ():String {
			return "PairInt (" + first + ", " + second + ")";
		}
		
	}

}