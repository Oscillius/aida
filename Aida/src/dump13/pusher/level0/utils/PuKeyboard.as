package dump13.pusher.level0.utils {
	import flash.display.Stage;
	import flash.events.KeyboardEvent;
	/**
	 * Простой менеджер клавиатуры, способный назначать задания по отслеживанию состояний для определённых клавиш, а также подписывать методы-слушатели на нажатие или отпускание заданной клавиши.
	 * @author dump13
	 */
	public class PuKeyboard {
		private var _stage:Stage;
		private var _tasks:Array/*.<PuKeyTask>*/ = [];
		
		public static const PRESS:int = 0;
		public static const RELEASE:int = 1;
		
		public function PuKeyboard (stage:Stage) {
			_stage = stage;
			_stage.addEventListener (KeyboardEvent.KEY_DOWN, onKeyDown);
			_stage.addEventListener (KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		public function destroy ():void {
			clearTasks ();
			
			_stage.removeEventListener (KeyboardEvent.KEY_DOWN, onKeyDown);
			_stage.removeEventListener (KeyboardEvent.KEY_UP, onKeyUp);
			
			_stage = null;
		}
		
		/**
		 * Создаёт задание на отслеживание состояния клавиши с кодом <code>keyCode</code>. Если для данной клавиши уже назначено задание, будет создано исключение.
		 * @param	keyCode Код клавиши, за которой необходимо следить (константа класса <code>Keyboard</code>).
		 */
		public function createTask (keyCode:uint):void {
			var curTask:PuKeyTask = _tasks[keyCode] as PuKeyTask;
			if (curTask) {
				//curTask.destroy ();
				throw new Error ("Task already exists.");
			}
			_tasks[keyCode] = new PuKeyTask (keyCode);
		}
		
		/**
		 * Создаёт задания на отслеживание состояния одной или нескольких клавиш с кодами <code>keyCodes</code>.
		 * @param	...keyCodes Коды клавиш, состояния которых необходимо отслеживать. Константа класса <code>Keyboard</code>.
		 */
		public function createTasks (...keyCodes):void {
			var n:int = keyCodes.length;
			while (n) {
				createTask (uint (keyCodes[--n]));
			}
		}
		
		/**
		 * Удаляет задание по отслеживанию состояния клавиши с кодом <code>keyCode</code>. Все слушатели событий для этой клавиши будут удалены. Если задание не найдено, создаётся исключение.
		 * @param	keyCode Код клавиши, состояние которой больше не нужно отслеживать. Константа класса <code>Keyboard</code>.
		 */
		public function deleteTask (keyCode:uint):void {
			var task:PuKeyTask = _tasks[keyCode] as PuKeyTask;
			if (!task) {
				throw new Error ("Task does not exist.");
			}
			task.destroy ();
			delete _tasks[keyCode];
		}
		
		/**
		 * Удаляет задания для всех клавиш. Все слушатели событий будут удалены.
		 */
		public function clearTasks ():void {
			for each (var task:PuKeyTask in _tasks) {
				deleteTask (task.keyCode);
			}
		}
		
		/**
		 * Возвращает задание для клавиши <code>keyCode</code>. Если такого задания не создавалось, метод вернёт <code>null</code>. Поиск не оптимизирован (он ведётся среди всех созданных заданий до первого совпадения).
		 * @param	keyCode Код клавиши, по которой ведётся поиск задания. Константа класса <code>Keyboard</code>.
		 * @return Экземпляр найденного задания для указанной клавиши или <code>null</code>, если задания не существует.
		 */
		public function getTaskByKeyCode (keyCode:uint):PuKeyTask {
			for each (var task:PuKeyTask in _tasks) {
				if (task.keyCode == keyCode) {
					return task;
				}
			}
			return null;
		}
		
		/**
		 * Проверяет, нажата ли клавиша с кодом <code>keyCode</code>. Если задания для этой клавиши не создавалось, метод кинет исключение.
		 * @param	keyCode Код клавиши, состояние которой необходимо проверить. Константа класса <code>Keyboard</code>.
		 * @return
		 */
		public function keyIsDown (keyCode:uint):Boolean {
			var task:PuKeyTask = _tasks[keyCode] as PuKeyTask;
			if (!task) {
				throw new Error ("Monitoring task doesn't exist.");
			}
			return task.keyIsDown;
		}
		
		/**
		 * Добавляет слушатель события (нажатия или отпускания) одной клавиши. Если для этой клавиши задания ещё не существует, оно будет создано.
		 * @param	keyCode Код клавиши, для которой необходимо зарегистрировать слушатель. Константа класса <code>Keyboard</code>.
		 * @param	eventType Тип события, для которого надо зарегистрировать слушатель. Константа класса <code>PuKeyboard</code>.
		 * @param	listener Слушатель - метод, который будет вызван при возникновении события.
		 */
		public function addListener (keyCode:uint, eventType:int, listener:Function):void {
			var task:PuKeyTask = _tasks[keyCode] as PuKeyTask;
			// Если задания не существует, создаём его.
			if (!task) {
				createTask (keyCode);
				task = _tasks[keyCode] as PuKeyTask;
			}
			// Добавляем слушатель к таску.
			task.addListener (eventType, listener);
		}
		
		/**
		 * Удаляет слушатель события, добавленный с помощью <code>addListener()</code>. Если для указанной клавиши и типа события слушатель зарегистрирован не был, создаётся исключение. Этот метод не удаляет само задание на отслеживание нажатий клавиши с кодом <code>keyCode</code>, которое могло быть создано методом <code>addListener()</code>.
		 * @param	keyCode Код клавиши, на которую был зарегистрирован удаляемый слушатель. Константа класса <code>Keyboard</code>.
		 * @param	eventType Тип события, на которое был зарегистрирован удаляемый слушатель. Константа класса <code>PuKeyboard</code>.
		 * @param	listener Слушатель, который необходимо удалить.
		 */
		public function removeListener (keyCode:uint, eventType:int, listener:Function):void {
			var task:PuKeyTask = _tasks[keyCode] as PuKeyTask;
			task.removeListener (eventType, listener);
		}
		
		// =======================================================
		// ====================== LISTENERS ======================
		// =======================================================
		
		private function onKeyDown (e:KeyboardEvent):void {
			var task:PuKeyTask = _tasks[e.keyCode] as PuKeyTask;
			if (task) {
				if (!task.keyIsDown) {
					task.keyIsDown = true;
					task.executeListeners (PuKeyboard.PRESS);
				}
			}
		}
		
		private function onKeyUp (e:KeyboardEvent):void {
			var task:PuKeyTask = _tasks[e.keyCode] as PuKeyTask;
			if (task) {
				if (task.keyIsDown) {
					task.keyIsDown = false;
					task.executeListeners (PuKeyboard.RELEASE);
				}
			}
		}
		
		// =======================================================
		// ====================== ACCESSORS ======================
		// =======================================================
		
		/**
		 * Копия массива с добавленными заданиями.
		 */
		public function get tasks ():Array { return _tasks.slice (); }
		
	}

}