package dump13.pusher.level0.utils {
	import dump13.msl.types.Set;
	/**
	 * Задание по отслеживанию состояния клавиши. Используется классом <code>PuKeyboard</code>, нет необходимости использовать этот класс напрямую.
	 * @author dump13
	 */
	public class PuKeyTask {
		public var keyCode:uint;
		public var keyIsDown:Boolean = false;
		private var _onPressListeners:Vector.<Function> = new Vector.<Function> ();
		private var _onReleaseListeners:Vector.<Function> = new Vector.<Function> ();
		
		
		/**
		 * Конструктор задания по отслеживанию состояния клавиши.
		 * @param	keyCode Код клавиши - константа класса <code>Keyboard</code>.
		 */
		public function PuKeyTask (keyCode:uint) {
			this.keyCode = keyCode;
		}
		
		/**
		 * Уничтожает задание по отслеживанию состояния клавиши. Все слушатели для каждого типа событий удаляются.
		 */
		public function destroy ():void {
			clearAllListeners ();
		}
		
		/**
		 * Добавляет слушатель на событие типа <code>eventType</code> - нажатие или отпускание клавиши. Нажатая и удерживаемая клавиша будет вызывать слушатель только один раз - в момент нажатия (при удержании клавиши события генерироваться уже не будут). Если данная пара "событие - слушатель" уже зарегистрирована, создаётся исключение.
		 * @param	eventType Тип события, на который необходимо подписаться (нажатие или отпускание клавиши). Константа класса <code>PuKeyboard</code>.
		 * @param	listener Слушатель - метод, который необходимо вызвать при возникновении события.
		 */
		public function addListener (eventType:int, listener:Function):void {
			var collection:Vector.<Function> = defineListenersCollForEventType (eventType);
			
			var index:int = collection.indexOf (listener);
			if (index != -1) {
				throw new Error ("Listener for this eventType and key already exists.");
			}
			collection.push (listener);
		}
		
		/**
		 * Удаляет слушатель, зарегистрированный с помощью метода <code>addListener()</code>. Если слушатель не зарегистрирован, создаётся исключение.
		 * @param	eventType Тип события, на который необходимо подписаться (нажатие или отпускание клавиши). Константа класса <code>PuKeyboard</code>.
		 * @param	listener Слушатель (обработчик).
		 */
		public function removeListener (eventType:int, listener:Function):void {
			var collection:Vector.<Function> = defineListenersCollForEventType (eventType);
			
			var index:int = collection.indexOf (listener);
			if (index == -1) {
				throw new Error ("Listener for this eventType is not registered.");
			}
			collection.splice (index, 1);
		}
		
		/**
		 * Удаляет все слушатели для события типа <code>eventType</code>.
		 * @param	eventType Тип события, для которого необходимо отписать всех слушателей.
		 */
		public function clearListeners (eventType:int):void {
			defineListenersCollForEventType (eventType).length = 0;
		}
		
		/**
		 * Удаляет абсолютно всех слушателей для этой кнопки.
		 */
		public function clearAllListeners ():void {
			_onPressListeners.length = 0;
			_onReleaseListeners.length = 0;
		}
		
		/**
		 * Проверяет, зарегистрирован ли слушатель <code>listener</code> для события типа <code>eventType</code>.
		 * @param	eventType Тип события - константа класса <code>PuKeyboard</code>.
		 * @param	listener Слушатель, наличие которого необходимо проверить.
		 * @return Значение <code>true</code>, если слушатель <code>listener</code> зарегистрирован для события типа <code>eventType</code>; иначе - <code>false</code>.
		 */
		public function hasListener (eventType:int, listener:Function):Boolean {
			var collection:Vector.<Function> = defineListenersCollForEventType (eventType);
			return collection.indexOf (listener) != -1;
		}
		
		/**
		 * Вызывает все слушатели, зарегистрированные на событие типа <code>eventType</code>.
		 * @param	eventType Тип события, слушателей которого необходимо выполнить.
		 */
		public function executeListeners (eventType:int):void {
			var collection:Vector.<Function> = defineListenersCollForEventType (eventType);
			var na:int = collection.length;
			for (var n:int = 0; n < na; n++) {
				collection[n] ();
			}
		}
		
		private function defineListenersCollForEventType (eventType:int):Vector.<Function> {
			switch (eventType) {
				case PuKeyboard.PRESS:
					return _onPressListeners;
				case PuKeyboard.RELEASE:
					return _onReleaseListeners;
				default:
					throw new Error ("Unknown eventType.");
					break;
			}
			return null;
		}
		
	}

}