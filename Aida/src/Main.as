package {
	import dump13.aida.tests.test000.AiTest000List;
	import dump13.aida.tests.test001.AiTest001ListNodePool;
	import dump13.aida.tests.test002.AiTest002Visuals;
	import dump13.aida.tests.test003.AiTest003StructPerf;
	import dump13.aida.tests.test004.AiTest004AidaVisuals;
	import dump13.aida.tests.test005.AiTest005Inlining;
	import dump13.aida.tests.test006.AiTest006SetPixel;
	import dump13.aida.tests.test007.AiTest007VectorAndSetLookup;
	import dump13.aida.tests.test008.AiTest008LocalVsGlobal;
	import dump13.aida.tests.test009.AiTest009QuadTree;
	import dump13.aida.tests.test010_rect.AiTest010RectIntersection;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author dump13
	 */
	public class Main extends Sprite {
		
		public function Main():void {
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void {
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			//new AiTest000List (this);
			//new AiTest001ListNodePool (this);
			//new AiTest002Visuals (this);
			
			//new AiTest003StructPerf (this);
			
			new AiTest004AidaVisuals (this);
			
			//new AiTest005Inlining (this);
			//new AiTest006SetPixel (this);
			//new AiTest007VectorAndSetLookup (this);
			//new AiTest008LocalVsGlobal (this);
			//new AiTest009QuadTree (this);
			//new AiTest010RectIntersection (this);
		}
		
	}
	
}